package com.vividing.pdf;

import java.util.List;

import org.apache.pdfbox.text.TextPosition;

import com.vividing.pdf.svg.Box;

/**
 * wrapper of TextPosition that adds flags to track status as linestart and paragraph start positions.
 * <p>
 * This is implemented as a wrapper since the TextPosition class doesn't provide complete access to its state fields
 * to subclasses. Also, conceptually TextPosition is immutable while these flags need to be set post-creation so it
 * makes sense to put these flags in this separate class.
 * </p>
 * 
 * @author m.martinez@ll.mit.edu
 */
public  class PositionWrapper extends PostionInfo {
    private boolean isParagraphStart = false;
    private boolean isPageBreak = false;
    private boolean isHangingIndent = false;
    private TextStyle textStyle = null;
    
    /**
	 * @return the textStyle
	 */
	public TextStyle getTextStyle() {
		return textStyle;
	}

	/**
	 * @param textStyle the textStyle to set
	 */
	public void setTextStyle(TextStyle textStyle) {
		this.textStyle = textStyle;
	}

	public PositionWrapper(VividingStripper stripper, TextPosition position, PostionInfo lastMyPostion, boolean sort) {
		super(stripper, position, lastMyPostion, sort);
	}

    public boolean isParagraphStart()
    {
        return isParagraphStart;
    }

    /**
     * sets the isParagraphStart() flag to true.
     */
    public void setParagraphStart()
    {
        this.isParagraphStart = true;
        reset();
    }
    

    public boolean isPageBreak()
    {
        return isPageBreak;
    }

    /**
     * Sets the isPageBreak() flag to true.
     */
    public void setPageBreak()
    {
        this.isPageBreak = true;
    }

    public boolean isHangingIndent()
    {
        return isHangingIndent;
    }

    /**
     * Sets the isHangingIndent() flag to true.
     */
    public void setHangingIndent()
    {
        this.isHangingIndent = true;
    }
    
 
//    public boolean isPSeparation(List<TextPosition> textList, int index){
//    		return (this.isPSeparation(textList, index, null, false) >= 0);
//    }
//    public int isPSeparation(List<TextPosition> textList, int index, Box box, boolean inBox){
//    		
//	    	if (lastMyPostion == null) {
//	    		return -1;
//	    	}
//	    	
//	    	float maxHeightForLine = getMaxHeightForLine();
//	    	//float yGap = Math.abs(getTextPosition().getYDirAdj() - lastMyPostion.getTextPosition().getYDirAdj());
//	    	float yGap = getTextPosition().getYDirAdj() - lastMyPostion.getTextPosition().getYDirAdj();
//        float newYVal = Util.multiplyFloat(2f, maxHeightForLine);
//        //float newYVal2 = Util.multiplyFloat(2.1f, height);
//        float GAP = inBox ? VividingStripperBase.boxLINEGAP :VividingStripperBase.LINEGAP;
//        float PARAGRAPHGAP = inBox ? VividingStripperBase.boxPARAGRAPHGAP :VividingStripperBase.PARAGRAPHGAP;
//        Character c = lastMyPostion.getTextPosition().getUnicode().charAt(0);
//        float x = getTextPosition().getX();
//        float x0 = lastMyPostion.getTextPosition().getX();
//        if (yGap == 0) {
//        		return -1;
//        } else if (index > 0 && yGap > 0) { //Index == 0, we are in the different page ....
//	        if (yGap <= newYVal) {
//	    			return -1;
//	        }
//	        if (yGap < GAP) {
//	    			return -1;
//	        }
//	        
//	        if (yGap >= PARAGRAPHGAP) {
//	        		return 1;
//	        }
//	        
//	        if (yGap >= GAP) {   ///LINE BR
//	        		
//	        		if (box != null) {
//	        			if ((x <= VividingStripperBase.boxTextX2+1)
//	        				&& (x0 > x && x0 < box.right - 20 * lastMyPostion.getTextPosition().getWidth())) {
//	        				return 0;
//	        			}
//	        		} else if (x0 <= 500) {
//	        			
//	        			if (x >= 133.25 +1) {
//	        				return 0;
//	        			} else {
//	        				return 1;
//	        			}
//	        		} else
//	        		{
//	        			return -1;
//	        		}
//	        }
//        } else {
////	        	if (!isDifferentPage()) {
////	    			System.err.println("Any thing wrong????");
////	    		}
//        		
//        		//Page change ????
//			if (Util.isLetterUpperOfDigit((VividingStripper)stripper, index, textList)){
//				TextStyle lastStyle =  TextStyle.getStyle(stripper, this.lastMyPostion.getTextPosition());
//		    		TextStyle style =  TextStyle.getStyle(stripper, this.position);
//		    	
//		    		if (style != lastStyle) {
//		    			return 1;
//		    		}				
//		    		
//				if (c == '.') {
//					return 1;
//				}
//				
////				if (x0 <= 575) {
////        			return 1;
////        		}
//			}
//        }
////		if (Util.isBold(getTextPosition())) {
////			return 1;
////		}
//
//		return -1;
//        
//    }
}
