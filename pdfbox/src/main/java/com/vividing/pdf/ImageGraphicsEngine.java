//package com.vividing.pdf;
//
//import java.awt.geom.Point2D;
//import java.awt.image.BufferedImage;
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.Arrays;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//
//import javax.imageio.ImageIO;
//
//import org.apache.pdfbox.contentstream.PDFGraphicsStreamEngine;
//import org.apache.pdfbox.cos.COSName;
//import org.apache.pdfbox.cos.COSStream;
//import org.apache.pdfbox.io.IOUtils;
//import org.apache.pdfbox.pdmodel.PDPage;
//import org.apache.pdfbox.pdmodel.PDResources;
//import org.apache.pdfbox.pdmodel.common.PDStream;
//import org.apache.pdfbox.pdmodel.font.PDCIDFont;
//import org.apache.pdfbox.pdmodel.font.PDCIDFontType2;
//import org.apache.pdfbox.pdmodel.font.PDFont;
//import org.apache.pdfbox.pdmodel.font.PDFontDescriptor;
//import org.apache.pdfbox.pdmodel.font.PDTrueTypeFont;
//import org.apache.pdfbox.pdmodel.font.PDType0Font;
//import org.apache.pdfbox.pdmodel.graphics.PDXObject;
//import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceGray;
//import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceRGB;
//import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
//import org.apache.pdfbox.pdmodel.graphics.form.PDTransparencyGroup;
//import org.apache.pdfbox.pdmodel.graphics.image.PDImage;
//import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
//import org.apache.pdfbox.pdmodel.graphics.state.PDSoftMask;
//import org.apache.pdfbox.tools.imageio.ImageIOUtil;
//
//import com.vividing.pdf.part.Chapter;
//import com.vividing.pdf.part.PartInfo;
//
//public class ImageGraphicsEngine extends PDFGraphicsStreamEngine
//{
//	static private final Set<COSStream> seen = new HashSet<COSStream>();
//	
//    private boolean directJPEG;
//    private String figure_prefix;
//    private String figure_smal_prefix;
//    private String font_prefix;
//    PartInfo chapter = null;
//    VividingStripper stripper = null;
//    boolean addKey = false;
//    private static final List<String> JPEG = Arrays.asList(
//            COSName.DCT_DECODE.getName(),
//            COSName.DCT_DECODE_ABBREVIATION.getName());
//    
//    public ImageGraphicsEngine(PDPage page, PartInfo chapter, VividingStripper stripper) throws IOException
//    {
//        super(page);
//        this.chapter = chapter;
//        this.stripper = stripper;
//        
//        if (chapter != null) {
//	        if (VividingStripper.isOneChapterOnly()) {
//	        		this.figure_prefix = "image-";
//	        		this.figure_smal_prefix = "image-sm-";
//		        this.font_prefix = "font-";
//	        }
//	        else {
//		        	int c = 0;
//		        	if (chapter instanceof Chapter) {
//		        		c = chapter.chapter;
//		        	}
//	        		this.figure_prefix = "Figure-" + c + "-";
//	        		this.figure_smal_prefix = "Figure-" + c + "-sm-";
//		        this.font_prefix = "Font-" + c + "-";
//	        }
//        } else {
//        	this.figure_prefix = "Figure-UNKNOWN-";
//        	this.figure_smal_prefix = "Figure-UNKNOWN-sm-";
//	        this.font_prefix = "Font-UNKNOWN-";
//        }
//    }
//
//    public void run() throws IOException
//    {
//        PDPage page = getPage();
//        processPage(page);
//        PDResources resources = page.getResources();
//        for (COSName name : resources.getExtGStateNames())
//        {
//        		//System.out.println("Resource name = " + name.getName() + ", ==>>>" + name);
//            PDSoftMask softMask = resources.getExtGState(name).getSoftMask();
//            if (softMask != null)
//            {
//                PDTransparencyGroup group = softMask.getGroup();
//                if (group != null)
//                {
//                    processSoftMask(group);
//                }
//            }
//        }
//        if (stripper.isIncludeFont()) //No font for now 
//        {
//        		this.processResources(resources, addKey);
//        }
//    }
//
//    private void processResources(PDResources resources, boolean addKey) throws IOException
//    {
//        if (resources == null)
//        {
//            return;
//        }
//
//        for (COSName key : resources.getFontNames())
//        {
//            PDFont font = resources.getFont(key);
//            // write the font
//            if (font instanceof PDTrueTypeFont)
//            {
//                String name = null;
//                if (addKey)
//                {
//                    name = font_prefix + key + "-" + this.chapter.nextFontId(); 
//                    		//getUniqueFileName(prefix + "_" + key, "ttf");
//                }
//                else
//                {
//                    //name = getUniqueFileName(prefix, "ttf");
//                	name = font_prefix + this.chapter.nextFontId();
//                }
//                writeFont(font.getFontDescriptor(), name);
//            }
//            else if (font instanceof PDType0Font)
//            {
//                PDCIDFont descendantFont = ((PDType0Font) font).getDescendantFont();
//                if (descendantFont instanceof PDCIDFontType2)
//                {
//                    String name = null;
//                    if (addKey)
//                    {
//                        //name = getUniqueFileName(prefix + "_" + key, "ttf");
//                    	name = font_prefix + key + "-" + this.chapter.nextFontId();
//                    }
//                    else
//                    {
//                        //name = getUniqueFileName(prefix, "ttf");
//                    	name = font_prefix + this.chapter.nextFontId();
//                    }
//                    writeFont(descendantFont.getFontDescriptor(), name);
//                }
//            }
//        }
//
//        for (COSName name : resources.getXObjectNames())
//        {
//            try {
//				PDXObject xobject = resources.getXObject(name);
//				if (xobject instanceof PDFormXObject)
//				{
//				    PDFormXObject xObjectForm = (PDFormXObject) xobject;
//				    PDResources formResources = xObjectForm.getResources();
//				    processResources(formResources, addKey);
//				}
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//        }
//
//    }
//    private void writeFont(PDFontDescriptor fd, String name) throws IOException
//    {
//        if (fd != null)
//        {
//            PDStream ff2Stream = fd.getFontFile2();
//            if (ff2Stream != null)
//            {
//                FileOutputStream fos = null;
//                try
//                {
//                		name = stripper.getOutputDir() + "fonts/"+ name;
//                    System.out.println("Writing font:" + name);
//                    
//                    fos = new FileOutputStream(new File(name + ".ttf"));
//                    IOUtils.copy(ff2Stream.createInputStream(), fos);
//                }
//                finally
//                {
//                    if (fos != null)
//                    {
//                        fos.close();
//                    }
//                }
//            }
//        }
//    }
//
//    
//    @Override
//    public void drawImage(PDImage pdImage) throws IOException
//    {
//        if (pdImage instanceof PDImageXObject)
//        {
//            PDImageXObject xobject = (PDImageXObject)pdImage;
//            
//            if (seen.contains(xobject.getCOSObject()))
//            {
//                // skip duplicate image
//                return;
//            }
//            seen.add(xobject.getCOSObject());
//        }
//        String fileSize = String.format("W=%s, H=%s",pdImage.getWidth(), pdImage.getHeight() );
//        		// save image
//        	String name = null;
//        	if (pdImage.getWidth() > stripper.getStripeImageWidth() && pdImage.getHeight() > stripper.getStripeImageHeight()) {
//        		name = figure_prefix + this.chapter.nextImageId();
//        	} else {
//        		name = figure_smal_prefix + this.chapter.nextSmallImageId();
//        	}
//        	
//        System.out.println("Writing image: " + name + ", " + fileSize);
//        write2file(pdImage, name, directJPEG);
//    }
//
//    @Override
//    public void appendRectangle(Point2D p0, Point2D p1, Point2D p2, Point2D p3)
//            throws IOException
//    {
//
//    }
//
//    @Override
//    public void clip(int windingRule) throws IOException
//    {
//
//    }
//
//    @Override
//    public void moveTo(float x, float y) throws IOException
//    {
//
//    }
//
//    @Override
//    public void lineTo(float x, float y) throws IOException
//    {
//
//    }
//
//    @Override
//    public void curveTo(float x1, float y1, float x2, float y2, float x3, float y3)
//            throws IOException
//    {
//
//    }
//
//    @Override
//    public Point2D getCurrentPoint() throws IOException
//    {
//        return new Point2D.Float(0, 0);
//    }
//
//    @Override
//    public void closePath() throws IOException
//    {
//
//    }
//
//    @Override
//    public void endPath() throws IOException
//    {
//
//    }
//
//    @Override
//    public void strokePath() throws IOException
//    {
//
//    }
//
//    @Override
//    public void fillPath(int windingRule) throws IOException
//    {
//
//    }
//
//    @Override
//    public void fillAndStrokePath(int windingRule) throws IOException
//    {
//
//    }
//
//    @Override
//    public void shadingFill(COSName shadingName) throws IOException
//    {
//
//    }
//    
//    /**
//     * Writes the image to a file with the filename prefix + an appropriate suffix, like "Image.jpg".
//     * The suffix is automatically set depending on the image compression in the PDF.
//     * @param pdImage the image.
//     * @param figure_prefix the filename prefix.
//     * @param directJPEG if true, force saving JPEG streams as they are in the PDF file. 
//     * @throws IOException When something is wrong with the corresponding file.
//     */
//    private void write2file(PDImage pdImage, String filename, boolean directJPEG) throws IOException
//    {
//        String suffix = pdImage.getSuffix();
//        if (suffix == null)
//        {
//            suffix = "png";
//        }
//
//        FileOutputStream out = null;
//        try
//        {
//            out = new FileOutputStream(stripper.getOutputDir() + "images/"+ filename + "." + suffix);
//            BufferedImage image = pdImage.getImage();
//            if (image != null)
//            {
//                if ("jpg".equals(suffix))
//                {
//                    String colorSpaceName = pdImage.getColorSpace().getName();
//                    if (directJPEG || 
//                            !hasMasks(pdImage) && 
//                                     (PDDeviceGray.INSTANCE.getName().equals(colorSpaceName) ||
//                                      PDDeviceRGB.INSTANCE.getName().equals(colorSpaceName)))
//                    {
//                        // RGB or Gray colorspace: get and write the unmodified JPEG stream
//                        InputStream data = pdImage.createInputStream(JPEG);
//                        IOUtils.copy(data, out);
//                        IOUtils.closeQuietly(data);
//                    }
//                    else
//                    {
//                        // for CMYK and other "unusual" colorspaces, the JPEG will be converted
//                        ImageIOUtil.writeImage(image, suffix, out);
//                    }
//                }
//                else 
//                {
//                    ImageIOUtil.writeImage(image, suffix, out);
//                }
//            } else {
//            		ImageIO.write(image, suffix, out);
//            }
//            
//            out.flush();
//        }
//        finally
//        {
//            if (out != null)
//            {
//                out.close();
//            }
//        }
//    }
//    
//    private boolean hasMasks(PDImage pdImage) throws IOException
//    {
//        if (pdImage instanceof PDImageXObject)
//        {
//            PDImageXObject ximg = (PDImageXObject) pdImage;
//            return ximg.getMask() != null || ximg.getSoftMask() != null;
//        }
//        return false;
//    }
//
//}
//
//
