package com.vividing.pdf.svg.element;

import org.apache.pdfbox.text.TextPosition;

import com.vividing.pdf.Line;
import com.vividing.pdf.TextStyle;
import com.vividing.pdf.VividingStripper;

public class SVGTextElement extends SVGElement {
	
	private Line line = new Line();
	
	public SVGTextElement(int pageNum) {
		super("text", pageNum);		
	}
	
	@Override
	public void write(VividingStripper stripper, StringBuilder out, StringBuilder def, boolean embededImg) {
		super.write(stripper, out, def, embededImg);	
		if (getLine() != null && getLine().text.length() > 0) {
			out.append(getStartTagsPart2()).append(getLine().text).append(String.format("%s\n", getEndTags()));
		} else {
			out.append(" />\n");
		}
	}
	
	public void toHtml(StringBuilder out) {
		//super.write(stripper, out, def, embededImg);	
		if (getLine() != null && getLine().text.length() > 0) {
			out.append("<span>").append(getLine().text).append("</span>\n");
		} else {
			out.append("<span></span>\n");
		}
	}
	
	public boolean find(String str) {
		if(str == null) {
			return false;
		}
		if (getLine() == null || getLine().text == null) {
			return false;
		}
		//return getLine().text.indexOf(str) != -1;
		return getLine().text.toString().toLowerCase().indexOf(str.toLowerCase()) != -1;
	}

//	public boolean remove(int at) {
//		return line.getLine().remove(at);
//	}
	
	public void removeAt(int at) {
		line.getLine().remove(at);
	}
		
	@Override
	public String toString() {
		return String.format("<text x=%s,y=%s,right=%s,bottom=%s>=%s" , x, y, right, bottom, getLine().text);
	}
	
	//First style
	//first position
	TextStyle textStyle = null;
	TextStyle lastTextStyle = null;
	TextPosition textPosition = null;
	TextPosition lastTextPosition = null;
	public TextPosition getTextPosition() {
		return textPosition;
	}

	public TextStyle getTextStyle() {
		return textStyle;
	}
	
	

	public TextStyle getLastTextStyle() {
		return lastTextStyle;
	}
	
	public TextPosition getLastTextPosition() {
		return lastTextPosition;
	}
	
	public void add(TextPosition textPosition, TextStyle textStyle) {
		if (this.textPosition == null) {
			this.textPosition = textPosition;
		}
		this.lastTextPosition = textPosition;
		if (this.textStyle == null) {
			this.textStyle = textStyle;
		}
		this.lastTextStyle = textStyle;
		
		getLine().add(textPosition, textStyle);
	}
	
	public void add(SVGTextElement e) {
		this.getLine().addAll(e.getLine().getLine());
		x = Math.min(x,  e.x);
		y = Math.min(y,  e.y);
		right = Math.max(right, e.right);
		bottom = Math.max(bottom, e.bottom);
		this.lastTextPosition = e.getLastTextPosition();
		this.lastTextStyle = e.getLastTextStyle();
		//body.append(e.body);
	}
	
	public void addBR() {
		if (getLine() != null) {
			getLine().addBR();
		}
	}
	
	public void addSpace() {
		addSpace(false);
	}
	
	public void addSpace(boolean gridy) {
		if (getLine() != null) {
			if (gridy) {
				getLine().addSpace();
				return;
			}
		
			if (getLine().text.length()>0) {
				if (getLine().text.charAt(getLine().text.length()-1) != ' ') {
					getLine().addSpace();
				}
			}
		}
	}
	public StringBuilder getText() {
		return getLine().text;
	}

	public Line getLine() {
		return line;
	}
	
	public int size() {
		return line.getLine().size();
	}

//	public void setLine(Line line) {
//		this.line = line;
//	}
	
};