package com.vividing.pdf.svg;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.graphics.image.PDImage;
import org.apache.pdfbox.pdmodel.graphics.state.PDGraphicsState;
import org.apache.pdfbox.rendering.PageDrawerParameters;
import org.apache.pdfbox.text.TextPosition;
import org.apache.pdfbox.util.Matrix;
import org.apache.pdfbox.util.Vector;

import com.vividing.pdf.Line;
import com.vividing.pdf.PositionWrapper;
import com.vividing.pdf.TextStyle;
import com.vividing.pdf.Util;
import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.part.PartInfo;
import com.vividing.pdf.svg.element.SVG;
import com.vividing.pdf.svg.element.SVGElement;
import com.vividing.pdf.svg.element.SVGTextElement;

public class SVGPageDrawer extends SVGPageDrawerBase {
	public SVG svg = null;
	
	PartInfo partInfo =null;
	//PDRectangle mediaBox = null;
	public SVGPageDrawer(PageDrawerParameters parameters, VividingStripper stripper, int pdfPageIndex, PartInfo partInfo) throws IOException {
		super(parameters, stripper, pdfPageIndex);
		//this.mediaBox = page.getMediaBox();
		this.svg = new SVG(pdfPageIndex, stripper); //
		this.partInfo = partInfo;
		//System.out.println("Page ... " + pageIndex);
		lastSVGElement = null;
	}
	
	public int getPageNum() {
		return svg.getPageNum();
	}

	@Override
	public void drawPage(Graphics g, PDRectangle pageSize) throws IOException {		
		this.pageSize = pageSize;
		
		if (VividingStripper.pageSize == null) {
			VividingStripper.pageSize = pageSize;
		}
		this.svg.setPageSize(this.pageSize);
		super.drawPage(g, pageSize);
		//System.out.println("drawPage ... " + getPageNum());
	}
	public void close() {
		System.out.println("Done... " + getPageNum());
		
		
		svg.grouping();
		//svg.checkTable();
		//svg.sorting();
		stripper.boxes.addAll(svg.getBoxes());
		
		svg.writeFile(this.stripper, false);
		svg.writeFile(this.stripper, true);
	}
	
	@Override
	public void strokePath() throws IOException {
		if (createPath()) { 
			this.svg.svgElement = null;
			this.svg.safeSVGPath(getGraphicsState());
			this.svg.strokeOrFill(getGraphics(), getGraphicsState(), null, true, false);		
			setPath();
		}
	}
	
	@Override
	public void fillPath(int windingRule) throws IOException {
		if (createPath()) { 
			this.svg.svgElement = null;
		this.svg.safeSVGPath(getGraphicsState());
		this.svg.strokeOrFill(getGraphics(), getGraphicsState(), windingRule, false, true);
		setPath();
		}
	}
			   
	@Override
	public void fillAndStrokePath(int windingRule) throws IOException {
		if (createPath()) {
			this.svg.svgElement = null;
		
			this.svg.safeSVGPath(getGraphicsState());
			this.svg.strokeOrFill(getGraphics(), getGraphicsState(), windingRule, true, true);
			setPath();
		}
	}

	private boolean createPath() {
		//if (true) 
		{
			return true;
		}
	}
	
	private void setPath() {
		AffineTransform tm = getGraphics().getTransform();
		GeneralPath generalPath = getLinePath();
		
		this.svg.toPath(tm, generalPath);
		generalPath.reset();	
	}
	
	//public static CustomImageWriter customimagewriter = new CustomImageWriter();
	
	static Map<String, List<Integer>> keys = new HashMap<String, List<Integer>>(); 
	private static String getNewId(String key) {
		List<Integer> list = keys.get(key);
		
		if (list == null) {
			list = new ArrayList<Integer>();
			list.add(new Integer(1));
			keys.put(key, list);
			return key + "-1";
		} else {
			int i = list.get(list.size() - 1);
			list.add(new Integer(i+1));
			return key + "-" + (i+1);
		}
		
	}
	
	@Override
	public void drawImage(PDImage pdImage) throws IOException {
		
		if (!VividingStripper.isTextOnly()) {
			//This is raw size. PDF will be (0, 0) at the bottom-left , while computer are different. SO, we need convert
			float width = pdImage.getWidth();
			float height = pdImage.getHeight();

			super.drawImage(pdImage);
			Graphics2D g = this.getGraphics();
		
			if (g instanceof SVGGraphics2D) {
				SVGGraphics2D svgGraphic = (SVGGraphics2D) this.getGraphics();
				
				Image image = svgGraphic.getImg();
				AffineTransform imageTransform = svgGraphic.getXform();
				
				PDGraphicsState state = getGraphicsState();
				Matrix m = state.getCurrentTransformationMatrix();
				
				if (image instanceof BufferedImage) {
					String name = null;
					int chapter = partInfo.getChapter();
					String figureName = null;
					String id = null;
					if (width < stripper.getStripeImageWidth() || height < stripper.getStripeImageHeight()) {
						//name = "sm-" + name;
						id = getNewId("sm-"+String.valueOf(chapter));
					} else {
						id = getNewId(String.valueOf(chapter));
					}
					
					if (chapter > -1) {
						figureName = String.format("figure-%s", id);
						
						name = figureName + "." + VividingStripper.IMAGEFORMAT;
					} else {	
						figureName = String.format("file-%s-%s", getPageNum(), ++imgID);
						name = figureName + "." + VividingStripper.IMAGEFORMAT;
					}
					
					
					Util.p("Image = " + name);
					
					VividingStripper.imageMap.put(figureName, Util.ID());
					float H = this.pageSize.getHeight();
					
					float x = (float)m.getTranslateX();
					float bottom = H - (float)m.getTranslateY(); 
					
					width *= imageTransform.getScaleX();
					height *= Math.abs(imageTransform.getScaleY());
					float y = bottom - height; 
					float right = x + width;
					
					BufferedImage bImage = (BufferedImage) image;
					
					

					String fileName = stripper.getImageDir() + name;
					
					try {
						//Util.p(fileName);
						ImageIO.write(bImage, VividingStripper.IMAGEFORMAT, new File(fileName));
						//customimagewriter.writeImage(bImage, new File(fileName));
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					
					this.svg.svgElement = null;
					SVGElement e = this.svg.safeSVGImg(this.getGraphicsState());
					
					e.x = x;
					e.y = y;
					e.right = right;
					e.bottom = bottom;
					
					e.putAttr("x", String.valueOf(x));
					e.putAttr("y", String.valueOf(y));
					e.putAttr("width", String.valueOf(width));
					e.putAttr("height", String.valueOf(height));
					
					
					String imgUrl = "../images/"+name;
					//Util.p("img url = " + imgUrl);
					e.putAttr("xlink:href", imgUrl);
					
					this.svg.svgElement = null;
					e = this.svg.safeSVGImg(this.getGraphicsState());
					e.x = x;
					e.y = y;
					e.right = right;
					e.bottom = bottom;
					
					e.putAttr("x", String.valueOf(x));
					e.putAttr("y", String.valueOf(y));
					e.putAttr("width", String.valueOf(width));
					e.putAttr("height", String.valueOf(height));
					ByteArrayOutputStream b = new ByteArrayOutputStream();
					ImageIO.write(bImage, VividingStripper.IMAGEFORMAT, b); 
					e.setEmbeded(true);
					e.putAttr("xlink:href", "data:image/" + 
					(VividingStripper.IMAGEFORMAT.equals("jpg") ? "jpeg" : "png") 
							+ ";base64," + Util.getHex(b.toByteArray()));
					this.svg.svgElement = null;
				} else {
					System.err.println("No supported");
				}
			} else if (g instanceof sun.java2d.SunGraphics2D) {
				sun.java2d.SunGraphics2D d = (sun.java2d.SunGraphics2D) g;
				System.err.println("SunGraphics2D");
			} else
				System.err.println(g.getClass().getName());
			}
		}
	
	
	
	PositionWrapper current = null;
	PositionWrapper lastPosition = null;
    boolean debug = false;
	SVGTextElement lastSVGElement = null;
	
	protected void showGlyph(Matrix textRenderingMatrix, PDFont font, int code, String unicode,
            Vector displacement) throws IOException {
		
		super.showGlyph(textRenderingMatrix, font, code, unicode, displacement);	
		//sb.append(unicode);
		TextPosition position = this.showGlyph1(textRenderingMatrix, font, code, unicode, displacement);
		
		if (position.getFontSizeInPt() == 208f) {
			Util.p(textRenderingMatrix.getScalingFactorX() + " === " + textRenderingMatrix);
		}
		position.setBookPageNum(pdfPageIndex+1-this.stripper.getBookPageStart());
		position.setPdfPageNum(pdfPageIndex+1);
		//stripper.processTextPosition(position);
		PDGraphicsState state = getGraphicsState();
		//String strokingColor = Util.getCSSColor(state.getStrokingColor());
		String nonStrokingColor = Util.getCSSColor(state.getNonStrokingColor());
		position.setRbg_color(nonStrokingColor);
		
		TextStyle textStyle = TextStyle.getStyle(stripper, position);
		
		current = new PositionWrapper(this.stripper, position, lastPosition, false);
		current.setTextStyle(textStyle);
//		AffineTransform tm = getGraphics().getTransform();
//		Point2D ptSrc = new Point2D.Float(textRenderingMatrix.getTranslateX(), textRenderingMatrix.getTranslateY());
//		Point2D.Float ptDst = new Point2D.Float();
//		tm.transform(ptSrc, ptDst);
//		
		Matrix textLineMatrix = getTextLineMatrix();
		
		if (this.svg.svgElement == null || !(this.svg.svgElement instanceof SVGTextElement) || 
				(lastPosition != null && TextStyle.getStyle(stripper, lastPosition.getTextPosition()) != textStyle) || 
				textLineMatrix != textLineMatrixOld ||
				!Util.sameElement(textRenderingMatrix, textMatrixOld) ||
				!current.isSameElement()
				) { 
//		if (this.svg.svgElement == null || 
//				//!Util.newTextElement(current, lastPosition)
//				!Util.sameLine(textRenderingMatrix, textMatrixOld)
//				//|| this.svg.m_svgElement.body.length() == 0) 
//				){
			if (this.svg.svgElement != null && this.svg.svgElement instanceof SVGTextElement) {
				//setupTextElement((SVGTextElement) this.svg.svgElement);
				//lastSVGElement = (SVGTextElement)this.svg.svgElement;
			}
	        	this.svg.svgElement = null;
	        	this.svg.safeSVGText(getGraphicsState());
	        	
	        
			this.svg.svgElement.putAttr("fill", nonStrokingColor);
			
			/*this.svg.svgElement.x = (float) ptDst.getX();
			this.svg.svgElement.y = (float) ptDst.getY();
			this.svg.svgElement.putAttr("x", ptDst.getX());
			this.svg.svgElement.putAttr("y", ptDst.getY());*/
			/*We don't know where isthe top most Y, just  a guess, left most is OK */
			
			this.svg.svgElement.x = position.getX();
			this.svg.svgElement.y = position.getY();//-position.getHeight() -3f;
			this.svg.svgElement.putAttr("x", position.getX());
			this.svg.svgElement.putAttr("y", position.getY());
			this.svg.svgElement.putAttr("class", textStyle.getId());		
		}
		
		SVGTextElement e = (SVGTextElement) this.svg.svgElement;
		
//		if (e.find("Conflict Theory")) {
//			Util.p(e);
//		}
//		
		
//		if (getPageNum() == 705 && e.find("Chapter")) {
//			debug = true;
//		}
//		
//		if (debug) {
//			Util.p(unicode+"\n"+e);
//		}
		if (e != null && current.isNewWord2(line)) {
			e.addSpace();
		}
		
		try {
			this.svg.svgElement.right = position.getX()+position.getWidth();
			this.svg.svgElement.bottom = position.getY()+position.getHeight();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//+position.getHeight(); Should not add height, need linespace???
		//this.svg.m_svgElement.body.append(Util.escape(unicode));
//      this.svg.m_svgElement.putAttr("transform", Util.toSVGTransform(textRenderingMatrix));
//		Matrix m = font.getFontMatrix();
//		this.svg.m_svgElement.putAttr("transform", Util.toSVGTransform(m));
		this.textLineMatrixOld = textLineMatrix;
		this.textMatrixOld = textRenderingMatrix;
		e.add(position, textStyle);
		lastPosition = current;
	}
	
	
    Line line = null;
    //StringBuilder sb = new StringBuilder();
    @Override
	public void beginText() throws IOException {
    		this.svg.svgElement = null;
    		//sb = new StringBuilder();	
    		line = new Line();
		super.beginText();
	}
    
	@Override
	public void endText() throws IOException {
		//Util.p(sb);
		//setupTextElement((SVGTextElement) this.svg.svgElement);
		//Util.p(e);
		this.svg.svgElement = null;
		this.textLineMatrixOld = null;
		super.endText();
	}
	boolean dispay = false;
	//public static Map<TextStyle, Float> LINEGAPS = new LinkedHashMap<TextStyle, Float>();
}
