package com.vividing.pdf.svg;

import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.RenderingHints.Key;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.RenderableImage;
import java.text.AttributedCharacterIterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.util.Matrix;

import com.vividing.pdf.Util;

public class SVGGraphics2D extends Graphics2D {
	protected static final Log logger = LogFactory.getLog(SVGGraphics2D.class);
	//SVGElement path = new SVGElement("path");
	Matrix matrix = new Matrix();
	
	@Override
	public void draw(Shape s) {

		//logger.info("draw");
	}

	Image img; 
	public Image getImg() {
		return img;
	}


	AffineTransform xform = null;
	
	public AffineTransform getXform() {
		return xform;
	}

	@Override
	public boolean drawImage(Image img, AffineTransform xform, ImageObserver obs) {

		this.img = img;
		this.xform = xform;
		return false;
	}

	@Override
	public void drawImage(BufferedImage img, BufferedImageOp op, int x, int y) {
		this.img = img;
System.err.println("drawImage");
	}

	@Override
	public void drawRenderedImage(RenderedImage img, AffineTransform xform) {

		System.err.println("drawRenderedImage");
	}

	@Override
	public void drawRenderableImage(RenderableImage img, AffineTransform xform) {


	}

	@Override
	public void drawString(String str, int x, int y) {

		logger.info("str = " + str);
	}

	@Override
	public void drawString(String str, float x, float y) {

		logger.info("str = " + str);
	}

	@Override
	public void drawString(AttributedCharacterIterator iterator, int x, int y) {


	}

	@Override
	public void drawString(AttributedCharacterIterator iterator, float x, float y) {


	}

	@Override
	public void drawGlyphVector(GlyphVector g, float x, float y) {

		System.out.println("drawGlyphVector");
		//path.toPath(g.getOutline());
	}

	@Override
	public void fill(Shape s) {

		//System.out.println("Fill Shape???" + s.getBounds());
	}

	@Override
	public boolean hit(Rectangle rect, Shape s, boolean onStroke) {

		return false;
	}

	@Override
	public GraphicsConfiguration getDeviceConfiguration() {

		//return GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		//return  super.getDeviceConfiguration();
		//return null;
		Rectangle virtualBounds = new Rectangle();
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
	      GraphicsDevice[] gs =
	              ge.getScreenDevices();
	      for (int j = 0; j < gs.length; j++) {
	          GraphicsDevice gd = gs[j];
	          GraphicsConfiguration[] gc =
	              gd.getConfigurations();
	          if (gc.length == 1) {
	        	  return gc[0];
	          }
	          for (int i=0; i < gc.length; i++) {
	              virtualBounds =
	                  virtualBounds.union(gc[i].getBounds());
	          }
	      }
	      //Util.p(virtualBounds);
	      return null; 
	}

	@Override
	public void setComposite(Composite comp) {

//What show we do????
	}

	@Override
	public void setPaint(Paint paint) {


	}

	Stroke currentStrake = null;
	@Override
	public void setStroke(Stroke s) {

		currentStrake = s;
	}

	@Override
	public void setRenderingHint(Key hintKey, Object hintValue) {


	}

	@Override
	public Object getRenderingHint(Key hintKey) {

		return null;
	}

	@Override
	public void setRenderingHints(Map<?, ?> hints) {


	}

	@Override
	public void addRenderingHints(Map<?, ?> hints) {


	}

	@Override
	public RenderingHints getRenderingHints() {

		return null;
	}

	@Override
	public void translate(int x, int y) {

//System.out.println(x + "translate == " + y);
matrix.translate(x, y);
	}

	@Override
	public void translate(double tx, double ty) {

		//System.out.println(tx + "translate == " + ty);
		matrix.translate((float)tx, (float)ty);
	}

	@Override
	public void rotate(double theta) {

		System.out.println("rotate == " + theta);
		matrix.rotate(theta);
	}

	@Override
	public void rotate(double theta, double x, double y) {

		matrix.translate((float)x, (float)y);
		matrix.rotate(theta);
		System.out.println("rotate == " + theta + " " +x + ", " + y);
	}

	@Override
	public void scale(double sx, double sy) {

		matrix.scale((float)sx, (float)sy);
		//System.out.println("scale == " + matrix.toString());
	}

	@Override
	public void shear(double shx, double shy) {


	}

	@Override
	public void transform(AffineTransform Tx) {
		System.out.println("transform == " + Tx);
		//matrix.transform(tx);

	}

	@Override
	public void setTransform(AffineTransform Tx) {


	}

	@Override
	public AffineTransform getTransform() {
		return matrix.createAffineTransform();
	}

	@Override
	public Paint getPaint() {

		return null;
	}

	@Override
	public Composite getComposite() {

		return null;
	}

	@Override
	public void setBackground(Color color) {


	}

	@Override
	public Color getBackground() {

		return null;
	}

	@Override
	public Stroke getStroke() {

		return currentStrake;
	}

	@Override
	public void clip(Shape s) {


	}

	@Override
	public FontRenderContext getFontRenderContext() {

		return null;
	}

	@Override
	public Graphics create() {

		return null;
	}

	@Override
	public Color getColor() {

		return c;
	}

	Color c = null;
	@Override
	public void setColor(Color c) {

		this.c = c;
	}

	@Override
	public void setPaintMode() {


	}

	@Override
	public void setXORMode(Color c1) {


	}

	@Override
	public Font getFont() {

		return null;
	}

	@Override
	public void setFont(Font font) {


	}

	@Override
	public FontMetrics getFontMetrics(Font f) {

		return null;
	}

	@Override
	public Rectangle getClipBounds() {

		return null;
	}

	@Override
	public void clipRect(int x, int y, int width, int height) {


	}

	@Override
	public void setClip(int x, int y, int width, int height) {


	}

	@Override
	public Shape getClip() {

		return clip;
	}

	Shape clip = null;
	@Override
	public void setClip(Shape clip) {
		this.clip = clip;

//		SVGElement path1 = new SVGElement("path");
//		path1.toPath(clip);
		//System.out.println(path1.toString());
		//LOG.trace("Clip: "+path.getDString());
	}

	@Override
	public void copyArea(int x, int y, int width, int height, int dx, int dy) {


	}

	@Override
	public void drawLine(int x1, int y1, int x2, int y2) {


	}

	@Override
	public void fillRect(int x, int y, int width, int height) {


	}

	@Override
	public void clearRect(int x, int y, int width, int height) {


	}

	@Override
	public void drawRoundRect(int x, int y, int width, int height, int arcWidth, int arcHeight) {


	}

	@Override
	public void fillRoundRect(int x, int y, int width, int height, int arcWidth, int arcHeight) {


	}

	@Override
	public void drawOval(int x, int y, int width, int height) {


	}

	@Override
	public void fillOval(int x, int y, int width, int height) {


	}

	@Override
	public void drawArc(int x, int y, int width, int height, int startAngle, int arcAngle) {


	}

	@Override
	public void fillArc(int x, int y, int width, int height, int startAngle, int arcAngle) {


	}

	@Override
	public void drawPolyline(int[] xPoints, int[] yPoints, int nPoints) {


	}

	@Override
	public void drawPolygon(int[] xPoints, int[] yPoints, int nPoints) {


	}

	@Override
	public void fillPolygon(int[] xPoints, int[] yPoints, int nPoints) {


	}

	@Override
	public boolean drawImage(Image img, int x, int y, ImageObserver observer) {

		return false;
	}

	@Override
	public boolean drawImage(Image img, int x, int y, int width, int height, ImageObserver observer) {

		return false;
	}

	@Override
	public boolean drawImage(Image img, int x, int y, Color bgcolor, ImageObserver observer) {

		return false;
	}

	@Override
	public boolean drawImage(Image img, int x, int y, int width, int height, Color bgcolor, ImageObserver observer) {

		return false;
	}

	@Override
	public boolean drawImage(Image img, int dx1, int dy1, int dx2, int dy2, int sx1, int sy1, int sx2, int sy2,
			ImageObserver observer) {

		return false;
	}

	@Override
	public boolean drawImage(Image img, int dx1, int dy1, int dx2, int dy2, int sx1, int sy1, int sx2, int sy2,
			Color bgcolor, ImageObserver observer) {

		return false;
	}

	@Override
	public void dispose() {


	}

}
