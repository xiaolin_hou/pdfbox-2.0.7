package com.vividing.pdf.svg;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.rendering.PageDrawer;
import org.apache.pdfbox.rendering.PageDrawerParameters;

import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.part.PartInfo;

public class SVGPDFRenderer extends PDFRenderer {
	VividingStripper stripper= null;
	PartInfo partInfo = null;
	int pdfPageNum = -1;
	
	SVGPageDrawer svgEngine = null;
	public SVGPDFRenderer(VividingStripper stripper, PDDocument document) {
		super(document);
		this.stripper = stripper;
	}

	@Override
	public BufferedImage renderImage(int pageIndex) throws IOException {
		return super.renderImage(pageIndex);
	}

	@Override
	public BufferedImage renderImage(int pageIndex, float scale) throws IOException {
		return super.renderImage(pageIndex, scale);
	}

	@Override
	public BufferedImage renderImageWithDPI(int pageIndex, float dpi) throws IOException {
		// TODO Auto-generated method stub
		return super.renderImageWithDPI(pageIndex, dpi);
	}

	@Override
	public BufferedImage renderImageWithDPI(int pageIndex, float dpi, ImageType imageType) throws IOException {
		// TODO Auto-generated method stub
		return super.renderImageWithDPI(pageIndex, dpi, imageType);
	}

	@Override
	public BufferedImage renderImage(int pageIndex, float scale, ImageType imageType) throws IOException {
		this.pdfPageNum = pageIndex;
		return super.renderImage(pageIndex, scale, imageType);
	}

	//@Override
	public void renderPageToGraphics(PartInfo partInfo, int pdfPageNum, Graphics2D graphics) throws IOException {
		this.partInfo = partInfo;
		this.pdfPageNum = pdfPageNum;
		
		super.renderPageToGraphics(pdfPageNum-1, graphics);
		
		this.svgEngine.close();
		partInfo.svgs.put(String.valueOf(pdfPageNum), this.svgEngine.svg);
	}

	@Override
	protected PageDrawer createPageDrawer(PageDrawerParameters parameters) throws IOException {
		this.svgEngine = new SVGPageDrawer(parameters, stripper, this.pdfPageNum, partInfo);
		return this.svgEngine;
	}

}
