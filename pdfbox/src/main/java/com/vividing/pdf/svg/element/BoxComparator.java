package com.vividing.pdf.svg.element;

import java.util.Comparator;

import com.vividing.pdf.svg.Box;

public class BoxComparator implements Comparator<Box> {
	@Override
	public int compare(Box b1, Box b2) {
	     if (b1.y < b2.y) {
	    	 	return -1;
	     }
	     if (b1.y > b2.y) {
	    	 	return 1;
	     }
	     if ((b1.bottom-b2.bottom) != 0) {
		     if ((b1.bottom-b2.bottom) > 0) {
		    	 	return -1;
		     }
		     return 1;
	     }
	     if (b1.x < b2.x) {
	    	 	return -1;
	     }
	     if (b1.x > b1.x) {
	    	 	return 1;
	     }
	     
	     if (b1.right < b2.right) {
	    	 	return -1;
	     }
	     
	     if (b1.right > b2.right) {
	    	 	return 1;
	     }
	    
	     return 0;
	 }
}
