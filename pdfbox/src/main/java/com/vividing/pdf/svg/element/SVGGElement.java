package com.vividing.pdf.svg.element;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import com.vividing.pdf.Line;
import com.vividing.pdf.LineItem;
import com.vividing.pdf.Util;
import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.svg.Box;
import com.vividing.pdf.svg.Table;
import com.vividing.pdf.tag.Article;
import com.vividing.pdf.tag.Paragraph;
import com.vividing.pdf.tag.Tag;

public class SVGGElement extends SVGElement{
	boolean topMost = false;
	
	protected List<SVGElement> elements = new ArrayList<SVGElement>();
	protected List<SVGElement> elements2 = new ArrayList<SVGElement>();	
	public Box box = null;
	public SVGGElement(int pageNum) {
		super("g", pageNum);
	}
	
	@Override
	public String toString() {
		return String.format("<g x=%s, y=%s, right=%s, bottom=%s>", x, y, right, bottom);
	}
	
	public static Map<String, Float> LINEGAPS = new LinkedHashMap<String, Float>();
	public static Map<String, Integer> WARNING_COUNT = new LinkedHashMap<>();
	
	@Override
	public void write(VividingStripper stripper, StringBuilder out, StringBuilder def, boolean embededImg) {
		super.write(stripper, out, def, embededImg);
		out.append(">\n");
		
		SVGTextElement current = null;
		//int lastIndex = Integer.MIN_VALUE;
		Map<SVGTextElement, SVGTextElement> line = new HashMap<SVGTextElement, SVGTextElement>();
		for (int i=0; i<elements.size(); i++) {
			SVGElement e = elements.get(i);
			
			e.write(stripper, out, def, embededImg);
			
			if (e instanceof SVGTextElement) {
				current = (SVGTextElement) e;
			} else {
				current = null;
				continue;
			}
			if (Util.getBox(e) != null ) {
				line.clear();
				continue;
			}
			if (line.size() == 0 && current != null) {
				line.put(current, current);
				continue;
			} 

			boolean gotIt = false;
			for (SVGTextElement last : line.values()) {
				
				float yGap = current.y-last.y;
				float xGap = current.x-last.x;
				
				
				String key = last.getTextStyle()+"|"+current.getTextStyle();
				
//				int idx = key.indexOf("font-family:WYLDPM+LiberationSans;font-size:9.0pt;color:#000000");
//				
//				if (idx != -1) {
//					report = true;
//				}
////				
//				if (report) {
//					Util.p(key);
//					Util.p(pageNum);
//					Util.p(last);
//					Util.p(current);
//					Util.p(xGap + ", " + yGap);
//				}
				//if (yGap > 4f && yGap < last.getTextPosition().getHeight()) {
				if (yGap > VividingStripper.minLINEGAP && yGap <= VividingStripper.maxLINEGAP) {
				
					
					if (!Util.equal(yGap, 0, 1f)) {
						Float gap = LINEGAPS.get(key);
						if (gap == null) {
							LINEGAPS.put(key, Math.abs(yGap));	
						} else if (!Util.equal(yGap, gap)) {
							if (yGap > gap && yGap < last.getTextPosition().getHeight() && yGap < current.getTextPosition().getHeight() / 2) {
								LINEGAPS.put(key, Math.abs(yGap));
							} else {
								
								if (Math.abs(yGap) < gap) {
									LINEGAPS.put(key, Math.abs(yGap));
								}
							}
						}
					}
					gotIt = true;
					
				}
				
			} //For
			
			if (gotIt) {
				line.clear();
			} else {
				line.put(current, current);
			}
			
		}
		out.append(getEndTags());
	}

	public List<SVGElement> getElements() {
		return elements;
	}
	
	public void sort() {
		//if (pageNum != 1312 && pageNum != 551 && pageNum != 698) {
		if (!VividingStripper.NOGROUPSORTING){
			if (elements.size() > 0) {
				try {
					elements.sort(new SVGElementComparator());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	public List<SVGElement> getElements2() {
		return elements2;
	}
	
	
	public void checkTable() {
//		if (pageNum==47) {
//			debug = true;
//		}
		
		
		Table  tableBox = VividingStripper.getTable(this);
		
		//SVGGElement gTable = null;
//		if (tableBox != null) {
//			debug = true;
//		}
//		
//		debug = tableBox != null;
		
		if (tableBox != null && VividingStripper.TABLE_NO_LEFT_RIGHT_BORDER) {
			SVGPathElement path = new SVGPathElement(this.pageNum);
			path.x = tableBox.x;
			path.y = tableBox.y;
			path.right = tableBox.x + 1;
			path.bottom = tableBox.bottom;
			
			path.path = Util.toPath(path);
			
//			if (path.isRectangle() && path.isVeticalLine()) {
//				Util.p("X");
//			}
			
			elements.add(0, path);
			
			path = new SVGPathElement(this.pageNum);
			path.x = tableBox.right;
			path.y = tableBox.y;
			path.right = tableBox.right + 1;
			path.bottom = tableBox.bottom;
			path.path = Util.toPath(path);
//			if (path.isRectangle() && path.isVeticalLine()) {
//				Util.p("X");
//			}
			elements.add(path);
			this.sort();
		}
		if (isTable()) {
		//if (tableBox != null || isTable()) {
//			SVGGElement gTable = 
			
			scanTableInfo();
			columnsMap.clear();
			rowsMap.clear();
		}
	}
	
	public void paragraphing() {
		
		checkTable();
		elements2.clear();
		SVGTextElement paragraph = null;
		SVGTextElement lastLine = null;
		for(SVGElement e : elements) {

			 if (Util.getBox(e.x, e.y, e.right-e.x, e.bottom-e.y) != null) {
				 continue;
			 }

//			 if (debug) {
//				 Util.p(e);
//			 }
			 if (paragraph == null) {
				 elements2.add(e);
				 if (e instanceof SVGTextElement) {
					 lastLine = paragraph = (SVGTextElement) e;
				 }				
				 continue;
			 }
			 
			 if (e instanceof SVGTextElement) {
				 
				 
				 SVGTextElement current = (SVGTextElement) e;

				 int r = Util.inLine(paragraph, current, lastLine, (this.right-this.x));
				 switch(r) {
					 case Util.NEWLINE: {
						elements2.add(current);
						lastLine = paragraph = current;
						continue;
					 }
					 case Util.BRLINE: {
						 paragraph.addBR();
						 paragraph.add(current);
						 lastLine = current;
						 continue;
					 }
					 case Util.NOSPACE: {
						
						 paragraph.add(current);
						 lastLine = current;
						 break;
					 }
						 
					 /*
					 case Util.NEWTABLE: {
						 if (current != e) {
							 current.addBR();
							 current.add(e);
						 }
						 break;
					 }
					 */
					 default:{
						paragraph.addSpace();
						paragraph.add(current);
						lastLine = current;
					 }
				 }
			 } else {
//				 if (e instanceof SVGImageElement) {
//					 Util.p("Hi");
//				 } 
				 elements2.add(e);
				 //last = null;					 
			 }
			 
			 //System.out.println(current);
		 }
	}
	protected int m_index = 0;
	protected void handleEelement(Article box, SVGElement e){
		this.handleEelement(box, e, true);
	}
	protected void handleEelement(Article box, SVGElement e, boolean ulOl){
		//debug = (e.pageNum == 31);
		
//		if (debug) {
//			Util.p(e);
//		}
		if (e instanceof SVGTextElement) {
			 SVGTextElement t = (SVGTextElement) e;
			 if (ulOl) {
				 if (t.find("•")) {
					 processUL(box);
					 return;
				 } 
				 
				 else if (Util.isOl(t)) {
					 processOL(box, t);
					 return;
				 } 
			 }
			 		
			 if (Util.getBox(e) != null) {
				 m_index++;
				 return;
			 }
			 if (t.getLine().getLine().size() > 0) {
				 box.addLine(t, "div", "s");
			 }
		 } 
		 else if (e instanceof SVGImageElement) {
			 
			 SVGImageElement i = (SVGImageElement) e;
			 if (!i.isEmbeded()) {
				
				 box.reset();
				 Map<String, String> h = new HashMap<String, String>();
				 String src = i.getAttrs().get("xlink:href");
				 src = src.replaceAll("\\.\\./", "./");
				 h.put("src", src);
				 h.put("class", "img-responsive");
				 //h.put("style", String.format("width:%s;height:%s", i.getAttrs().get("width"),i.getAttrs().get("height")));
				 h.put("style", String.format("width:%s", i.getAttrs().get("width")));
				 Article img = box.safeDiv("i");
				 img.addLine("img", "", h);
				 box.reset();
			 }
		 } else if (e instanceof SVGPathElement) {
				SVGPathElement p = (SVGPathElement) e;
				if (!VividingStripper.NOSTYLE) {
				if (p.isRectangle() && Util.notEmpty(p.getAttrs().get("fill"))) {
					if (p.isLine()) {
						Tag lastP =  box.lastParagraph();
						if (lastP == null) {
							Tag t = box.getParent();
							if (t instanceof Article) {
								lastP = ((Article) t).lastParagraph(box);
							}
						}
						if (lastP != null) {
							//For bottom only for now, right????
							String s = String.format("%spx %s %s", p.getBound().height(), "solid", p.getAttrs().get("fill"));
							lastP.addStyle("border-bottom",  s);
							//.getAttrs().put(key, value)
						}
					}	
					} else if (p.isBox()){
						
					}
				}
			}  
		 else if (e instanceof SVGGElement) {
			 ((SVGGElement)e).processHTML(box, (SVGGElement)e);
		 }
		 m_index++;
	}
	
	public void processHTML(Article article) {
		this.processHTML(article, null);
	}
	
 
	private Article addBox(Article article) {
		Article box = article.safeDiv();
		box.putAttr("class", "panel panel-warning panel-sm msg-box");
		Article header = box.safeArticle("div", "panel-heading");
		
		String lastColor = null;
		
		while (m_index < elements2.size()) {
			 //SVGTextElement e = elements2.get(m_index);
			 SVGElement e = elements2.get(m_index);
			 if (e instanceof SVGPathElement) {
				 
				 if (!VividingStripper.NOSTYLE) {
					if (e.getAttrs().get("fill") != null && !e.getAttrs().get("fill").equals("#000000")) {
						//Assume this first one will be used as background
						//Need a lot of more work here to make it right, header, body, ....
						if (lastColor == null) {
							lastColor = e.getAttrs().get("fill");
							box.addStyle("background-color", lastColor);
						} else {
							box.addStyle("border", String.format("solid 2px %s", e.getAttrs().get("fill")));
						}
						//background-color:#c5cdca;border:solid 2px #004b74;border:solid 2px #ffffff
						//background-color:#fbeab3;border:solid 2px #e28f26;border:solid 2px #004b74
					}
				}
				m_index++;
				continue;
			} else if (e instanceof SVGTextElement) {
				if (Util.isBold((SVGTextElement)e)) {
				//Util.p(e);
					header.addLine((SVGTextElement)e, "h2");
					m_index++;					
				}//Othewise, there is no title
				break;
			}
			m_index++;
		 }
		
		box.reset();
		return box.safeArticle("div", "panel-body");
	}
	
	public void processHTML(Article article, SVGGElement gElement) {
		
		article.reset();
		Article box = null;
		//
		if (isTable())  {
			article.reset();
			Table  tableBox = VividingStripper.getTable(this);
			if (tableBox == null) {
				tableBox = new Table(m_index, m_index, m_index, m_index, m_index, "#FFFFFF");
			} else {
				debug = true;
			}
			this.handleTable(article, tableBox);
			return;
		} else {
			if (gElement == null) {
				box = article;
			} else {
				
				box = addBox(article);
			}
		}
		if (elements2.size() == 0) {
			return;
		}
		m_index = 0;
		
		
		SVGElement e = elements2.get(m_index);
		
		while (m_index < elements2.size()) {
			 //SVGTextElement e = elements2.get(m_index);
			 e = elements2.get(m_index);
			 //Util.p(e);
			 //System.out.println(e);
			 //String s = e.getAttrs().get("class");
			 this.handleEelement(box, e);
			 
			 //book.append(String.format("<div class=\"%s s\">%s</div>", s, e.line.text));
		 }		 
	 }
	
	//decimal
	//cjk-ideographic
	//lower-alpha
	//lower-greek	The marker is lower-greek	Play it »
	//lower-latin	The marker is lower-latin (a, b, c, d, e, etc.)	Play it »
	//lower-roman
	
	
	public void processOL(final Article article, SVGTextElement first) {
		article.reset(); 
		
		
		Article ul = article.safeOL();
		boolean decimal = Util.isDecimalOl(first);
		if (decimal) {
			ul.getAttrs().put("style", " list-style-type:decimal;");
		} else {
			ul.getAttrs().put("style", " list-style-type:lower-alpha;");
		}
		
		//int last = -1;
		String last = null;
		while (m_index < elements2.size()) {
			 SVGElement t = elements2.get(m_index);
			 
			 if (t instanceof SVGTextElement) {
				 SVGTextElement e = (SVGTextElement) t;

				 Matcher m = Util.getOl(e, decimal);
				 
				 if (m.find() ) {
					 String current = m.group(1);
					 //int current = 0;
//					 if ( m.group(1).length() == 1) {
//						 current = (int) m.group(1).charAt(0);	 
//					 } else {
//						 System.err.println("NOT SUPPORTED, " + m.group(1)) ;
//					 }
					 
					 //if (last != -1 && current != last + 1) {
					 if (last != null) {
						 
						 boolean inLi = true;
						 if (decimal) {
							 inLi = (Integer.parseInt(last) + 1 == Integer.parseInt(current));
						 } else {	 
							 if (current.length() == last.length()) {
								 for(int i = 0; i < current.length(); i++) {
									 char c1 = last.charAt(i);
									 char c2 = current.charAt(i);
									 if (i+1 == current.length()) {
										 if (c2 != c1 + 1) {
											 inLi = false;
											 break;
										 }
									 } else {
										 if (c2 != c1 ) {
											 inLi = false;
											 break;
										 }
									 }
								 }
								 
							 } else if (current.length() != last.length() + 1){
								 inLi = false;
								 Util.err("NOT SUPPOERED");
							 } 
						 }
						 if (!inLi) {
							 article.reset();
							 return;
						 }
					 }
					 Line l = e.getLine();
					 
					 
					 //Util.p(m.group(1));
					 
					 int len = m.group(1).length() + 2;
					 
					 if (l.getLine().size() > len) {
						 
						 for (int i = 0; i < len; i++) {
							 l.getLine().remove(0);
						 }
					 }
					 ul.addLine(e, "li");
					 
					 last = current;
					 
				 } else {
					 //m_index--;					 
					 article.reset();
					 return;
				 }
				 m_index++;	
			 } else if (t instanceof SVGPathElement) {
				 m_index++;
			 }
			 else {
				 article.reset();
				 return;
			 }
			 
		 }
		 
		 article.reset();
	 }
	
	public void processUL(final Article article) {
		article.reset(); 
		Article ul = article.safeUL();
		while (m_index < elements2.size()) {
			 SVGElement t = elements2.get(m_index);
			 
			 if (t instanceof SVGTextElement) {
				 SVGTextElement e = (SVGTextElement) t;
				 //System.out.println(e);
//				 if (e.find("Explain why it is worthwhile to study sociology")) {
//					 System.out.println(e);
//				 }
				 if (e.find("•")) {
					 Line l = e.getLine();
					 int index = -1;
					 for(int i=0; i < l.getLine().size(); i++) {
						 LineItem l0 = l.getLine().get(i);
						 if (l0.getTextPosition() != null && l0.getTextPosition().getUnicode().charAt(0) == '•') {
							 //l.getLine().remove(index);
							 index = i;
							 break;
						 }
					 }
					 
					 if (index != -1) {
						 l.getLine().remove(index);
					 }
					 ul.addLine(e, "li");
				 } else {
					 m_index--;
					 
					 article.reset();
					 return;
				 }
				 m_index++;	
			 } else {
				 article.reset();
				 return;
			 }
			 
		 }
		 
		 article.reset();
	 }
	
	int findLeftClosestPath(SVGTextElement t) {
		float MIN = 0;
		//float closest = 0;
		if (debug) {
			Util.p(t);
		}
		for (SVGElement e : elements) {
			if (e == t) {
				continue;
			}
			if (VividingStripper.TABLEWITHLINE) {
				if (e instanceof SVGPathElement) {
					SVGPathElement g = (SVGPathElement)e;
					if (debug) {
						Util.p(g);
					}
					if (g.isVeticalLine()) {
						if (e.x <= t.x) {
							
							//if (Util.equal(t.x-e.x, t.x-MIN, 1f)){
							if (t.x-e.x < t.x-MIN) {
								MIN = e.x;
							}
						}
					}  
					/* else if (g.isHorizontalLine()) {
						if (e.x <= t.x) {
							float xxx = t.x-e.x;
							float yyy = t.x-MIN;
							if (t.x-e.x <= t.x-MIN) {
								MIN = e.x;
							}
						}
					}*/
				} else {
					continue;
				}
			}
				
			
		}
		
		if (MIN == 0) {
//			if (VividingStripper.TABLE_NO_LEFT_RIGHT_BORDER) {
//				MIN = this.x;
//			} else 
			MIN = x;
		}
		return Math.round(MIN);
	}
	
	int findTopClosestPath(SVGTextElement t) {
		float MIN = -1;
		for (SVGElement e : elements) {
			
			if (e instanceof SVGPathElement) {
				if (Util.getBox(t) != null) {
					 continue;
				}
				if (!((SVGPathElement)e).isHorizontalLine()) {
					continue;
				}
				if (e.y <= t.y) {
					
					if (MIN == -1) {
						MIN=e.y;
					} else
					if (t.y-e.y < t.y-MIN) {
						MIN = e.y;
					}
				}
			}
		}
		
		if (MIN == -1) {
			MIN = y;
		}
		return Math.round(MIN);
	}
	
	Map<Integer, List<SVGTextElement> > columnsMap = new LinkedHashMap<Integer, List<SVGTextElement>>();
	Map<Integer, List<SVGTextElement> > rowsMap = new LinkedHashMap<Integer, List<SVGTextElement>>();
	
	void printInfo(Map<Integer, List<SVGTextElement> > map) {
		//Util.p("XXXXXXXXXX\n" + map.size() +"\n" + map.keySet());
		//Collection.sort(map.keySet());
		for(Integer f: map.keySet()) {
			List<SVGTextElement> rowList = map.get(f);
			for(SVGTextElement t : rowList) {
				Util.p(t);	
			}
		}
	}
	
	public boolean isTable() {
		if (VividingStripper.useTable && columnsMap.size() == 0 && rowsMap.size() == 0) {
			scanCellInfo(textElements());
		}
		return columnsMap.size()>=VividingStripper.TABLEMinCOL 
				&& rowsMap.size() >= VividingStripper.TABLEMinROW;
	}
	
	protected void scanCellInfo(List<SVGTextElement> list) {
		columnsMap.clear();
		rowsMap.clear();
		
		for(SVGTextElement t : list) {
			if (Util.getBox(t) != null) {
				 continue;
			}
			
			if (t.width() + 100 > VividingStripper.pageSize.getWidth()) {
				//Too long
				continue;
			}
			
			if (debug) {
				Util.p(t);
			}
			int left = findLeftClosestPath(t);
			int top = findTopClosestPath(t);
			List<SVGTextElement> rowList = columnsMap.get(left);
			List<SVGTextElement> colList = rowsMap.get(top);
			if (debug) {
				Util.p(left + ", " + top + ", " + t.getText());
			}
			if(rowList == null) {
				for (int c :columnsMap.keySet()) {
					if (Math.abs(c-left) < 20) {
						rowList = columnsMap.get(c);
					}
				}
				if (rowList == null) {
					rowList = new ArrayList<SVGTextElement>();
					columnsMap.put(left, rowList);
				}
			}
			
			if(colList == null) {
				colList = new ArrayList<SVGTextElement>();
				rowsMap.put(top, colList);
			}
			colList.add(t);
			rowList.add(t);
		}
		
		if (debug) {
			printInfo(columnsMap);
			Util.p("=================");
			printInfo(rowsMap);
		}
	}
	
	List<SVGTextElement> getCellElements(List<SVGTextElement> list, int rowStart, int rowEnd) {
		List<SVGTextElement> cellElemet = new ArrayList<SVGTextElement>();
		if (list == null) {
			return cellElemet;
		}
		for(SVGTextElement e :list) {
			//Util.p(e);
			if (e.y >= rowStart && e.y <= rowEnd) {
				cellElemet.add(e);
			}
		}
		return cellElemet;
	}
	
	//TOSINGLECOL
	protected void handleTable(Article article) {
		ArrayList<Integer> rows = new ArrayList<Integer>();
		rows.addAll(rowsMap.keySet());
		
		Collections.sort(rows);
		for(int row: rows) {
			article.reset();
			Article table = article.safeDiv("s");
			table.addLine(Util.mergeElement(rowsMap.get(row)));
		}
	}
	protected void handleTable(Article article, Table tableBox) {
		Article table = article.safeTable();
		if(Util.notEmpty(tableBox.background)) {
			table.addStyle("background-color", tableBox.background);
		}
		
		//debug = true;
		
		int columnsCount = columnsMap.size();
		
		ArrayList<Integer> columns = new ArrayList<Integer>();
		columns.addAll(columnsMap.keySet());
		
		Collections.sort(columns);
		
		ArrayList<Integer> rows = new ArrayList<Integer>();
		rows.addAll(rowsMap.keySet());
		
		Collections.sort(rows);
		if (debug) {
			Util.p("Table = " + rows.size()+ "X" +columnsCount);
			Util.p(columns);
			Util.p(rows);
		
		}
		for(int r = 0; r < rows.size(); r++) {
			if (debug) {
				Util.p("row = "+ r + ", " + rows.get(r));
			}
			List<SVGTextElement> row =  rowsMap.get(rows.get(r));
			if (debug) { 
				Util.p("row = "+ row);
			}
			table.reset();
			Article tr = table.safeTR();
			
			if (row.size() == 1 && r == 0) {
				Paragraph td = tr.safeParagraph("td");
				td.addLine(row.get(0));
				td.addAttr("colspan", String.valueOf(columnsCount));
				td.addAttr("class", "text-center");
				if (tableBox.header != null) {
					td.getAttrs().put("style", "background-color:"+tableBox.header);
				}
				continue;
			}
			
			//NOW, build the row of the table
			
			List<Paragraph> rowParagraphs = new ArrayList<>();
			//int last = 0;
			Paragraph td1 = null;
			for(int col : columns) {
				if (debug) {
					Util.p(col);
				}
				tr.reset();
				if (td1 != null) {
					td1.getAttrs().put("data-x2", String.valueOf(col));
				}
				tr.reset();
				td1 = tr.safeParagraph("td");
				
				td1.getAttrs().put("data-x1", String.valueOf(col));
				rowParagraphs.add(td1);
			}
			td1.getAttrs().put("data-x2", String.valueOf(Integer.MAX_VALUE));
			
			for(SVGTextElement e : row) {
				for (Paragraph td : rowParagraphs) {
					int x1 = Integer.parseInt(td.getAttrs().get("data-x1"));
					int x2 = Integer.parseInt(td.getAttrs().get("data-x2"));
					
					if (e.x >= x1 && e.x <= x2) {
						td.addLine(e, false);
						break;
					}
					
				}
			}
		}
	}
	
Map<Float, List<SVGPathElement> > columnsPath = new LinkedHashMap<>();
	
	
SVGGElement gTable(Map<Float, List<SVGPathElement>> map) {
		int rows = map.size();
		int cols = 0;
		
		float x1 = Float.MAX_VALUE, y1 = Float.MAX_VALUE, right1 = 0f, bottom1= 0f;
		//Util.p(x1 + " " + y1);
		for(Float f: map.keySet()) {
			if (debug) {
				Util.p("====================\n"+ f);
			}
			List<SVGPathElement> rowList = map.get(f);
			cols = Math.max(rowList.size(), cols);
			for(SVGPathElement t : rowList) {
				if (debug) {
					Util.p(t);
				}
				x1 = Math.min(x1, t.x);
				y1 = Math.min(y1, t.y);
				right1 = Math.max(right1, t.right);
				bottom1 = Math.max(bottom1, t.bottom);
			}
		}
		
//		Util.p("====================\n");
		
		if (debug) {
			Util.p(x1 + "," + y1 + "," + right1 + ", " + bottom1);
		}
		Table table = new Table(x1, y1, right1, bottom1 );
//		
		SVGGElement g = new SVGGElement(pageNum);
		g.x = x1;
		g.y = y1;
		g.right = right1;
		g.bottom = bottom1;
		
		List<SVGElement> gChildren = new ArrayList<>();
		for(SVGElement t : elements) {
			if (table.contains(t)) {
				gChildren.add(t);
			}
		}
		this.elements.add(g);
		
		for(SVGElement t : gChildren) {
			this.elements.remove(t);
			g.elements.add(t);
		}
		
		if (debug) {
			Util.p(table);
		}
		return g;
	}


SVGGElement scanTableInfo() {
	columnsPath.clear();
	SVGPathElement lastPath = null;
	
	if (debug) {
		Util.p("XXXXXXXXX ==================== XXXXXXXX");
	}
	
	//Sometime Table without border, they just use background box instead...
	SVGPathElement backGroundPath = null;
	
	
	for(SVGElement t : elements) {
		if (Util.getBox(t.x, t.y, t.right-t.x, t.bottom-t.y) != null) {
			 continue;
		}
		
		if (t instanceof SVGPathElement) {
			SVGPathElement currentPath = (SVGPathElement) t;
			
			
			if (!currentPath.isRectangle()) {
				continue;
			}
			
			
			if (backGroundPath != null) {
 				if ((backGroundPath.width() <= currentPath.width()) && (backGroundPath.height() <= currentPath.height())) {
					backGroundPath = currentPath;
				}
			}
			
			if (lastPath == null) {
				backGroundPath = lastPath = currentPath;
				
				List<SVGPathElement> x = new ArrayList<>();
				columnsPath.put(lastPath.y, x);
				continue;
			}
			
			if (debug) {
				Util.p(lastPath);
				Util.p(t);
			}
			if (lastPath.y == currentPath.y) { //If we are in the same row
				if (lastPath.x == currentPath.x && lastPath.right == currentPath.right) {
					if (currentPath.bottom > lastPath.bottom) {
						lastPath = currentPath;
						continue;
					}
				} else if (Util.equal(lastPath.right, currentPath.x, 2f)){
					
					List<SVGPathElement> x = columnsPath.get(lastPath.y);
					
					if (x == null) {
						x = new ArrayList<>();
						columnsPath.put(lastPath.y, x);
					}
					
					x.add(lastPath);
					lastPath = currentPath;
				}
			} else {
				List<SVGPathElement> x = columnsPath.get(lastPath.y);
				if (x != null) {
					x.add(lastPath);
				}
				lastPath = currentPath;
			}
		}
	}
	if (lastPath != null) {
		List<SVGPathElement> x = columnsPath.get(lastPath.y);
		if (x != null) {
			x.add(lastPath);
		}
	}
	
	if (backGroundPath != null) {
		//Background need to be big....
		if (lastPath != null && lastPath.y < backGroundPath.bottom) {
				List<SVGPathElement> x = new ArrayList<>();
				x.add(backGroundPath);
				columnsPath.put(backGroundPath.bottom, x);
		}
	}
	return gTable(columnsPath);
}
	
	protected boolean hasText() {
		for(SVGElement e : elements2) {
			if (e instanceof SVGTextElement) {
				return  true;
			} else if (e instanceof SVGGElement) {
				boolean b = ((SVGGElement)e).hasText();
				if (b) {
					return true;
				}
			}
		}
		return false;
	}
	
	protected List<SVGTextElement> textElements() {
		List<SVGTextElement> t = new ArrayList<SVGTextElement>();
		for(SVGElement e : elements) {
			if (e instanceof SVGTextElement) {
				t.add((SVGTextElement)e);
			} else if (e instanceof SVGGElement) {
				t.addAll(((SVGGElement)e).textElements());
			}
		}
		return t;
	}
	
	protected List<SVGPathElement> pathElements() {
		List<SVGPathElement> t = new ArrayList<SVGPathElement>();
		for(SVGElement e : elements) {
			if (e instanceof SVGPathElement) {
				t.add((SVGPathElement)e);
			}
		}
		return t;
	}
};
