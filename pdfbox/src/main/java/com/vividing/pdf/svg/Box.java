package com.vividing.pdf.svg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.pdfbox.text.TextPosition;

import com.vividing.pdf.Util;
import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.svg.element.BoxComparator;
import com.vividing.pdf.svg.element.SVG;
import com.vividing.pdf.svg.element.SVGElement;
import com.vividing.pdf.svg.element.SVGGElement;
import com.vividing.pdf.svg.element.SVGPathElement;
import com.vividing.pdf.svg.element.SVGTextElement;

public class Box {
	
	public Box parent = null;
	public List<Box> children = new ArrayList<Box>();
	public String name = "box";
	public int pageNum = 0;
	public float x = 0;
	public float y = 0;
	public float right = 0;
	public float bottom = 0;
	public String background = null;
	public SVGPathElement svgElement = null;

	public boolean isTop() {
		return name.equals("main");
	}
	public Box(float x, float y, float right, float bottom) {
		if (x < right) {
			this.x = x;
			this.right = right;
		} else {
			this.x = right;
			this.right = x;
		}
		if (y < bottom) {
			this.y = y;
			this.bottom = bottom;
		} else {
			this.y = bottom;
			this.bottom = y;
		}
	}
	
	public Box(float x, float y, float right, float bottom, int pageNum, String background) {
		this(x, y, right, bottom);
		this.pageNum = pageNum;
		this.background = background;
	}

	public Box(SVGPathElement svgElement, float x, float y, float right, float bottom, int pageNum) {

		this(x, y, right, bottom, pageNum, svgElement != null ? svgElement.getAttrs().get("fill"):null);
		this.svgElement = svgElement;
	}

	public Box() {
		x = 0;
		y = 0;
		right = 0;
		bottom = 0;
	}

	public boolean isSame(Box box) {
		
		return x == box.x && y == box.y && right == box.right && bottom == box.bottom; 
	}
	public String toString() {
		// return String.format("[%s, x=%s,y=%s,right=%s,bottom=%s]", name,
		// x,y,right, bottom);
		return String.format("[%s,x=%s,y=%s,right=%s,bottom=%s,width=%s,height=%s]", name, x, y, right, bottom, width(), height());
	}

	public boolean contains(SVGElement e) {
		float delta = 1f;
		if (e instanceof SVGTextElement) {
			// SVGTextElement t = (SVGTextElement) e;
			delta = 2f;
			// Util.p(delta);
			return this.contains(e.x, e.y - delta);
			// || this.in(e.right, e.bottom) ;
		}
		return this.contains(e.x, e.y, e.right, e.bottom, delta);
	}

	public boolean contains(float x, float y) {

		return Util.between(this.x, this.right, x) && Util.between(this.y, this.bottom, y);
	}

	public boolean contains(TextPosition position) {

		return contains(position.getX(), position.getY() - position.getHeight());
	}

	public boolean contains(Box b) {
		return this.contains(b.x, b.y, b.right, b.bottom, 0);
	}

	public boolean contains(float X, float Y, float RIGHT, float BOTTOM, float delta) {
		return Util.between(x, right, X, RIGHT, delta) && Util.between(y, bottom, Y, BOTTOM, delta);
	}

	// Without conatins,
	// we r ingor x,
	public boolean intersects(Box b) {
		if (Util.between(y, bottom, b.y)) {
			if (bottom < b.bottom) {
				return true;
			}
		}

		if (Util.between(b.y, b.bottom, y)) {
			if (b.bottom < bottom) {
				return true;
			}
		}

		return false;
	}

	public boolean equalTo(Box b) {
		return (x == b.x && y == b.y && right == b.right && bottom == b.bottom);
	}

	public float width() {
		return this.right - this.x;
	}

	public float height() {
		return this.bottom - this.y;
	}
	// public void print(String tab) {
	// Util.p(tab + this);
	//
	// for(Box b: children) {
	// b.print(tab+"\t");
	// }
	// }

	
	public void grouping() {
		List<Box> boxes = children;

		// Box last = null;
		boxes.sort(new BoxComparator());

		List<Box> bbb = new ArrayList<Box>();
		for (Box b : boxes) {

			if (bbb.size() == 0) {
				bbb.add(b);
				continue;
			}

			Box l = null;
			boolean f = false;
			for (Box last : bbb) {

				// if (last.pageNum == 2) {
				// Util.p(last);
				// Util.p(b);
				// }
				if (last.contains(b)) {
					b.children.add(last);
					bbb.add(b);
					l = last;
					break;
				} else if (b.contains(last)) {
					last.children.add(b);
					f = true;
					break;
				}
			}

			if (l != null) {
				bbb.remove(l);
			}

			if (!f) {
				bbb.add(b);
			}

		}

		for (Box b : bbb) {
			// b.print("");
			b.parent = this;
			b.grouping();
		}

		children = bbb;
	}

	public Box find(SVGElement e) {

		if (contains(e)) {
			if (children.size() == 0) {
				return this;
			}
			for (Box b : children) {
				Box b0 = b.find(e);

				if (b0 != null) {
					return b0;
				}
			}
			// No find it then return this....
			return this;
		}
		return null;
	}

	public SVGGElement m_svgGElement = null;

	// pg parent g
	// Create G for all box
	public void createG(SVG svg, SVGGElement pg) {

		if (svgElement != null) {
			svg.elements.remove(this.svgElement);
		}

		m_svgGElement = new SVGGElement(pageNum);
		m_svgGElement.x = this.x;
		m_svgGElement.y = this.y;
		m_svgGElement.right = this.right;
		m_svgGElement.bottom = this.bottom;
		m_svgGElement.box = this;
		// if (this.parent == null) {
		// m_svgGElement.setTopMost(true);
		// }
		if (pg == null) {
			svg.elements.add(m_svgGElement);
		} else {
			pg.getElements().add(m_svgGElement);
		}
		if (svgElement != null) {
			m_svgGElement.getElements().add(svgElement);
		}

		for (Box b : children) {
			b.createG(svg, m_svgGElement);
		}
	}

	public void createG(SVGGElement pg) {

		m_svgGElement = new SVGGElement(pageNum);
		m_svgGElement.box = this;
		m_svgGElement.x = this.x;
		m_svgGElement.y = this.y;
		m_svgGElement.right = this.right;
		m_svgGElement.bottom = this.bottom;
		int idx = -1;
//		Util.p(pg);
//		Util.p(pg.getElements().size());
		for (int i = 0; i < pg.getElements().size();i++) {
			SVGElement e = pg.getElements().get(i);
			//Util.p(e);
			if (e.y > this.y) {
				idx = i;
				break;
			}
		}
		if (idx != -1) {
			pg.getElements().add(idx, m_svgGElement);
		} else {
			pg.getElements().add(m_svgGElement);
		}
	}
	public void sort() {
		if (m_svgGElement != null) {
			m_svgGElement.sort();
		}
		for (Box b : children) {
			b.sort();
		}
	}

	public void paragraphing() {
		if (m_svgGElement != null) {
			m_svgGElement.paragraphing();
		}
		for (Box b : children) {
			b.paragraphing();
		}
	}

	public void checkTable() {
		List<Box> boxes = null;
		if (m_svgGElement != null) {
			m_svgGElement.checkTable();
		}
		for (Box b : children) {
			b.checkTable();
		}
	}
	
	public void combine(Box box) {

		x = Math.min(x, box.x);
		y = Math.min(y, box.y);
		bottom = Math.max(bottom, box.bottom);
		right = Math.max(right, box.right);
	}

	public void add(Box box) {
		int idx = -1;
		for (Box b : children) {
			if (b.equalTo(box)) {
				return;
			}
			if (b.contains(box)) {
				b.add(box);
				return;
			} else if (b.contains(box)) {
				idx = children.indexOf(b);
				box.add(b);
				break;
			} else if (b.intersects(box)) {
				b.combine(box);
				return;
			}
		}

		if (idx != -1) {
			children.remove(idx);
			children.add(idx, box);
		} else {
			children.add(box);
		}
	}
	
	public void addBox(Box box) {
		children.add(box);
}
	
	public boolean isBelongToBox(SVGPathElement p) {
		List<Float> list  = new ArrayList<>();   
		list.add(Math.abs(p.y - y));
		list.add(Math.abs(p.y - bottom));
		list.add(Math.abs(p.bottom - y));
		list.add(Math.abs(p.bottom - bottom));
		return Collections.min(list) < VividingStripper.BOX_MIN_HEIGHT;
	}
	public void expendBox(SVGPathElement p) {
		x = Math.min(p.x, x);
		y = Math.min(p.y, y);
		right = Math.max(p.right, right);
		bottom =  Math.max(p.bottom, bottom);
	}

}
