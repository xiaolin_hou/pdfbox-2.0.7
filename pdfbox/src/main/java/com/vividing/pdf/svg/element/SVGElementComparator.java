package com.vividing.pdf.svg.element;

import java.util.Comparator;

import com.vividing.pdf.Util;
import com.vividing.pdf.VividingStripper;

public class SVGElementComparator implements Comparator<SVGElement> {
	
	boolean multiColum = false;
	
	public SVGElementComparator() {
		
	}
	
	public SVGElementComparator(boolean multiColum) {
		this.multiColum = multiColum;
	}
	@Override
	public int compare(SVGElement e1, SVGElement e2) {
		boolean debug = false;
		if (e1.pageNum != e2.pageNum) {
			return e1.pageNum - e2.pageNum;
		}
		
		if (e1.pageNum == 80) {
			debug = true;
		}
		float x1 = e1.x;
		float x2 = e2.x;

		float dx = x1 - x2;
			
		float y1 = e1.y;
		float y2 = e2.y;
		float dy = y1 - y2;
		if (debug) {
			Util.p(e1.pageNum + " === " + e1 + " <> " + e2 + ", dx = " + dx + ", dy = " + dy);
		}
		if (e1 instanceof SVGTextElement && e2 instanceof SVGTextElement) {
			SVGTextElement t1 = (SVGTextElement) e1;
			SVGTextElement t2 = (SVGTextElement) e2;
			
			if (t1.getTextStyle() == t2.getTextStyle()) {
				if (y1 != y2) {
					if (debug) {
						Util.p("XXXX 12A");
					}
					return (int) Math.signum(dy);
				} else if (x1 != x2) {
					if (debug) {
						Util.p("XXXX 12B");
					}
					return (int) Math.signum(dx); 
				} else if (e1.right != e2.right) {
						if (debug) {
							Util.p("XXXX 10b");
						}
						return (int) Math.signum(e1.right - e2.right); 
					} else if (e1.bottom != e2.bottom) {
					if (debug) {
						Util.p("XXXX 10c");
					}
					return (int) Math.signum(e1.bottom - e2.bottom);
				}
				return 0;
			}
			if (Util.equal(y1, y2, VividingStripper.minLINEGAP)) {
				
				if (x1 != x2) {
					if (debug) {
						Util.p("XXXX 10a");
					}
					return (int) Math.signum(dx); 
				}
				
				if (e1.right != e2.right) {
					if (debug) {
						Util.p("XXXX 10b");
					}
					return (int) Math.signum(e1.right - e2.right); 
				}
	
				if (e1.bottom != e2.bottom) {
					if (debug) {
						Util.p("XXXX 10c");
					}
					return (int) Math.signum(e1.bottom - e2.bottom);
				}
				
				if (y1 != y2) {
					if (debug) {
						Util.p("XXXX 10d");
					}
					return (int) Math.signum(dy);
				}
//				Util.p(e1);
//				Util.p(e2);
				if (debug) {
					Util.p("XXXX 10e");
				}
				return 0;
				//return (int) Math.signum(yDifference);
			}
			if (debug) {
				Util.p("XXXX 10F");
			}
			return (int) Math.signum(dy);
		} 
		//else 
		else if (e1 instanceof SVGPathElement && e2 instanceof SVGPathElement || e1 instanceof SVGGElement && e2 instanceof SVGGElement) {
			
			if (y1 != y2) {
				if (debug) {
					Util.p("XXXX 11a");
				}
				
				if (Math.abs(dy) >= 1) {
					return (int)dy;
				}
				return (int) Math.signum(dy);
			} else if (e1.bottom != e2.bottom) {
				if (debug) {
					Util.p("XXXX 11b");
				}
				return (int) Math.signum(e1.bottom - e2.bottom);
			} else if (x1 != x2) {
				if (debug) {
					Util.p("XXXX 11c");
				}
				return (int) Math.signum(dx); 
			} if (e1.right != e2.right) {
				if (debug) {
					Util.p("XXXX 11d");
				}
				return (int) Math.signum(e1.right - e2.right); 
			}
			
			if (debug) {
				Util.p("XXXX 11e");
			}
			return 0;
		} 
//		if (e1 instanceof SVGTextElement) {
//			if (debug) {
//				Util.p("XXXX 12a");
//			}
//			return 1;
//		}
		{
			if (y1 != y2) {
				if (debug) {
					Util.p("XXXX 12A");
				}
				return (int) Math.signum(dy);
			} else if (x1 != x2) {
				if (debug) {
					Util.p("XXXX 12B");
				}
				return (int) Math.signum(dx); 
			} 
			return 0;			
		}
	}
}
