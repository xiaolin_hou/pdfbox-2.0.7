package com.vividing.pdf.svg;

public class Table extends Box {

//	public float[] columns = null;
//	public float[] rows = null;
	public String header = null;
	public String title = null;
	public Table(int pageNum, float x, float y, float right, float bottom, String background){
		super(x, y, right, bottom, pageNum, background);		
	}
	
	public Table(float x, float y, float right, float bottom) {
		super(x, y, right, bottom);		
	}
}
