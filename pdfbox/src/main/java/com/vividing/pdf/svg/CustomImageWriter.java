package com.vividing.pdf.svg;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

public class CustomImageWriter {
	   
    private ExecutorService executorService;
 
    public CustomImageWriter() {
        this.executorService = Executors.newFixedThreadPool(1);
    }
 
    public void writeImage(final BufferedImage image, final File file) throws IOException {
        //Slow
        //ImageIO.write(image, "jpg", file);
 
        //My suggestion
        Runnable r = new Runnable() {
            @Override
            public void run() {
                try {
                    BufferedOutputStream imageOutputStream = new BufferedOutputStream(new FileOutputStream(file));
                    ImageIO.write(image, "jpg", imageOutputStream);
                    imageOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        r.run();
        //executorService.submit(r);
    }
   
    public void waitForImages() {
        executorService.shutdown();
 
        try {
            executorService.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
 
}