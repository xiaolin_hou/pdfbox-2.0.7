package com.vividing.pdf.svg.element;

import com.vividing.pdf.VividingStripper;

public class SVGImageElement extends SVGElement {
	public SVGImageElement(int pageNum) {
		super("image", pageNum);
	}
	
	@Override
	public void write(VividingStripper stripper, StringBuilder out, StringBuilder def, boolean embededImg) {
		if ((isEmbeded() == embededImg)) {
			putAttr("data-embeded", String.valueOf(embeded));
			super.write(stripper, out, def, embededImg);	
			out.append(" />\n");
		}
	}

	@Override
	public String toString() {
		return String.format("<image x=%s, y=%s, right=%s, bottom=%s>", x, y, right, bottom);
	}
};