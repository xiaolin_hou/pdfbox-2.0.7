package com.vividing.pdf.svg.element;

import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.tag.Tag;

public class SVGElement extends Tag{
	protected SVGElement(String tag, int pageNum) {
		this.pageNum = pageNum;
		this.addTag(tag);
	}
	
	protected int id = -1;
	protected int clip = -1;
	protected boolean embeded = false; //image??
	public int pageNum = 0;
	public float right = 0;
	public float bottom = 0;
	public float x = -1;
	public float y = -1;
	
	public float width() {
		return right-x; 
	}
	
	public float height() {
		return bottom-y; 
	}
	
	private SVGElement clipPath = null;
	
//	public String getFill() {
//		return this.m_attrs.get("fill");
//	}
	public void fill(String fill) {
		this.putAttr("fill", fill);
	}
	public void stroke(String stroke) {
		this.putAttr("stroke", stroke);
	}
	public void strokeWidth(float strokeWidth) {
		this.putAttr("stroke-width", String.valueOf(strokeWidth));
	}
	
	public void writeFile(VividingStripper stripper, StringBuilder body, StringBuilder def, boolean embededImg) {
		this.write(stripper, body, def, embededImg);
	}

	@Override
	public void write(VividingStripper stripper, StringBuilder out) {
		putAttr("data-x", String.valueOf(x));
		putAttr("data-y", String.valueOf(y));
		putAttr("data-right", String.valueOf(right));
		putAttr("data-bottom", String.valueOf(bottom));
		out.append(String.format("<%s", getStartTagsPart1()));
		if (getAttrs().size() > 0) {
			for (String key : getAttrs().keySet()) {
				out.append(" ").append(key).append("=\"").append(getAttr(key)).append("\"");
			}
		}
	
	}
	public void write(VividingStripper stripper, StringBuilder out, StringBuilder def, boolean embededImg) {		
		if (clip != -1) {
			StringBuilder sb = new StringBuilder();
			this.write(stripper, sb);
			def.append(String.format("<clipPath id=\"clip-%s\">", clip));
			def.append(sb);
			def.append("</clipPath>\n");		
		}  else {
			this.write(stripper, out);
		}
		
	}

	@Override
	public void clear() {
	}
	
	public void clearStrokeandFill() {
		fill(null);
		stroke(null);
	}
	
	
//	@Override
//	public String toString() {
//		return super.toString();
//	}
//	
	public int getClip() {
		return clip;
	}

	public void setClip(int clip) {
		this.clip = clip;
	}

	public boolean isEmbeded() {
		return embeded;
	}

	public void setEmbeded(boolean embeded) {
		this.embeded = embeded;
	}
	public SVGElement getClipPath() {
		return clipPath;
	}
	public void setClipPath(SVGElement clipPath) {
		this.clipPath = clipPath;
	}
};