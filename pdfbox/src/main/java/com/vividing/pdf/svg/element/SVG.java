package com.vividing.pdf.svg.element;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Path2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.PDLineDashPattern;
import org.apache.pdfbox.pdmodel.graphics.state.PDGraphicsState;

import com.vividing.pdf.TextStyle;
import com.vividing.pdf.Util;
import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.svg.Box;
import com.vividing.pdf.svg.Table;
import com.vividing.pdf.tag.Tag;

//extends SVGGElement {
public class SVG extends Tag {
	
	private PDRectangle pageSize;
	private int pageNum;

	private int element_counter = 1;

	private VividingStripper stripper = null;
	
	public SVG(int page, VividingStripper stripper) {
		this.addTag("svg");
		this.pageNum = page;
		this.stripper = stripper;
	}

	public int getPageNum() {
		return pageNum;
	}

	// should add directly...., right??
	public List<SVGElement> elements = new ArrayList<SVGElement>();
	Box bound = null;

	public SVGElement svgElement = null;

	public SVGElement getElement() {
		return this.bound.m_svgGElement;
	}
	
	public List<SVGElement> getElements() {
		if (elements.size() == 1) {
			return ((SVGGElement) elements.get(0)).elements;
		}
		return elements;
	}

	public List<SVGElement> getElements1() {
		return this.bound.m_svgGElement.getElements();
	}
	
	//float delta = 40f;

	public void toPath(AffineTransform tm, GeneralPath p) {
		SVGPathElement e = (SVGPathElement) svgElement;
		
		e.toPath(tm, p);
		//debug = pageNum == 46;
//		if (pageNum == 46 ) {
//			
//			debug = true;
//		}
		
		if (debug) {
			Util.p(e);
		}

		if (e.bound.width() > VividingStripper.BOX_MIN_WIDTH && e.bound.height() >= VividingStripper.BOX_MIN_HEIGHT) {
			bound.add(e.bound);
		} else {
			if (debug) {
				Util.p(e.isRectangle());
			}
		}
//		else if (e.isRectangle()) {
//			bound.add(e.bound);
//		}
		
	}

	public void toPath(AffineTransform tm, Shape shape) {
		SVGPathElement e = (SVGPathElement) svgElement;
		e.toPath(tm, shape);
	}

	int clipIndex = 0;

	public SVGElement lastSVGElement() {
		if (elements.size() < 2) {
			return null;
		} else {
			int i = elements.size() - 2;
			while (i > 0) {
				SVGElement e = elements.get(i);
				if (e.getClip() == -1) {
					return e;
				}
				i--;
			}
		}
		return null;
	}

	public SVGElement safeSVGPath(PDGraphicsState pdGraphicsState) {
		return this.safeSVGElement(pdGraphicsState, "path");
	}

	public SVGElement safeSVGImg(PDGraphicsState pdGraphicsState) {
		return this.safeSVGElement(pdGraphicsState, "image");
	}

	public SVGElement safeSVGText(PDGraphicsState pdGraphicsState) {
		SVGTextElement e = (SVGTextElement) this.safeSVGElement(pdGraphicsState, "text");
		//texts.add(e);
		return e;
	}

	public SVGElement safeSVGG(PDGraphicsState pdGraphicsState) {
		return this.safeSVGElement(pdGraphicsState, "g");
		// texts.add(e);
		// return e;
	}

	private SVGElement safeSVGElement(PDGraphicsState pdGraphicsState, String tag) {
		if (svgElement != null) {
			return svgElement;
		}
		float lineWidth = 0;
		if (pdGraphicsState != null) {
			lineWidth = pdGraphicsState.getLineWidth();
		}
		// Util.p("tag=" + tag);
		if (tag.equals("path")) {
			svgElement = new SVGPathElement(pageNum);
		} else if (tag.equals("image")) {
			svgElement = new SVGImageElement(pageNum);
		} else if (tag.equals("text")) {
			svgElement = new SVGTextElement(pageNum);
		} else if (tag.equals("g")) {
			svgElement = new SVGGElement(pageNum);
		} else {
			System.err.println("Unsupported type " + tag);
		}
		if (!tag.equals("g")) {
			svgElement.id = element_counter;
			svgElement.getAttrs().put("id", String.valueOf(element_counter++));
		}
		elements.add(svgElement);
		if (lineWidth != 0 && lineWidth != 1) {
			svgElement.strokeWidth(lineWidth);
		}
		return svgElement;
	}

	public void clearStrokeandFill() {
		svgElement.fill(null);
		svgElement.stroke(null);
	}

	public void setClip() {
		svgElement.clip = ++clipIndex;
		SVGElement last = lastSVGElement();
		if (last != null) {
			last.putAttr("clip-path", String.format("url(#clip-%s)",
			svgElement.clip));
		 }
	}

	public void strokeOrFill(Graphics2D graphics2D, PDGraphicsState pdGraphicsState, Integer windingRule,
			boolean stroke, boolean fill) {
		AffineTransform tm = graphics2D.getTransform();
		// RenderingIntent renderIntent = pdGraphicsState.getRenderingIntent();
		// // probably ignorable at first (converts color maps)
		PDLineDashPattern dashPattern = pdGraphicsState.getLineDashPattern();
		try {
			float lineWidth = pdGraphicsState.getLineWidth();
			
			
			if (windingRule != null) {
				if (windingRule == Path2D.WIND_EVEN_ODD) {
					svgElement.putAttr("fill-rule", "evenodd");
				} else if (windingRule == Path2D.WIND_NON_ZERO) {
					svgElement.putAttr("fill-rule", "nonzero");
				}
			}

			if (fill) {
				String nonStrokingColor = Util.getCSSColor(pdGraphicsState.getNonStrokingColor());
				if (Util.notEmpty(nonStrokingColor)) {
					svgElement.fill(nonStrokingColor);
				}
			} else {
				svgElement.fill("none");
			}

			if (stroke) {
				String strokingColor = Util.getCSSColor(pdGraphicsState.getStrokingColor());
				svgElement.stroke(strokingColor);
				if (lineWidth != 1) {
					svgElement.putAttr("strke-width", lineWidth);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (dashPattern != null) {
			String s = getDashArray(dashPattern);
			if (s != null && s.length() != 0) {
				svgElement.putAttr("stroke-dasharray", s);
			}
		}

		SVGPathElement p = getAndFormatClipPath(tm, pdGraphicsState);
		if (p != null) {
			svgElement.setClipPath(p);
			svgElement.putAttr("clip-path", String.format("url(#clip-%s)", svgElement.getClipPath().getClip()));
		}
	}

	Map<String, SVGPathElement> clipPathMap = new HashMap<String, SVGPathElement>();

	private SVGPathElement getAndFormatClipPath(AffineTransform tm, PDGraphicsState pdGraphicsState) {

		Shape shape = pdGraphicsState.getCurrentClippingPath();
		SVGPathElement path = new SVGPathElement(this.pageNum);
		path.toPath(tm, shape);
		String key = path.path.toString();

		if (path.getBound().equalTo(this.bound)) { //Skip the page as clip
			return null;
		}

		if (clipPathMap.containsKey(key)) {
			return clipPathMap.get(key);
		}
		this.elements.add(0, path);
		clipPathMap.put(key, path);
		path.setClip(++clipIndex);

		return path;
	}

	private String getDashArray(PDLineDashPattern dashPattern) {
		float[] dashIntegerList = dashPattern.getDashArray();
		if (dashIntegerList == null || dashIntegerList.length == 0) {
			return null;
		}
		StringBuilder sb = new StringBuilder();
		for (float f : dashIntegerList) {
			if (sb.length() > 0) {
				sb.append(" ");
			}
			sb.append(f);
		}
		return sb.toString();
	}

	public List<Box> getBoxes() {
		return bound.children;
	}

	public void writeFile(VividingStripper stripper, boolean imageOnly) {
		StringBuilder body = new StringBuilder();
		StringBuilder def = new StringBuilder();

		StringBuilder style = new StringBuilder();
		//StringBuilder embededImg = new StringBuilder();
		if (!imageOnly) {
			for (TextStyle s : TextStyle.classMap.values()) {
				style.append('.').append(s.getId()).append("{").append(s.toSVGString()).append("}\n");
			}

			for (String fontName : TextStyle.pdFontMap.keySet()) {
				//AMIFont font = TextStyle.pdFontMap.get(fontName);
				style.append(" @font-face{font-family:").append(fontName).append(";");
				style.append("src: url(\"../fonts/").append(fontName).append(".ttf\") format(\"truetype\");}\r\n");
			}

			def.append("<style type=\"text/css\"><![CDATA[").append(style).append("]]></style>");
		}
		for (SVGElement e : elements) {

			e.writeFile(stripper, body, def, imageOnly);
		}

//		if (imageOnly) {
//			if (embededImg.length() > 0) {
//				body.append(embededImg);
//			} else {
//				return; // Empt, Do nothing....
//			}
//		}
		String fileName = stripper.getOutputDir() + "svg/" + (imageOnly ? "img-" : "") + pageNum + ".svg";
		try {
			// System.out.println("fileName = " + fileName);
			Util.toSvgFile(stripper, body, def, this.getPageSize().getUpperRightX(), this.getPageSize().getUpperRightY(), fileName);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	@Override
	public void write(VividingStripper stripper, StringBuilder sb) {
		sb.append(String.format(
				"<svg width=\"%s\" height=\"%s\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:svg=\"http://www.w3.org/2000/svg\">",
				bound.right, bound.bottom));
		for (SVGElement e : elements) {
			e.write(stripper, sb);
			sb.append("\n");
		}
		sb.append("</svg>");
	}

	@Override
	public void clear() {
	}

	public PDRectangle getPageSize() {
		return pageSize;
	}

	final float LIMIT = 66666f;
	public void setPageSize(PDRectangle pageSize) {
		this.pageSize = pageSize;

		float x = pageSize.getLowerLeftX(), y= pageSize.getLowerLeftY();
		float right =pageSize.getLowerLeftX() + pageSize.getWidth();
		//float bottom = pageSize.getLowerLeftY() + pageSize.getHeight();
		this.bound = new Box(x,y,
				pageSize.getLowerLeftX() + pageSize.getWidth(), pageSize.getLowerLeftY() + pageSize.getHeight(),
				pageNum, "#000");
		this.bound.name = "main";
		//topLevel
		
		List<Table> tables =  stripper.getTable(this.pageNum);
		
		for(Table table : tables) {
			Box b = new Box(table.x, table.y, table.right, table.bottom, pageNum, table.background);
			this.bound.add(b);
		}
		//this.bound.setPageGroup
		//Some of the stuff may be outside,???? 
		if (VividingStripper.LAYOUT == 2) {
			Box leftBox = new Box(-LIMIT,-LIMIT, (float)Math.ceil(right/2), LIMIT, pageNum, "#000");
			this.bound.add(leftBox);
			
			Box rightBox = new Box((float)Math.floor(right/2),y,LIMIT, LIMIT, pageNum, "#000");
			this.bound.add(rightBox);
		}
		
	}

	Map<Box, SVGGElement> groupMap = new HashMap<Box, SVGGElement>();

	Box belongToBox(List<Box> boxes, SVGPathElement p) {
		for(Box b: boxes) {
			if (b == null) {
				Util.err("What ???");
			}
			if (b.isBelongToBox(p)) {
				return b;
			}
		}
		return null;
	}
	
	protected List<SVGPathElement> pathElements() {
		List<SVGPathElement> t = new ArrayList<SVGPathElement>();
		for(SVGElement e : bound.m_svgGElement.elements) {
			if (e instanceof SVGPathElement) {
				t.add((SVGPathElement)e);
			}
		}
		return t;
	}
	private List<Box> moreBoxes() {
		
		List<SVGPathElement> paths = pathElements();
		if (paths.size() > 2) {
//			if (debug) {
//				Util.p(paths.size());
//			}
			List<Box> boxes = new ArrayList<>();
			
			for(SVGPathElement p:paths) {
				//Util.p(p);	
				Box b = belongToBox(boxes, p);
				if (b == null) {
					b = Util.getRectangle(p.path);
					
					if (b != null) {
						b.pageNum = this.pageNum;
						boxes.add(b);
					}
				} else {
					b.expendBox(p);
				}
			}
			
//			if (boxes.size() == 1 && boxes.get(0).isSame(this.box)) {
//				return null;
//			}
			return boxes;
			//Util.p("=============" + b);
		}
		return null;
	}
	public void grouping() {
		// Set up the Hierarchy and place holder
		//Each box will be a SVG g

		bound.grouping();
		// boolean report = false;
		bound.createG(this, null);

		List<SVGElement> toBeRemove = new ArrayList<SVGElement>();
		// List<SVGElement> toBeAdd = new ArrayList<SVGElement>();
		for (SVGElement e : elements) {
			if (e == bound.m_svgGElement) {
				continue;
			}
			

			Box b = bound.find(e);
			// if (b != null && b != bound) {
			if (b != null) {
				SVGGElement g = b.m_svgGElement;

				if (g != null) {
					g.getElements().add(e);
					toBeRemove.add(e);
				} else {
					System.err.print("NO GROUP for " + b);
				}
			} else {
				System.err.println("Out of bound, >>>> "+ e);
				bound.m_svgGElement.getElements().add(e);
				toBeRemove.add(e);
			}
		}

		for (SVGElement e : toBeRemove) {
			this.elements.remove(e);
		}
		if (this.pageNum == 47) {
			debug = true;
		}
		
		List<Box> boxes = moreBoxes();
		
		if (boxes != null) {
			
			for(Box b: boxes) {
				if(debug) {
					Util.p(b);
				
					Util.p("1 >>" + bound.m_svgGElement.elements.size());
				}
				b.createG(bound.m_svgGElement);
				if (debug) {
					Util.p("2 >>" +  bound.m_svgGElement.elements.size());
				}
				for(SVGElement e : bound.m_svgGElement.elements) {
					if (b.m_svgGElement != e && b.contains(e)) {
						b.m_svgGElement.elements.add(e);
					}
				} 
			}
			for(Box b: boxes) {
				for(SVGElement e : b.m_svgGElement.elements) {
					bound.m_svgGElement.elements.remove(e);
				}
			}
			if (debug) {
				Util.p("3 >>" + bound.m_svgGElement.elements.size());
			}
			for(Box b: boxes) {
				
				if (VividingStripper.TABLE_NO_LEFT_RIGHT_BORDER) {
					SVGPathElement path = new SVGPathElement(this.pageNum);
					path.x = b.x;
					path.y = b.y;
					path.right = b.x + 1;
					path.bottom = b.bottom;
					
					path.path = Util.toPath(path);
					b.m_svgGElement.elements.add(0, path);
					
					path = new SVGPathElement(this.pageNum);
					path.x = b.right;
					path.y = b.y;
					path.right = b.right + 1;
					path.bottom = b.bottom;
					path.path = Util.toPath(path);

					b.m_svgGElement.elements.add(path);
					//this.sort();
				}
				
				bound.children.add(b);
			}
			if (debug) {
				Util.p("4 >>" + bound.m_svgGElement.elements.size());
			}
		}
		bound.sort();
		
		//Now, Check Table ????
	}

	public void paragraphing() {
//		Util.p(pageNum);
//		if (pageNum == 31) {
//			debug = true;
//		}
		bound.paragraphing();
	}
	
//	public void sorting () {
//		bound.sorting();
//	}
	public void checkTable() {
		bound.checkTable();
	}
}
