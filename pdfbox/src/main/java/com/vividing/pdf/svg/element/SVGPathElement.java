package com.vividing.pdf.svg.element;

import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;

import com.vividing.pdf.Util;
import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.svg.Box;

public class SVGPathElement extends SVGElement{
	
	//static int BOX_HEIGHT = 30;
	public SVGPathElement(int pageNum) {
		super("path", pageNum);
	}
	
	Box bound = new Box();

	public Box getBound() {
		return bound;
	}

	public StringBuilder path = new StringBuilder();
	
	@Override
	public String toString() {
		return String.format("<path x=%s, y=%s, right=%s, bottom=%s, p=%s>", x, y, right, bottom, path);
	}
	
	@Override
	public void write(VividingStripper stripper, StringBuilder out) {
		super.write(stripper, out);	
		if (path != null&& path.length() > 0) {
			out.append(" d=\"").append(path);
			if (out.charAt(out.length() - 1) != 'Z') {
				out.append('Z');
			}
			out.append("\"");
		}
		out.append(" />\n");
	}
	
	public void toPath(AffineTransform at, GeneralPath p) {
		this.path= Util.toPath(p.getPathIterator(at));
		Rectangle rect = p.getBounds();
		setBounds(at, rect);
	}
	
	void setBounds(AffineTransform at, Rectangle rect) {
		this.bound = Util.getBounds(at, rect, this, pageNum);
		
		if (isRectangle()) {
			x = this.box.x;
			y = this.box.y;
			right = this.box.x + this.box.width();
			bottom = this.box.y + this.box.height();
		} else { //Come back here later, need find min x, y 
			if (this.bound != null) {
				x = this.bound.x;
				y = this.bound.y;
				right = this.bound.x + this.bound.width();
				bottom = this.bound.y + this.bound.height();
			}
		}
	}
	
	public void toPath(AffineTransform at, Shape shape) {
		this.path= Util.toPath(shape.getPathIterator(at));
		Rectangle rect = shape.getBounds();
		setBounds(at, rect);
	}
	
	
	Box box = null;
	
	public boolean isRectangle() {
		if (box == null) {
			box = Util.getRectangle(path);
			//Util.p(box);
		}
		return (box != null);		
	}
	
	public boolean isLine() {
		return isHorizontalLine();
	}
	
	public boolean isHorizontalLine() {
		if (isRectangle()) {
			return (box.height() < 5 && box.width() >= 10);
		}
		return false;
	}
	
	public boolean isVeticalLine() {
		if (isRectangle()) {
			return (box.height() >= 10 && box.width() < 5);
		}
		return false;
	}
	public boolean isBox() {
		if (isRectangle()) {
			return (box.height() >= VividingStripper.BOX_MIN_HEIGHT);
		}
		return false;
	}
};