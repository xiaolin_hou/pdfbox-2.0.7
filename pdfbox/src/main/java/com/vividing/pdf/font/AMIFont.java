//package com.vividing.pdf.font;
//
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Set;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.apache.pdfbox.cos.COSArray;
//import org.apache.pdfbox.cos.COSBase;
//import org.apache.pdfbox.cos.COSDictionary;
//import org.apache.pdfbox.cos.COSInteger;
//import org.apache.pdfbox.cos.COSName;
//import org.apache.pdfbox.pdmodel.font.PDFont;
//import org.apache.pdfbox.pdmodel.font.PDFontDescriptor;
//import org.apache.pdfbox.pdmodel.font.PDFontFactory;
//import org.apache.pdfbox.pdmodel.font.PDType0Font;
///** wrapper for PDType1Font. is meant to manage the badFontnames, other
// * fontTypes, etc and try to convert them to a standard approach. 
// * Splits all Italic, etc. to attributes of AMIFont. 
// * May ultimately be unneccessary
// * 
// * @author pm286
// *
// */
//public class AMIFont {
//
//	protected static final Log LOG = LogFactory.getLog(AMIFont.class);
//	private static final String SYMBOL = "Symbol";
//	public static final String N_NAME = "Name";
//	public static final String N_BASE_FONT = "BaseFont";
//
//	
//	// order may matter - longest/unique strings first
//	private static final String[] BOLD_SUFFIXES = new String[]{
//		"-SemiBold",
//		"SemiBold",
//		"-Bold", 
//		".Bold", 
//		".B", 
//		"-B", 
//		"Bold",
//		};
//	private static final String[] ITALIC_SUFFIXES = new String[]{
//		"-Italic", 
//		".Italic", 
//		".I", 
//		"-I", 
//		"Italic",
//		"-Oblique", 
//		".Oblique", 
//		"Oblique",
//		"-Inclined", 
//		".Inclined", 
//		};
//	
//	public static final String MONOTYPE_SUFFIX = "MT";  // rubbish - means MathType!
//	public static final String POSTSCRIPT_SUFFIX = "PS";
//	public static final String ENCODING = "Encoding";
//	static Pattern LEADER_PATTERN = Pattern.compile("^([A-Z]{6})\\+(.*)$");
//	
////	private Boolean isBold = null;
////	private Boolean isItalic = null;
////	private Boolean isSymbol = null;
//	
//	private String fontFamilyName;
//	private String fontName;
//	
//	private PDFont pdFont;
//	private PDFontDescriptor fontDescriptor;
//	private String fontType;
//	
//	private String finalSuffix;
////	private Encoding encoding;
////	private String fontEncoding;
////	private String baseFont;
//	private Map<String, String> pathStringByCharnameMap;
//	
//	private COSDictionary dictionary;
//	private COSArray dictionaryArray;
//	private COSName dictionaryName;
//	private COSDictionary dictionaryDictionary;
//	private COSInteger dictionaryInteger;
//
//	private PDFont firstDescendantFont;
//	
////	static {
////		String[] standard14Names = PDType1Font.getStandard14Names();
////		for (String standard14Name : standard14Names) {
////			System.out.println(standard14Name);
////		}
////	}
//
//	/**
//        addFontMapping("Times-Roman","TimesNewRoman");
//        addFontMapping("Times-Italic","TimesNewRoman,Italic");
//        addFontMapping("Times-Italic","TimesNewRoman,Italic");
//        addFontMapping("Times-ItalicItalic","TimesNewRoman,Italic,Italic");
//        addFontMapping("Helvetica-Oblique","Helvetica,Italic");
//        addFontMapping("Helvetica-ItalicOblique","Helvetica,Italic,Italic");
//        addFontMapping("Courier-Oblique","Courier,Italic");
//        addFontMapping("Courier-ItalicOblique","Courier,Italic,Italic");
//        
//and
//    // TODO move the Map to PDType1Font as these are the 14 Standard fonts
//    // which are definitely Type 1 fonts
//    private static Map<String, FontMetric> getAdobeFontMetrics()
//    {
//        Map<String, FontMetric> metrics = new HashMap<String, FontMetric>();
//        addAdobeFontMetric( metrics, "Courier-Italic" );
//        addAdobeFontMetric( metrics, "Courier-ItalicOblique" );
//        addAdobeFontMetric( metrics, "Courier" );
//        addAdobeFontMetric( metrics, "Courier-Oblique" );
//        addAdobeFontMetric( metrics, "Helvetica" );
//        addAdobeFontMetric( metrics, "Helvetica-Italic" );
//        addAdobeFontMetric( metrics, "Helvetica-ItalicOblique" );
//        addAdobeFontMetric( metrics, "Helvetica-Oblique" );
//        addAdobeFontMetric( metrics, "Symbol" );
//        addAdobeFontMetric( metrics, "Times-Italic" );
//        addAdobeFontMetric( metrics, "Times-ItalicItalic" );
//        addAdobeFontMetric( metrics, "Times-Italic" );
//        addAdobeFontMetric( metrics, "Times-Roman" );
//        addAdobeFontMetric( metrics, "ZapfDingbats" );
//        return metrics;
//    }
//        
//	 */
//	
//	/** try to create font from name
//	 * usually accessed through createFontFromName()
//	 * @param fontName
//	 */
////	private AMIFont(String fontName) {
////		this();
////		fontFamilyName = null;
////		this.fontName = fontName;
////		stripFontNameComponents();
////	}
//
//	/** create font from family and key attributes
//	 * currently used when compiling an external table
//	 */
//	public AMIFont(String fontFamilyName,  String type) {
//		this.fontFamilyName = fontFamilyName;
//		this.fontType = type;
//	}
//
//	/** create font from family and key attributes
//	 * currently used when compiling an external table
//	 */
//	public AMIFont(String fontFamilyName,  String type, COSDictionary dictionary) {
//		this(fontFamilyName, type);
//		this.dictionary = dictionary;
//		analyzeDictionary();
//	}
//
//	private void analyzeDictionary() {
//		Set<COSName> keySet = dictionary.keySet();
//		for (COSName key : keySet) {
//			COSBase object = dictionary.getDictionaryObject(key);
//			if (object instanceof COSArray) {
//				dictionaryArray = (COSArray) object;
//				for (int i = 0; i < dictionaryArray.size(); i++) {
//					LOG.trace(dictionaryArray.getName(i)+": "+dictionaryArray.getObject(i));
//				}
//			} else if (object instanceof COSName) {
//				this.dictionaryName = (COSName) object;
//			} else if (object instanceof COSDictionary) {
//				this.dictionaryDictionary = (COSDictionary) object;
//			} else if (object instanceof COSInteger) {
//				this.dictionaryInteger = (COSInteger) object;
//			} else {
//				LOG.debug(object.getClass());
//			}
//		}
//	}
//
//	public AMIFont(PDFont pdFont) {
//		fontDescriptor = getFontDescriptorOrDescendantFontDescriptor(pdFont);
//		this.firstDescendantFont = getFirstDescendantFont(pdFont);
//		//this.baseFont = pdFont.getBaseFont();
////		this.fontType = pdFont.getClass().getSimpleName();
//		this.fontType = pdFont.getType();
//		
////		this.encoding = pdFont.getFontEncoding();
////		if (encoding == null && pdFont instanceof PDType0Font) {
////			pdFont = firstDescendantFont;
////			encoding = pdFont.getFontEncoding();
////		}
//		//fontEncoding = (encoding == null) ? null : encoding.getClass().getSimpleName();
//		this.pdFont = pdFont;
//		fontFamilyName = null;
//		if (fontDescriptor != null) {
//			fontName = fontDescriptor.getFontName();
////			fontFamilySave = fontDescriptor.getFontFamily();
//			
//			stripFontNameComponents();
//			if (fontFamilyName == null) {
//				fontFamilyName = fontName;
//			}
//			LOG.trace("FFFFF "+fontFamilyName);
////			// take fontDescriptor over name extraction
////			isBold = fontDescriptor.isForceBold() ? true : isBold;
////			if (isBold) {
////				LOG.trace("bold from Font Descriptor");
////			}
////			isItalic = fontDescriptor.isItalic() ? true : isItalic;
////			if (isItalic) {
////				LOG.trace("italic from Font Descriptor");
////			}
////			isSymbol = fontDescriptor.isSymbolic();
////			if (isSymbol) {
////				LOG.trace("symbol from Font Descriptor");
////			}
//			
//			fontName = fontDescriptor.getFontName();
//			//LOG.trace("name="+fontName+" fam="+fontFamilyName+" type="+pdFont.getSubType()+" bold="+isForceBold() +" it="+isItalic()+" face="+finalSuffix+" sym="+isSymbolic()+ " enc="+(encoding == null ? "null" : encoding.getClass().getSimpleName()));
//		} else {
//			//fontName = baseFont;
//			stripFontNameComponents();
//			if (fontFamilyName == null) {
//				fontFamilyName = fontName;
//			}
//			LOG.debug(this.toString());
//			//LOG.warn("font had no descriptor: "+baseFont+" / "+fontFamilyName);
//		}
//	}
//
//		
//	private void stripFontNameComponents() {
//		processInitialPrefix();
//		processStandardFamilies();
////		processIsBoldInName();
////		processIsItalicInName();
//		processFinalSuffix();
//	}
//
//	private void processInitialPrefix() {
//		String initialPrefix = null;
//		if (fontName != null){
//			Matcher matcher = LEADER_PATTERN.matcher(fontName);
//			if (matcher.matches()) {
//				initialPrefix = matcher.group(1);
//			}
//		}
//	}
//
//	private void processFinalSuffix() {
//		finalSuffix = null;
//		if (fontName != null) {
//			if (fontName.endsWith(MONOTYPE_SUFFIX)) {
//				finalSuffix = MONOTYPE_SUFFIX;
//			} else if (fontName.endsWith(POSTSCRIPT_SUFFIX)) {
//				finalSuffix = POSTSCRIPT_SUFFIX;
//			}
//		}
//	}
//
//	private void processStandardFamilies() {
//		processAsFamily("TimesNewRoman");
//		if (fontFamilyName != null) return;
//		processAsFamily("Courier");
//		if (fontFamilyName != null) return;
//		processAsFamily("Helvetica");
//		if (fontFamilyName != null) return;
//		processAsFamily(SYMBOL);
//		if (fontFamilyName != null) return;
//		processAsFamily("ZapfDingbats");
//	}
//	
////	private void processIsBoldInName() {
////		// syntactic variants 
////		boolean isBoldInName = false;
////		for (String bString : BOLD_SUFFIXES) {
////			isBoldInName = isIncluded(bString);
////			if (isBoldInName) break;
////		}
////		isBold = (isBold != null) ? isBold : isBoldInName;
////	}
////	
////	private void processIsItalicInName() {
////		// syntactic variants 
////		boolean isItalicInName = false;
////		for (String iString : ITALIC_SUFFIXES) {
////			isItalicInName = isIncluded(iString);
////			if (isItalicInName) break;
////		}
////		isItalic = (isItalic != null) ? isItalic : isItalicInName;
////	}
////	
////	private boolean isOK() {
////		return 
////		isBold != null &&
////		isItalic != null &&
////		isSymbol != null &&
////		fontFamilyName != null &&
////		fontName != null;
////	}
//
//	private void processAsFamily(String standardFamilyName) {
//		if (fontName != null) {
//			String fontNameLower = fontName.toLowerCase();
//			int currentIndex = fontNameLower.indexOf(standardFamilyName.toLowerCase());
//			if (currentIndex != -1) {
//				removeFromFontName(standardFamilyName, currentIndex);
//				fontFamilyName = standardFamilyName;
//			}
//		}
//	}
//
//	private Boolean isIncluded(String suffix) {
//		boolean isIncluded = false;
//		if (fontName != null) {
//			String fontNameLower = fontName.toLowerCase();
//			int currentIndex = fontNameLower.indexOf(suffix.toLowerCase());
//			if (currentIndex != -1) {
//				isIncluded = true;
//			}
//		}
//		return isIncluded;
//	}
//
//	private void removeFromFontName(String subName, int idx) {
//		if (fontName.substring(idx, idx+subName.length()).equalsIgnoreCase(subName)) {
//			fontName = fontName.substring(0, idx)+fontName.substring(idx+subName.length());
//		}
//	}
//
//	
//
////	public DictionaryEncoding getDictionaryEncoding() {
////		return (encoding instanceof DictionaryEncoding) ? (DictionaryEncoding) encoding : null;
////	}
//
////	public String getFontWeight() {
////		return (isBold != null && isBold) ? AMIFontManager.BOLD : null;
////	}
//
//	public String getFontName() {
//		return fontName;
//	}
//	
//	public String getFontFamilyName() {
//		return fontFamilyName;
//	}
//	
//	public String getFontType() {
//		return fontType;
//	}
//	
////	public boolean isSymbol() {
////		return (isSymbol == null) ? false : isSymbol;
////	}
//	
////	public String getBaseFont() {
////		return baseFont;
////	}
//
////	public Boolean isItalic() {
////		return isItalic;
////	}
//	
////	public Boolean isBold() {
////		return ;isBold
////	}
//
//	public Map<String, String> getPathStringByCharnameMap() {
//		ensurePathStringByCharnameMap();
//		return pathStringByCharnameMap;
//	}
//
//	private void ensurePathStringByCharnameMap() {
//		if (this.pathStringByCharnameMap == null) {
//			pathStringByCharnameMap = new HashMap<String, String>();
//		}
//	}
//	
//	public static String getFontName(COSDictionary dict) {
//		String fontName = null;
//		String baseFontS = null;
//		for (COSName key : dict.keySet()) {
//			String keyName = key.getName();
//			if (keyName == null) {
//				LOG.error("Null key");
//				continue;
//			} else if (!(key instanceof COSName)) {
//				LOG.error("key not COSName");
//				continue;
//			}
//			String cosNameName = null;
//			COSBase cosBase = dict.getDictionaryObject(key);
//			if (cosBase instanceof COSName) {
//				COSName cosName = (COSName) cosBase;
//				cosNameName = cosName.getName();
//				LOG.trace("Name:"+cosNameName);
//			} else if (cosBase instanceof COSInteger) {
//				COSInteger cosInteger = (COSInteger) cosBase;
//				LOG.trace("Integer: "+cosInteger.intValue());
//			} else if (cosBase instanceof COSArray) {
//				COSArray cosArray = (COSArray) cosBase;
//				LOG.trace("Array: "+cosArray.size()+" / "+cosArray);
//			} else if (cosBase instanceof COSDictionary) {
//				COSDictionary cosDictionary = (COSDictionary) cosBase;
//				LOG.trace("Dictionary: "+cosDictionary);
//			} else{
//				LOG.error("COS "+cosBase);
//			}
//			if (cosNameName != null && keyName.equals(N_NAME)) {
//				fontName = cosNameName;
//			} else if(cosNameName != null && keyName.equals(N_BASE_FONT)) {
//				baseFontS = cosNameName;
//			}
//		}
//		if (fontName == null) {
//			fontName = baseFontS;
//		}
//		return fontName;
//	}
//
//	public static PDFontDescriptor getDescendantFontDescriptor(PDFont pdFont) {
//		PDFontDescriptor fd = null;
//		PDFont descendantFont = getFirstDescendantFont(pdFont);
//		fd = (descendantFont == null) ? null : descendantFont.getFontDescriptor();
//		LOG.trace("fd ("+fd.getFontName()+") "+fd);
//		return fd;
//	}
//
//	public static PDFont getFirstDescendantFont(PDFont pdFont) {
//		COSDictionary dict = (COSDictionary) pdFont.getCOSObject();
//		COSArray array = dict == null ? null : (COSArray) dict.getDictionaryObject(COSName.DESCENDANT_FONTS);
//		PDFont descendantFont = null;
//		try {
//			descendantFont = array == null ? null : PDFontFactory.createFont((COSDictionary) array.getObject(0));
//		} catch (IOException e) {
//			LOG.error("****************** Can't create descendant font! for "+pdFont);
//		}
//		return descendantFont;
//	}
//
//	public static PDFontDescriptor getFontDescriptorOrDescendantFontDescriptor(PDFont pdFont) {
//		PDFontDescriptor fd = pdFont.getFontDescriptor();
////		getToUnicode(pdFont);
//		if (fd == null && pdFont instanceof PDType0Font) {
//			fd = AMIFont.getDescendantFontDescriptor(pdFont);
//		}
//		return fd;
//	}
//
////	public COSDictionary getToUnicode() {
////		COSDictionary cosDictionary = (COSDictionary) ((PDSimpleFont) pdFont).getToUnicode();
////		return cosDictionary;
////	}
//
//	public String toString() {
//
//		StringBuilder sb = new StringBuilder();
//		sb.append("isBold: ");
//		sb.append(isForceBold());
//		sb.append("; isItalic: ");
//		sb.append(isItalic());
//		sb.append("; isSymbol: ");
//		sb.append(isSymbolic());
//		sb.append("; fontFamilyName: ");
//		sb.append(fontFamilyName);
//		sb.append("; fontName: ");
//		sb.append(fontName);
//		sb.append("; pdFont: ");
//		sb.append(pdFont);
//		sb.append("; fontDescriptor: ");
//		sb.append(fontDescriptor);
//		sb.append("; fontType: ");
//		sb.append(fontType);
////		sb.append("; encoding: ");
////		sb.append(encoding);
////		sb.append("; fontEncoding: ");
////		sb.append(fontEncoding);
////		sb.append("; baseFont: ");
////		sb.append(baseFont);
//		sb.append("\n");
//		sb.append("; dictionary: ");
//		sb.append(dictionary);
//		sb.append("; dictionaryName: ");
//		sb.append(dictionaryName);
//		sb.append("; dictionaryArray: ");
//		sb.append(dictionaryArray);
//		sb.append("; dictionaryDictionary: ");
//		sb.append(dictionaryDictionary);
//		sb.append("; dictionaryInteger: ");
//		sb.append(dictionaryInteger);
//		sb.append("\n");
//		sb.append("; isFixedPitch(): ");
//		sb.append(isFixedPitch());
//		sb.append("\n");
//		sb.append("; isHeuristicBold(): ");
//		sb.append(isHeuristicBold());
//		sb.append("; isHeuristicFixedPitch(): ");
//		sb.append(isHeuristicFixedPitch());
//		
//		return sb.toString();
//	}
//
//	public void setFontName(String fontName) {
//		this.fontName = fontName;
//	}
//
//	public PDFont getPDFont() {
//		return pdFont;
//	}
//
//	public PDFontDescriptor getFontDescriptor() {
//		return fontDescriptor;
//	}
//
//	/** delegates from dictionary
//	 */
//	public COSDictionary getDictionaryDictionary() {
//		return dictionaryDictionary;
//	}
//
//	public COSInteger getDictionaryInteger() {
//		return dictionaryInteger;
//	}
//
//	public COSName getDictionaryName() {
//		return dictionaryName;
//	}
//	/*
//	public Float getFontWidth(byte[] c, int offset, int length)
//			throws IOException {
//		return pdFont == null ? null : pdFont.getFontWidth(c, offset, length);
//	}
//
//	public Float getFontHeight(byte[] c, int offset, int length)
//			throws IOException {
//		return pdFont == null ? null : pdFont.getFontHeight(c, offset, length);
//	}
//
//	public Float getStringWidth(String string) throws IOException {
//		return pdFont == null ? null : pdFont.getStringWidth(string);
//	}
//
//	public Float getAverageFontWidth() throws IOException {
//		return pdFont == null ? null : pdFont.getAverageFontWidth();
//	}
//
//	public String encode(byte[] c, int offset, int length) throws IOException {
//		return pdFont == null ? null : pdFont.encode(c, offset, length);
//	}
//
//	public Integer encodeToCID(byte[] c, int offset, int length) throws IOException {
//		return pdFont == null ? null : pdFont.encodeToCID(c, offset, length);
//	}
//
//	public String getSubType() {
//		return pdFont == null ? null : pdFont.getSubType();
//	}
//
//	public List<Float> getWidths() {
//		return pdFont == null ? null : pdFont.getWidths();
//	}
//
//	public PDMatrix getFontMatrix() {
//		return pdFont == null ? null : pdFont.getFontMatrix();
//	}
//
//	public PDRectangle getFontBoundingBox() throws IOException {
//		PDRectangle pdRect = null;
//		pdRect = fontDescriptor == null ? null : fontDescriptor.getFontBoundingBox();
//		return pdRect != null ? pdRect : ((pdFont == null) ? null : pdFont.getFontBoundingBox());
//	}
//
//	public Float getFontWidth(int charCode) {
//		return pdFont == null ? null : pdFont.getFontWidth(charCode);
//	}
//*/
//	/** delegates from fontDescriptor */
//	public String getFontStretch() {
//		return fontDescriptor == null ? null : fontDescriptor.getFontStretch();
//	}
//
//	public Float getFontWeightFloat() {
//		return fontDescriptor == null ? null : fontDescriptor.getFontWeight();
//	}
//
//	public String getFontFamilyString() {
//		return fontDescriptor == null ? null : fontDescriptor.getFontFamily();
//	}
//
//	public Integer getFlags() {
//		return fontDescriptor == null ? null : fontDescriptor.getFlags();
//	}
//
//	public Boolean isFixedPitch() {
//		return fontDescriptor == null ? null : fontDescriptor.isFixedPitch();
//	}
//
//	public Boolean isSerif() {
//		return fontDescriptor == null ? null : fontDescriptor.isSerif();
//	}
//
//	public Boolean isSymbolic() {
//		return fontDescriptor == null ? null : fontDescriptor.isSymbolic();
//	}
//
//	public Boolean isScript() {
//		return fontDescriptor == null ? null : fontDescriptor.isScript();
//	}
//
//	public Boolean isNonSymbolic() {
//		return fontDescriptor == null ? null : fontDescriptor.isNonSymbolic();
//	}
//
//	public Boolean isItalic() {
//		return fontDescriptor == null ? null : fontDescriptor.isItalic();
//	}
//
//	public Boolean isAllCap() {
//		return fontDescriptor == null ? null : fontDescriptor.isAllCap();
//	}
//
//	public Boolean isSmallCap() {
//		return fontDescriptor == null ? null : fontDescriptor.isSmallCap();
//	}
//
//	public Boolean isForceBold() {
//		return fontDescriptor == null ? null : fontDescriptor.isForceBold();
//	}
//
//	public Float getItalicAngle() {
//		return fontDescriptor == null ? null : fontDescriptor.getItalicAngle();
//	}
//
//	public Float getAscent() {
//		return fontDescriptor == null ? null : fontDescriptor.getAscent();
//	}
//
//	public Float getDescent() {
//		return fontDescriptor == null ? null : fontDescriptor.getDescent();
//	}
//
//	public Float getLeading() {
//		return fontDescriptor == null ? null : fontDescriptor.getLeading();
//	}
//
//	public Float getCapHeight() {
//		return fontDescriptor == null ? null : fontDescriptor.getCapHeight();
//	}
//
//	public Float getXHeight() {
//		return fontDescriptor == null ? null : fontDescriptor.getXHeight();
//	}
//
//	public Float getStemV() {
//		return fontDescriptor == null ? null : fontDescriptor.getStemV();
//	}
//
//	public Float getStemH() {
//		return fontDescriptor == null ? null : fontDescriptor.getStemH();
//	}
//
//	public Float getAverageWidth() throws IOException {
//		return fontDescriptor == null ? null : fontDescriptor.getAverageWidth();
//	}
//
//	public Float getMaxWidth() {
//		return fontDescriptor == null ? null : fontDescriptor.getMaxWidth();
//	}
//
//	public String getCharSet() {
//		return fontDescriptor == null ? null : fontDescriptor.getCharSet();
//	}
//
//	public Float getMissingWidth() {
//		return fontDescriptor == null ? null : fontDescriptor.getMissingWidth();
//	}
//
////	public AMIFontFamily getAMIFontFamily() {
////		String ff = getFontFamilyString();
////		return ff == null ? null : new AMIFontFamily(ff);
////	}
//
//	/** guesses bold from name
//	 * 
//	 * @return
//	 */
//	public boolean isHeuristicBold() {
//		boolean bold = fontName.toLowerCase().contains("bold") || fontName.toLowerCase().contains(".b");
//		return bold;
//	}
//
//	/** guesses italic from name
//	 * 
//	 * @return
//	 */
//	public boolean isHeuristicItalic() {
//		boolean bold = fontName.toLowerCase().contains("ital") || fontName.toLowerCase().contains(".i");
//		return bold;
//	}
//
//	/** guesses bold from name
//	 * 
//	 * @return
//	 */
//	public boolean isHeuristicFixedPitch() {
//		boolean fixed = fontFamilyName.toLowerCase().contains("cmtt") || fontName.toLowerCase().contains("cmtt") ;
//		return fixed;
//	}
//
//}
