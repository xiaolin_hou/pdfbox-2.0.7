//package com.vividing.pdf.font;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.apache.pdfbox.cos.COSArray;
//import org.apache.pdfbox.cos.COSDictionary;
//import org.apache.pdfbox.cos.COSName;
//import org.apache.pdfbox.pdmodel.font.PDFont;
//import org.apache.pdfbox.pdmodel.font.PDFontDescriptor;
//import org.apache.pdfbox.pdmodel.font.PDTrueTypeFont;
//import org.apache.pdfbox.pdmodel.font.PDType0Font;
//import org.apache.pdfbox.pdmodel.font.PDType1Font;
//import org.apache.pdfbox.pdmodel.font.PDType3Font;
//
//public class AMIFontManager {
//
//	protected static final Log LOG = LogFactory.getLog(AMIFontManager.class);
//	
//	static private AMIFontManager m_AMIFontManager = null;
//	
//	public static AMIFontManager instance() {
//		if (m_AMIFontManager == null) {
//			m_AMIFontManager = new AMIFontManager();
//		}
//		
//		return m_AMIFontManager;
//	}
//	private String getFontName(PDFont pdFont) {
//		String fontName;
//		AMIFont amiFont;
//		PDFontDescriptor fd = AMIFont.getFontDescriptorOrDescendantFontDescriptor(pdFont);
//		if (fd == null) {
////			if (nullFontDescriptorReport) {
////				LOG.error("****************** Null Font Descriptor : "+pdFont+"\n       FURTHER ERRORS HIDDEN");
////				nullFontDescriptorReport = false;
////			}
//		}
//		if (fd == null) {
//			amiFont = this.lookupOrCreateFont(0, (COSDictionary) pdFont.getCOSObject());
//			fontName = amiFont.getFontName();
//			if (fontName == null) {
//				throw new RuntimeException("No currentFontName");
//			}
//		} else {
//			fontName = fd.getFontName();
//		}
//		return fontName;
//	}
//	
//	private Map<String, AMIFont> amiFontByFontNameMap = new HashMap<>();
//	
//	public AMIFont getAmiFontByFontName(String fontName) {
//		return amiFontByFontNameMap.get(fontName);
//	}
//	private AMIFont lookupOrCreateFont(int level, COSDictionary dict) {
//		/**
//Type = COSName{Font}
//Subtype = COSName{Type1}
//BaseFont = COSName{Times-Roman}
//Name = COSName{arXivStAmP}		
//LastChar = COSInt{32}
//Widths = COSArray{[COSInt{19}]}
//FirstChar = COSInt{32}
//FontMatrix = COSArray{[COSFloat{0.0121}, COSInt{0}, COSInt{0}, COSFloat{-0.0121}, COSInt{0}, COSInt{0}]}
//ToUnicode = COSDictionary{(COSName{Length}:COSInt{212}) (COSName{Filter}:COSName{FlateDecode}) }
//FontBBox = COSArray{[COSInt{0}, COSInt{0}, COSInt{1}, COSInt{1}]}
//Resources = COSDictionary{(COSName{ProcSet}:COSArray{[COSName{PDF}, COSName{ImageB}]}) }
//Encoding = COSDictionary{(COSName{Differences}:COSArray{[COSInt{32}, COSName{space}]}) (COSName{Type}:COSName{Encoding}) }
//CharProcs = COSDictionary{(COSName{space}:COSDictionary{(COSName{Length}:COSInt{67}) (COSName{Filter}:COSName{FlateDecode}) }) }*/
//		
//		AMIFont amiFont = null;
//		String fontName = AMIFont.getFontName(dict);
//		
//		String typeS = null;
//		amiFont = getAmiFontByFontName(fontName);
//		if (amiFont == null) {
//			// some confusion here between fontName and fontFamilyName
//			amiFont = new AMIFont(fontName, typeS, dict);
//			amiFont.setFontName(fontName);
//			amiFontByFontNameMap.put(fontName, amiFont);
//	
//			String indent = "";
//			for (int i = 0; i < level; i++) {
//				indent += " ";
//			}
//	
//			LOG.debug(String.format("%s****************** level %d font dict:",
//					indent, level));
//	
//			level++;
//			indent += "    ";
//	
//			for (COSName key : dict.keySet()) {
//				String keyName = key.getName();
//				Object object = dict.getDictionaryObject(key);
//				LOG.debug(String.format("%s****************** %s = %s", indent,
//						keyName, object));
//			}
//	
//			COSArray array = (COSArray) dict
//					.getDictionaryObject(COSName.DESCENDANT_FONTS);
//			if (array != null) {
//				LOG.debug(String.format(
//						"%s****************** descendant fonts (%d):", indent,
//						array.size()));
//				amiFont = lookupOrCreateFont(level, (COSDictionary) array.getObject(0));
//			}
//		}
//		return amiFont;
//	}
//	
//	public AMIFont getAmiFontByFont(PDFont pdFont) {
//		//ensureAMIFontMaps();
//		String fontName = null;
//		AMIFont amiFont = null;
//		fontName = getFontName(pdFont);
//		if (fontName == null) {
//			throw new RuntimeException("No currentFontName");
//		}
//		amiFont = amiFontByFontNameMap.get(fontName);
//		if (amiFont == null) {
//			if (pdFont instanceof PDType1Font ||
//				pdFont instanceof PDTrueTypeFont || 
//				pdFont instanceof PDType0Font ||
//				pdFont instanceof PDType3Font) {
//				amiFont = new AMIFont(pdFont);
//				amiFontByFontNameMap.put(fontName, amiFont);
//				String fontFamilyName = amiFont.getFontFamilyName();
////				amiFont.setNonStandardFontFamily(this.getFontFamilyByFamilyName(fontFamilyName));
////				recordExistingOrAddNewFontFamily(fontFamilyName, amiFont);
//			} else {
//				throw new RuntimeException("Cannot find font type: "+pdFont+" / "+pdFont.getSubType()+", ");
//			}
//		}
//		return amiFont;
//	}
//
//}
