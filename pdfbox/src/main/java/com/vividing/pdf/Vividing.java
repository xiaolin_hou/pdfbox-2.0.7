package com.vividing.pdf;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDDocumentNameDictionary;
import org.apache.pdfbox.pdmodel.PDEmbeddedFilesNameTreeNode;
import org.apache.pdfbox.pdmodel.common.filespecification.PDComplexFileSpecification;
import org.apache.pdfbox.pdmodel.common.filespecification.PDEmbeddedFile;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;

public final class Vividing {
    private static final String PASSWORD = "-password";
    private static final String DEBUG = "-debug";
    private static final String OUTPUTDIR = "-outputDir";
    private static final String OPENSTACK = "-openStack";
    private static final String FEEDBOOKS = "-feedBooks";
    
    private static final String PROPERTIES = "-properties";
    
    private boolean debug = true;
    private static final String VERSION = "Vividing PDF to HTML/SVG Converter 1.0.20 (Build at Sat Feb  3 11:55:02 EST 2018)";
    private boolean openstack = false;
    private boolean feedbooks = false;

    /**
     * Infamous main method.
     *
     * @param args Command line arguments, should be one and a reference to a file.
     *
     * @throws IOException if there is an error reading the document or extracting the text.
     */
    public static void main( String[] args ) throws IOException
    {
        // suppress the Dock icon on OS X
        System.setProperty("apple.awt.UIElement", "true");
        Vividing extractor = new Vividing();
        extractor.startExtraction(args);
    }
    /**
     * Starts the text extraction.
     *  
     * @param args the commandline arguments.
     * @throws IOException if there is an error reading the document or extracting the text.
     */
    public void startExtraction( String[] args ) throws IOException {
        String password = "";
        String pdfFile = null;
        //String outputFile = null;
        // Defaults to text files
        String outputDir = null;
        String properties_file_name = "config.properties";
        for( int i=0; i<args.length; i++ ) {
        	//Util.p(args[i]);
            if( args[i].equals( PASSWORD ) )
            {
                i++;
                if( i >= args.length )
                {
                    usage();
                }
                password = args[i];
            }
            
            else if( args[i].equals( PROPERTIES ) ) {
                i++;
                if( i >= args.length )
                {
                    usage();
                }
                properties_file_name = args[i];
            }             
            else if( args[i].equals( OUTPUTDIR ) )
            {
                i++;
                if( i >= args.length )
                {
                    usage();
                }
                outputDir = args[i];
            }
           
            else if( args[i].equals( DEBUG ) )
            {
                debug = true;
            }
            else if( args[i].equals( OPENSTACK ) )
            {
            		openstack = true;
            		VividingStripper.bookBy = "openStack";
            		//properties_file_name = "openStack.properties";
            }else if( args[i].equals( FEEDBOOKS ) )
            {
            		feedbooks = true;
            		VividingStripper.bookBy = "feedbooks";
            		//properties_file_name = "feedbooks.properties";
            }
            
            else
            {
                if( pdfFile == null )
                {
                    pdfFile = args[i];
                }
            }
        }

        if( pdfFile == null )
        {
            usage();
        }
        else
        {
            
            PDDocument document = null;
            try
            {
            		String outputFileName = null;
            		
                long startTime = startProcessing("Loading PDF "+pdfFile);
                
                Util.p(pdfFile);
                File f = new File(pdfFile);
                String fileName = f.getName();
	        		int idx = fileName.lastIndexOf('.');
	        		
	        		if (idx != -1) {
	        			fileName = fileName.substring(0, idx);
	        		}
        		
                if (outputDir != null && outputDir.length() > 0) {
                		
	                	if (outputDir.charAt(outputDir.length() -1) != '/') {
	                		outputDir += '/';                	
	                }
	                	 
	                			//f.getName().substring(0, f.getName().length() - 4);
                } else {
	                	if (f.getParent() != null) {
	                		outputDir = f.getParent().toString();
	                	} else {
	                		outputDir = Paths.get(".").toAbsolutePath().normalize().toString();
	                	}
                		//Util.p(outputDir);
                		File dir = Util.getSafeDir(outputDir, fileName);
                		outputDir = dir.getPath();
                		//Util.p(outputDir);
                }
                if (!outputDir.endsWith("/")) {
            			outputDir += '/';
                }
                Util.getSafeDir(outputDir, "data", true);
                Util.getSafeDir(outputDir, "config");
                Util.getSafeDir(outputDir, "svg", true);
                Util.getSafeDir(outputDir, "images", true);
                Util.getSafeDir(outputDir, "fonts", true);
                outputFileName = outputDir + fileName;
                String properties_full_file_name = outputDir + "config/" + properties_file_name;
                //String taget_properties_file_name = outputDir + "config/config.properties";
                
                System.out.println("Welcome to " + VERSION);
                System.out.println("outputDir = " + outputDir);
                System.out.println("Config File = " + properties_file_name);
                System.out.println("Loading file , " + pdfFile);
                System.out.println("Please wait ....");
                
                Properties prop = new Properties();
                if (properties_full_file_name != null) {
                		InputStream input;
					try {
						input = new FileInputStream(properties_full_file_name);
						// load a properties file
						prop.load(input);
						input.close();
					} catch (Exception e) {
												
						String source = Util.getProperty(this, properties_file_name).toString();
						String tocInfo = "";
						source = source.replace("${tocInfo}", tocInfo);
						source = source.replace("${fileName}", fileName);
						source = source.replace("${dateTime}", new Date().toLocaleString());
							
						Util.saveFile(new File(properties_full_file_name), source);
						

						input = new FileInputStream(properties_full_file_name);
						// load a properties file
						prop.load(input);
						input.close();
					}						    
                }
                document = PDDocument.load(new File( pdfFile ), password);
                                
                AccessPermission ap = document.getCurrentAccessPermission();
                if( ! ap.canExtractContent() )
                {
                    throw new IOException( "You do not have permission to extract text" );
                }
                
                stopProcessing("Time for loading: ", startTime);

             // Scans for plug-ins on the application class path, loads their service provider
                // classes, and registers a service provider instance for each one found with the
                // IIORegistry.
                
                ImageIO.scanForPlugins();
//                IIORegistry registry = IIORegistry.getDefaultInstance();
//                registry.registerServiceProvider(new com.github.jaiimageio.jpeg2000.impl.J2KImageReaderSpi()); 
//                
                //Cannot read JPEG2000 image: Java Advanced Imaging (JAI) Image I/O Tools are not installed
                

                System.out.println("LOADED\nStart Processing....");
                
                ImageIO.setUseCache(false);
                VividingStripper stripper = new VividingStripper();
                stripper.setOutputDir(outputDir);
                stripper.setOutputName(outputFileName);
                stripper.setOutputDir(outputDir);
                //stripper.setBookId(bookId);
				stripper.setProp(prop);
                startTime = startProcessing("Starting text extraction");
                
                
                
                //((VividingStripper)stripper).strippedOfImages(document, outputDir + fileName + "-no-image.pdf");
                stripper.writeText(document);
                
                // ... also for any embedded PDFs:
                PDDocumentCatalog catalog = document.getDocumentCatalog();
                PDDocumentNameDictionary names = catalog.getNames();    
                if (names != null)
                {
                    PDEmbeddedFilesNameTreeNode embeddedFiles = names.getEmbeddedFiles();
                    if (embeddedFiles != null)
                    {
                        Map<String, PDComplexFileSpecification> embeddedFileNames = embeddedFiles.getNames();
                        if (embeddedFileNames != null)
                        {
                            for (Map.Entry<String, PDComplexFileSpecification> ent : embeddedFileNames.entrySet()) 
                            {
                                if (debug)
                                {
                                    System.err.println("Processing embedded file " + ent.getKey() + ":");
                                }
                                PDComplexFileSpecification spec = ent.getValue();
                                PDEmbeddedFile file = spec.getEmbeddedFile();
                                if (file != null && "application/pdf".equals(file.getSubtype()))
                                {
                                    if (debug)
                                    {
                                        System.err.println("  is PDF (size=" + file.getSize() + ")");
                                    }
                                    InputStream fis = file.createInputStream();
                                    PDDocument subDoc = null;
                                    try 
                                    {
                                        subDoc = PDDocument.load(fis);
                                    } 
                                    finally 
                                    {
                                        fis.close();
                                    }
                                    try 
                                    {
                                        stripper.writeText( subDoc);
                                    } 
                                    finally 
                                    {
                                        IOUtils.closeQuietly(subDoc);                                       
                                    }
                                }
                            } 
                        }
                    }
                }
                System.out.println("Finish processing file = " + pdfFile);
                
                System.out.println("All DONE");
                System.out.println("Thanks for using " + VERSION);
                stopProcessing("Time for extraction: ", startTime);
            }
            finally
            {
                IOUtils.closeQuietly(document);
            }
        }
    }
    
    private long startProcessing(String message) 
    {
        if (debug) 
        {
            System.err.println(message);
        }
        return System.currentTimeMillis();
    }
    
    private void stopProcessing(String message, long startTime) 
    {
    		//SVGPageDrawer.customimagewriter.waitForImages();
        if (debug)
        {
            long stopTime = System.currentTimeMillis();
            float elapsedTime = ((float)(stopTime - startTime))/1000;
            System.err.println(message + elapsedTime + " seconds");
        }
    }

    /**
     * This will print the usage requirements and exit.
     */
    private static void usage()
    {
        String message = "Usage: java -jar pdfbox-app-x.y.z.jar ExtractText [options] <inputfile> [output-text-file]\n"
            + "\nOptions:\n"
            + "  -password  <password>        : Password to decrypt document\n"
            + "  -ignoreBeads                 : Disables the separation by beads\n"
           
            + "  -debug                       : Enables debug output about the time consumption of every stage\n"
            
            + "  <inputfile>                  : The PDF document to use\n"
            
            + "  [output-text-file]           : The file to write the text to";
        
        System.err.println(message);
        System.exit( 1 );
    }
}
