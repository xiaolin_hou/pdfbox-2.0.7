package com.vividing.pdf;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDFontDescriptor;
import org.apache.pdfbox.text.TextPosition;

public class TextStyle implements Comparable<TextStyle> {
	public static Map<String, TextStyle> classMap = new LinkedHashMap<String, TextStyle>();
	public static Map<String, PDFont> pdFontMap = new LinkedHashMap<>();
	//word-spacing
	//letter-spacing
	static public TextStyle getStyle(VividingStripper stripper, TextPosition textPosition) {
		if (textPosition == null) {
			return null;
		}
		TextStyle style = new TextStyle(textPosition);
				
		if (classMap.containsKey(style.toString())) {
			return classMap.get(style.toString());
		}

		String ID = Util.getID("s");
		style.setId(ID);
		classMap.put(style.toString(), style);
		//+ name;
		PDFontDescriptor descriptor = textPosition.getFont().getFontDescriptor();
		String fontName = Util.getFontFamilyName(descriptor);
		
		
		System.out.println(ID + " > Font=" + fontName +  ", style = " + style);
		

		if (!pdFontMap.containsKey(fontName)) {
			pdFontMap.put(fontName, textPosition.getFont());
		} 
		
		return style;
	}
	
	static public TextStyle getTextStyle(String style) {
		if (!Util.notEmpty(style)) {
			return null;
		}
		
				
		if (classMap.containsKey(style)) {
			return classMap.get(style);
		}

		TextStyle textStyle = new TextStyle(style);
		String ID = Util.getID("b");
		textStyle.setId(ID);
		
		classMap.put(style, textStyle);
		//+ name;
		return textStyle;
	}
	
	static public TextStyle getStyle(String key) {
		return classMap.get(key);
	}
	
	String color = null;
	String id;
	StringBuilder style = new StringBuilder();
	
	StringBuilder svgStyle = new StringBuilder();
	Map<String, String> attrs = new HashMap<String, String>();
	
	boolean bold = false;
	
	public TextStyle(String style) {
		this.style.append(style);
	  	//svgStyle.append(";stroke:").append(color);
	}
	public TextStyle(TextPosition textPosition) {
        	PDFontDescriptor descriptor = textPosition.getFont().getFontDescriptor();
        	
        	style = new StringBuilder();
	    	String n = Util.getFontFamilyName(descriptor);
        	//String n = descriptor.getFontFamily();
        	if (Util.notEmpty(n)) {
//        		if (n.indexOf(',') != -1) {
//  				  Util.p(n);
//  			  }
        		n = n.replace (",", "-");
			  //Normilaize ....
			  
			  
			  style.append("font-family:").append(n);
        	}
          //descriptor.getFontWeight();
	  
	
	  
	  color = textPosition.getRbg_color();
	  
      
	  //(int)(fontSize * textMatrix.getScalingFactorX()));
	  //Util.p(textPosition.getTextMatrix() + ", " + textPosition.getFontSize() +  ", " + textPosition.getTextMatrix().getScalingFactorX() + ", " + textPosition.getTextMatrix().getScalingFactorY());
      svgStyle.append(style);
      svgStyle.append(";font-size:").append(((int)textPosition.getFontSizeInPt()-1)+"pt"); //THe font is too big, ....
      style.append(";font-size:").append((int)textPosition.getFontSizeInPt()+"pt");
      
      //svgStyle.append(";font-size:").append((int)textPosition.getFontSize())
      //.append(";transform:matrix(" + textPosition.getTextMatrix().toString()).append(")");
      //.append(";scale(" + textPosition.getTextMatrix().getScalingFactorX())
      //.append(",").append(textPosition.getTextMatrix().getScalingFactorY())
      //.append(")");
      
      //scale(sx, sy)
       //Util.p(o);
      //textPosition.getTextMatrix().getScaleX();
      
      if(Util.isBold(descriptor)) {
    	  	svgStyle.append(";font-weight:bold");
      }
      
      if(Util.isItalic(descriptor)) {
  	  	svgStyle.append(";font-style:italic");
      }
//  			sb.append("transform:").append(textPosition.getTextMatrix());  
      
	  if (color == null) {
		  	color = "#000000";
	  }
	  	style.append(";color:").append(color);
	  	svgStyle.append(";stroke:").append(color);
	}

//	public void addColor(Stripper stripper, TextPosition textPosition) {
//		if (color == null) {
//			color = textPosition.getRbg_color();
//			if (color != null) {
//				style.append(";color:").append(color);
//			}
//		} else if (!color.equals(textPosition.getRbg_color())) {
//			getStyle(stripper, textPosition); //Start a new one			
//		}
//		
//		
//		//New we are going to save the Font, right ????
//	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public StringBuilder getStyle() {
		return style;
	}

	public void setStyle(StringBuilder style) {
		this.style = style;
	}

	@Override
	public String toString() {
		return style.toString();
	}
	
	public String toSVGString() {
		return svgStyle.toString();
	}
	
	
	@Override
    public int compareTo(TextStyle style2) {
        if(this.toString().equalsIgnoreCase(style2.toString()))
            return 0;
        else {
	        	String k1 = getId().substring(2);
	        String k2 = style2.getId().substring(2);
	        return Integer.parseInt(k1)-Integer.parseInt(k2);
        }            
    }

}
