package com.vividing.pdf;

import java.util.List;

import org.apache.pdfbox.text.TextPosition;

import com.vividing.pdf.tag.Article;
import com.vividing.pdf.tag.Paragraph;
import com.vividing.pdf.tag.Tag;

public class PostionInfo {

	public static final float END_OF_LAST_TEXT_X_RESET_VALUE = -1;
	public static final float MAX_Y_FOR_LINE_RESET_VALUE = -Float.MAX_VALUE;
	public static final float EXPECTED_START_OF_NEXT_WORD_X_RESET_VALUE = -Float.MAX_VALUE;
	public static final float MAX_HEIGHT_FOR_LINE_RESET_VALUE = -1;
	public static final float MIN_Y_TOP_FOR_LINE_RESET_VALUE = Float.MAX_VALUE;
	public static final float LAST_WORD_SPACING_RESET_VALUE = -1;
	public static final int INWORD = 0x01;
	public static final int NEXTLINE = 0x02;
	public static final int NEXTCELL = 0x04;
	public static final int NEXTROW = 0x08;
	public static final int NEXTWORD = 0x10;
	public static final int OUTOFTABLE = 0x1000;
	public static final int UNKNOWN = 0x8000;
	public float positionX;
	public float positionY;
	public float positionWidth;
	public float positionHeight;
	private float maxHeightForLine = MAX_HEIGHT_FOR_LINE_RESET_VALUE;
	private float minYTopForLine = MIN_Y_TOP_FOR_LINE_RESET_VALUE;
	// private float endOfLastTextX = END_OF_LAST_TEXT_X_RESET_VALUE;
	private float maxYForLine = MAX_Y_FOR_LINE_RESET_VALUE;
	public PostionInfo lastMyPostion = null;
	public TextPosition position = null;
	float wordSpacing = LAST_WORD_SPACING_RESET_VALUE;
	float averageCharWidth = 0;
	float expectedStartOfNextWordX = 0;
	protected VividingStripper stripper = null;
	/// test if our TextPosition starts after a new word would be expected to
	/// start

	public PostionInfo(VividingStripper stripper, TextPosition position, PostionInfo lastMyPostion, boolean sort) {
		this.position = position;
		this.lastMyPostion = lastMyPostion;
		this.stripper = stripper;
		if (position == null) {
			return;
		}
		if (lastMyPostion == null || lastMyPostion.position == null) {
			this.reset();
		} else {
			if ((position.getFont() != lastMyPostion.position.getFont()
					|| position.getFontSize() != lastMyPostion.position.getFontSize())) {
				lastMyPostion.averageCharWidth = -1;
			}
			maxYForLine = lastMyPostion.maxYForLine;
		}
		// If we are sorting, then we need to use the text direction
		// adjusted coordinates, because they were used in the sorting.
		if (sort) {
			positionX = position.getXDirAdj();
			positionY = position.getYDirAdj();
			positionWidth = position.getWidthDirAdj();
			positionHeight = position.getHeightDir();
		} else {
			positionX = position.getX();
			positionY = position.getY();
			positionWidth = position.getWidth();
			positionHeight = position.getHeight();
		}
		this.something();
	}

	/**
	 * Returns the underlying TextPosition object.
	 * 
	 * @return the text position
	 */
	public TextPosition getTextPosition() {
		return position;
	}

	public boolean newWord(Line line) {
		return this.newWord(line, false);
	}
	
	public boolean newWord(Line line, boolean inBox) {

		return isNewWord(line, inBox) || !overlap(line, inBox);
	}
	
	public boolean isNewWord(Line line) {
		return isNewWord(line, false);
	}
	
	public boolean isNewWord(Line line,  boolean inBox) {
		return isNewWord(line, -1, inBox);
	}

	public boolean isNewWord(Line line, float delta) {
		return this.isNewWord(line, delta, false);
	}
	
	public boolean isDifferentPage() {
		if (lastMyPostion == null) {
			return false;
		}
		return this.position.getPdfPageNum() != this.lastMyPostion.position.getPdfPageNum();
	}
	
	public boolean isNewWord(Line line, float delta, boolean inBox) {
		if (lastMyPostion == null) {
			return false;
		}

		if (isDifferentPage()) {
			return true;
		}
		if (delta == -1) {
			if (Character.isUpperCase(position.getUnicode().charAt(0)) || Util.isBold(this.position)) {
				delta = 1f;
			} else {
				delta = 0.1f;
			}
			/*
			 if (Character.isUpperCase(position.getUnicode().charAt(0)) || Util.isBold(this.position)) {
				delta = 0.5f;
			} else {
				delta = 0.002f;
			}
			 */
			//delta = Util.isBold(this.position) ? 0.5f : 0.001f;
		}
		
		float GAP = VividingStripperBase.LINEGAP;
		float yGap = Math.abs(getTextPosition().getYDirAdj() - lastMyPostion.getTextPosition().getYDirAdj());
		
		if (yGap >= GAP) { //We are in different line now
			return true;
		}
		
		boolean word = (

		// && expectedStartOfNextWordX !=
		// EXPECTED_START_OF_NEXT_WORD_X_RESET_VALUE
		(expectedStartOfNextWordX < (positionX + delta) || lastMyPostion.positionWidth + lastMyPostion.positionX
				+ lastMyPostion.wordSpacing <= (positionX + delta)));

		// boolean word =Util.within(expectedStartOfNextWordX, positionX, 0.01f)
		// ||
		// Util.within(lastMyPostion.positionWidth + lastMyPostion.positionX +
		// lastMyPostion.wordSpacing, positionX, 0.01f);
		// lastMyPostion.positionWidth + lastMyPostion.positionX +
		// lastMyPostion.wordSpacing
		// System.out.println(expectedStartOfNextWordX-positionX);
		// System.out.println(lastMyPostion.positionWidth +
		// lastMyPostion.positionX + lastMyPostion.wordSpacing - positionX);
		// System.out.println(this.diff());

		if (word) {
			return true;
		}
		
		//Me is Upper and  the last one is lower ...., is not a good way...
		// Something to do with the Upper case....
		
//		if (Character.isLowerCase(lastMyPostion.position.getUnicode().charAt(0))) {
//			float a2 = positionX - (lastMyPostion.positionWidth + lastMyPostion.positionX);
//			if (Character.isUpperCase(position.getUnicode().charAt(0))) {
//				//System.out.println(this.diff());
////				float a1 = expectedStartOfNextWordX - (positionX + delta);
////				float a3 =  lastMyPostion.wordSpacing - (positionX + delta);
//	
//				return true;
//			} else if  (a2 > 2){
//				return true;
//			}
//		}

		return false;
	}

	public boolean isNewWord2(Line line) {
		return isNewWord2(line, -1);
	}
	
	public boolean isNewWord2(Line line, float delta) {
		if (lastMyPostion == null) {
			return false;
		}

		if (delta == -1) {
			if (Character.isUpperCase(position.getUnicode().charAt(0)) || Util.isBold(this.position)) {
				delta = 1f;
			} else {
				delta = 0.1f;
			}
			
		}
		
		//float GAP = inBox ? VividingStripperBase.boxLINEGAP :VividingStripperBase.LINEGAP;
		
		
//		float yGap = Math.abs(getTextPosition().getYDirAdj() - lastMyPostion.getTextPosition().getYDirAdj());
		

		return (expectedStartOfNextWordX < (positionX + delta) || lastMyPostion.positionWidth + lastMyPostion.positionX
				+ lastMyPostion.wordSpacing <= (positionX + delta));
// 
		
	}
	
	
	public boolean isSameElement() {
		if (lastMyPostion == null) {
			return false;
		}

		float delta = -1;
		if (delta == -1) {
			if (Character.isUpperCase(position.getUnicode().charAt(0)) || Util.isBold(this.position)) {
				delta = 1f;
			} else {
				delta = 0.1f;
			}
			
		}
		
		
		 if ((positionX + delta - expectedStartOfNextWordX ) > 10) {
			 return false;
		 }
		return true;
		
	}
	
	public boolean inTable(Line line, boolean useTable) {
		if (lastMyPostion == null || !useTable) {
			return false;
		}
		
		///XIaolin .....
//		if (!this.position.isTable()) {
//			return false;
//		}
//		if (line.text.indexOf("Bone classification") != -1) {
//			System.out.println(line.text);
//			System.out.println("xxxxxx");
//		}
		// if (!newWord() || (this.positionY != lastMyPostion.positionY)) {
		boolean b = isNewWord(line, 0) || !overlap(line);
		// System.out.println(line.text);
		if (!b) {
			return false;
		}

		float DX = Math.max(lastMyPostion.wordSpacing + lastMyPostion.positionWidth,
				this.positionWidth + this.wordSpacing);
		if (!Util.isBold(lastMyPostion.getTextPosition())) {
			DX *= 1.4;
		}
		float dx = this.positionX - (expectedStartOfNextWordX + DX);

		if (Math.abs(dx) > 3 * lastMyPostion.positionWidth && line.text.length() <= 5) {
			// To far away, not a table
			return false;
		}

		b = (dx > 0);

		// float DY = Math.abs(lastMyPostion.positionY - positionY);
		float DY = positionY - lastMyPostion.positionY;
		if (Math.abs(DY) < 0.2f) {
			return b;
		}

		if (DY > lastMyPostion.positionHeight) {
			return false;
		} else
		// We are in different line, back to line before ????
		// If they are too far away, the if must be isolated words
		if (Math.abs(DY) > 3 * lastMyPostion.positionHeight) {
			return false;
		}

		// else {
		// return b;
		// }

		// We need more testing to make sure we are in table
		// if (b)

		return b;
	}

	static boolean isFirstRow(Article table) {
		return table.getParagraphs().size() == 1;
	}

	static private VVV tdx(List<Tag> paragraphs, int column) {
		//float right = 0;
		VVV vvv = new VVV();
		
		for (int i = 0; i < paragraphs.size(); i++) {
			Article row = (Article)paragraphs.get(i);
			if (row.getParagraphs().size() == 0) {
				continue;
			}
			if (row.getParagraphs().size() > column) {				
				Paragraph td = (Paragraph) row.getParagraphs().get(column);
				vvv.right = Math.max(vvv.right, td.getRect().right);
				vvv.x = Math.min(vvv.x, td.getRect().x);
			}
			if (row.getParagraphs().size() > column + 1) {				
				Paragraph td = (Paragraph) row.getParagraphs().get(column+1);
				vvv.nextX = Math.min(vvv.nextX, td.getRect().x);
			}
			if (row.getParagraphs().size() > (column - 1) && column > 0) {				
				Paragraph td = (Paragraph) row.getParagraphs().get(column-1);
				vvv.lastX = Math.max(vvv.lastX, td.getRect().right);
			}
		}
		return vvv;
	}
	public int checkTableInfo(Article table, Line line) {

		int result = INWORD;

		boolean b = isNewWord(line, 0) || !overlap(line);
		if (!b) {
			return result;
		}

//		if (line.text.indexOf("It is an intriguing") != -1) {
//			System.out.println(line.text);
//		}
		//Article row1 = null;
		int col = 0;
		int row = table.getParagraphs().size() - 1;
		VVV vvv = null;
//		if (row >= 1) {
//			row1 = (Article) table.paragraphs.get(0);
//		}
	   
		if (table.m_currentArticle == null) { //We are in the first ROW		   
			if (row > 0) {
				vvv = tdx(table.getParagraphs(), 0);
			}
		} else {
			col = table.m_currentArticle.getParagraphs().size() -1;
			if (row > 0 && col >= 0) {
				vvv = tdx(table.getParagraphs(), col);	
			}
		}
	   
		float dx = (this.positionX - lastMyPostion.positionX);
		float dy = (this.positionY - lastMyPostion.positionY);

//		if (vvv != null) {
//			System.out.println(vvv + ", Col = " + col + " : row = " + row);			
//		}
		Rect tdRect = line.m_rect;
		// As long as we are in the middle of the cell, we are in
		if (positionX >= tdRect.x && (positionX + positionWidth) < tdRect.right) {
			// if ((positionY - tdRect.bottom) > this.positionHeight) {
			// return NEXTROW;
			// }
			if (vvv != null && col == 0 && line.m_rect.x <= vvv.x && line.m_rect.right > vvv.right) {
				return OUTOFTABLE;
			}
			return NEXTLINE;
		}

		

//		if (col == 0 && row != 0) {
//			if (vvv != null) {
//				if (this.positionX-vvv.right > 40) {
//					return OUTOFTABLE;
//				}
//			}
//		}
		
		// ON the left side and y is out the bound, then next row
		if (this.positionY > tdRect.bottom && positionX < tdRect.x) {
			if (tdRect.x - positionX > 2 * this.positionWidth) { // Next Row or
				//System.out.println(line.text + " = " + (tdRect.x - positionX));
				return NEXTROW;
			} else {
				return NEXTLINE;
			}
		}

		// if (positionX > tdRect.x && (positionY > tdRect.yMin && positionY <=
		// tdRect.yMax))
		if (positionX > tdRect.x) // On the forward side
		{
			// float DX = this.wordSpacing + this.positionWidth;
			// TextPosition last = lastMyPostion.getTextPosition();
			float DX = lastMyPostion.wordSpacing + lastMyPostion.positionWidth;

			boolean bb = Util.isBold(lastMyPostion.getTextPosition());
			if (bb) {
				DX *= (1.25);
			}

			if (this.positionX > expectedStartOfNextWordX + DX) {
				return NEXTCELL;
			}
		}
		// System.out.println(line.lineText + "\n" + diff());
		if (dy <= lastMyPostion.positionHeight) { // They in the same line, for
													// now, they must be new
													// world
			// if (dx > Math.max(positionWidth, lastMyPostion.positionWidth)) {
			if (dx >= lastMyPostion.positionWidth) {
				return NEXTCELL;
			}
			return NEXTWORD;
		} else if (dx > lastMyPostion.positionWidth && dy < 0) {
			return NEXTCELL;
		}
		// 
		return UNKNOWN;
	}

	public String toString() {
		// return String.format("[%s, x=%s, y=%s, w=%s, h=%s, right=%s,
		// bottom=%s]\n", this.position.getUnicode(), this.position.getX(),
		// this.position.getY(), this.position.getWidth(),this.position.getH,
		// this.position.getX() + this.position.getWidth(), this.position.getY()
		// + this.position.getHeight()) +
		return String.format("[%s, x=%s, y=%s, w=%s, h=%s, right=%s, bottom=%s]", this.position.getUnicode(),
				this.positionX, this.positionY, this.positionWidth, this.positionHeight,
				this.positionX + this.positionWidth, this.positionY + this.positionHeight);
	}

	public String diff() {
		if (lastMyPostion != null) {
			return String.format("[%s, dx=%s, dy=%s, dw=%s, dh=%s, w=%s, h=%s]", this.position.getUnicode(),
					(this.positionX - lastMyPostion.positionX), (this.positionY - lastMyPostion.positionY),
					(this.positionWidth - lastMyPostion.positionWidth),
					(this.positionHeight - lastMyPostion.positionHeight), this.positionWidth, this.positionHeight);
		}
		return "[]";
	}

	public float delta() {
		float deltaSpace;
		if (wordSpacing == 0 || Float.isNaN(wordSpacing) || lastMyPostion == null) {
			deltaSpace = Float.MAX_VALUE;
		} else {
			if (lastMyPostion.wordSpacing < 0) {
				deltaSpace = wordSpacing * stripper.getSpacingTolerance();
			} else {
				deltaSpace = (wordSpacing + lastMyPostion.wordSpacing) / 2f * stripper.getSpacingTolerance();
			}
		}
		return deltaSpace;
	}

	public void something() {
		int wordCharCount = position.getIndividualWidths().length;
		// position.getFont().getBoundingBox()
		// Estimate the expected width of the space based on the
		// space character with some margin.
		wordSpacing = position.getWidthOfSpace();
		float deltaSpace = this.delta();

		// Estimate the expected width of the space based on the average
		// character width
		// with some margin. This calculation does not make a true average
		// (average of
		// averages) but we found that it gave the best results after numerous
		// experiments.
		// Based on experiments we also found that .3 worked well.
		if (lastMyPostion != null) {
			boolean bb = Util.isBold(lastMyPostion.getTextPosition());
			if (lastMyPostion.averageCharWidth < 0) {
				averageCharWidth = positionWidth / wordCharCount;
			} else {
				averageCharWidth = ((bb ? (1.5f * lastMyPostion.averageCharWidth) : lastMyPostion.averageCharWidth)
						+ positionWidth / wordCharCount) / 2f;
			}

		} else {
			averageCharWidth = positionWidth / wordCharCount;
		}

		float deltaCharWidth = averageCharWidth * stripper.getAverageCharTolerance();

		// Compares the values obtained by the average method and the
		// wordSpacing method
		// and picks the smaller number.
		expectedStartOfNextWordX = EXPECTED_START_OF_NEXT_WORD_X_RESET_VALUE;
		float endOfLastTextX1 = getEndOfLastTextX();

		if (endOfLastTextX1 != END_OF_LAST_TEXT_X_RESET_VALUE) {
			expectedStartOfNextWordX = endOfLastTextX1 + Math.max(deltaSpace, deltaCharWidth);
		}
	}

	public void setMaxYForLine() {
		if (this.positionY >= maxYForLine) {
			maxYForLine = this.positionY;
		}
	}

	public float getEndOfLastTextX() {
		if (lastMyPostion != null) {
			return lastMyPostion.getEndOfTextX();
		}

		return END_OF_LAST_TEXT_X_RESET_VALUE;
		/*
		 * endOfLastTextX = this.positionX + this.positionWidth; return
		 * endOfLastTextX;
		 */
	}

	public float getEndOfTextX() {
		return this.positionX + this.positionWidth;
	}

	public float getMinYTopForLine() {
		minYTopForLine = Math.min(minYTopForLine, this.positionY - this.positionHeight);
		return minYTopForLine;
	}

	public void setMinYTopForLine(float minYTopForLine) {
		this.minYTopForLine = minYTopForLine;
	}

	public float getMaxHeightForLine() {
		maxHeightForLine = Math.max(maxHeightForLine, this.positionHeight);
		return maxHeightForLine;
	}

	public boolean overlap(Line line) {
		return this.overlap(line, false);
	}
	public boolean overlap(Line line, boolean inBox) {
		if (lastMyPostion == null) {
			return true;
		}
		// System.out.println(this.diff());
		float y1 = this.positionY;
		float height1 = this.positionHeight;
		float y2 = (line.m_rect.yMin + line.m_rect.bottom) / 2;
		float height2 = this.getMaxHeightForLine();
		float GAP = VividingStripperBase.LINEGAP;
		if (y1 == line.m_rect.yMin) {
			return true;
		}

		if (y1 < line.m_rect.yMin) {
			y2 = line.m_rect.yMin;
		}
		//System.out.println(this.diff());
//		System.out.println(this.positionY - lastMyPostion.positionY
//				+ " == " + (this.positionHeight) + " << >>"
//+				Math.abs(positionX - lastMyPostion.positionX) + ", X " + position.getFont().getAverageFontWidth());
//				
if (Math.abs(positionX - lastMyPostion.positionX) < lastMyPostion.position.getFont().getAverageFontWidth()
&& Math.abs(this.positionY - lastMyPostion.positionY) < lastMyPostion.position.getHeight() / 2) {
//		if (Math.abs(positionX - lastMyPostion.positionX) < lastMyPostion.positionWidth / 2
//				&& Math.abs(this.positionY - lastMyPostion.positionY) < lastMyPostion.positionHeight / 2) {

			return true;
		}

		//Change line???
		if (this.positionX - lastMyPostion.positionX < 0 &&  
			this.positionY - lastMyPostion.positionY > GAP) {
			return false;
		}
		// lastMyPostion.position.getHeight();
//		 float d = position.getHeight();
//		 boolean b1 = Util.within(y1, y2, d);
//		 boolean b2 = y2 <= y1;
//		 boolean b3 = y2 >= y1 - height1;
//		 boolean b4 = y1 <= y2;
//		 boolean b5 = y1 >= y2 - height2;
		boolean b = Util.within(y1, y2, lastMyPostion.position.getHeight()) || y2 <= y1 && y2 >= y1 - height1
				|| (y1 <= y2 && y1 >= y2 - height2);
		return b;
	}

	public void reset() {
		maxHeightForLine = MAX_HEIGHT_FOR_LINE_RESET_VALUE;
		minYTopForLine = MIN_Y_TOP_FOR_LINE_RESET_VALUE;
		// endOfLastTextX = END_OF_LAST_TEXT_X_RESET_VALUE;
		maxYForLine = MAX_Y_FOR_LINE_RESET_VALUE;
	}

	
	public boolean inRect(Rect rect) {
		return (positionX >= rect.x && (positionX + positionWidth <= rect.right) && positionY >= rect.yMin && (positionY + positionHeight <= rect.bottom));
	}
	
	static final protected class VVV {
		//Rect m_rect = new Rect();
		
		float x = Integer.MAX_VALUE;
		float right = 0;
		float nextX = Integer.MAX_VALUE;
		float lastX = 0;
		
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return String.format("x=%s, right=%s, lastX=%s, nextX=%s", x, right, lastX, nextX);
		}
	}	
}




