package com.vividing.pdf;

import java.awt.Color;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.pdfbox.cos.COSInputStream;
import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.font.PDCIDFont;
import org.apache.pdfbox.pdmodel.font.PDCIDFontType2;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDFontDescriptor;
import org.apache.pdfbox.pdmodel.font.PDTrueTypeFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.font.PDType1CFont;
import org.apache.pdfbox.pdmodel.graphics.color.PDColor;
import org.apache.pdfbox.text.TextPosition;
import org.apache.pdfbox.util.Matrix;
import org.apache.pdfbox.util.QuickSort;

import com.vividing.pdf.part.Chapter;
import com.vividing.pdf.part.PartInfo;
import com.vividing.pdf.svg.Box;
import com.vividing.pdf.svg.element.SVGElement;
import com.vividing.pdf.svg.element.SVGGElement;
import com.vividing.pdf.svg.element.SVGPathElement;
import com.vividing.pdf.svg.element.SVGTextElement;

public class Util {
	//protected static final Log LOG = LogFactory.getLog(Util.class);
	// Stupid here
	public static Chapter getLastChapter(Map<String, Chapter> map) {
		// map.keySet().toArray(a)
		if (map.size() == 0) {
			return null;
		}
		String key = map.keySet().toArray()[map.size() - 1].toString();
		return map.get(key);
		// .key.map.values()
	}

	public static PartInfo getPartInfo(Map<String, PartInfo> map) {
		// map.keySet().toArray(a)
		if (map.size() == 0) {
			return null;
		}
		String key = map.keySet().toArray()[map.size() - 1].toString();
		return map.get(key);
		// .key.map.values()
	}

	public static PartInfo getLast(Map<String, PartInfo> map) {
		// map.keySet().toArray(a)
		if (map.size() == 0) {
			return null;
		}
		String key = map.keySet().toArray()[map.size() - 1].toString();
		return map.get(key);
		// .key.map.values()
	}

	static public boolean isItalic(PDFontDescriptor descriptor) {
		if (descriptor.isItalic()) {
			return true;
		}
		return descriptor.getFontName().contains("Italic");
	}
	
	static public boolean isBold(SVGTextElement e) {
		return isBold(e.getLastTextPosition());
	}

	static public boolean isItalic(TextPosition textPosition) {
		return isItalic(textPosition.getFont().getFontDescriptor());
	}
	
	static public boolean isBold(PDFontDescriptor descriptor) {
		if (descriptor.isForceBold()) {
			return true;
		}
		return descriptor.getFontName().contains("Bold");
	}

	static public boolean isBold(TextPosition textPosition) {
		PDFontDescriptor descriptor = textPosition.getFont().getFontDescriptor();
		return isBold(descriptor);
	}

	static public float multiplyFloat(float value1, float value2) {
		// multiply 2 floats and truncate the resulting value to 3 decimal
		// places
		// to avoid wrong results when comparing with another float
		return Math.round(value1 * value2 * 1000) / 1000f;
	}

	/**
	 * This will determine of two floating point numbers are within a specified
	 * variance.
	 *
	 * @param first
	 *            The first number to compare to.
	 * @param second
	 *            The second number to compare to.
	 * @param variance
	 *            The allowed variance.
	 */
	static public boolean within(float first, float second, float variance) {
		return second < first + variance && second > first - variance;
	}

	public static String escape(String chars) {
		
		if (chars == null) {
			return "";
		}
		StringBuilder builder = new StringBuilder(chars.length());
		for (int i = 0; i < chars.length(); i++) {
			builder.append(escape(chars.charAt(i)));
		}
		return builder.toString();
	}

	public static String escape(char character) {
		if ((character < 32) || (character > 126)) {
			//return ("&#" + (int)character + ";");
			return String.valueOf(character);
		} else {
			switch (character) {
			case 34:
				return "&quot;";
				
			case 38:
				return  "&amp;";
			case 39:
				return "&apos;";	
			case 60:
				return  "&lt;";
			case 62:
				return  "&gt;";
			default:
				return  (String.valueOf(character));
			}
		}
	}

	public static boolean contain(StringBuilder sb, String words) {
		String[] word = words.split("\\W+");

		for (String s : word) {
			if (sb.indexOf(s) == -1) {
				return false;
			}
		}
		return true;
	}

	public static TextPosition positionAt(List<TextPosition> textList, int index) {
		if (index < 0 || index >= textList.size()) {
			return null;
		}
		//try {
			return textList.get(index);
//		} catch (Exception e) {
//			
//			e.printStackTrace();
//			return null;
//		}
	}

	

//	public static boolean forwardMatching(List<TextPosition> textList, int startIndx, String word) {
//		if (startIndx <= 0 && startIndx > textList.size() + word.length()) {
//			return false;
//		}
//
//		for (int i = 0; i < word.length(); i++) {
//			if (startIndx + i >= textList.size()) {
//				return false;
//			}
//			if (textList.get(startIndx + i).getUnicode().charAt(0) != word.charAt(i)) {
//				return false;
//			}
//		}
//		return true;
//	}

	static Map<String, String> keyMap = new HashMap<String, String>();

	static String getID(String key) {
		for (int i = 1;; i++) {
			String k = key + "-" + i;
			if (!keyMap.containsKey(k)) {
				keyMap.put(k, k);
				return k;
			}
		}
	}
	static List<Matching> anyMatching(VividingStripper stripper, List<TextPosition> textList, StringBuilder sb, List<String> keys, Map<String, String> fonts) {
		return anyMatching(stripper, textList, sb, keys, fonts, false);
	}
	static List<Matching> anyMatching(VividingStripper stripper, List<TextPosition> textList, StringBuilder sb, List<String> keys, Map<String, String> fonts, boolean sorted) {
		List<Matching> l = new ArrayList<Matching>();
		Matching last = null;
		for (String key : keys) {
			int idx = sb.indexOf(key);
			if (idx != -1) {
//				if (key.equals("Key Terms")) {
//					//System.out.println(idx);
//					if (!sorted) {
//						TextPositionComparator comparator = new TextPositionComparator();
//						QuickSort.sort(textList, comparator);
//						return anyMatching(stripper, textList, sb, keys, fonts, true);
//					}
//				}
				boolean firstOne = (last == null);
				if (!firstOne) {
					last.length = idx - last.index;
				}
				last = new Matching(key, idx);
				last.firstOne = firstOne;
				TextPosition positon = textList.get(idx);
				TextStyle.getStyle(stripper, positon);
				if (Util.getBox(VividingStripper.getSkipBoxes(), positon) != null) {
					continue;
				}
				l.add(last);
//				if (style.toString().equals(fonts.get(key))) {
//				//Map<String, String> FONTS
//					l.add(last);
//				} else {
//					System.err.println(key + "\nWarning, font not matching for key >> "
//				 +positon.getX() + ", " + positon.getY() + ", " + (positon.getY() + positon.getHeight())
//							+ "\n" + style);
//				}
			}
		}
		if (l.size() > 1) {
			QuickSort.sort(l);
		}
		return l;
	}

	// [\d]+(.*) Chapter [\d]+[\s]?\|(.*)
	private static String HEADERPATTERN = "Chapter (\\d)+ \\| (.*)(\\d)+|[\\d]+[\\s]?Chapter [\\d]+[\\s]?\\|(.*)"
			+ "|Answer Key (\\d)+ \\| (.*)(\\d)+|[\\d]+[\\s]?Answer Key [\\d]+[\\s]?\\|(.*)"
			+ "|Index (\\d)+ \\| (.*)(\\d)+|[\\d]+[\\s]?Index [\\d]+[\\s]?\\|(.*)"
			// + "|Index (\\d)+ \\| (.*)(\\d)+|[\\d]+[\\s]?Index
			// [\\d]+[\\s]?\\|(.*)"
			+ "|Appendix [A-Z]+ (\\d)+ \\| (.*)(\\d)+|[\\d]+[\\s]?Appendix [A-Z]+ [\\d]+[\\s]?\\|(.*)";

	static String FOOTER = "This OpenStax book is available for free at";
	// static public boolean isHeader(StringBuilder sb) {
	// Matcher m = getHeader(sb);
	// return m.find();
	// }

	static public Matcher getHeader(StringBuilder sb) {
		Pattern r = Pattern.compile(HEADERPATTERN);
		return r.matcher(sb);

	}

	static public List<TextPosition> splices(List<Matching> matchingList, List<TextPosition> textList,
			StringBuilder sb) {
		List<TextPosition> head = new ArrayList<TextPosition>();
		// List<Matching> matchingList = Util.anyMatching(sb, MATCHINGKEYS);
		if (matchingList.size() <= 0) {
			return null;
		}
		Matching last = null;
		for (int i = 0; i < matchingList.size(); i++) {
			Matching m = matchingList.get(i);
			if (i == 0) {
				head = getTextPositions(textList, sb, 0, m.index);
			}

			if (m != last) {
//				if (i == 1 && matchingList.size() == 1 && (last.name.equals(VividingStripper.KEYTERMS))) {
//					System.out.println(last.name);
//				}
				
				
				if (last != null) {
					if (i == 1 && (last.name.equals(VividingStripper.KEYTERMS))) {
						last.textList = getTextPositions(textList, sb, 0, m.index);
					} else {
						last.textList = getTextPositions(textList, sb, last.index, m.index);
					}
				}
				last = m;
			}

			if (i == matchingList.size() - 1) {
				if (m.textList.size() == 0) {
//					if (matchingList.size() == 1 && (last.name.equals(VividingStripper.KEYTERMS)
//							)) {
//						//m.textList = getTextPositions(textList, sb, 0, textList.size());
//					} else {
//						m.textList = getTextPositions(textList, sb, m.index, textList.size());
//					}
					m.textList = getTextPositions(textList, sb, m.index, textList.size());
				}
			}
			
			if (last != null && last.name != null) {
				//System.out.println(last.name  +"\n" +sb);
			}
		}
		return head;
	}

	static public List<TextPosition> getTextPositions(List<TextPosition> textList, StringBuilder sb) {
		return Util.getTextPositions(textList, sb, 0, textList.size());
	}

	static public List<TextPosition> getTextPositions(List<TextPosition> textList, StringBuilder sb, int start,
			int end) {

		int idx = sb.indexOf(FOOTER);
		List<TextPosition> subList = null;
		if (idx != -1) {
			sb.setLength(idx);
			if (idx <= end && start >= idx) {
				subList = new ArrayList<TextPosition>(textList.subList(start, idx));
			}
		}

		if (subList == null) {
			subList = new ArrayList<TextPosition>(textList.subList(start, end));
		}
		Matcher m = getHeader(sb);
		return getTextPositions(subList, m, start, end);
	}

	static private List<TextPosition> getTextPositions(List<TextPosition> subList, Matcher m, int start, int end) {

		if (m.find() && m.start() >= start && m.end() <= end) {
			if (m.start() > 0) {
				// subList = subList
				List<TextPosition> subList1 = subList.subList(0, m.start() - start);
				subList1.addAll(subList.subList(m.end() - start, subList.size()));
				return subList1;
			} else {
				return subList.subList(m.end() - start, subList.size());
			}
		}
		return subList;
	}


	public static File getTempDir(String dir) {

		final File sysTempDir = new File(System.getProperty("java.io.tmpdir"));

		return getSafeDir(sysTempDir, dir);
	}

	public static File getSafeDir(String parent, String dir) {
		return getSafeDir(parent, dir, false);
	}

	public static File getSafeDir(String parent, String dir, boolean cearfirst) {
		final File parentDir = new File(parent);
		return getSafeDir(parentDir, dir, cearfirst);
	}
	public static File getSafeDir(File parentDir, String dir) {
		return getSafeDir(parentDir, dir, false);
	}

	public static File getSafeDir(File parentDir, String dir, boolean cearfirst) {

		File newDir = new File(parentDir, dir);

		if (newDir.exists() && cearfirst) {
			recursiveDelete(newDir, true);
		}

		if (!newDir.exists()) {

			newDir.mkdirs();
		}
		return newDir;
	}

	static public void recursiveDelete(File rootDir, boolean deleteRoot) {
		File[] childDirs = rootDir.listFiles();
		for (int i = 0; i < childDirs.length; i++) {
			if (childDirs[i].isFile()) {
				childDirs[i].delete();
			} else {
				recursiveDelete(childDirs[i], deleteRoot);
				childDirs[i].delete();
			}
		}

		if (deleteRoot) {
			rootDir.delete();
		}
	}

	public static File getSafeFile(File parentDir, String file) {

		File newDir = new File(parentDir, file);

		if (!newDir.exists()) {

			newDir.mkdirs();
		}
		return newDir;
	}
	// File baseDir = new File(System.getProperty("java.io.tmpdir"));

	public static String relativePath(String rootDir, String dir) {
		int idx = dir.indexOf(rootDir);
		if (idx >= 0) {
			return dir.substring(idx + 1 + rootDir.length()).replace('\\', '/');
		}
		return dir;
	}

	public static String ID() {
		return "v" + UUID.randomUUID().toString();
	}

	public static void saveFile(File file, CharSequence str) throws IOException {
		OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream(file), "UTF8");
		BufferedWriter bw = new BufferedWriter(fw);
		//bw.write(str);
		bw.append(str);
		bw.close();
		fw.close();
	}

	
	
	public static boolean saveFile(File file, byte[] bytes) {
		FileOutputStream os = null;

		try {
			os = new FileOutputStream(file);

			os.write(bytes);
			return true;
		} catch (FileNotFoundException e) { 
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (Exception e) {

				}
			}
		}
		return false;
	}

	// some code to generate the Object user...

	public static StringBuilder getFile(InputStream inputStream) throws IOException {

		final int bufferSize = 1024;
		final char[] buffer = new char[bufferSize];
		final StringBuilder out = new StringBuilder();
		Reader in = new InputStreamReader(inputStream, "UTF-8");
		for (; ; ) {
		    int rsz = in.read(buffer, 0, buffer.length);
		    if (rsz < 0)
		        break;
		    out.append(buffer, 0, rsz);
		}
		return out;
	}
	public static StringBuilder getFile(File file) throws IOException {
		StringBuilder sb = new StringBuilder();
		// RandomAccessFile aFile = new RandomAccessFile(file, "r");
		// FileChannel inChannel = aFile.getChannel();
		// ByteBuffer buffer = ByteBuffer.allocate(1024 * 4);
		// while (inChannel.read(buffer) > 0) {
		// buffer.flip();
		// sb.append(buffer);
		// /*
		// * for (int i = 0; i < buffer.limit(); i++) {
		// * System.out.print((char) buffer.get()); }
		// */
		// buffer.clear(); // do something with the data and clear/compact it.
		// }
		// inChannel.close();
		// aFile.close();

		BufferedReader br = new BufferedReader(new FileReader(file));
		try {
			// StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
			// return sb.toString();
		} finally {
			br.close();
		}
		return sb;
	}

	static public String copyURLToFile(String url, File topDir) {

		try {
			// String original = URLDecoder.decode(filename, "UTF-8");
			String filename = URLEncoder.encode(url, "UTF-8");
			return copyURLToFile(url, topDir, filename);
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		return null;
	}

	// How do we know what's kind of stuff are coming back???
	static public String copyURLToFile(String url, File topDir, String fileName) {
		try {

			File downloadDir = new File(topDir, "/EPUB/images");
			File file = new File(downloadDir, fileName);
			URL website = new URL(url);
			ReadableByteChannel rbc = Channels.newChannel(website.openStream());
			// Map<String, List<String>> map =
			// website.openConnection().getHeaderFields();
			FileOutputStream fos = new FileOutputStream(file);
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
			fos.close();
			return fileName;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	static Map<String, StringBuilder> templateMap = new HashMap<String, StringBuilder>();

	// static StringBuilder template = null;
	static public StringBuilder getHTML(Object o) {
		return getTempalte(o, "html.html");
	}

	static public StringBuilder getSVG(Object o) {
		return getTempalte(o, "svg.svg");
	}
	
	static public StringBuilder getStyle(Object o) {
		return getTempalte(o, "style.css");
	}

	static public StringBuilder getProperty(Object o) {
		return getProperty(o, "config.properties");
	}
	
	static public StringBuilder getProperty(Object o, String name) {
		return getTempalte(o, name);
	}
	
	static public StringBuilder retriveTempalte(Object o, String fileName) {
		
		try {
			if (templateMap.containsKey(fileName)) {
				return templateMap.get(fileName);
			}
			ClassLoader classLoader = o.getClass().getClassLoader();
			InputStream in = classLoader.getResourceAsStream(fileName);
			StringBuilder template = getFile(in);
			templateMap.put(fileName, template);
			return template;
		} catch (Exception e) {
			//e.printStackTrace();
			return null;
		}

	}

	static public StringBuilder getTempalte(Object o, String fileName) {
		
		if (VividingStripper.bookBy != null) {
			String fileName2 = "com/vividing/"+ VividingStripper.bookBy + "/" + fileName;
			StringBuilder sb =  retriveTempalte(o, fileName2);
			
			if (sb != null) {
				return sb;
			}
		}
		fileName = "com/vividing/"+ fileName;
		return retriveTempalte(o, fileName);

	}
	public static void toFile(VividingStripper stripper, StringBuilder book, StringBuilder style, String title) throws IOException {
		Util.toFile(stripper, book, style, title, true);
	}
	public static void toFile(VividingStripper stripper, StringBuilder book, StringBuilder sb, String title,
			boolean withTable) throws IOException {
		if (withTable) {
			Util.toFile(stripper, book, sb, title, stripper.getOutputName() + "-" + title + ".html");
		} else {
			Util.toFile(stripper, book, sb, title, stripper.getOutputName() + "-" + title + "-no-table.html");
		}
	}

	public static void toFile(VividingStripper stripper, StringBuilder book, StringBuilder style, String title, String fileName) throws IOException {
		if (book.length() > 0) {
			String source = Util.getHTML(stripper).toString();
			if (title == null) {
				title = "unkown";
			}
			source = source.replace("${title}", title);
			source = source.replace("${style}", style.toString());
			source = source.replace("${body}", book.toString());
			source = source.replace("${pageNum}", String.valueOf(VividingStripper.DISPLAYITEMS));
			
			Util.saveFile(new File(fileName), source);
		}
	}
	public static void toSvgFile(VividingStripper stripper, StringBuilder body, 
			StringBuilder def,
			float width, float height,
			String fileName) throws IOException {
		String source = Util.getSVG(stripper).toString();
		source = source.replace("${width}", String.valueOf(width));
		source = source.replace("${height}", String.valueOf(height));
		source = source.replace("${def}", def);
		
		source = source.replace("${body}", body);
		Util.saveFile(new File(fileName), source);
	}
	//Util.saveFile(new File(fileName), sb);

	public static void toStyleFile(VividingStripper stripper, StringBuilder style) throws IOException {
		String source = Util.getStyle(stripper).toString();
		source = source.replace("${style}", style.toString());
		String fileName = stripper.getOutputDir() + "main.css";
		Util.saveFile(new File(fileName), source);
	}

	public static String toStyleClass(String name) {
		return name.toLowerCase().replace(' ', '-');
	}

	// box.1=100,550,485,700
	// box.2=640,550,1025,700

	public static Rect getBox(Properties prop, String key) {
		String box = prop.getProperty(key);
		try {
			if (box != null) {
				return new Rect(box);
			}
		} catch (RectFormatException e) {
			e.printStackTrace();
			
		}
		return null;
	}
	public static List<Rect> getBoxes(VividingStripperBase stripper, Properties prop) {
		List<Rect> list = new ArrayList<Rect>();
		Enumeration<?> e = prop.propertyNames();
//		String bookId = stripper.getBookId();
		for (; e.hasMoreElements();) {
			String key = (String) e.nextElement();
//			int idx = key.indexOf(bookId);
//
//			if (idx < 0) {
//				continue;
//			}
			//String key2 = key.substring(idx + bookId.length() + 1);
			if (key.startsWith("box.")) {
				Rect r = getBox(prop, key);
				if (r != null) {
					list.add(r);
				}
			}
		}
		return list;
	}

	public static Rect getBox(List<Rect> list, PostionInfo position) {
		// List<Rect> list = new ArrayList<Rect>();
		if (position == null) {
			return null;
		}
		for (Rect rect : list) {
			if (position.inRect(rect)) {
				return rect;
			}
		}
		return null;
	}
	
	public static Rect getBox(List<Rect> list, Point2D leftPoint, Point2D rightPoint) {
		// List<Rect> list = new ArrayList<Rect>();
		if (leftPoint == null || rightPoint == null) {
			return null;
		}		
		
		for (Rect rect : list) {
			if (rect.in(leftPoint)) {
				return rect;
			}
			
			if (rect.in(rightPoint)) {
				return rect;
			}
//			if ((rect.x <= leftPoint.getX() 
//					&& rect.yMin <= leftPoint.getY()
////					&& rect.right <= leftPoint.getX() 
////					&& rect.bottom <= leftPoint.getY()
////					)||(
////					rect.x <= rightPoint.getX()
////					&& rect.yMin <= rightPoint.getY()
//					&& rect.right >= rightPoint.getX()
//					&& rect.bottom >= rightPoint.getY())
//				) {
//				return rect;
//			}
		}
		return null;
	}
	
	public static Rect getBox(SVGElement e) {
		return getBox(e.x, e.y, e.right-e.x, e.bottom-e.y);
	}
	
	public static Rect getBox(float x, float y, float width, float height) {
		return getBox(VividingStripper.getSkipBoxes(), x, y, width, height);
	}
	
	private static Rect getBox(List<Rect> list, float x, float y, float width, float height) {
		if (list == null || list.size() == 0) {
			return null;
		}
		for (Rect rect : list) {
			if (x >= rect.x && (x + width <= rect.right)
					&& y >= rect.yMin && (y + height <= rect.bottom)
					) {
				return rect;
			}
		}
		return null;
	}
	public static Rect getBox(TextPosition position) {
		return getBox(VividingStripper.getSkipBoxes(), position);
	}
	private static Rect getBox(List<Rect> list, TextPosition position) {
		// List<Rect> list = new ArrayList<Rect>();
		if (position == null) {
			return null;
		}
		for (Rect rect : list) {
			if (position.getX() >= rect.x && (position.getX() + position.getWidth() <= rect.right)
					&& position.getY() >= rect.yMin && (position.getY() + position.getHeight() <= rect.bottom)
					) {
				return rect;
			}
		}
		return null;
	}
	

	public static boolean containOrMatch(VividingStripperBase stripper, StringBuilder sb, Properties prop) {
		String containes = prop.getProperty(stripper.getBookId() + ".skip");
		String regex = prop.getProperty(stripper.getBookId() + ".skip-pattern");
		// String containes = prop.getProperty("skip");
		// String regex = prop.getProperty("skip-pattern");
		return containOrMatch(sb, containes, regex);
	}

	public static boolean containOrMatch(StringBuilder sb, String containes, String regex) {
		String str = sb.toString();

		if (containes != null && containes.length() > 0) {
			String[] ss = containes.split(",");

			for (String s : ss) {
				if (str.contains(s)) {
					return true;
				}
			}
		}
		if (regex != null && regex.length() > 0) {
			return str.matches(regex);
		}
		return false;

	}
	
//	public static boolean isLetterUpperOfDigit(VividingStripper stripper, List<TextPosition> listPosition) {
//		return isLetterUpperOfDigit(stripper, 0, listPosition);
//	}
//	
//	public static boolean isLetterUpperOfDigit(VividingStripper stripper, int index, List<TextPosition> listPosition) {
//		if (index < 0 && index > listPosition.size()) {
//			return false;
//		}
//		
//		TextPosition p = listPosition.get(index);
//		if (p == null) {
//			return false;
//		}
//		
//		
//		Character c = p.getUnicode().charAt(0);
//		if (Character.isLowerCase(c)) {
//			if (index + 1 <listPosition.size()){
//				TextPosition p2 = listPosition.get(index+1);
//				
//				if (p2 != null) {
//					
//					TextStyle lastStyle =  TextStyle.getStyle(stripper, p);
//			    		TextStyle style =  TextStyle.getStyle(stripper, p2);
//			    	
//			    		if (style != lastStyle) {
//			    			return true;
//			    		}
//		    		
//					Character c2 = p2.getUnicode().charAt(0);
//					if(!Character.isAlphabetic(c2)) {
//						return true;
//					}
//				} else {
//					//System.out.println(c + ", " + index + ", " + line.text);;
//				}
//			}
//		}
//		if (c == '“') {
//			if (index + 1 <listPosition.size()){
//				c = listPosition.get(index+1).getUnicode().charAt(0);
//			}
//		}		
//		
//		if (Character.isUpperCase(c)) {
//			for(String s : KEYSWORD) {
//				if (forwardMatching(listPosition, index, s) ) {
//					return false;
//				}
//			}
//			return true;
//		}
//		
//		return !Character.isAlphabetic(c);	
//	}
//	
//	public static boolean isLetterUpperOfDigit(Line line, List<TextPosition> textList, int listIndex) {
//		return Util.isLetterUpperOfDigit(line, 0, textList, listIndex);
//	}
	
//	static String[] KEYSWORD = "Runners Aztecs Francisco Columbus Muslims Christopher English European Santa Dominican Waldseemuller German Tordesillas Cortés Smarting Pizarro States Spaniards Europeans Asians Americas Mexico South Corn, Quetzalcoatl Coast. Kukulkan However, Aztlán Aztec Mayan Mesa Chaco Canada Christianity Church’s God Muhammad Charles Christians Columbus’s North Christianity Jews Genoa East Democratic Nigeria Louisiana, Alabama Alaska Arizona Arkansas California Colorado Connecticut Delaware Florida Georgia Hawaii Idaho Illinois Indiana Iowa Kansas Kentucky Louisiana Maine Maryland Massachusetts Michigan Minnesota Mississippi Missouri Montana Nebraska Nevada New Hampshire New Jersey New Mexico New York North Carolina North Dakota Ohio Oklahoma Oregon Pennsylvania Rhode Island South Carolina South Dakota Tennessee Texas Utah Vermont Virginia Washington West Virginia Wisconsin Wyoming".split(" ");
//	public static boolean isLetterUpperOfDigit(Line line, int index, List<TextPosition> textList, int listIndex) {
//		
//		if (index >= line.line.size()) {
//			return false;
//		}
//		
//		TextPosition p = line.line.get(index).getTextPosition();
//		if (p == null) {
//			return false;
//		}
//		Character c = p.getUnicode().charAt(0);
//		if (Character.isLowerCase(c)) {
//			if (index + 1 <line.line.size()){
//				if (line.line.get(index+1).getTextPosition() != null) {
//					Character c2 = line.line.get(index+1).getTextPosition().getUnicode().charAt(0);
//					if(!Character.isAlphabetic(c2)) {
//						return true;
//					}
//				} else {
//					//System.out.println(c + ", " + index + ", " + line.text);;
//				}
//			}
//		}
//		if (c == '“') {
//			if (index + 1 <line.line.size()){
//				c = line.line.get(index+1).getTextPosition().getUnicode().charAt(0);
//			}
//		}
//		
////		
//		
//		if (Character.isUpperCase(c)) {
//			for(String s : KEYSWORD) {
//				if (forwardMatching(textList, listIndex, s) ) {
//					return false;
//				}
//			}
//			return true;
//		}
//		//return Character.isUpperCase(c) || !Character.isAlphabetic(c);
//		return !Character.isAlphabetic(c);
//	}

	
//	public static void appendLine(VividingStripper stripper, Article article, Line line) {
//		TextStyle ts = TextStyle.getStyle(stripper, line.line.get(0).getTextPosition());
//		if (ts.toString().equals(VividingStripperBase.SECTIONTITLEFONT)) {
//			article.appendLine(line, "div", "sn");
//		} else if (ts.toString().equals(VividingStripperBase.BODYTITLEFONT)) {
//			article.appendLine(line, "div", "body-title");
//		} else {
//			article.addLine(line, "div", "s");
//		}
//	}
//	public static void addLine(VividingStripper stripper, Article article, Line line, List<TextPosition> textList, int listIndex) {
//		if (line.line.size() > 0) {
//			TextStyle ts = TextStyle.getStyle(stripper, line.line.get(0).getTextPosition());
//			if (ts != null && ts.toString().equals(VividingStripperBase.SECTIONTITLEFONT)) {
//				article.addLine(line, "div", "section-title");
//			} else if (ts != null && ts.toString().equals(VividingStripperBase.BODYTITLEFONT)) {
//				article.addLine(line, "div", "body-title");
//			} else {
//				if (Util.isLetterUpperOfDigit(line, textList, listIndex) || stripper.pageBreak) {
//					article.addLine(line, "div", "s");
//				} else {								
//					article.appendLine(line, "div", "s");
//				}
//			}
//		}
//	}
	
	public static String getCSSColor(Paint paint) {
		if (paint instanceof Color) {
			return getCSSColor(((Color) paint).getRed(), ((Color) paint).getGreen(), ((Color) paint).getBlue());
		}
		return null;
	}
	
	public static String getCSSColor(PDColor pdColor) {
		try {
			if (pdColor != null) {
				return String.format("#%06x", pdColor.toRGB());
			}
		} catch (Exception e) {
			err(e.getMessage());
		}
		return null;
	}
	
	public static String getCSSColor(int r, int g, int b) {
		String colorS = null;
		int rgb = (r<<16)+(g<<8)+b;
		colorS = String.format("#%06x", rgb);
//		if (rgb != 0) {
//			p("Paint "+rgb+" "+colorS);
//		}		
		return colorS;
	}
	
	public static StringBuilder getToc(VividingStripper stripper, List<PartInfo> tocMap) {
		StringBuilder toc = new StringBuilder();
		if (tocMap.size() > 0) {
			
			toc.append("<h1 class=\"text-center\">Table of Contents<h1>");
			toc.append("<hr />");
			toc.append("<table class=\"table\">");
			for (PartInfo info : tocMap) {
				if (info.type.equals(VividingStripper.CHAPTER)) {
				toc.append("<tr class=\"bg-success info-success\">");
				} else {
					toc.append("<tr>");
				}
				toc.append("<td>").append(info.key);
				toc.append("</td>");
				if (info.type.equals(VividingStripper.CHAPTER)) {
					toc.append("<td>").append(String.format("<a href=\"%s-%s.html\">", stripper.getOutputName(), info.key))
						.append(info.name)
						.append("</a>");
				} else {
					//toc.append("<td>").append(info.name);
					toc.append("<td>").append(String.format("<a href=\"%s%s.html\">", stripper.getOutputDir(), info.key))
					.append(info.name)
					.append("</a>");
				}
				toc.append("</td>");

				toc.append("<td>");
				if (info.page != -1) {
					toc.append(info.page);
				}
				toc.append("</td>");
				toc.append("<td>");
					toc.append(info.endPage);
				toc.append("</td>");

				toc.append("</tr>");
			}
			
		}
		return toc;
	}
	
	
	public static StringBuilder getBoxesAsTSV(List<Box> boxes) {
		StringBuilder sb = new StringBuilder();
		if (boxes.size() > 0) {
			sb.append("page").append('\t');
			sb.append("x").append('\t');
			sb.append("y").append('\t');
			sb.append("right").append('\t');
			sb.append("bottom").append('\t');
			sb.append("background");
			sb.append('\n');
			for (Box info : boxes) {
				sb.append(info.pageNum).append('\t');
				sb.append(info.x).append('\t');
				sb.append(info.y).append('\t');
				sb.append(info.right).append('\t');
				sb.append(info.bottom).append('\t');
				sb.append(info.background).append('\n');
			}
		}
		return sb;
	}
	
	public static StringBuilder getGapAsTsv(Map<String, ?> linegaps) {
		StringBuilder sb = new StringBuilder();
		if (linegaps.size() > 0) {
			sb.append("key").append('\t');
			sb.append("gap").append('\n');
			
			asTsv(linegaps, sb);
		}
		return sb;
	}

	public static StringBuilder getImageListAsTsv(Map<String, ?> linegaps) {
		StringBuilder sb = new StringBuilder();
		if (linegaps.size() > 0) {
			sb.append("Name").append('\t');
			sb.append("ID").append('\n');
			
			asTsv(linegaps, sb);
		}
		return sb;
	}
	
	public static void asTsv(Map<String, ?> linegaps, StringBuilder sb) {
			
		for (String key : linegaps.keySet()) {
			sb.append(key).append('\t');
			sb.append(linegaps.get(key)).append('\n');
			//Util.p(linegaps.get(key));
		}
	}
	
	public static TextStyle getStyle(VividingStripper stripper, List<LineItem> lines) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		for(LineItem line :lines) {
			if (line.getTextPosition() != null) {
				TextStyle textStyle = TextStyle.getStyle(stripper, line.getTextPosition());
				Integer i = map.get(textStyle.toString());
				
				if (i != null) {
					map.put(textStyle.toString(), new Integer(i+1));
				} else {
					map.put(textStyle.toString(), new Integer(1));
				}
			}
		}
		String textStyle = null;
		int count = 0;
		for(String key:map.keySet()) {
			int ii = map.get(key).intValue();
			if (count < ii) {
				count = ii;
				textStyle = key;
			}
		}
		return TextStyle.getStyle(textStyle);
	}
	
	public static String toSVGMatrix (Matrix m){
		String s = m.toString();
		s = s.replaceAll("\\[", "");
		s = s.replaceAll("\\]", "");
		return s;
	}
	
	public static String toSVGTransform(AffineTransform m){		
		return toSVGTransform(new Matrix(m));
	}
	
	public static String toSVGMatrix (AffineTransform m){
		return toSVGMatrix(new Matrix(m));
	}
	public static String toSVGTransform(Matrix m){		
		return String.format("matrix(%s)", Util.toSVGMatrix(m));
	}

	//TO, Sould we use GAP????instead of 7
	public static boolean sameElement(Matrix textMatrix1, Matrix textMatrix2) {
		if (textMatrix1 == null || textMatrix2 == null) {
			return false;
		}
		//p(textMatrix1.getTranslateY() - textMatrix2.getTranslateY());
		return Math.abs(textMatrix1.getTranslateY() - textMatrix2.getTranslateY()) <= 6;
		//return (textMatrix1.getTranslateY() - textMatrix2.getTranslateY() == 0);
	}

	
	public static String getHex(byte[] raw) {
		return Base64.getEncoder().encodeToString(raw);
	}
	
	public static String getFromHex(byte[] raw) {
		return new String(Base64.getDecoder().decode(raw));
	}
//	public static void writeFont(AMIFont amiFont, String dir) throws IOException {
//		writeFont(amiFont.getPDFont(), dir);
//	}
	
	public static void writeFont(PDFont font, String dir) throws IOException {
		if (font instanceof PDTrueTypeFont)  {
			writeFont(font.getFontDescriptor(), dir);
		} else if (font instanceof PDType0Font){
			PDCIDFont descendantFont = ((PDType0Font) font).getDescendantFont();
            if (descendantFont instanceof PDCIDFontType2) {
            		writeFont(descendantFont.getFontDescriptor(), dir);
            } else {
            	err("Unsupported Font Type " + descendantFont.getClass().toString());
            }
		} 	else if (font instanceof PDType1CFont) {
			writeFont(font.getFontDescriptor(), dir);
            	//System.err.println("Unsupported Font Type >>>" + font.getClass().toString());
            	//writeFont(font.getFontDescriptor(), dir);
		}
    }
	
	private static void writeFont(PDFontDescriptor fontDescriptor, String dir) throws IOException
    {
        if (fontDescriptor != null) {
        	
        	PDStream ff1Stream = fontDescriptor.getFontFile();
            PDStream ff2Stream = fontDescriptor.getFontFile2();
            PDStream ff3Stream = fontDescriptor.getFontFile3();
            if (ff2Stream != null)
            {
            	String fontName = dir + Util.getFontFamilyName(fontDescriptor) + ".ttf";                    
                writeFont(fontName, ff2Stream.createInputStream());
            } else {
            
            		
            		if (ff1Stream != null) {
            			err("Type 1");
            			String fontName = dir + fontDescriptor.getFontName() + ".ttf";                    
            			writeFont(fontName, ff1Stream.createInputStream());
                } else if (ff3Stream != null) { 
                		err("TYPE NEW .... " + ff3Stream.getClass().toString());
                		String fontName = dir + fontDescriptor.getFontName() + ".ttf";
                		writeFont(fontName, ff3Stream.createInputStream());
                }
            }
            
            
        }
    }
	
	static void writeFont(String fontName, COSInputStream inputStream) {
		FileOutputStream fos = null;
        try
        {
            System.out.println("Writing font:" + fontName);
            
            fos = new FileOutputStream(new File(fontName));
            
//            COSInputStream s = ff2Stream.createInputStream();
//            byte[] buffer = new byte[s.available()];
//            s.read(buffer);
//            fos.write(buffer);
            IOUtils.copy(inputStream, fos);
            //fos.write(ff2Stream.toByteArray());
            
//            Path textFile = Paths.get(fontName);
//            Files.write(textFile, ff2Stream.toByteArray()
//            		, StandardOpenOption.APPEND);
            //, StandardCharsets.UTF_8);

           
//            Writer out = new BufferedWriter(new OutputStreamWriter(
//            	    new FileOutputStream("outfilename"), "UTF-8"));
//            	try {
//            		for(int ii : ff2Stream.toByteArray()) {
//            			System.out.print(ii);
//            			out.write(ii);
//            		}
//            	} finally {
//            	    out.close();
//            	}
        } catch(Exception e) {
        		e.printStackTrace();
        }
        finally
        {
            if (fos != null)
            {
                try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
            }
        }
	}
	
//	public static String getFontName(PDFontDescriptor descriptor) {
//	  return descriptor.getFontName();	  
//	}
	
	public static String getFontFamilyName(PDFontDescriptor descriptor) {
		  String fontFamily = descriptor.getFontFamily();
		  
		  if (!notEmpty(fontFamily)) {
			  fontFamily = descriptor.getFontName();
		  }
		  if (notEmpty(fontFamily)) {
			  String[] s = fontFamily.split("\\+");
			  
			  if (s.length > 0) {
				  return s[s.length - 1];
			  }
		  }
		  return null;
	}
	
	public static String getStartTagsPart1(List<String> tag) {
		 if (tag.size() == 0) {
			 return null;
		 } else {
			 return tag.get(0);
		 }
	}
	
	public static boolean tagEqual(List<String> tags, String tag) {
		if (tags.size() != 1) {
			return false;
		}
		return tags.get(0).equalsIgnoreCase(tag);
	}
	public static String getStartTagsPart2(List<String> tag) {
		if (tag.size() == 0) {
			 return null;
		 } 
		 if (tag.size() == 1) {
			 return ">";
		 }
		 
		 String tags = ">";
		 for(int i =1; i< tag.size(); i++) {
			tags += "<" + tag.get(i) + ">";
		}
		 return tags;
	}
	public static String getEndTags(List<String> tag) {
		String tags = "";
		
		for(int i = tag.size(); i>0; i--) {
			tags += "</" + tag.get(i-1) + ">";
		}
		return tags;
	}
	static public int compare(SVGTextElement s1, SVGTextElement s2) {
		 if (!Util.equal(s1.y, s2.y, 0.9f)) {
			 if (s1.y > s2.y) {
				 return 1;
			 } else if (s1.y < s2.y){
				 return -1;
			 }
			 
			 if (s1.x > s2.x) {
				 return -1;
			 } else if (s1.x < s2.x){
				 return 1;
			 }
		 }
		 return 0;
	 }
	
	
	
	static int checkLi(SVGTextElement e) {
		return checkLi(e.getText());
	}
	static int checkLi(StringBuilder sb) {
		Matcher m = r.matcher(sb);
		if (m.find()) {
			
			if (sectionR.matcher(sb).find()) {
				return 0;
			}
			return NEWLINE;
		}
		return 0;
	}
	
	static int checkLiStart(SVGTextElement e) {
		return checkLiStart(e.getText());
	}
	static int checkLiStart(StringBuilder sb) {
		Matcher m = liStartR.matcher(sb);
		if (m.find()) {
			
			return NEWLINE;
		}
		return 0;
	}
	//static private String newLinePattern = "^[a-zA-Z0-9]{1,2}[\\.•]";
	//static private String newLinePattern = "^[a-hA-H0-9]{1,2}[\\.•]";
	static Pattern r = Pattern.compile("^[a-hA-H][\\.•]|^[0-9]{1,2}[\\.•]|^[\\s]?[•]");
	static Pattern sectionR = Pattern.compile("^[\\d]{1,2}[\\.][\\d]+");
	static Pattern liStartR = Pattern.compile("^[aA][\\.•]|^[1][\\.•]|^[\\s]?[•]");
	
	static boolean debug = false;
	
	static public float safeGap(SVGTextElement e1, SVGTextElement e2) {
		
		TextStyle ts1 = e1.getTextStyle(), ts2 = e2.getTextStyle();
		String key = ts1+"|"+ts2;
		Float f = VividingStripper.LINEGAPS.get(key);
		if (f != null) {
			return f;
		}
		
		f = SVGGElement.LINEGAPS.get(key);
		if (f != null) {
			return f;
		}
		
		Integer i = SVGGElement.WARNING_COUNT.get(key);
		
		if (i != null) {
			i++;
		} else {
			i = 1;
		}
		
		
		SVGGElement.WARNING_COUNT.put(key, i);
		err("Page #" + e1.pageNum + ", COUNT =" +  i + " >> NO GAP >> " + key + ", id >>" + e1.getTextStyle().getId() + ":" + e2.getTextStyle().getId());
//		p(e1);
//		p(e2);
		return VividingStripper.LINEGAP;
	}
	
	
	
	public static boolean equal(float f1, float f2) {
		return equal(f1, f2, 0.1f);
	}
	
	public static boolean equal(float f1, float f2, float variance) {
		return (Math.abs(f1-f2) <= variance);
	}
	public static boolean largeThan(float f1, float f2) {
		return largeThan(f1, f2, 0.1f);
	}
	
	public static boolean largeThan(float f1, float f2, float variance) {
		return (f1-variance > f2);
	}
	
	public static void p(Object o) {
		if (o != null) {
			System.out.println(o.toString());
		}
	}

	public static void err(Object o) {
		if (o != null) System.err.println(o.toString());
	}
	public static boolean notEmpty(String s) {
		return (s != null && s.length() > 0);
	}
	
	public static boolean between(float a1, float a2, float num) {
		return (num >= a1 && num <=a2);
	}
	
	public static boolean between(float a1, float a2, float num, float delta) {
		return (num >= (a1 - delta) && num <= (a2+delta));
	}
	
	public static boolean betweenExact(float a1, float a2, float n1, float n2) {
		return between(a1, a2, n1) && between(a1, a2, n2);
	}
	
	public static boolean between(float a1, float a2, float n1, float n2, float delta) {
		return between(a1, a2, n1, delta) && between(a1, a2, n2, delta);
	}
	
	static public StringBuilder toPath(PathIterator pi) {	
		double [] vals = new double[6];
		StringBuilder path = new StringBuilder();
		
	    while (!pi.isDone()) {
	        Arrays.fill( vals, 0 );
	        int type = pi.currentSegment(vals);
	        
	        switch(type) {
	        //case PathIterator.SEG_CLOSE:   closePath(); break;
	        case PathIterator.SEG_MOVETO:
	        	path.append(String.format("M%s,%s", (float)vals[0], (float)vals[1]));
	        	break;
	        	case PathIterator.SEG_LINETO:
	        	path.append(String.format("L%s,%s",(float)vals[0], (float)vals[1]));
	        	break;
	        	case PathIterator.SEG_CUBICTO:
	        	path.append(String.format("C%s,%s,%s,%s,%s,%s", (float)vals[0], (float)vals[1],
	                    (float)vals[2], (float)vals[3],
	                    (float)vals[4], (float)vals[5]));	
	        	break;
	        }
	        pi.next();
	        
	    }
	    return path;
	}
	
	public static Box sbToBox(SVGPathElement e, StringBuilder sb, int pageNum) {
		String pattern = "M([+-]?[0-9]+[.][0-9]*),([+-]?[0-9]+[.][0-9]*)L([+-]?[0-9]+[.][0-9]*),([+-]?[0-9]+[.][0-9]*)L([+-]?[0-9]+[.][0-9]*),([+-]?[0-9]+[.][0-9]*)L([+-]?[0-9]+[.][0-9]*),([+-]?[0-9]+[.][0-9]*)";
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(sb);
		if (m.find()) {

//			Util.p(m.group(1));
//			Util.p(m.group(2));
//			Util.p(m.group(3));
//			Util.p(m.group(4));
//			Util.p(m.group(5));
//			Util.p(m.group(6));
			
			Box b = new Box(e, Float.valueOf(m.group(1)).floatValue(), Float.valueOf(m.group(2)).floatValue(),
					Float.valueOf(m.group(5)).floatValue(), Float.valueOf(m.group(6)).floatValue(), pageNum);
			//Util.p(b);
			return b;
		}
		return null;
	}
	public static Box getBounds(AffineTransform tm, Rectangle p, SVGPathElement e, int pageNum) {
		Rectangle rect = p.getBounds();
		
		
		StringBuilder sb = toPath(rect.getPathIterator(tm));
		return sbToBox(e, sb, pageNum);
		
		
	}
	
	public static final int NEWLINE = 0x01;
	public static final int BRLINE = 0x02;
	public static final int NOSPACE = 0x08;
	public static final int NEWTABLE = 0x04;
	public static final int FIRSTLETTER = 0x10;
	
	static public int canMergeTwoLine(SVGTextElement s1, SVGTextElement s2) {
		if (s1.getTextStyle() != s2.getTextStyle()) {
			return NEWLINE;
		}
		
		StringBuilder t1 = s1.getText();
		int r = checkLi(t1);
		
		if (r != 0) {
			return r;
		}
		StringBuilder t2 = s2.getText();
		if (t1.length() == 0 || t2.length() == 0) {
			return 0;
		}
		
		
		char c1 = t1.charAt(0);
		char c2 = t2.charAt(t2.length()-1);
		
		if (Character.isLowerCase(c1)) {
			return 0;
		}
		
		if (c1 == '“' || c1 == '"') { //????????????
			return 0;
		}
		
		if (c2 == '.' || c2 == ';' || c2 == '?') {
			return NEWLINE;
		}
		
		if (s1.getTextStyle() != s2.getTextStyle()){
			return NEWLINE;
		}
		
		
		return 0;
		//if (s1.line.text.endWi)
	}
	
	public static boolean isPart(SVGElement e) {
		if (e instanceof SVGTextElement) {
			boolean b = isPart(((SVGTextElement)e).getTextStyle());
			
			if (b) {
				return true;
			}
			return ((SVGTextElement) e).getText().indexOf("Part") != -1;
		}
		
		return false;
	}
	
	public static boolean isPartTitle(SVGElement e) {
		if (e instanceof SVGTextElement) {
			return isPartTitle(((SVGTextElement)e).getTextStyle());
		}
		
		return false;
	}
	
	public static boolean isFont(String font, SVGElement e) {
		if (e == null) {
			return false;
		}
		
		if (e instanceof SVGTextElement) {
			return isFont(font, ((SVGTextElement)e).getTextStyle());
		}
		return false;
	}
		
	public static boolean isFont(String font, TextStyle textStyle) {
		//if (Util.notEmpty(VividingStripper.CHAPTERNUMFONT) && e1.getTextStyle().toString().equals(VividingStripper.CHAPTERNUMFONT)) {
		if (textStyle == null) {
			return false;
		}
		
		return notEmpty(font) && font.equalsIgnoreCase(textStyle.toString());
	}
			
	public static boolean isPart(TextStyle textStyle) {
		if (textStyle == null) {
			return false;
		}
		
		if (notEmpty(VividingStripper.PARTFONT) && textStyle.toString().equals(VividingStripper.PARTFONT)) {
			return true;
		}
		if (notEmpty(VividingStripper.PARTTITLEFONT) && textStyle.toString().equals(VividingStripper.PARTTITLEFONT)) {
			return true;
		}
		return false;
	}
	
	public static boolean isPartTitle(TextStyle textStyle) {
		if (textStyle == null) {
			return false;
		}
		
		if (notEmpty(VividingStripper.PARTTITLEFONT) && textStyle.toString().equals(VividingStripper.PARTTITLEFONT)) {
			return true;
		}
		return false;
	}
	
	static public boolean isTitle(SVGElement e) {
		
		if (isBodyTitle(e)) {
			return true;
		}
		
		if (isChapterTitle(e)) {
			return true;
		}
		
		if (isSectionTitle(e)) {
			return true;
		}
		
		if (isReviewTitle(e)) {
			return true;
		}
		
		return false;
	}
	
	
	public static boolean isBodyTitle(SVGElement e) {
		if (e instanceof SVGTextElement) {
			return isBodyTitle(((SVGTextElement)e).getTextStyle());
		}
		
		return false;
	}
	
	
	
	public static boolean isDefault(TextStyle textStyle) {
		if (textStyle == null) {
			return false;
		}
		
		if (notEmpty(VividingStripper.DEFAULTFONT)) {
			 String style = textStyle.toString();
			 String[] ss = VividingStripper.DEFAULTFONT.split(",");
			 for (String s: ss) {
				 if (style.equals(s)) {
					 return true;		 
				 }
			 }
		}
		
		return false;
	}
	
	public static boolean isBodyTitle(TextStyle textStyle) {
		if (textStyle == null) {
			return false;
		}
		
		if (notEmpty(VividingStripper.BODYTITLEFONT)) {
			 String style = textStyle.toString();
			 String[] ss = VividingStripper.BODYTITLEFONT.split("|");
			 for (String s: ss) {
				 if (style.equals(s)) {
					 return true;		 
				 }
			 }
		}
		
		return false;
	}
	public static boolean isChapterTitle(SVGElement e) {
		if (e instanceof SVGTextElement) {
			boolean b =  isChapterTitle(((SVGTextElement)e).getTextStyle());
			
			if (b) {
				return true;
			}
			return ((SVGTextElement) e).getText().indexOf("Chapter") != -1;
		}
		
		return false;
	}
	
	
	public static boolean isChapterTitle(TextStyle textStyle) {
		if (textStyle == null) {
			return false;
		}
		
		if (notEmpty(VividingStripper.CHAPTERTITLEFONT) && textStyle.toString().equals(VividingStripper.CHAPTERTITLEFONT)) {
			return true;
		}
		
		if (notEmpty(VividingStripper.CHAPTERFONT) && textStyle.toString().equals(VividingStripper.CHAPTERFONT)) {
			return true;
		}
		
		if (notEmpty(VividingStripper.CHAPTERNUMFONT) && textStyle.toString().equals(VividingStripper.CHAPTERNUMFONT)) {
			return true;
		}
		
		if (notEmpty(VividingStripper.CHAPTERSEPARATORONT) && textStyle.toString().equals(VividingStripper.CHAPTERSEPARATORONT)) {
			return true;
		}
		
		return false;
	}
	
	public static boolean isSectionTitle(SVGElement e) {
		if (e instanceof SVGTextElement) {
			return isSectionTitle(((SVGTextElement)e).getTextStyle());
		}
		
		return false;
	}
	public static boolean isSectionTitle(TextStyle textStyle) {
		if (textStyle == null) {
			return false;
		}
		
		if (notEmpty(VividingStripper.SECTIONTITLEFONT) && textStyle.toString().equals(VividingStripper.SECTIONTITLEFONT)) {
			return true;
		}
		
		if (notEmpty(VividingStripper.SECTIONNUMFONT) && textStyle.toString().equals(VividingStripper.SECTIONNUMFONT)) {
			return true;
		}
		
		if (notEmpty(VividingStripper.SECTIONSEPARATORFONT) && textStyle.toString().equals(VividingStripper.SECTIONSEPARATORFONT)) {
			return true;
		}
		return false;
	}
	
	public static boolean isReviewTitle(SVGElement e) {
		if (e instanceof SVGTextElement) {
			return isReviewTitle(((SVGTextElement)e).getTextStyle());
		}
		
		return false;
	}
	public static boolean isReviewTitle(TextStyle textStyle) {
		if (textStyle == null) {
			return false;
		}
		
		if (notEmpty(VividingStripper.REVIEWFONT) && textStyle.toString().equals(VividingStripper.REVIEWFONT)) {
			return true;
		}
		
		if (notEmpty(VividingStripper.REVIEWTITLEFONT) && textStyle.toString().equals(VividingStripper.REVIEWTITLEFONT)) {
			return true;
		}
		
		if (notEmpty(VividingStripper.KEYTERMSFONT) && textStyle.toString().equals(VividingStripper.KEYTERMSFONT)) {
			return true;
		}
		return false;
	}
	public static Pattern decimalPattern = Pattern.compile("^([\\d]+)\\. (.*)");
	public static Pattern lowerAlphaPattern = Pattern.compile("^([a-z]+)\\. (.*)");
	//public static Pattern olPattern = Pattern.compile("^([a-z]|[\\d]+)\\. (.*)");
	public static Pattern olPattern1 = Pattern.compile("^([a]|[1])\\. ");
	//public static Pattern olPattern2 = Pattern.compile("^([\\d]+\\.[1]");
	public static Pattern decimalPattern1 = Pattern.compile("^([1])\\. ");
	public static Pattern lowerAlphaPattern1 = Pattern.compile("^([a])\\. ");
	
	public static boolean isOl(SVGTextElement t) {
		Matcher m = olPattern1.matcher(t.getText());
		return (m.find());		
	}
	
	public static boolean isDecimalOl(SVGTextElement t) {
		Matcher m = decimalPattern1.matcher(t.getText());
		return (m.find());		
	}
	
	public static boolean isLowerAlphaOl(SVGTextElement t) {
		Matcher m = lowerAlphaPattern1.matcher(t.getText());
		return (m.find());		
	}
	
	public static Matcher getOl(SVGTextElement t, boolean decimal) {
		
		if (decimal) {
			return decimalPattern.matcher(t.getText());
		} else {
			return lowerAlphaPattern.matcher(t.getText());
		}
	}
	
	static public SVGTextElement mergeElement(List<SVGTextElement> list) {
		SVGTextElement last = null;
		for(SVGTextElement e: list) {
			if (last == null) {
				last = e;
			} else {
				if (VividingStripper.wordSpace) {
					last.addSpace();
				}
				last.add(e);
			}
		}
		return last;
	}
	
	public static Pattern rectangleParten = Pattern.compile("M([0-9]+[.][0-9]*),([+-]?[0-9]+[.][0-9]*)L([+-]?[0-9]+[.][0-9]*),([+-]?[0-9]+[.][0-9]*)L([+-]?[0-9]+[.][0-9]*),([+-]?[0-9]+[.][0-9]*)L([+-]?[0-9]+[.][0-9]*),([+-]?[0-9]+[.][0-9]*)");
	
	public static Box getRectangle(StringBuilder path) {
		Matcher m = Util.rectangleParten.matcher(path);
		if (m.find()) {
	
			return new Box(Float.valueOf(m.group(1)).floatValue(), Float.valueOf(m.group(2)).floatValue(),
					Float.valueOf(m.group(5)).floatValue(), Float.valueOf(m.group(6)).floatValue());
			
		}
		return null;
	}
	
	
	
	static public int inLine(SVGTextElement paragraph, SVGTextElement current, SVGTextElement last, float gWidth) {
		
		if (last == null) {
			return 0;
		}
		
		if (current.pageNum > last.pageNum) {
			return canMergeTwoLine(current,last);
		}
		StringBuilder t1 = current.getText();
		StringBuilder t2 = last.getText();
		
		if (t2 == null || t1 == null || t2.length() == 0 || t1.length() == 0) {
			return 0;
		}
//		if (last.find("I write a little word to tell you with how much satisfac-") 
//				//|| current.find("a city in California")
//				){
//			
//			debug = true;
//		}
		
 		if (current.find("THE NATURE OF SCIENCE")) {
 			debug = true;
 		}
// //		if (last.pageNum == 705) {
////			debug = true;
////		}
		if (debug) {
			p(last);
			p(current);
			p("---------------");
		}
		
		float diffY  = current.y - last.y;
		
		boolean inLi = 0 != checkLi(paragraph);
		int r = checkLi(t1);
		
		//Li must be in different line,
		if (diffY != 0 && r != 0) {
			if (inLi) {
				return r;
			}
//			if (0 != checkLiStart(t1)) {
//				return r;
//			}
			if (t2.indexOf("Figure") < 0) {
				return r;
			}
			//p("LI ??? " + t1);
		}
		
		float diffX  = current.x - last.x;
		//float diffX1  = current.x - last.right;
		
		//float diffY1  = current.y - last.bottom;
		
		boolean bTitle1 = isTitle(last);
		boolean bTitle2 = isTitle(current);
		
		if (bTitle1) {
			if (!bTitle2) {
				return NEWLINE;
			}
		} else {
			if (bTitle2) {
				return NEWLINE;
			}
		}
		//float diffX3  = current.x - last.right;
		float h0 = last.getTextPosition().getHeight();
		//float h1 = current.getTextPosition().getHeight();
		//float w0 = last.getTextPosition().getWidth();
		//float w1 = current.getTextPosition().getWidth();
		
		if (equal(diffY,0, 0.1f)) { 
			//We r in the same line, mighht be i n talble 
			if (VividingStripper.NOLINEBR) {
				return NOSPACE;
			} else if (largeThan(diffX, last.getTextPosition().getWidth(), 0.1f)){
				return 0;
			} else {
				return NOSPACE;
			}
		}
//		if (VividingStripper.NOBR) {
//			return BRLINE;
//		}
		if (diffX == 0) {
			boolean inPARAGRAPH_LINE_X = false;
			if (VividingStripper.PARAGRAPH_LINE_X != null) {
				String[] PARAGRAPH_LINE_X = VividingStripper.PARAGRAPH_LINE_X.split(",");
				
				for(String s : PARAGRAPH_LINE_X) {
					if(current.x == Float.valueOf(s)) {
						inPARAGRAPH_LINE_X = true;
					}
				}
			}
			if (!inPARAGRAPH_LINE_X && VividingStripper.PARAGRAPHX != null) {
				String[] PARAGRAPHX = VividingStripper.PARAGRAPHX.split(",");
				
				for(String s : PARAGRAPHX) {
					if(current.x == Float.valueOf(s)) {
						if (VividingStripper.NOBR) {
							return NEWLINE;
						} else {
							return BRLINE;
						}
					}
				}
			}
			float GAP = safeGap(last, current);
			if (largeThan(diffY, GAP, h0)) {
				return NEWLINE;
			}
			
			if (bTitle1 ||  bTitle2) {
				if (!(bTitle1 && bTitle2)) {
					return NEWLINE;
				}
			} 
			if (VividingStripper.NOLINEBR) {
				return NOSPACE;
			}
			
//			if (current.getTextStyle() != last.getTextStyle() && (isBold(current) || isBold(last))){
//				return NEWLINE;
//			}
			
			if (!inPARAGRAPH_LINE_X)  { 
				if(!inLi && ((last.right-last.x) / gWidth) < 0.7) {
					if (VividingStripper.NOBR) {
						return NEWLINE;
					} else {
						return BRLINE;
					}
				}
			} else {
				if(!inLi && ((last.right-last.x) / gWidth) < 0.2) {
					if (VividingStripper.NOBR) {
						return NEWLINE;
					} else {
						return BRLINE;
					}
				}
			
			}
		 } else {
			 float GAP = safeGap(last, current);
			 if (equal(diffY, GAP, h0/2)) {
				 
				 if (notEmpty(VividingStripper.FIRSTLETTERFONT) && current.getTextStyle().toString().equals(VividingStripper.FIRSTLETTERFONT)) {
					 return FIRSTLETTER;
				 } 
				 
				 else 
//					 if (bTitle1 ||  bTitle2) {
//							if (!(bTitle1 && bTitle2)) {
//								return NEWLINE;
//							}
//						}
					 if (current.getTextStyle() != last.getTextStyle()){					
						if (isBold(current) || isBold(last)) {
							
							if (t1.length() > 0 && Character.isUpperCase(t1.charAt(0))) {
								return NEWLINE;
							}
						}
					//return NEWLINE;
				} 
				//In this case,
				 if (VividingStripper.PARAGRAPHX != null) {
				 	String[] PARAGRAPHX = VividingStripper.PARAGRAPHX.split(",");
					
					for(String s : PARAGRAPHX) {
						//if(current.x == Float.valueOf(s)) {
						if(last.right < gWidth && current.x == Float.valueOf(s)) {
							if (VividingStripper.NOBR) {
								return NEWLINE;
							} else {
								return BRLINE;
							}
						}
					}
				 }
//				if (last.right < gWidth && current.x == VividingStripper.PARAGRAPHX) {
//					//Some time we need break into lines or para
//					//return NEWLINE;
//					
//					if (VividingStripper.NOBR) {
//						return NEWLINE;
//					} else {
//						return BRLINE;
//					}
//				}
				 
			 } else //if (diffX < 0) 
			 {
//				 if (equal(diffY,GAP)) {
//					 return 0;
//				 } else 
				 if (largeThan(diffY,GAP)) {
					 return NEWLINE;
				 } 
				 //else
				 //If in table, 
			 }
		 }
		 
		if (diffX > 0 && diffX <= last.getTextPosition().getWidth()) {
			return NOSPACE;
		}
		 
		 //TODO Apply samething to Page merge, ....XH
		 char lastC = t2.charAt(t2.length() -1);
		 if (lastC == '/') //Check more, we r in url????
		 {
			 return NOSPACE;
		 }
		 if (lastC == '-') {//Check more, we r in url????
//			 int idx = last.getText().lastIndexOf(" ");
//			 
//			 if (idx != -1) {
//				 String lastWord = last.getText().substring(idx+1);
//				 
//				 if (lastWord.length() <= VividingStripper.LONGESTLENGTH) {
//					 
//					 idx = lastWord.indexOf("-");
//					 
//					 if (idx != lastWord.length()-1) {
//						 System.err.println("Word with - ????" + lastWord);
//						 return NOSPACE;
//					 }
//					 System.err.println("page # " + last.pageNum + ", took out - >> " + last.getText() + " | " + current.getText());
//					 last.getLine().getLine().remove(last.getLine().getLine().size()-1);
//					 return NOSPACE;
//				 }
//				 //System.err.println("Last workd is == " + lastWord);
//			 }
			 
			 int idx = last.getText().indexOf("-");
			 if (idx == last.getText().length()-1) {
				 
				 if (VividingStripper.NOHYPHEN) {
					 //last.getLine().line.remove(last.getLine().line.size()-1);
					 //Util.p(last);
					 if (paragraph.size() > 0) {
						paragraph.removeAt(paragraph.size()-1);
					 }
					 if (last.size()>0) {
						 last.removeAt(last.size()-1);
						 err("page # " + last.pageNum + ", Warning, removed nohyphen \"-\" " + last.getText());
					 }
					 //Util.p(last);
					 
				 } else {
					 err("page # " + last.pageNum + ", Warning, to remove \"-\" " + last.getText());
				 }
			 }
			 //
			 return NOSPACE;
		 }
		 
		 if (Character.isAlphabetic(lastC)) {
			 //Blackwell Encyclopedia of Sociology
			 //. Retrieved October
			 //MERGR Sentence
			 if (t1.length() >= 2) {
				 if (t1.charAt(0) == '.' && t1.charAt(1) == ' ') {
					 return NOSPACE; 
				 } 
			 }
		 }
		 
		 	 
//			Pattern r0 = Pattern.compile("^([a-zA-Z])[\\.] (.*)");
//			Matcher m = r0.matcher(t1);
//			 if (m.find()) {
//				 //Not  sure wht to return
//				 return NEWLINE;
//			 }
		
		 return 0; //fornow
	}
	
	public static StringBuilder toPath(SVGPathElement path) {
		StringBuilder sb = new StringBuilder();
		sb.append("M").append(path.x).append(",").append(path.y);
		sb.append("L").append(path.right).append(",").append(path.y);
		sb.append("L").append(path.right).append(",").append(path.bottom);
		sb.append("L").append(path.x).append(",").append(path.bottom);
		sb.append("L").append(path.x).append(",").append(path.y).append("Z");
		
		return sb;
		// TODO Auto-generated method stub

	}
}
