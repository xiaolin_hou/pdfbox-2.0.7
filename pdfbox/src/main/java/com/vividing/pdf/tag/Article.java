package com.vividing.pdf.tag;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vividing.pdf.Line;
import com.vividing.pdf.LineItem;
import com.vividing.pdf.Rect;
import com.vividing.pdf.TextStyle;
import com.vividing.pdf.Util;
import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.svg.element.SVGTextElement;

public class Article extends Tag {
	private List<Tag> paragraphs = new ArrayList<Tag>();
	
	public Map<String, String> colors = new HashMap<String, String>();
	
	public Article m_currentArticle = null;
	Paragraph m_paragraph = null;
	String lastTag = "p";
	
	public Article(String tag, Tag parent, String strClass) {
		this.addTag(tag);
		this.parent = parent;
		
		if (Util.notEmpty(strClass)) {
			this.putAttr("class", strClass);
		}
	}
	public Article(String tag, Tag parent, Map<String, String> attrs) {
		this.addTag(tag);
		this.parent = parent;
		if (attrs != null) {
			this.getAttrs().putAll(attrs);
		}		
	}	
	
	public Paragraph safeParagraph(String tag) {
		if (m_paragraph == null) {
			m_paragraph = new Paragraph(tag, this);
			addTag(m_paragraph);
		}
		return m_paragraph;
	}

	public Paragraph safeParagraph(String tag, String value) {
		if (m_paragraph == null) {
			m_paragraph = new Paragraph(tag, this, value);
			addTag(m_paragraph);
		}
		return m_paragraph;
	}

	public void addLine(Line line) {
		this.addLine(line, lastTag);
	}

	public void addLine(Line line, String tag) {
		this.addLine(line, tag, "");
	}

	public void addLine(SVGTextElement e) {
		this.addLine(e.getLine());
	}


	public void addLine(SVGTextElement e, String tag) {
		this.addLine(e, tag, new HashMap<>());
	}
	
	public void addLine(SVGTextElement e, String tag, Map<String, String> attrs) {
		//this.addLine(e.getLine(), tag, attrs);
		
		lastTag = tag;
		this.safeParagraph(tag);
		if (attrs != null) {
			m_paragraph.getAttrs().putAll(attrs);
		}
		m_paragraph.addLine(e);
		m_paragraph = null;
		
	}
	 
	public void addLine(SVGTextElement e, String tag, String strClass) {
		//this.addLine(e.getLine(), tag, attrs);
		
		lastTag = tag;
		this.safeParagraph(tag);
		if (strClass != null && strClass.length() > 0) {
			m_paragraph.putAttr("class", strClass);
		}
		m_paragraph.addLine(e);
		m_paragraph = null;
		
	}	
	
	// Should we use Add or Put to Append Or New ??????
	public void addLine(String tag, String value) {
		this.safeParagraph(tag, value);
		m_paragraph = null;
	}
	
	public void addLine(String tag, String value, Map<String, String> attrs) {
		this.safeParagraph(tag, value);
		if (attrs != null) {
			m_paragraph.getAttrs().putAll(attrs);
		}
		m_paragraph = null;
	}

	public void appendLine(SVGTextElement e, String tag) {
		this.appendLine(e.getLine(), tag);
		//m_paragraph.elements.add(e);
	}
	
	public void appendLine(Line line, String tag) {
		this.appendLine(line, tag, null);
	}
	
	public Tag lastParagraph() {
		if (getParagraphs().size() > 0) {
			return getParagraphs().get(getParagraphs().size()-1);
		}
		return null;
	}
	
	public Tag lastParagraph(Tag tag) {
		if (m_paragraph == null) {
			for(int i = getParagraphs().size()-1; i >= 0; i--) {
				Tag p =getParagraphs().get(i);
				if (p != tag) {
					if (p instanceof Paragraph) {
						m_paragraph = (Paragraph)p;
						break;
					} else if (p instanceof com.vividing.pdf.tag.Article) {
						return p;
					} else 
					{
						System.err.println("Waht is this??? " + p.getClass().getName());
					}
				}
			}
		}
		return m_paragraph;
	}
	
	public Tag lastParagraph(String tag) {
		if (m_paragraph == null) {
			for(int i = getParagraphs().size()-1; i >= 0; i--) {
				Tag p =getParagraphs().get(i);
				if (p.tagEqual(tag)) {
					if (p instanceof Paragraph) {
						m_paragraph = (Paragraph) p;
					} else if (p instanceof Article) {
						//System.out.println(line.text);
						//m_paragraph = ((Article) p).m_paragraph;
						//System.err.println("Not a Paragraph ? "+ p.getClass().getName());
					}
					break;
				}
			}
		}
		return m_paragraph;
	}
	public void appendLine(Line line, String tag, String strClass) {
		lastParagraph(tag);
		if (m_paragraph == null) {
			this.addLine(line, tag, strClass);
		} else {
			if (strClass != null && strClass.length() > 0) {
				m_paragraph.putAttr("class", strClass);
			}
			
			////?????,why????
			//This this case, we need insert a space in the from for the line
			line.getLine().add(0, LineItem.getWordSeparator());
			m_paragraph.addLine(line);
			
		}
		m_paragraph = null;
	}
	
	public void addLine(String tag, String value, String strClass) {
		this.safeParagraph(tag, value);
		if (strClass != null && strClass.length() > 0) {
			m_paragraph.putAttr("class", strClass);
		}		
		m_paragraph = null;
	}
	
	public void addLine(Line line, String tag, String strClass) {
		Map<String, String> h = new HashMap<String, String>();
		if (Util.notEmpty(strClass)) {
			h.put("class", strClass);
		}
		this.addLine(line, tag, h);
	}

	public void addLine(Line line, String tag, Map<String, String> attrs) {
		lastTag = tag;
		this.safeParagraph(tag);
		if (attrs != null) {
			m_paragraph.getAttrs().putAll(attrs);
		}
		m_paragraph.addLine(line);
		m_paragraph = null;
	}
	/* Article m_currentTR = null; */
	public Article safeArticle(String tag) {
		return safeArticle(tag, "");
	}
	public Article safeArticle(String tag, String sClass) {
		if (m_currentArticle == null || !m_currentArticle.tagEqual(tag)) {
			m_currentArticle = new Article(tag, this, sClass);
			this.addTag(m_currentArticle);
		}
		return m_currentArticle;
	}
	
	public Article safeArticle(String tag, Map<String, String> attrs) {
		if (m_currentArticle == null || !m_currentArticle.tagEqual(tag)) {
			m_currentArticle = new Article(tag, this, attrs);
			this.addTag(m_currentArticle);
		}
		return m_currentArticle;
	}
	
	public Article safeDiv(String sClass) {
		return this.safeArticle("div", sClass);
	}

	public Article safeDiv() {
		return this.safeDiv("panel panel-info panel-sm");
	}

	public Article safeUL(String sClass) {
		return this.safeArticle("ul", sClass);
	}

	public Article safeUL() {
		return this.safeUL(null);
	}

	public Article safeOL(String sClass) {
		return this.safeArticle("ol", sClass);
	}

	public Article safeOL() {
		return this.safeOL(null);
	}

	public Article safeTR() {
		//return this.safeTR("tr");
		
		return this.safeTR(null);
	}
	
	public Article safeTR(String sClass) {
		return this.safeArticle("tr",sClass);
	}
	
	public Article safeTable() {
		return safeTable("table table-bordered");
	}

	public Article safeTable(String sClass) {
		return this.safeArticle("table", sClass);
	}

	public void reset() {
		m_currentArticle = null;
		m_paragraph = null;
	}

	public void addTag(Tag paragraph) {
		getParagraphs().add(paragraph);
	}

	public List<Tag> getTags() {
		return getParagraphs();
	}

	public void clear() {
		for (Tag p : getParagraphs()) {
			p.clear();
		}
		getParagraphs().clear();
	}

	public Rect getRect() {

		Rect rect = new Rect();
		for (Tag tag : getParagraphs()) {

			Rect r = null;
			if (tag instanceof Paragraph) {
				r = ((Paragraph) tag).getRect();
			} else if (tag instanceof Article) {
				r = ((Article) tag).getRect();
			}
			if (r != null) {
				if (rect.x == 0) {
					rect.x = r.x;
				} else {
					rect.x = Math.min(r.x, rect.x);
				}
				if (rect.yMin == 0) {
					rect.yMin = r.yMin;
				} else {
					rect.yMin = Math.min(r.yMin, rect.yMin);
				}

				if (rect.right == 0) {
					rect.right = r.right;
				} else {
					rect.right = Math.max(r.right, rect.right);
				}
				if (rect.bottom == 0) {
					rect.bottom = r.bottom;
				} else {
					rect.bottom = Math.max(r.bottom, rect.bottom);
				}
			}

		}
		return rect;
	}
	
	@Override
	public void write(VividingStripper stripper, StringBuilder sb) {
		
		if (getAttrs().size() == 0) {
			sb.append(String.format("<%s>", getStartTagsPart1()));
		} else {
			sb.append(String.format("<%s", getStartTagsPart1()));
			
			
			String style = getAttr("style");
			if (Util.notEmpty(style)) {
				TextStyle t = TextStyle.getTextStyle(style);
				getAttrs().remove("style");
				
				if (!Util.isDefault(t)) {
					addAttr("class", t.getId());
				}
			}
			for (String key : getAttrs().keySet()) {
				if (Util.notEmpty(key) && Util.notEmpty(getAttr(key))) {
					sb.append(" ").append(key).append("=\"").append(getAttr(key)).append("\"");
				}
			}
			sb.append(getStartTagsPart2());
		}
		
		if (head.length() > 0) {
			sb.append(head);
		}
		
		for (Tag p : getTags()) {
			p.write(stripper, sb);
		}
		
		if (getFooter().length() > 0) {
			sb.append(getFooter());
		}
		sb.append(String.format("%s%s", getEndTags(), VividingStripper.LINE_SEPARATOR));
	}
	public List<Tag> getParagraphs() {
		return paragraphs;
	}
	public void setParagraphs(List<Tag> paragraphs) {
		this.paragraphs = paragraphs;
	}
}
