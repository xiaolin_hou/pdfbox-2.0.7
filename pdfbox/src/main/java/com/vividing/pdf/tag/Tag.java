package com.vividing.pdf.tag;

import com.vividing.pdf.VividingStripper;

public abstract class Tag extends TagBase{	
	abstract protected void write(VividingStripper stripper, StringBuilder sb);
	abstract public void clear();
}
