package com.vividing.pdf.tag;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.vividing.pdf.Util;

public abstract class TagBase {
	protected boolean debug = false;
	private List<String> tags = new ArrayList<String>();
	protected Tag parent = null;
	private Map<String, String> m_attrs = new LinkedHashMap<>();
	
	public Map<String, String> getAttrs() {
		return m_attrs;
	}
	public StringBuilder head = new StringBuilder(); //Add Before anything
	private StringBuilder footer = new StringBuilder();//Add after anything
	
	public void addTag(String tag) {
		if (tag != null) {
			tags.add(tag);
		}
	}
	public void addAll(List<String> tags) {
		if (tags != null) {
			this.tags.addAll(tags);
		}
	}
	
	public boolean tagEqual(String tag) {
		return Util.tagEqual(this.tags, tag);
	}
	
	public String getStartTagsPart1() {
		return Util.getStartTagsPart1(tags);
	}
	
	public String getStartTagsPart2() {
		return Util.getStartTagsPart2(tags);
	}
	
	public String getEndTags() {
		return Util.getEndTags(tags);
	}
	public Tag getParent() {
		return parent;
	}

	public void setParent(Tag parent) {
		this.parent = parent;
	}

	public List<String> getTag() {
		return tags;
	}

	public void setTag(List<String> tag) {
		this.tags = tag;
	}

	public void addAttr(String key, String value) {
		String v = m_attrs.get(key);
		String newValue = value;
		if (v != null) {
			newValue = v + " " + value;
			if (v.contains(value)) {
				return;
			}
			// System.out.println(x);
		}
		m_attrs.put(key, newValue);
		// article.m_currentArticle.attrs.put("class", "qti-question");
	}

	public void addStyle(String key, String value) {
		
		String v = m_attrs.get("style");
		
		String newValue = String.format("%s:%s", key, value);
		if (v != null) {
			//newValue = v + ";" + newValue;
			if (v.contains(value)) { //we should find the key ,,, FOR NOW, this is OK
				return;
			} else {
				
				if (v.contains(key+":")) {
					Util.err(v + " > contains " + key + "=" + value);
					return;
				}
				newValue = v + ";" + newValue;
			}
			// System.out.println(x);
		}
		m_attrs.put("style", newValue);
		// article.m_currentArticle.attrs.put("class", "qti-question");
	}
	
	public void putStyle(String key, String value) {
		
		String newValue = String.format("%s:%s", key, value);
		m_attrs.put("style", newValue);
	}
	
	public void putStyle(String value) {		
		m_attrs.put("style", value);
	}
	public void putAttr(String key, int value) {
		putAttr(key, String.valueOf(value));
	}
	
	public void putAttr(String key, double value) {
		putAttr(key, String.valueOf(value));
	}
	
	public void putAttr(String key, float value) {
		putAttr(key, String.valueOf(value));
	}
	
	public void putAttr(String key, String value) {
		if (value == null) {
			if (this.m_attrs.containsKey(key)) {
				this.m_attrs.remove(key);
			}
		} else {
			m_attrs.put(key, value);
		}
	}
	
	public StringBuilder getFooter() {
		return footer;
	}
	public void setFooter(StringBuilder footer) {
		this.footer = footer;
	}
	
	public String getAttr(String key) {
		return this.m_attrs.get(key);
	}
	
}

