package com.vividing.pdf.tag;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.pdfbox.text.TextPosition;

import com.vividing.pdf.Line;
import com.vividing.pdf.LineItem;
import com.vividing.pdf.Rect;
import com.vividing.pdf.TextStyle;
import com.vividing.pdf.Util;
import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.svg.element.SVGTextElement;

public class Paragraph extends Tag {
	List<LineItem> lines = new ArrayList<LineItem>();
	private List<SVGTextElement> elements = new ArrayList<>();
	boolean noLineItem = false;
	StringBuilder text = new StringBuilder();
	Rect m_rect = new Rect();

	public Paragraph(String tag, Tag parent) {
		this.addTag(tag);
		this.parent = parent;
	}

	public Paragraph(String tag, Tag parent, String text) {
		this.addTag(tag);
		this.parent = parent;
		this.noLineItem = true;
		this.text.append(text);
	}
	
	public Rect getRect() {
		return m_rect;
	}
	
	
	public void addLine(List<SVGTextElement> list) {
		SVGTextElement last = null;
		for(SVGTextElement e: list) {
			if (last == null) {
				last = e;
			} else {
				if (VividingStripper.wordSpace) {
					last.addSpace();
				}
				last.add(e);
			}
		}
		this.addLine(last);
	}
	public void addLine(SVGTextElement e) {
		this.elements.add(e);
		this.addLine(e.getLine());
	}
	
	public void addLine(SVGTextElement e, boolean clear) {
		this.elements.add(e);
		this.addLine(e.getLine(), clear);
	}
	void addLine(Line line) {
		this.addLine(line, true);
	}
	
	void addLine(Line line, boolean clear) {
		m_rect.setRect(line.m_rect);
		if (lines.size() > 0) {
			if (VividingStripper.wordSpace) {
				lines.add(LineItem.WORD_SEPARATOR);
			}
		}
		lines.addAll(line.getLine());
		if (text.length() > 0) {
			text.append('\n');
		}
		text.append(line.text);
		line.clear();
	}

	public void clear() {
		lines.clear();
		text = new StringBuilder();
	}

	public StringBuilder getText() {
		return text;
	}
	
	//@Override
	protected void write2(VividingStripper stripper, StringBuilder sb) {
		
		/*
		TextStyle fontStyle = Util.getStyle(stripper, lines);
		Holder holder = new Holder(null);
		

		holder.textStyle = fontStyle;
		scanLine(stripper, lines, holder);
		holder.toHtml(this, sb);
		*/
		
		Paragraph paragraph = this;
		//String classValeue = paragraph.getAttr("class");
		
//		if (Util.notEmpty(classValeue)) {
//			if (textStyle != null) {
//				classValeue = textStyle.getId() + " " + classValeue; 
//			}
//		} else if (textStyle != null) {
//			classValeue = textStyle.getId(); 
//		}
		//paragraph.putAttr("class", classValeue);
		
		if (getTag().size() == 0) {
			setTag(paragraph.getTag());
		}
		
		String strStartTag =  getStartTagsPart1();
		
		sb.append(String.format("<%s", strStartTag));
		
		//Util.p("XXX = " +strStartTag + ", classValeue = " + paragraph.attrs.get("class"));

		if (paragraph.getAttrs().size() > 0) {
			for (String key : paragraph.getAttrs().keySet()) {
				if (Util.notEmpty(paragraph.getAttr(key))) {
					sb.append(" ").append(key).append("=\"").append(paragraph.getAttr(key)).append("\"");
				}
			}
		}
//		
		sb.append(getStartTagsPart2());
		if (head.length() > 0) {
			sb.append(head);
		}
		if (noLineItem) {
			sb.append(text);
		} else {
			
			for(SVGTextElement t: elements) {
				
				t.toHtml(sb);				
			}
		}
		
		if (getFooter().length() > 0) {
			sb.append(getFooter());
		}
		sb.append(String.format("%s%s", getEndTags(), VividingStripper.LINE_SEPARATOR));
		
	}
	
	@Override
	protected void write(VividingStripper stripper, StringBuilder sb) {
		TextStyle fontStyle = Util.getStyle(stripper, lines);
		Holder holder = new Holder(null);
		
		
//		if (getText().indexOf("Appleton and Company.") != -1 ||
//				getText().indexOf("The Structure of Sociological Theory") != -1) {
//			System.out.println(getText());
//			System.out.println("=========");
//			
//			debug = true;
//		}
		holder.textStyle = fontStyle;
		scanLine(stripper, lines, holder);
		holder.toHtml(this, sb);		
	}
	
	class Holder extends TagBase{
		TextStyle textStyle;
		Holder parent = null;
		TextPosition lastTextPosition = null;
		float y = -1;
		float x = Integer.MAX_VALUE;
		float meanY = -1;
		int flag = 0;
		Holder(String tag) {
			if (tag !=null) {
				this.addTag(tag);	
			}
		}
		Holder(String tag, int flag) {
			if (tag !=null) {
				this.addTag(tag);
			}
			this.flag = flag;
		}
		
		Holder(List<String> tagList, int flag) {
			this.addAll(tagList);
			this.flag = flag;
		}
		List<Object> objects = new ArrayList<Object>();
//		void add(String str) {
//			objects.add(str);
//		}
		
		void addPosition(LineItem item) {
			
			TextPosition position = item.getTextPosition();
			if (item.isWordSeparator()) {
				objects.add(" ");
			} else if (item.isBR()) {
				objects.add("<br>");
			} else {
				
				 y = Math.max(y, position.getY());
				 meanY = Math.max(meanY, (position.getY() + position.getHeight()/2));
				 x = Math.min(x, position.getX());
				 objects.add(Util.escape(position.getUnicode()));
				 lastTextPosition = position;
			}
			//objects.add(position);
		}
		Holder addTag(TextPosition textPosition, TextStyle textStyle1) {
			return this.addTag(getFlag(textPosition), textStyle1);
		}
		Holder addTag(int flag, TextStyle textStyle1) {
			//String tag1 = null;
			List<String> tagList = new ArrayList<String>();
			if ((flag & SUP) == SUP) {
				tagList.add("sup");					
			} else if ((flag & SUB) == SUB) {
				tagList.add("sub");					
			}
			
			if ((flag & ITALIC) == ITALIC) {
				//moreClass = "italic";
				tagList.add("i");
			}
			if ((flag & BOLD) == BOLD) {
				tagList.add("b");
			}
		
			Holder h = new Holder(tagList, flag);
			
			this.add(h);
			boolean addClass = true;
			
			
			String parentClass= getAttrs().get("class");
			if (parentClass!= null) {
				//Util.p(parentClass +", " + textStyle1.getId());
				
				if (parentClass.indexOf(textStyle1.getId()) != -1) {
					addClass = false;
				}
			}
			if (Util.isDefault(textStyle1)) {
				//Util.p("XXX");
				//Not add default font
				//Util.p("skip Class = " + textStyle1.getId());
			} else {
			
				if (addClass) {
					h.putAttr("class", textStyle1.getId());
					//Util.p("Add Class = " + textStyle1.getId());
				}
			}
//			if (moreClass != null && moreClass.length() > 0) {
//				h.putAttr("class", moreClass);
//			}
			return h;
		}
		
		void add(Holder holder) {
			holder.parent = this;
			objects.add(holder);
		}
		
		Holder getRoot() {
			if (parent == null) {
				return this;
			} else {
				return parent.getRoot();
			}
		}
		
		Holder getParent(TextStyle textStyle, TextPosition textPosition) {
			if (parent == null) {
				return null;
			} 
			
			if (parent.textStyle == textStyle && parent.flag == parent.getFlag(textPosition)){
				return parent;
			}
			return parent.getParent(textStyle, textPosition);
		}
		
		int getFlag(TextPosition textPosition) {
			int flag = 0x0;
			if (lastTextPosition != null) {
				float d = textPosition.getHeight() - lastTextPosition.getHeight();
				float dy =  textPosition.getY() - lastTextPosition.getY();
				
				//if (Math.abs(dy) < ((lastTextPosition != null) ? Math.max(textPosition.getHeight(), lastTextPosition.getHeight()) : textPosition.getHeight()) && dy != 0 ) {
				//Not Uppercase,can be -+...
				if (!Character.isUpperCase(textPosition.getUnicode().charAt(0)) && Util.equal(Math.abs(dy), textPosition.getHeight(), VividingStripper.SUBSUPGAP) && dy != 0 ) {
					if (d < 0) { // Curent is smaller
						if (dy > 0) {
							flag |= SUB;
						} else {
							flag |= SUP;
						}
					} 
				}
			}
			if (Util.isBold(textPosition)) {
				flag |= BOLD;
			}
			
			if (Util.isItalic(textPosition)) {
				flag |= ITALIC;
			}
			return flag;
		}
		
		
		StringBuilder getTagStart(Paragraph paragraph) {
			StringBuilder header = new StringBuilder();
			String strStartTag =  getStartTagsPart1();
			
			header.append(String.format("<%s", strStartTag));
			
			//Util.p("XXX = " +strStartTag + ", classValeue = " + paragraph.attrs.get("class"));

			if (paragraph.getAttrs().size() > 0) {
				for (String key : paragraph.getAttrs().keySet()) {
					if (Util.notEmpty(paragraph.getAttr(key))) {
						header.append(" ").append(key).append("=\"").append(paragraph.getAttr(key)).append("\"");
					}
				}
			}
//			
			header.append(getStartTagsPart2());
			return header;
		}
		void toHtml(Paragraph paragraph, StringBuilder html) {
			boolean inParentheses = false;
			StringBuilder sb = new StringBuilder();
			
			//String classValeue = paragraph.getAttr("class");
			
			/* if (Util.notEmpty(classValeue)) {
				if (textStyle != null) {
					classValeue = textStyle.getId() + " " + classValeue; 
				}
			} else if (textStyle != null) {
				classValeue = textStyle.getId(); 
			}
			*/
			if (textStyle != null && !Util.isDefault(textStyle)) {
				paragraph.addAttr("class", textStyle.getId());
			}
			
			
			String style = paragraph.getAttr("style");
			if (Util.notEmpty(style)) {
				TextStyle t = TextStyle.getTextStyle(style);
				paragraph.getAttrs().remove("style");
				if (t != null && !Util.isDefault(t)) {
					paragraph.addAttr("class", t.getId());
				}
			}
			
			if (getTag().size() == 0) {
				setTag(paragraph.getTag());
			}
			

			if (head.length() > 0) {
				sb.append(head);
			}
			if (noLineItem) {
				sb.append(text);
			} else {
				
				//String s = null;
				for(Object o:objects) {
					if (o instanceof String) {
						
//						if (debug) {
//							Util.p(o);
//						}
//						if (sb.indexOf("The poster shown above") != -1) {
//							debug = true;
//						}
						
						sb.append(o);
						
//						if (s != null) {
//							s += o;
//						}
						if (((String) o).charAt(0) == '(') {
							//s = (String)o;
							inParentheses = true;
						}
//						 
						if (((String) o).charAt(0) == ')') {
							//Util.p(s);
							inParentheses = false;
						}
					} else {
						Holder h = (Holder) o;
						h.toInnerHtml(paragraph, sb, inParentheses);
					}
				}
			}
			
			if (getFooter().length() > 0) {
				sb.append(getFooter());
			}
			
			//data-source=\"#figure-1-1\""
			Matcher m = figurePattern.matcher(sb);
			if (m.find()) {
				
//				Pattern figurePattern = Pattern.compile("Figure ([\\d]+).([\\d]+)([a-z]?)");
//				</span> Figure 1.1</button>
//				id="figure-1-1"
				String id = "figure-"+m.group(1)+"-" + m.group(2);
				
				if (!inParentheses && sb.indexOf(id) < 0) {
					//Util.p(sb);
					paragraph.getAttrs().put("data-source", String.format("#figure-%s-%s", m.group(1), m.group(2)));
				}
			}
			sb.append(String.format("%s%s", getEndTags(), VividingStripper.LINE_SEPARATOR));
			
			html.append(getTagStart(paragraph));
			html.append(sb);
			
		}
		
		Pattern figurePattern = Pattern.compile("Figure ([\\d]+).([\\d]+)([a-z]?)");
		
		void toInnerHtml(Paragraph paragraph, StringBuilder html, boolean inParentheses) {
			
			StringBuilder sb = new StringBuilder();
			if (getTag().size() == 0) {
				this.addTag("span");
			}
			if (getTag().size() > 0) {
//				if (getStartTagsPart1().equals("b")) {
//					
//					Util.p(paragraph);
//					Util.p(this);
//				}
				if (inParentheses) {
					//Util.p("X");
				} else {
					sb.append(String.format("<%s", getStartTagsPart1()));
				}
				if (getAttrs().size() > 0) {
					if (!inParentheses) {
					for (String key : getAttrs().keySet()) {
						if (key.equals("class")) {
							String s = getAttr(key);
							if (paragraph != null) {
							
								String parentClass= paragraph.getAttrs().get("class");
								
								if (parentClass!= null) {
									if (parentClass.indexOf(s) != -1) {
										continue;	
									}
								}
							}
						} 
						sb.append(" ").append(key).append("=\"").append(getAttr(key)).append("\"");
						
					}
					}
				}
				if (!inParentheses) 
				{
					sb.append(getStartTagsPart2());
				}
			}
			String s = null;
			boolean inParentheses2 = false;
			for(Object o:objects) {
				if (o instanceof String) {
					if (s != null) {
						if (((String) o).charAt(0) == ')') {
							inParentheses2 = false;
							if (debug) {
								Util.p(o);
							}
	//						if (sb.indexOf("The poster shown above") != -1) {
	//							debug = true;
	//						}
							if (s.indexOf("http") != -1) {
								s = String.format("<a href=\"%s\">%s</a>", s, s);
							} else {
//								if (s.length() < 10) {
//									Util.p(s);	
//								}
								
							}
							sb.append(s);
							//sb.append("(").append(s).append(")");
							s = null;
						}
					}
					if (s == null) {
						sb.append(o);
						
//						if (sb.indexOf("Figure") != -1) {
//							Util.p(sb);
//						}
					} else {
						s += o;
					}
					
					if (((String) o).charAt(0) == '(') {
						s = "";
						inParentheses2 = true;
					}
					
				} else {
					Holder h = (Holder) o;
					h.toInnerHtml(paragraph, sb, inParentheses2);
				}
			}
			
			Matcher m = figurePattern.matcher(sb);
			if (m.find()) {
				if (inParentheses) {
					//String button = "<div class=\"dropdown dropdown-inline\"><button class=\"btn btn-warning btn-xs dropdown-toggle\" type=\"button\" data-figure-id=\"figure-$1-$2$3\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\"><span class=\"glyphicon glyphicon-info-sign\"></span> Figure $1.$2$3</button><div class=\"dropdown-menu\" aria-labelledby=\"figure-$1-$2$3\"><div class=\"s text-center\"></div></div></div>";
					String button = "<div class=\"dropdown dropdown-inline\"><button class=\"btn btn-warning btn-xs dropdown-toggle\" type=\"button\" data-figure-id=\"figure-$1-$2$3\" data-toggle=\"dropdown\"><span class=\"glyphicon glyphicon-info-sign\"></span> Figure $1.$2$3</button><div class=\"dropdown-menu\" data-label-figure-id=\"figure-$1-$2$3\"><div class=\"s text-center\"></div></div></div>";
					button=button.replaceAll("\\$1", m.group(1));
					button=button.replaceAll("\\$2", m.group(2));
					button= Util.notEmpty(m.group(3)) ? button.replaceAll("\\$3", m.group(3)): button.replaceAll("\\$3", "");
					sb.replace(m.start(), m.end(), button);					
				} else {
					//Util.p(sb);
				}
			}
			
			sb.append(String.format("%s", getEndTags()));
			html.append(sb);
		}
	}
//	static boolean report = false;
	protected void scanLine(VividingStripper stripper, List<LineItem> line, Holder holder) {
		while (mIndex < line.size()) {
			LineItem item = line.get(mIndex);
			if (item.isWordSeparator() || item.isBR()) {
				holder.addPosition(item);
				mIndex++;
				continue;
			}
			
			
			TextPosition textPosition = item.getTextPosition();
//			if (debug) {
//				Util.p(textPosition.getUnicode());
//			}
			//String character = textPosition.getUnicode();
			TextStyle textStyle = TextStyle.getStyle(stripper, textPosition);
			
//			if (report) {
//				Util.p(textPosition);
//				Util.p(textStyle);
//				if (holder.textStyle != null) {
//					Util.p(holder.textStyle);
//				}
//			}
			
//			if (report && holder.textStyle != textStyle) {
//				Util.p(textPosition);
//				if (holder.textStyle != null) {
//					Util.p(holder.textStyle);
//				}
//			}
			int flag = holder.getFlag(textPosition);
			if (holder.textStyle == null || (holder.flag == flag && holder.textStyle == textStyle)) {
				if (holder.textStyle == null) {
					holder.textStyle = textStyle;
				}
				holder.addPosition(item);
				mIndex++;
				continue;
			} else {  //Font not the same, should we wrap it up or contain it????
				//This is not right, ????
				Holder p =  holder.getParent(textStyle, textPosition);
				
				if (p != null) {
					scanLine(stripper, line, p);
					return;
				}
				//
				Holder h = null;
				if (holder.parent != null && holder.flag > 0) { 
					//&& (holder.parent.meanY == (textPosition.getY() + textPosition.getHeight() /2) )) {
					//Need to recalc the position
					h = holder.parent.addTag(textPosition, textStyle);
				} else {
					h = holder.addTag(flag, textStyle);
				}

				scanLine(stripper, line, h);				
			}  
		}
	}
	int mIndex = 0;
	
	static final int BOLD = 0x01;
	static final int ITALIC = 0x02;
	static final int SUP = 0x04;
	static final int SUB = 0x08;
}




