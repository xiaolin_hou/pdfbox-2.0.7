package com.vividing.pdf.tag;

import java.util.Comparator;

public class ParagraphComparator implements Comparator<Tag>
{
    @Override
    public int compare(Tag t1, Tag t2)
    {
    	
    	if (t1 instanceof Paragraph  &&  t2 instanceof Paragraph)
    	{
    		Paragraph paragraph1 = (Paragraph) t1;
    		Paragraph paragraph2 = (Paragraph) t2;
    	//
        if (paragraph1.m_rect.yMin > paragraph2.m_rect.yMin) {
        		return 1;
        } else if (paragraph1.m_rect.yMin < paragraph2.m_rect.yMin) {
        		return -1;
        }
        
        if (paragraph1.m_rect.x - paragraph2.m_rect.x > 0)  {
        		return 1;
        }
        
        if (paragraph1.m_rect.x - paragraph2.m_rect.x < 0)  {
    			return -1;
        }
        return 0;
    }
    	return 0;
    }
}
