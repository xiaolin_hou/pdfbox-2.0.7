package com.vividing.pdf;

import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.text.TextPosition;

public class Matching implements Comparable<Matching> {
	int index;
	String name;
	int length = -1;
	boolean firstOne = true;
	List<TextPosition> textList = new ArrayList<TextPosition>();
	
	public Matching(String name, int index) {
		this.name = name;
		this.index = index;
	}
	@Override
	public int compareTo(Matching o) {
		return index - o.index;
	}
	
}
