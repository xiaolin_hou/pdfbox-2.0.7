package com.vividing.pdf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.font.encoding.GlyphList;
import org.apache.pdfbox.text.TextPosition;

import com.vividing.pdf.part.AnswerKey;
import com.vividing.pdf.part.Appendices;
import com.vividing.pdf.part.Appendix;
import com.vividing.pdf.part.Book;
import com.vividing.pdf.part.Chapter;
import com.vividing.pdf.part.PartInfo;
import com.vividing.pdf.svg.Box;
import com.vividing.pdf.svg.SVGPDFRenderer;
import com.vividing.pdf.svg.Table;
import com.vividing.pdf.svg.element.SVGElement;
import com.vividing.pdf.svg.element.SVGGElement;

public class VividingStripper extends VividingStripperBase {
	public static GlyphList glyphList = null;
	
	public VividingStripper() throws IOException {
		String path = "org/apache/pdfbox/resources/glyphlist/additional.txt";
      InputStream input = GlyphList.class.getClassLoader().getResourceAsStream(path);
      glyphList = new GlyphList(GlyphList.getAdobeGlyphList(), input);
	}

	public void writeText(PDDocument doc) throws IOException {
		resetEngine();
		document = doc;
		processPages(document.getPages());
	}

	int totalPage = -1;

	public List<Box> boxes = new ArrayList<Box>();
	static List<Table> tables = new ArrayList<Table>();
	static public Table getTable(SVGElement e) {
		for (Table t : tables) {
			if (t.pageNum == e.pageNum) {
				if (t.contains(e)) {
					return t;
				}
			}
		}
		return null;
	}
	
	static public List<Table> getTable(int pageNum) {
		List<Table> list = new ArrayList<>();
		for (Table t : tables) {
			if (t.pageNum == pageNum) {
				list.add(t);
			}
		}
		return list;
	}
	
	void loadTableFile() throws IOException {
		File tableFile = new File(getConfigDir() + "table.tsv"); 
		if (tableFile.exists()) {
			try {
				loadTableFile(tableFile);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	void loadTableFile(File tableFile) throws IOException{
		// Open the file
		FileInputStream fstream = new FileInputStream(tableFile);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

		String strLine = br.readLine();
		//System.out.println(strLine);
		//Read File Line By Line
		while ((strLine = br.readLine()) != null)   {
			//System.out.println(strLine);
			String[] s = strLine.split("\t");
						
			int page = Integer.parseInt(s[0]);
			
			Table b = new Table(page, Float.parseFloat(s[1]), Float.parseFloat(s[2]), Float.parseFloat(s[3]), Float.parseFloat(s[4]), s[5]);
			b.header = (s.length > 6)? s[6] : null;
			b.title = (s.length > 7) ? s[7] : null;
			tables.add(b);
		}
		br.close();
	}
	void loadGapFile() throws IOException{
		File gapFile = new File(getConfigDir() + "gaps.tsv"); 
		if (gapFile.exists()) {
			try {
				loadGapFile(gapFile);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	void loadGapFile(File file) throws IOException{
		// Open the file
		FileInputStream fstream = new FileInputStream(file);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String strLine = br.readLine();
		//System.out.println(strLine);
		//Read File Line By Line
		while ((strLine = br.readLine()) != null)   {
			//System.out.println(strLine);
			String[] s = strLine.split("\t");
			
			if (s.length !=2) {
				System.err.println("XXX");
			}
			
			LINEGAPS.put(s[0], Float.parseFloat(s[1]));
		}
		br.close();
	}
	
	public static Map<String, Float> LINEGAPS = new LinkedHashMap<String, Float>();
	void loadTocFile(File tocFile) throws IOException{
		// Open the file
		FileInputStream fstream = new FileInputStream(tocFile);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

		String strLine;

		//Map<String, PartInfo> maps = new HashMap<String, PartInfo>();
		String header = br.readLine();
		//System.out.println(header);
		//Read File Line By Line
		Chapter lastChapter = null;
		//Appendices appendices = null;
		while ((strLine = br.readLine()) != null)   {
			String[] s = strLine.split("\t");
			//Util.p(strLine);
			
			if (s.length > 6) {
				PartInfo info = null;
				
				//We are going to TREAT APPENDIX AS Section, all the APPENDIX will be in a Chpater..
				if (s[0].equalsIgnoreCase(CHAPTER)) {
					info = lastChapter = new Chapter(s[1], s[0], s[2], Integer.parseInt(s[3]));
						
//					String c = s[1].replace("Chapter ", "").trim();
//					info.chapter =  Integer.parseInt(c);
//					info.endPage = Integer.parseInt(s[4]);
				} 
				else if (s[0].equalsIgnoreCase(APPENDIX)) {
					info = new Appendix(s[1], s[0], s[2], Integer.parseInt(s[3]));
				}
				else if (s[0].equals(ANSWERKEY)) {
				
					info = new AnswerKey(s[1], s[0], s[2], Integer.parseInt(s[3])); 
				} else {
					info = new PartInfo(s[1], s[0], s[2], Integer.parseInt(s[3]));
					if (s[0].equalsIgnoreCase(SECTION)) {
						String s1 = s[1].replace("Chapter ", "").trim();
						String[] c = s1.split("\\.");
						if (c.length == 2) {
							//info.chapter =  Integer.parseInt(c[0]);
							info.section =  Integer.parseInt(c[1]);
						} else {
							System.err.println("XXXXX");
						}
					}
				}		
				info.endPage = Integer.parseInt(s[4]);
				info.chapter = Integer.parseInt(s[6]);
				//info.parent =  
//				maps.put(s[1], info);
//				if (s.length == 6 && s[5] != null) {
//					PartInfo p = maps.get(s[5]);
//					if (p != null) {
//						info.parent = p;
//					}
//				}
				tocList.add(info);
			} else {
				Util.err("ERROR S length == " + s.length);
			}
			// Print the content on the console
		  //System.out.println (strLine);
		}

		//Close the input stream
		br.close();
	}

	protected void processPages(PDPageTree pages) throws IOException {
		totalPage = pages.getCount();
		try {
			loadTableFile();
			loadGapFile();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		SVGPDFRenderer renderer = new SVGPDFRenderer(this, document);
		List<List<TextPosition>> textList = new ArrayList<List<TextPosition>>();
		System.out.println("Loading Book...");
		int tocStart = getTocStartPage();
		int tocEnd = getTocEndPage();
		System.out.println("Loading Table Of Contents from "
				+ String.format("Start=%s End=%s", getTocStartPage(), getTocEndPage()));
		boolean withToc = true;
		if (tocStart > tocEnd || tocStart == -1) {
			System.err.println("Warning, NO TOC INFO");
			withToc = false;
			// System.exit(0);
		}

		Book pdfBook = new Book(getTitle(), 1);
		StringBuilder style = new StringBuilder();
		if (withToc) {
			
			// if found tsv file, load it
			File tocFile = new File(getConfigDir() +VividingStripper.TOC + ".tsv"); 
			if (tocFile.exists()) {
				try {
					loadTocFile(tocFile);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				pdfBook.loadToc(this, pages, renderer, tocStart, tocEnd);
//				for (currentPageNo = tocStart - 1; currentPageNo <= tocEnd - 1; currentPageNo++) {
//					PDPage page = pages.get(currentPageNo);
//	
//					if (page.hasContents()) {
//						processPage(page, textList);
//					}
//				}
				// Now, we got the infomation from TOC, Process it
//				for (List<TextPosition> posList : textList) {
//					processToc(posList, 0, -1);
//				}
			}
			System.out.println("Done with TOC");
			System.out.println("Analying...");
			// Scan the information as much as possible ....

			

			// New we have tocMap can use To Scan the rest of Page, and CUP into
			// Parts ...
			textList.clear();

			System.out.println("Processing...");
			// currentPageNo will be using the book #, not the document #, book
			// start from Preface
			
			
			scanBookInfo(pdfBook);
			pdfBook.process(this, pages, renderer);
			

			System.out.println("Done...\nReporting");
			//svgThing();
			// TOC

			//Util.toFile(this, Util.getToc(this, this.tocMap), style, VividingStripper.TOC, false);
			//Util.toFile(this, , style, );
			
			//Util.saveFile(new File(getOutputDir() +  VividingStripper.TOC + ".html"), Util.getToc(this, this.tocMap));
			
			Util.toFile(this, Util.getToc(this, this.tocList), new StringBuilder(),VividingStripper.TOC ,
					getOutputDir() +  VividingStripper.TOC + ".html");
			
			Util.saveFile(new File(getDataDir() +  VividingStripper.TOC + ".tsv"), 
					pdfBook.toTSV());
					
							
			// Now, here we go. We got something here, right????
			// pdfBook.pages.getCount()
		} else {
			//if (!withChapter) {
			oneChapterOnly = true;
			pdfBook.setOneChapterOnly(this, pages.getCount());
			//} 
			// in this case, only one Chapter for the whole book ;)
			//this.setEndPage(Math.min(this.getEndPage(), pages.getCount()));
			pdfBook.process(this, pages, renderer);
			//svgThing();
		}

		Util.saveFile(new File(getDataDir() + "box.tsv"), Util.getBoxesAsTSV(this.boxes));
		
		Util.saveFile(new File(getDataDir() + "gaps.tsv"), Util.getGapAsTsv(SVGGElement.LINEGAPS));
		Util.saveFile(new File(getDataDir() + "images.tsv"), Util.getImageListAsTsv(imageMap));
		//Util.saveFile(new File(getDataDir() + "fileName.txt"), Util.getImageListAsTsv(fileNameIdMap));
		
//		for(String key:SVGGElement.LINEGAPS.keySet()) {
//			if (SVGGElement.WARNING_COUNT.containsKey(key)) {
//				SVGGElement.WARNING_COUNT.remove(key);
//			}
//		}
		
		
		pdfBook.toHtml(this, style);
		
		Util.saveFile(new File(getDataDir() + "gaps-warning.tsv"), Util.getGapAsTsv(SVGGElement.WARNING_COUNT));
		//Something might not right, but...
		StringBuilder font = new StringBuilder();
		
		for(TextStyle ts : TextStyle.classMap.values()) {
			style.append('.').append(ts.getId()).append("{").append(ts).append("}\n");
		}
		
		for(String fontFamily: TextStyle.pdFontMap.keySet()) {
			font.append(" @font-face{font-family:").append(fontFamily).append(";");
			font.append("src: url(\"fonts/").append(fontFamily).append(".ttf\") format(\"truetype\");}\r\n");
			
			try {
				Util.writeFont(TextStyle.pdFontMap.get(fontFamily), this.getFontsDir());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		style.append("\n").append(font);
		// System.out.println("Style size = " + classMap.size());
		// System.out.println(style);
		Util.toStyleFile(this, style);
		// this.getFonts();
		//Line.report();
		System.out.println("Book Loaded");
	}
	public static Map<String, String> imageMap = new LinkedHashMap<>();
	//public static Map<String, String> fileNameIdMap = new LinkedHashMap<>();
	List<PartInfo> tocList = new ArrayList<PartInfo>();
	String lastPart = "";

	int lastChapter = 0;
	int lastSection = 0;
	private void getTocInfo(Line line) {
		
		if (Util.containOrMatch(this, line.text, this.getProp())) {
			return;
		}
		getTocInfo(line.text);
	}
	
	public void getTocInfo(StringBuilder line) {
		String text = line.toString().trim();
		
		//System.out.println(text);
		text = text.replace(" . ", "$");
		System.out.println(text);
		if (text.indexOf("Index") == 0) {
			String pattern = "Index [\\$]+(\\d+)";
			if (INDEXPATTERN != null && INDEXPATTERN.length() > 0) {
				pattern = INDEXPATTERN;
			}

			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(text);
			if (m.find()) {
				PartInfo info = new PartInfo("Index", INDEX, "Index", Integer.parseInt(m.group(1)));
				tocList.add(info);
			}
		} 
		else if (text.indexOf("Preface") == 0) {
			lastPart = "Preface";
			Pattern r = Pattern.compile("^Preface[\\s]?[\\$]?(\\d+)");
			Matcher m = r.matcher(text);
			if (m.find()) {
				PartInfo info = new PartInfo("Preface", PREFACE, "Preface", Integer.parseInt(m.group(1)));
				tocList.add(info);
			}
		} else if (text.indexOf("Unit ") == 0) {
			lastPart = "Unit";
			String pattern = "(Unit.*)\\:(.*)";
			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(text);
			if (m.find()) {
				PartInfo info = new PartInfo(m.group(1), lastPart, m.group(2), -1);
				tocList.add(info);
			}
		} else if (text.indexOf(APPENDIX) != -1) {
			lastPart = APPENDIX;
			String pattern = "^Appendix ([A-Z]):([^\\$]*)[\\$]+([\\d]+)";
			
			if (APPENDIXPATTERN != null && APPENDIXPATTERN.length() > 0) {
				pattern = APPENDIXPATTERN;
			}
			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(text);
			// System.out.println(text);

			if (m.find()) {
				// System.out.println(m.group(2));
				// PartInfo info = new PartInfo(APPENDIX + "-" + m.group(1),
				// lastPart, m.group(2), Integer.parseInt(m.group(3)));
				PartInfo info = new Appendix(m.group(1), APPENDIX, m.group(2), Integer.parseInt(m.group(3)));
				info.subType = APPENDIX;
				tocList.add(info);
			}
		}
		else if (text.indexOf("Chapter") == 0) {
			lastPart = CHAPTER;
			//String pattern = "(Chapter)[\\s]*(\\d+)\\:([\\w\\s\\:\\,\'!\\?–\\-\\d]*)[\\.\\s]*([\\d]+)$";
			String pattern = "(Chapter)[\\s]*([\\d]+):([^\\$]*)[\\$]+([\\d]+)$";
			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(text);
			if (m.find()) {

				PartInfo info = new Chapter(m.group(1) + " " + m.group(2), lastPart, m.group(3),
						Integer.parseInt(m.group(4)));
				lastChapter = info.chapter = Integer.parseInt(m.group(2));
				lastSection = 0;
				
				tocList.add(info);
			}
		} else if (text.indexOf("Table of Contents") >= 0) {
		} else {
			if (CHAPTERPARTEN != null && CHAPTERPARTEN.length() > 0) {
				Pattern r = Pattern.compile(CHAPTERPARTEN);
				Matcher m = r.matcher(text);
				if (m.find()) {
					lastPart = CHAPTER;
					PartInfo info = new Chapter(CHAPTER + " " + m.group(1), lastPart, m.group(2),
							Integer.parseInt(m.group(3)));
					if (m.group(1) != null && m.group(1).length() > 0) {
						info.chapter = Integer.parseInt(m.group(1));
					} else {
						info.chapter = 1;
					}
					lastChapter = info.chapter;
					lastSection = 0;
					// System.out.println(m.group(1) + ", " + m.group(2) + "," +
					// m.group(3));
					tocList.add(info);
					return;
				}
			}

			if (SUBSECTIONPATTERN != null && SUBSECTIONPATTERN.length() > 0) {
				Pattern r = Pattern.compile(SUBSECTIONPATTERN);
				Matcher m = r.matcher(text);
				if (m.find()) {
					PartInfo info = new PartInfo("Chapter " + m.group(1) + "." + m.group(2) + "." + m.group(3), SUBSECTION, m.group(4),
							Integer.parseInt(m.group(5)));
					info.chapter = Integer.parseInt(m.group(1));
					info.section = Integer.parseInt(m.group(2));
					info.subSection = Integer.parseInt(m.group(3));
					tocList.add(info);
					return;
				}
			}
			
			if (SECTIONPATTERN != null && SECTIONPATTERN.length() > 0) {
				Pattern r = Pattern.compile(SECTIONPATTERN);
				Matcher m = r.matcher(text);
				if (m.find()) {
					PartInfo info = new PartInfo("Chapter " + m.group(1) + "." + m.group(2), SECTION, m.group(3),
							Integer.parseInt(m.group(4)));
					
					if (m.group(1) != null && m.group(1).length() > 0){
						info.chapter = Integer.parseInt(m.group(1));
					} else {
						info.chapter = lastChapter;
					}
					if (m.group(2) != null && m.group(1).length() > 0) {
						info.section = Integer.parseInt(m.group(2));
					} else {
						info.section = ++lastSection;
					}
					info.key="Chapter " + info.chapter + "." + info.section;
					tocList.add(info);
					return;
				}
			}
			if (APPENDIXPATTERN != null && APPENDIXPATTERN.length() > 0) {
				Pattern r = Pattern.compile(APPENDIXPATTERN);
				Matcher m = r.matcher(text);
				if (m.find()) {
					PartInfo info = new Appendix(m.group(1), APPENDIX, m.group(2), Integer.parseInt(m.group(3)));
					info.subType = APPENDIX;
					tocList.add(info);
					return;
				}
			}

			lastPart = CHAPTER;
			String pattern = "(\\d+)\\.(\\d+)([a-z\\sA-Z\\?\\-\\,]+)([\\$])+(\\d+)";
			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(text);
			// System.out.println(text);
			if (m.find()) {

				PartInfo info = new PartInfo("Chapter " + m.group(1) + "." + m.group(2), SECTION, m.group(3), Integer.parseInt(m.group(5)));
				info.chapter = Integer.parseInt(m.group(1));
				info.section = Integer.parseInt(m.group(2));
				tocList.add(info);
			}
		}
	}

	int m_index = 0;
	void scanBookInfo(Book book) {
		PartInfo lastSection = null;
		PartInfo lastChapter = null;
		PartInfo lastSubSection = null;
		Appendix lastAppendix = null;
		for (PartInfo info : tocList) {
			if (info.type.equals(PREFACE)) {
//				if (book.preface == null) {
//					book.put(info);
//				}
				book.setPreface(info);
			} else if (info.type.equals(CHAPTER)) {
				
			
				// chapter++;
				if (lastSection != null) {
					//lastSection.endPage = info.page; // THIS IS WRONG...., WE
														// NEED SCAN KEY TERMS
														// ...
					if (lastSection != null) {
						lastSection.endPage = (info.page > lastSection.page) ? info.page -1 : lastSection.page;
					}
					lastSection = null;
				}
				if (book.getPreface() != null && book.getPreface().endPage == -1) {
					book.getPreface().endPage = info.page - 1;
					// System.out.println("Set Preface END of page " +
					// book.getPreface().endPage );
				} else if (lastChapter != null) {
					lastChapter.endPage = info.page - 1;
					// System.out.println("Set Chapter END at " +
					// lastChapter.endPage );
				}

				// Now move on
				lastChapter = info;
				lastChapter.parent = book;
				// info.parent =
				if (lastChapter instanceof Chapter) {
					book.chaptersMap.put(info.key, (Chapter) lastChapter);
				} else {
					System.err.println("What is this??? " + lastChapter.getClass().getName()
							+ ",  "+ lastChapter.type);
				}

			} else if (info.type.equals(APPENDIX)) {
				
				int chapterNum = -1;
				if (lastChapter != null) {
					if (lastChapter.endPage == -1) {
						lastChapter.endPage = info.page-1;
					}
					chapterNum = lastChapter.chapter;
					// System.out.println("Set Chapter END at " +
					// lastChapter.endPage );
				}
				// Now move on
				//lastChapter = info;
				lastChapter.parent = book;
				lastChapter = book.getAppendices();
				
				if (lastChapter.page == -1) {
					lastChapter.page = info.page;
					lastChapter.chapter = chapterNum + 1;
				}
				
				if (lastAppendix != null) {
					lastAppendix.endPage = info.page - 1;
				}
				lastAppendix = (Appendix)info;
				lastAppendix.chapter = lastChapter.chapter;
				((Appendices)lastChapter).add(lastAppendix);
				//book.chaptersMap.put(info.key, (Chapter) lastChapter);
				// info.parent =
				// book.appendixMap.put(info.key, info);

			} else if (info.type.equals(SECTION)) {
				if (lastChapter == null) {
					System.err.println("VERY WRONG, section = \n" + info);
				} else {
					if (lastSubSection != null) {
						lastSubSection.endPage = (info.page > lastSubSection.page) ? info.page -1 : lastSubSection.page;
						lastSubSection = null;
					}
					
					if (lastSection != null) {
						lastSection.endPage = (info.page > lastSection.page) ? info.page -1 : lastSection.page;
					}
					if (lastChapter instanceof Chapter) { 
						((Chapter) lastChapter).sections.put(info.key, info);
					} else {
						System.err.println("What is this??? " + lastChapter.getClass().getName()
								+ ",  "+ lastChapter.type);
					}
					info.parent = lastChapter;
				}

				lastSection = info;

			} else if (info.type.equals(SUBSECTION)) {
				if (lastChapter == null || lastSection == null) {
					System.err.println("VERY WRONG, section = \n" + info);
				} else {
					if (lastSubSection != null) {
						lastSubSection.endPage = (info.page > lastSubSection.page) ? info.page -1 : lastSubSection.page;
					}
					lastSection.children.put(info.key, info);
					info.parent = lastSection;
				}

				lastSubSection = info;

			} else if (info.type.equals(ANSWERKEY)) {
				if (lastChapter == null) {
					System.err.println("VERY WRONG, in INDEX = \n" + info);
				} else {
					if (lastSection != null) {
						lastSection.endPage = info.page;
					}

					if (lastChapter.endPage == -1) {
						lastChapter.endPage = info.page-1;
					}
					info.parent = book;
					//info.endPage = totalPage;
					// info.parent =
					book.setAnswerKey((AnswerKey)info);
				}
				if (lastAppendix != null) {
					lastAppendix.endPage = info.page - 1;
					lastAppendix = null;
				}
				lastSection = info;
			}else if (info.type.equals(REFERENCES)) {
				if (lastAppendix != null) {
					lastAppendix.endPage = info.page - 1;
					lastAppendix = null;
				}
				if (lastChapter == null) {
					System.err.println("VERY WRONG, in INDEX = \n" + info);
				} else {
					if (lastSection != null) {
						lastSection.endPage = info.page;
					}

					if (lastChapter.endPage == -1) {
						lastChapter.endPage = info.page-1;
					}
					info.parent = book;
					//info.endPage = totalPage;
					// info.parent =
					book.setReference(info);
				}
				lastSection = info;
			}
//			else if (info.type.equals(APPENDICES)) {
//				
//				info.parent = book;
//				if (info.endPage == -1) {
//					info.endPage = totalPage;
//				}
//				// info.parent =
//				book.setAppendices((Appendices)info);
//				
//			}
			 else if (info.type.equals(INDEX)) {
				 if (lastAppendix != null) {
						lastAppendix.endPage = info.page - 1;
						lastAppendix = null;
					}
				 if (lastChapter == null) {
					System.err.println("VERY WRONG, in INDEX = \n" + info);
				} else {
					if (lastSection != null) {
						lastSection.endPage = info.page-1;
					}

					if (lastChapter.endPage == -1) {
						lastChapter.endPage = info.page-1;
					}
					
					//lastChapter
					info.parent = book;
					if (info.endPage == -1) {
						info.endPage = totalPage;
					}
					// info.parent =
					book.setIndex(info);
				}
				lastSection = info;
			}
		}
	}
	
	
}
