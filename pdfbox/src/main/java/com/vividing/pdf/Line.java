package com.vividing.pdf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.text.TextPosition;

public final class Line {
	public static Rect m_border = new Rect();
	static Map<String, Integer> leftMostCount = new HashMap<String, Integer>();
	public Rect m_rect = new Rect();
	List<LineItem> line = new ArrayList<LineItem>();
	List<TextPosition> textPositionList = new ArrayList<TextPosition>();
	/**
	 * @return the textPositionList
	 */
	public List<TextPosition> getTextPositionList() {
		return textPositionList;
	}

	public StringBuilder text = new StringBuilder();
	
	
	public Rect getM_rect() {
		return m_rect;
	}
	
	public List<LineItem> getLine() {
		return line;
	}
	public void addAll(List<LineItem> line) {
		for(LineItem l : line) {
			if (l.getTextPosition() != null) {
				m_border.setRect(l.getTextPosition());
				m_rect.setRect(l.getTextPosition());
				text.append(Util.escape(l.getTextPosition().getUnicode()));
				textPositionList.add(l.getTextPosition());
			}
			if (l.isWordSeparator()) {
				text.append(" ");
			}
			//line.add(l);
		}
		this.line.addAll(line);
	}
	
	public void add(TextPosition p) {
		m_border.setRect(p);
		m_rect.setRect(p);
		//wordText += p.getUnicode();
		text.append(Util.escape(p.getUnicode()));
		line.add(new LineItem(p));
		textPositionList.add(p);
	}
	
	public void add(TextPosition p, TextStyle fontStyle) {
		m_border.setRect(p);
		m_rect.setRect(p);
		text.append(Util.escape(p.getUnicode()));
		line.add(new LineItem(p, fontStyle));
		textPositionList.add(p);
	}
	
	public void addBR() {
		text.append("<br>");
		//lastWordText = wordText;
		//wordText = "";
		line.add(LineItem.getBR());
	}
	
	public void addSpace() {
		text.append(" ");
//		lastWordText = wordText;
//		wordText = "";
		line.add(LineItem.getWordSeparator());
	}
	
	static public void report() {
//		if (VividingStripper.report) {
//			System.out.println("Border >> " + m_border);
//			System.out.println("Header >> " + m_border.toHeader());
//			System.out.println("Footer >> " + m_border.toFooter());
//			System.out.println("Left Most\n");
//			
//			for (String key: leftMostCount.keySet()) {
//				if (leftMostCount.get(key).intValue() > 1) {
//					System.out.println(key + " === " + leftMostCount.get(key));
//				}
//			}
//		}
	}
	
	public void clear() {
		this.clear(false);
	}
	
	public void clear (boolean skip) {
		if (VividingStripper.NOSECTION) {
			if (!skip && line.size() > 0) {
			//LineItem l = line.get(0);
			//String key = String.valueOf(l.getTextPosition().getX());
//			String key = String.valueOf(m_rect.x);
//			Integer v = leftMostCount.get(key);
//			
//			if (v != null) {
//				v = new Integer(v.intValue() + 1);
//				//System.out.println(text);
//				//System.out.println(m_rect);
//				//System.out.println(l.getTextPosition().getUnicode() + " == " + v);
//			} else {
//				v =  new Integer(1);
//			}
//			
//			leftMostCount.put(key, v);
		}
	}
		//lastWordText = wordText = "";
		text = new StringBuilder();
		//m_rect.x = m_rect.yMin = m_rect.right = m_rect.bottom = 0;
		line.clear();
	}

	int size() {
		return line.size();
	}
	
	public boolean remove(int at) {
		if (at < size()) {
			if (null != line.remove(at)) {
				text.deleteCharAt(at);
//				m_border.setRect(l.getTextPosition());
//				m_rect.setRect(l.getTextPosition());
				return true;
			}
			
		} 
		return false;
	}
	
}