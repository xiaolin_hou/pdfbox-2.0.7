package com.vividing.pdf;

import org.apache.pdfbox.text.TextPosition;

/**
 * internal marker class. Used as a place holder in a line of TextPositions.
 */
public final class LineItem
{
    public static LineItem WORD_SEPARATOR = new LineItem();
    public static LineItem BR = new LineItem();
    
    public static LineItem getWordSeparator()
    {
        return WORD_SEPARATOR;
    }

    public static LineItem getBR()
    {
        return BR;
    }
    private final TextPosition textPosition;
    private TextStyle textStyle =null;
	
	private LineItem()
    {
        textPosition = null;
    }

    LineItem(TextPosition textPosition)
    {
        this.textPosition = textPosition;
    }

    LineItem(TextPosition textPosition, TextStyle textStyle)
    {
        this.textPosition = textPosition;
        this.textStyle = textStyle;
    }
    
    public TextPosition getTextPosition()
    {
        return textPosition;
    }
    
    public boolean isWordSeparator() {
    		return this == WORD_SEPARATOR; 
    }

    public boolean isBR() {
		return this == BR; 
    }
 
    public TextStyle getTextStyle() {
		return textStyle;
	}
    
}