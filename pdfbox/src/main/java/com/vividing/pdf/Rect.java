package com.vividing.pdf;

import java.awt.geom.Point2D;

import org.apache.pdfbox.text.TextPosition;

public class Rect {
//	//We should use the page rect, but I don't have time to figure it out/
//	public float leftMost = Integer.MAX_VALUE;
//	public float rightMost = 0;

	public float x = 0;
	public float xWidth = 0;
	public float yMin = 0;
	public float yHeight = 0;
	
	public float yMax = 0; //For compute bottom????
	public float right = 0;
	public float rightWidth = 0;
	public float bottom = 0;
	public float bottomHeight = 0;
	
	public Rect(String rect) throws RectFormatException {
		
		String[] ss = rect.split(",");
		if (ss.length == 4) {
			x = Float.valueOf(ss[0]);
			yMin = Float.valueOf(ss[1]);
			right = Float.valueOf(ss[2]);
			bottom = Float.valueOf(ss[3]);
		} else {
			throw new RectFormatException();
		}
	}
	public Rect() {
		x = 0;
		yMin = 0;
		right = 0;
		bottom = 0;
		xWidth = 0;
		yHeight = 0;
		rightWidth = 0;
		bottomHeight = 0; 
	}
	
	public boolean in(Point2D point) {
		return 
				(x <= point.getX() && yMin <= point.getY() 
				&& right >= point.getX()
				&& bottom >= point.getY());
	}
	
	public void setRect(Rect r)
	{
		if (x == 0) {
			 x = r.x;
		 } else {
			 x = Math.min(x, r.x);
		 }
		if (xWidth == 0) {
			xWidth = r.xWidth;
		 } else {
			 xWidth = Math.min(xWidth, r.xWidth);
		 }
		 if (yMin == 0) {
			 yMin = r.yMin;
		 } else {
			 yMin = Math.min(yMin, r.yMin);
		 }
		 
		 if (yHeight == 0) {
			 yHeight = r.yHeight;
		 } else {
			 yHeight = Math.min(yHeight, r.yHeight);
		 }
		 
		if (yMax == 0) {
			yMax = r.yMax;
		 } else {
			yMax = Math.max(yMax, r.yMax);
		 }
		 if (right == 0) {
			 right = r.right;
		 } else {
			 right = Math.max(right, r.right);
		 }
		 
		 if (bottom == 0) {
			 bottom = r.bottom;
		 } else {
			 bottom = Math.max(bottom, r.bottom);
		 }
		 
		 if (rightWidth == 0) {
			 rightWidth = r.rightWidth;
		 } else {
			 rightWidth = Math.min(rightWidth, r.rightWidth);
		 }
		 
		 if (bottomHeight == 0) {
			 rightWidth = r.rightWidth;
		 } else {
			 rightWidth = Math.min(rightWidth, r.rightWidth);
		 }
	}
	
//	void setPage() {
//		leftMost = Math.min(x, leftMost);
//		 rightMost = Math.min(right, rightMost);
//	}
	void setRect(TextPosition p)
	{
		if (x == 0) {
			x = p.getX();
			xWidth = p.getWidth();
		}
		x = Math.min(x, p.getX());
		
		if (yMin == 0) {
			yMin = p.getY();
			yHeight = p.getHeight();
		}
		
		if (yMax == 0) {
			yMax = p.getY();
		}
		
		yMin = Math.min(yMin, p.getY());
		yMax = Math.min(yMax, p.getY());
		
		if (right == 0) {
			right = p.getX() + p.getWidth();
			rightWidth = p.getWidth();
		}
		right = Math.max(right, p.getX() + p.getWidth());
		
		if (bottom == 0) {
			bottom = p.getY() + p.getHeight();
			bottomHeight = p.getHeight();
		}
		bottom = Math.max(bottom, p.getY() + p.getHeight());
		//setPage();
	}
	public String toString() {
		return String.format("[x=%s,y=%s,right=%s,bottom=%s]", x,yMin,right, bottom)
				+ String.format("\n[x=%s,y=%s,right=%s,bottom=%s]", (x+xWidth),yMin+yHeight,right-rightWidth, bottom-bottomHeight);
	}
	
	public String toHeader() {
		//return String.format("\n[x=%s,y=%s,right=%s,bottom=%s, bottom2=%s]", x, yMin, right, yMax+yHeight, yMin+yHeight);
		return String.format("\n[%s,%s,%s,%s]", x, yMin, right, yMin+yHeight)
				+String.format("\n[%s,%s,%s,%s]", 0, 0, 2 * right, yMin+yHeight);
	}
	
	public String toFooter() {
		//return String.format("\n[x=%s,y=%s,right=%s,bottom=%s]", (x),bottom-bottomHeight,right-rightWidth, bottom);
		return String.format("\n[%s,%s,%s,%s]", (x),bottom-bottomHeight,right, bottom)
				+ String.format("\n[%s,%s,%s,%s]", (0),bottom-bottomHeight, 2 * right, bottom);
	}
}