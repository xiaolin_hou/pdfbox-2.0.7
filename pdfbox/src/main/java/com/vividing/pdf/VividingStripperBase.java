package com.vividing.pdf;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.text.Bidi;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineItem;
import org.apache.pdfbox.pdmodel.interactive.pagenavigation.PDThreadBead;
import org.apache.pdfbox.text.TextPosition;

abstract public class VividingStripperBase { 
//extends LegacyPDFStreamEngine {
	protected static float defaultIndentThreshold = 2.0f;
	protected static float defaultDropThreshold = 2.5f;
	protected static final Log LOG = LogFactory.getLog(VividingStripper.class);

	
	public VividingStripperBase() throws IOException {
	}

	// enable the ability to set the default indent/drop thresholds
	// with -D system properties:
	// pdftextstripper.indent
	// pdftextstripper.drop
	static {
		String strDrop = null, strIndent = null;
		try {
			String className = VividingStripper.class.getSimpleName().toLowerCase();
			String prop = className + ".indent";
			strIndent = System.getProperty(prop);
			prop = className + ".drop";
			strDrop = System.getProperty(prop);
		} catch (SecurityException e) {
			// PDFBOX-1946 when run in an applet
			// ignore and use default
		}
		if (strIndent != null && strIndent.length() > 0) {
			try {
				defaultIndentThreshold = Float.parseFloat(strIndent);
			} catch (NumberFormatException nfe) {
				// ignore and use default
			}
		}

		if (strDrop != null && strDrop.length() > 0) {
			try {
				defaultDropThreshold = Float.parseFloat(strDrop);
			} catch (NumberFormatException nfe) {
				// ignore and use default
			}
		}
	}

	static {
		// check if we need to use the custom quicksort algorithm as a
		// workaround to the PDFBOX-1512 transitivity issue of
		// TextPositionComparator:
		boolean is16orLess = false;
		try {
			String version = System.getProperty("java.specification.version");
			StringTokenizer st = new StringTokenizer(version, ".");
			int majorVersion = Integer.parseInt(st.nextToken());
			int minorVersion = 0;
			if (st.hasMoreTokens()) {
				minorVersion = Integer.parseInt(st.nextToken());
			}
			is16orLess = majorVersion == 1 && minorVersion <= 6;
		} catch (SecurityException x) {
			// when run in an applet ignore and use default
			// assume 1.7 or higher so that quicksort is used
		} catch (NumberFormatException nfe) {
			// should never happen, but if it does,
			// assume 1.7 or higher so that quicksort is used
		}
	}

	/**
	 * The platform's line separator.
	 */
	public static final String LINE_SEPARATOR = System.getProperty("line.separator");
	public static boolean NOSECTION = false;
	//protected boolean addWrodSpace = true;
	public String lineSeparator = LINE_SEPARATOR;
	protected String wordSeparator = " ";
	protected String bookId = "book-id";
	protected int tocStartPage = -1;
	protected int bookPageStart = -1;
	static public boolean oneChapterOnly = false;
	
	static public boolean withChapter = false;
	static public boolean NOSTYLE = false;
	static public String bookBy = null;
	static public int DISPLAYITEMS = 10;
	
	/**
	 * @return the oneChapterOnly
	 */
	static public boolean isOneChapterOnly() {
		return oneChapterOnly;
	}
	public int getBookPageStart() {
		if (bookPageStart == -1) {
			return tocEndPage;
		}
		return bookPageStart;
	}

	protected int chapterStartPage = -1;
	protected int tocEndPage = -1;
	static public boolean useTable = true;
	public boolean pageBreak = false; //force to break the paged
	static public boolean TABLE_NO_LEFT_RIGHT_BORDER = false;
	static public boolean TABLE_NO_HEADER_SEPARATOR = false;
	public static boolean wordSpace = true; //Adding space for new world, but for Chinese, this is no need 
	protected boolean allImage = false;
	protected int stripeImageWidth = 204;
	//protected int column2X = -1;
	protected Properties prop = null;
	static protected List<Rect> boxes = null;
	static protected List<Rect> skipBoxes = null;
	//static public boolean NOSOURTING = false;
	public static List<Rect> getSkipBoxes() {
		return skipBoxes;
	}

	public static void setSkipBoxes(List<Rect> skipBoxes) {
		VividingStripperBase.skipBoxes = skipBoxes;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public static List<Rect> getBoxes() {
		return boxes;
	}

	public static void setBoxes(List<Rect> boxes) {
		VividingStripperBase.boxes = boxes;
	}

	public Properties getProp() {
		return prop;
	}

	public void setProp(Properties prop) {
		this.prop = prop;
		boxes = Util.getBoxes(this, prop);
		skipBoxes = new ArrayList<Rect>();
		Rect r = Util.getBox(prop, "headBox");
		if (r != null) {
			skipBoxes.add(r);
		}
		r = Util.getBox(prop, "footerBox");
		if (r != null) {
			skipBoxes.add(r);
		}
		
		r = Util.getBox(prop, "leftBox");
		if (r != null) {
			skipBoxes.add(r);
		}
		r = Util.getBox(prop, "rightBox");
		if (r != null) {
			skipBoxes.add(r);
		}
		if (prop.getProperty("imageWidth") != null) {
			try {
				stripeImageWidth = Integer.valueOf(prop.getProperty("imageWidth"));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (prop.getProperty("imageHeight") != null) {
			try {
				stripeImageHeight = Integer.valueOf(prop.getProperty("imageHeight"));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (prop.getProperty("tocStartPage") != null) {
			try {
				tocStartPage = Integer.valueOf(prop.getProperty("tocStartPage"));
			} catch (NumberFormatException e) {
				tocStartPage = -1;
			}
		}
		if (prop.getProperty("bookPageStart") != null) {
			try {
				bookPageStart = Integer.valueOf(prop.getProperty("bookPageStart"));
			} catch (NumberFormatException e) {
				bookPageStart = -1;
			}
		}
		
		if (prop.getProperty("LAYOUT") != null) {
			try {
				LAYOUT = Integer.valueOf(prop.getProperty("LAYOUT"));
			} catch (NumberFormatException e) {
				LAYOUT = -1;
			}
		}
		
		
		if (prop.getProperty("tocEndPage") != null) {
			try {
				tocEndPage = Integer.valueOf(prop.getProperty("tocEndPage"));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				tocEndPage = -1;
			}
		}
		if (prop.getProperty("startPage") != null) {
			try {
				startPage = Integer.valueOf(prop.getProperty("startPage"));
			} catch (NumberFormatException e) {
				startPage = 0;
			}
		}
		if (prop.getProperty("endPage") != null) {
			try {
				endPage = Integer.valueOf(prop.getProperty("endPage"));
			} catch (NumberFormatException e) {
				endPage = Integer.MAX_VALUE;
			}
		}

		if (prop.getProperty("firstTime") != null) {
			try {
				firstTime = Boolean.valueOf(prop.getProperty("firstTime"));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				firstTime = true;
			}
		}
		if (prop.getProperty("MULTICOLUMNS") != null) {
			try {
				MULTICOLUMNS = Boolean.valueOf(prop.getProperty("MULTICOLUMNS"));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				MULTICOLUMNS = true;
			}
		}
		
		if (prop.getProperty("withChapter") != null) {
			try {
				withChapter = Boolean.valueOf(prop.getProperty("withChapter"));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				withChapter = false;
			}
		}
		
		if (prop.getProperty("DISPLAYITEMS") != null) {
			try {
				DISPLAYITEMS = Integer.valueOf(prop.getProperty("DISPLAYITEMS"));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				DISPLAYITEMS = 10;
			}
		}
		
		if (prop.getProperty("NOSTYLE") != null) {
			try {
				NOSTYLE = Boolean.valueOf(prop.getProperty("NOSTYLE"));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				NOSTYLE = false;
			}
		}
		
		
		if (prop.getProperty("TOSINGLECOL") != null) {
			try {
				TOSINGLECOL = Boolean.valueOf(prop.getProperty("TOSINGLECOL"));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				TOSINGLECOL = false;
			}
		}
		
		if (prop.getProperty("NOLINEBR") != null) {
			try {
				NOLINEBR = Boolean.valueOf(prop.getProperty("NOLINEBR"));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				NOLINEBR = false;
			}
		}
		
		if (prop.getProperty("TABLEWITHLINE") != null) {
			try {
				TABLEWITHLINE = Boolean.valueOf(prop.getProperty("TABLEWITHLINE"));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				TABLEWITHLINE = false;
			}
		}
		if (prop.getProperty("NOBR") != null) {
			try {
				NOBR = Boolean.valueOf(prop.getProperty("NOBR"));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				NOBR = false;
			}
		}
		
		if (prop.getProperty("TABLEMinCOL") != null) {
			try {
				TABLEMinCOL = Integer.valueOf(prop.getProperty("TABLEMinCOL"));
			} catch (NumberFormatException e) {
				TABLEMinCOL = 2;
			}
		}
		
		if (prop.getProperty("TABLEMinROW") != null) {
			try {
				TABLEMinROW = Integer.valueOf(prop.getProperty("TABLEMinROW"));
			} catch (NumberFormatException e) {
				TABLEMinROW = 1;
			}
		}
		

		if (prop.getProperty("LINEGAP") != null) {
			try {
				LINEGAP = Float.valueOf(prop.getProperty("LINEGAP"));
			} catch (NumberFormatException e) {
				LINEGAP = 13;
			}
		}
		
		if (prop.getProperty("maxLINEGAP") != null) {
			try {
				maxLINEGAP = Float.valueOf(prop.getProperty("maxLINEGAP"));
			} catch (NumberFormatException e) {
				maxLINEGAP = 13;
			}
		}
		if (prop.getProperty("LONGESTLENGTH") != null) {
			try {
				LONGESTLENGTH = Integer.valueOf(prop.getProperty("LONGESTLENGTH"));
			} catch (NumberFormatException e) {
				LONGESTLENGTH = 30;
			}
		}
		
		
		
		
		if (prop.getProperty("BOX_MIN_HEIGHT") != null) {
			try {
				BOX_MIN_HEIGHT = Float.valueOf(prop.getProperty("BOX_MIN_HEIGHT"));
			} catch (NumberFormatException e) {
				BOX_MIN_HEIGHT = 60;
			}
		}
		
		if (prop.getProperty("BOX_MIN_WIDTH") != null) {
			try {
				BOX_MIN_WIDTH = Float.valueOf(prop.getProperty("BOX_MIN_WIDTH"));
			} catch (NumberFormatException e) {
				BOX_MIN_WIDTH = 200;
			}
		}
		if (prop.getProperty("minLINEGAP") != null) {
			try {
				minLINEGAP = Float.valueOf(prop.getProperty("minLINEGAP"));
			} catch (NumberFormatException e) {
				minLINEGAP = 10;
			}
		}
		
		if (prop.getProperty("SUBSUPGAP") != null) {
			try {
				SUBSUPGAP = Float.valueOf(prop.getProperty("SUBSUPGAP"));
			} catch (NumberFormatException e) {
				SUBSUPGAP = 7f;
			}
		}
		
		if (prop.getProperty("PARAGRAPHX") != null) {
			try {
				PARAGRAPHX = prop.getProperty("PARAGRAPHX");
			} catch (NumberFormatException e) {
			}
		}
		
		if (prop.getProperty("PARAGRAPH_LINE_X") != null) {
			try {
				PARAGRAPH_LINE_X = prop.getProperty("PARAGRAPH_LINE_X");
			} catch (NumberFormatException e) {
			}
		}
		
		
		
		
//		
//		
//		if (prop.getProperty("boxTextX1") != null) {
//			try {
//				boxTextX1 = Float.valueOf(prop.getProperty("boxTextX1"));
//			} catch (NumberFormatException e) {
//				boxTextX1 = -1;
//			}
//		}
//		
//		
//		
//		if (prop.getProperty("boxTextX2") != null) {
//			try {
//				boxTextX2 = Float.valueOf(prop.getProperty("boxTextX2"));
//			} catch (NumberFormatException e) {
//				boxTextX2 = -1;
//			}
//		}
		
		if (prop.getProperty("useTable") != null) {
			useTable = Boolean.valueOf(prop.getProperty("useTable"));
		}

		if (prop.getProperty("pageBreak") != null) {
			pageBreak = Boolean.valueOf(prop.getProperty("pageBreak"));
		}
		
		if (prop.getProperty("TABLE_NO_LEFT_RIGHT_BORDER") != null) {
			TABLE_NO_LEFT_RIGHT_BORDER = Boolean.valueOf(prop.getProperty("TABLE_NO_LEFT_RIGHT_BORDER"));
		}
		
		if (prop.getProperty("TABLE_NO_HEADER_SEPARATOR") != null) {
			TABLE_NO_HEADER_SEPARATOR = Boolean.valueOf(prop.getProperty("TABLE_NO_HEADER_SEPARATOR"));
		}
		
		
//		if (prop.getProperty("NOSOURTING") != null) {
//			NOSOURTING = Boolean.valueOf(prop.getProperty("NOSOURTING"));
//		}
		if (prop.getProperty("wordSpace") != null) {
			wordSpace = Boolean.valueOf(prop.getProperty("wordSpace"));
		}
		
		if (prop.getProperty("allImage") != null) {
			allImage = Boolean.valueOf(prop.getProperty("allImage"));
		}
		
		

		if (prop.getProperty("noSorting") != null) {
			noSorting = Boolean.valueOf(prop.getProperty("noSorting"));
		}
		if (prop.getProperty("NOGROUPSORTING") != null) {
			NOGROUPSORTING = Boolean.valueOf(prop.getProperty("NOGROUPSORTING"));
		}
		
		
		
		if (prop.getProperty("textOnly") != null) {
			textOnly = Boolean.valueOf(prop.getProperty("textOnly"));
		}
		
		if (prop.getProperty("NOHYPHEN") != null) {
			NOHYPHEN = Boolean.valueOf(prop.getProperty("NOHYPHEN"));
		} else {
			NOHYPHEN = false;
		}
		
		if (prop.getProperty("SKIPLASTPAGE") != null) {
			SKIPLASTPAGE = Boolean.valueOf(prop.getProperty("SKIPLASTPAGE"));
		} else {
			SKIPLASTPAGE = false;
		}
		
		
		if (prop.getProperty("includeFont") != null) {
			includeFont = Boolean.valueOf(prop.getProperty("includeFont"));
		}
		if (prop.getProperty("separateBeads") != null) {
			shouldSeparateByBeads = Boolean.valueOf(prop.getProperty("separateBeads"));
		}
		if (prop.getProperty("NOSECTION") != null) {
			NOSECTION = Boolean.valueOf(prop.getProperty("NOSECTION"));
		} else {
			NOSECTION = false;
		}

//		if (prop.getProperty("addWrodSpace") != null) {
//			addWrodSpace = Boolean.valueOf(prop.getProperty("addWrodSpace"));
//		}
		
		if (prop.getProperty("indentThreshold") != null) {
			indentThreshold = Float.valueOf(prop.getProperty("indentThreshold"));
		}
		
		if (prop.getProperty("dropThreshold") != null) {
			dropThreshold = Float.valueOf(prop.getProperty("dropThreshold"));
		}
		
		if (prop.getProperty("BODYBACKGROUND") != null) {
			BODYBACKGROUND = prop.getProperty("BODYBACKGROUND");
		}


		if (prop.getProperty("IMAGEFORMAT") != null) {
			IMAGEFORMAT = prop.getProperty("IMAGEFORMAT");
		}
		
		
		if (prop.getProperty("linkBoxBACKGROUND") != null) {
			linkBoxBACKGROUND = prop.getProperty("linkBoxBACKGROUND");
		}
		if (prop.getProperty("linkBoxHeaderFont") != null) {
			linkBoxHeaderFont = prop.getProperty("linkBoxHeaderFont");
		}
		
		
		if (prop.getProperty("boxBACKGROUND") != null) {
			boxBACKGROUND = prop.getProperty("boxBACKGROUND");
		}
		
		if (prop.getProperty("PREFACE") != null) {
			PREFACE = prop.getProperty("PREFACE");
		}
		// if (prop.getProperty("$1") != null) {$1 = prop.getProperty("$1"));}
		if (prop.getProperty("CHAPTER") != null) {
			CHAPTER = prop.getProperty("CHAPTER");
		}
		if (prop.getProperty("SECTION") != null) {
			SECTION = prop.getProperty("SECTION");
		}
		
		if (prop.getProperty("CHAPTERREVIEWPARTEN") != null) {
			CHAPTERREVIEWPARTEN = prop.getProperty("CHAPTERREVIEWPARTEN");
		}
		
		if (prop.getProperty("KEYTERMS") != null) {
			KEYTERMS = prop.getProperty("KEYTERMS");
		}
		
		
		if (prop.getProperty("REVIEW") != null) {
			REVIEW = prop.getProperty("REVIEW");
		}
		
//		REVIEWSHORTANSWER=Short Answer
//				REVIEWFURTHERRESEARCH=Further Research
//				REVIEWREFERENCES=References

		if (prop.getProperty("REVIEWSHORTANSWER") != null) {
			REVIEWSHORTANSWER = prop.getProperty("REVIEWSHORTANSWER");
		}
		if (prop.getProperty("REVIEWFURTHERRESEARCH") != null) {
			REVIEWFURTHERRESEARCH = prop.getProperty("REVIEWFURTHERRESEARCH");
		}
		if (prop.getProperty("REVIEWREFERENCES") != null) {
			REVIEWREFERENCES = prop.getProperty("REVIEWREFERENCES");
		}

		if (prop.getProperty("CHAPTERREVIEW") != null) {
			CHAPTERREVIEW = prop.getProperty("CHAPTERREVIEW");
		}
		
		if (prop.getProperty("INTERACTIVELINK") != null) {
			INTERACTIVELINK = prop.getProperty("INTERACTIVELINK");
		}
		if (prop.getProperty("CONNECTIONQUESTIONS") != null) {
			CONNECTIONQUESTIONS = prop.getProperty("CONNECTIONQUESTIONS");
		}
		
		
		if (prop.getProperty("REVIEWQUESTIONS") != null) {
			REVIEWQUESTIONS = prop.getProperty("REVIEWQUESTIONS");
		}
		if (prop.getProperty("CRITICALTHINKING") != null) {
			CRITICALTHINKING = prop.getProperty("CRITICALTHINKING");
		}
		
		if (prop.getProperty("FURTHERRESEARCH") != null) {
			FURTHERRESEARCH = prop.getProperty("FURTHERRESEARCH");
		}
		
		if (prop.getProperty("FURTHERSTUDY") != null) {
			FURTHERSTUDY = prop.getProperty("FURTHERSTUDY");
		}
		
		if (prop.getProperty("TWOCOLUMNS") != null) {
			TWOCOLUMNS = prop.getProperty("TWOCOLUMNS");
		}
		
		
		
		if (prop.getProperty("ANSWERKEY") != null) {
			ANSWERKEY = prop.getProperty("ANSWERKEY");
		}
		if (prop.getProperty("QUESTIONSEPARATOR") != null) {
			QUESTIONSEPARATOR = prop.getProperty("QUESTIONSEPARATOR");
		}
		
		if (prop.getProperty("REFERENCES") != null) {
			REFERENCES = prop.getProperty("REFERENCES");
		}
		if (prop.getProperty("APPENDIX") != null) {
			APPENDIX = prop.getProperty("APPENDIX");
		}
		if (prop.getProperty("INDEX") != null) {
			INDEX = prop.getProperty("INDEX");
		}
		if (prop.getProperty("KEYTERMSFONT") != null) {
			KEYTERMSFONT = prop.getProperty("KEYTERMSFONT");
		}
		
		if (prop.getProperty("FIRSTLETTERFONT") != null) {
			FIRSTLETTERFONT = prop.getProperty("FIRSTLETTERFONT");
		}
		
		
		if (prop.getProperty("REVIEWSECTIONFONT") != null) {
			REVIEWSECTIONFONT = prop.getProperty("REVIEWSECTIONFONT");
		}
		
		if (prop.getProperty("REVIEWSHORTANSWERFONT") != null) {
			REVIEWSHORTANSWERFONT = prop.getProperty("REVIEWSHORTANSWERFONT");
		}
		
		
		if (prop.getProperty("REVIEWFURTHERRESEARCHFONT") != null) {
			REVIEWFURTHERRESEARCHFONT = prop.getProperty("REVIEWFURTHERRESEARCHFONT");
		}
		
		if (prop.getProperty("REVIEWREFERENCESFONT") != null) {
			REVIEWREFERENCESFONT = prop.getProperty("REVIEWREFERENCESFONT");
		}
		 
		if (prop.getProperty("KEYFONT") != null) {
			KEYFONT = prop.getProperty("KEYFONT");
		}
		
		if (prop.getProperty("REVIEWTITLEFONT") != null) {
			REVIEWTITLEFONT = prop.getProperty("REVIEWTITLEFONT");
		}
		
		if (prop.getProperty("CHAPTERREVIEWFONT") != null) {
			CHAPTERREVIEWFONT = prop.getProperty("CHAPTERREVIEWFONT");
		}
		if (prop.getProperty("REVIEWFONT") != null) {
			REVIEWFONT = prop.getProperty("REVIEWFONT");
		}
		
		if (prop.getProperty("INTERACTIVELINKFONT") != null) {
			INTERACTIVELINKFONT = prop.getProperty("INTERACTIVELINKFONT");
		}
		if (prop.getProperty("CONNECTIONQUESTIONSFONT") != null) {
			CONNECTIONQUESTIONSFONT = prop.getProperty("CONNECTIONQUESTIONSFONT");
		}
		
		
		if (prop.getProperty("REVIEWQUESTIONSFONT") != null) {
			REVIEWQUESTIONSFONT = prop.getProperty("REVIEWQUESTIONSFONT");
		}
		if (prop.getProperty("CRITICALTHINKINGFONT") != null) {
			CRITICALTHINKINGFONT = prop.getProperty("CRITICALTHINKINGFONT");
		}
		if (prop.getProperty("ANSWERKEYFONT") != null) {
			ANSWERKEYFONT = prop.getProperty("ANSWERKEYFONT");
		}
		if (prop.getProperty("REFERENCESFONT") != null) {
			REFERENCESFONT = prop.getProperty("REFERENCESFONT");
		}
		if (prop.getProperty("INDEXFONT") != null) {
			INDEXFONT = prop.getProperty("INDEXFONT");
		}
		if (prop.getProperty("BOXTITLEFONT") != null) {
			BOXTITLEFONT = prop.getProperty("BOXTITLEFONT");
		}
		if (prop.getProperty("BOXSUBTITLEFONT") != null) {
			BOXSUBTITLEFONT = prop.getProperty("BOXSUBTITLEFONT");
		}
		
		if (prop.getProperty("BOXBODYFONT") != null) {
			BOXBODYFONT = prop.getProperty("BOXBODYFONT");
		}
		if (prop.getProperty("CHAPTEROBJECTIVE") != null) {
			CHAPTEROBJECTIVE = prop.getProperty("CHAPTEROBJECTIVE");
		}

		if (prop.getProperty("CHAPTERTITLEFONT") != null) {
			CHAPTERTITLEFONT = prop.getProperty("CHAPTERTITLEFONT");
		}
		
		
		if (prop.getProperty("CHAPTERFONT") != null) {
			CHAPTERFONT = prop.getProperty("CHAPTERFONT");
		}
		
		if (prop.getProperty("PARTFONT") != null) {
			PARTFONT = prop.getProperty("PARTFONT");
		}
		
		if (prop.getProperty("PARTTITLEFONT") != null) {
			PARTTITLEFONT = prop.getProperty("PARTTITLEFONT");
		}
		if (prop.getProperty("DEFAULTFONT") != null) {
			DEFAULTFONT = prop.getProperty("DEFAULTFONT");
		}
		
		if (prop.getProperty("CHAPTERSEPARATORONT") != null) {
			CHAPTERSEPARATORONT = prop.getProperty("CHAPTERSEPARATORONT");
		}
		if (prop.getProperty("SECTIONSEPARATORFONT") != null) {
			SECTIONSEPARATORFONT = prop.getProperty("SECTIONSEPARATORFONT");
		}

		if (prop.getProperty("CHAPTERNUMFONT") != null) {
			CHAPTERNUMFONT = prop.getProperty("CHAPTERNUMFONT");
		}
		if (prop.getProperty("SECTIONNUMFONT") != null) {
			SECTIONNUMFONT = prop.getProperty("SECTIONNUMFONT");
		}
		
		if (prop.getProperty("SECTIONTITLEFONT") != null) {
			SECTIONTITLEFONT = prop.getProperty("SECTIONTITLEFONT");
		}
		if (prop.getProperty("BODYTITLEFONT") != null) {
			BODYTITLEFONT = prop.getProperty("BODYTITLEFONT");
		}

		if (prop.getProperty("CHAPTERPARTEN") != null) {
			CHAPTERPARTEN = prop.getProperty("CHAPTERPARTEN");
		}
		if (prop.getProperty("SECTIONPATTERN") != null) {
			SECTIONPATTERN = prop.getProperty("SECTIONPATTERN");
		}
		
		if (prop.getProperty("SUBSECTIONPATTERN") != null) {
			SUBSECTIONPATTERN = prop.getProperty("SUBSECTIONPATTERN");
		}
		if (prop.getProperty("APPENDIXPATTERN") != null) {
			APPENDIXPATTERN = prop.getProperty("APPENDIXPATTERN");
		}
		if (prop.getProperty("INDEXPATTERN") != null) {
			INDEXPATTERN = prop.getProperty("INDEXPATTERN");
		}

		
		if (Util.notEmpty(TWOCOLUMNS)) {
			String[] ss = TWOCOLUMNS.split(",");
			for (String s: ss) {
				TWOCOLUMNSMAP.put(s, true);
			}
			
		}
		FONTS.clear();
		FONTS.put(KEYTERMS, KEYTERMSFONT);
		FONTS.put(REVIEWSHORTANSWER, REVIEWSHORTANSWERFONT);
		FONTS.put(REVIEWFURTHERRESEARCH, REVIEWFURTHERRESEARCHFONT);
		FONTS.put(REVIEWREFERENCES, REVIEWREFERENCESFONT);
		
		FONTS.put(CHAPTERREVIEW, CHAPTERREVIEWFONT);
		FONTS.put(INTERACTIVELINK, INTERACTIVELINKFONT);
		FONTS.put(CONNECTIONQUESTIONS, CONNECTIONQUESTIONSFONT);
		
		FONTS.put(REVIEWQUESTIONS, REVIEWQUESTIONSFONT);
		FONTS.put(CRITICALTHINKING, CRITICALTHINKINGFONT);
		FONTS.put(ANSWERKEY, ANSWERKEYFONT);
		FONTS.put(REFERENCES, REFERENCESFONT);
		FONTS.put(INDEX, INDEXFONT);

		MATCHINGKEYS.clear();
		MATCHINGKEYS.add(KEYTERMS);
		MATCHINGKEYS.add(CHAPTERREVIEW);
		MATCHINGKEYS.add(INTERACTIVELINK);
		MATCHINGKEYS.add(CONNECTIONQUESTIONS);
		MATCHINGKEYS.add(REVIEWQUESTIONS);
		MATCHINGKEYS.add(CRITICALTHINKING);
		MATCHINGKEYS.add(ANSWERKEY);
		MATCHINGKEYS.add(REFERENCES);
		MATCHINGKEYS.add(INDEX);
	}

	List<String> MATCHINGKEYS = new ArrayList<String>();
	// {KEYTERMS, CHAPTERREVIEW, INTERACTIVELINK, REVIEWQUESTIONS,
	// CRITICALTHINKING,ANSWERKEY,REFERENCES, INDEX};
	Map<String, String> FONTS = new HashMap<String, String>();
	
	static public Map<String, Boolean> TWOCOLUMNSMAP = new HashMap<>();

	public int getStripeImageWidth() {
		return stripeImageWidth;
	}

	public int getStripeImageHeight() {
		return stripeImageHeight;
	}

	protected int stripeImageHeight = 204;

	public boolean isAllImage() {
		return allImage;
	}

	public void setAllImage(boolean allImage) {
		this.allImage = allImage;
	}

	protected String outputDir = "";
	protected String outputName = "";
	static public String IMAGEFORMAT = "jpg";
	static protected boolean textOnly = false;
	static public boolean NOHYPHEN = false;
	static public boolean SKIPLASTPAGE = true;
	
	protected boolean includeFont = false;
	static public boolean noSorting = false;
	static public boolean NOGROUPSORTING = false;

//	public boolean isNoSorting() {
//		return noSorting;
//	}
//
//	public boolean isIncludeFont() {
//		return includeFont;
//	}

	static public boolean isTextOnly() {
		return textOnly;
	}

	public String getOutputName() {
		return outputName;
	}

	public void setOutputName(String outputName) {
		this.outputName = outputName;
	}

	public String getDataDir() {
		return outputDir + "data/";
	}
	
	public String getFontsDir() {
		return outputDir + "fonts/";
	}
	
	public String getImageDir() {
		return outputDir + "images/";
	}
	
	public String getConfigDir() {
		return outputDir + "config/";
	}
	
	public String getOutputDir() {
		return outputDir;
	}

	public void setOutputDir(String outputDir) {
		this.outputDir = outputDir;
	}

//	public boolean isUseTable() {
//		return useTable;
//	}
//
//	public void setUseTable(boolean useTable) {
//		this.useTable = useTable;
//	}

	public int getTocStartPage() {
		return tocStartPage;
	}

	public int getTocEndPage() {
		return tocEndPage;
	}

	public int currentPageNo = 0;
	public static int startPage = 1;

//	public int getStartPage() {
//		return startPage;
//	}
//
//	public int getEndPage() {
//		return endPage;
//	}

//	public void setEndPage(int endPage) {
//		this.endPage = endPage;
//	}

	public static int endPage = Integer.MAX_VALUE;
	static public float LINEGAP = 13f; //NROMAL SPACE between LINE
	static public float maxLINEGAP = 55f;
	static public float minLINEGAP = 10f;
	static public float SUBSUPGAP = 7f;
	static public String PARAGRAPHX = null;
	static public String PARAGRAPH_LINE_X = null;
	
//	static public float boxTextX1 = -1;
//	static public float boxTextX2 = -1;
//	
	static public boolean firstTime = true; //We Do scan TOC
	static public boolean MULTICOLUMNS = true; //We Do scan TOC
	static public PDRectangle pageSize = null;
	static public int TABLEMinCOL = 2;
	static public int TABLEMinROW =1;
	static public int LAYOUT = 0;
	static public boolean TOSINGLECOL = false;
	static public boolean NOLINEBR = false;
	public static boolean TABLEWITHLINE = false;
	public static boolean NOBR = false; //NO BR


	protected PDOutlineItem startBookmark = null;

	protected int startBookmarkPageNumber = -1;
	protected int endBookmarkPageNumber = -1;

	protected PDOutlineItem endBookmark = null;
	protected boolean suppressDuplicateOverlappingText = true;
	protected boolean shouldSeparateByBeads = true;
	protected boolean sortByPosition = false;
	protected boolean addMoreFormatting = false;

	protected float indentThreshold = defaultIndentThreshold;
	static float dropThreshold = defaultDropThreshold;

	// we will need to estimate where to add spaces, these are used to help
	// guess
	protected float spacingTolerance = .5f;
	protected float averageCharTolerance = .3f;

	protected List<PDRectangle> beadRectangles = null;
	protected Map<String, TreeMap<Float, TreeSet<Float>>> characterListMapping = new HashMap<String, TreeMap<Float, TreeSet<Float>>>();

	protected PDDocument document;
	// protected Writer output;

	protected void resetEngine() {
		currentPageNo = 0;
		document = null;
		if (charactersByArticle != null) {
			charactersByArticle.clear();
		}
		if (characterListMapping != null) {
			characterListMapping.clear();
		}
	}

	/**
	 * The charactersByArticle is used to extract text by article divisions. For
	 * example a PDF that has two columns like a newspaper, we want to extract
	 * the first column and then the second column. In this example the PDF
	 * would have 2 beads(or articles), one for each column. The size of the
	 * charactersByArticle would be 5, because not all text on the screen will
	 * fall into one of the articles. The five divisions are shown below
	 *
	 * Text before first article first article text text between first article
	 * and second article second article text text after second article
	 *
	 * Most PDFs won't have any beads, so charactersByArticle will contain a
	 * single entry.
	 */
	protected ArrayList<List<TextPosition>> charactersByArticle = new ArrayList<List<TextPosition>>();

	/**
	 * Normalize certain Unicode characters. For example, convert the single
	 * "fi" ligature to "f" and "i". Also normalises Arabic and Hebrew
	 * presentation forms.
	 *
	 * @param word
	 *            Word to normalize
	 * @return Normalized word
	 */
	protected String normalizeWord(String word) {
		StringBuilder builder = null;
		int p = 0;
		int q = 0;
		int strLength = word.length();
		for (; q < strLength; q++) {
			// We only normalize if the codepoint is in a given range.
			// Otherwise, NFKC converts too many things that would cause
			// confusion. For example, it converts the micro symbol in
			// extended Latin to the value in the Greek script. We normalize
			// the Unicode Alphabetic and Arabic A&B Presentation forms.
			char c = word.charAt(q);
			if (0xFB00 <= c && c <= 0xFDFF || 0xFE70 <= c && c <= 0xFEFF) {
				if (builder == null) {
					builder = new StringBuilder(strLength * 2);
				}
				builder.append(word.substring(p, q));
				// Some fonts map U+FDF2 differently than the Unicode spec.
				// They add an extra U+0627 character to compensate.
				// This removes the extra character for those fonts.
				if (c == 0xFDF2 && q > 0 && (word.charAt(q - 1) == 0x0627 || word.charAt(q - 1) == 0xFE8D)) {
					builder.append("\u0644\u0644\u0647");
				} else {
					// Trim because some decompositions have an extra space,
					// such as U+FC5E
					builder.append(Normalizer.normalize(word.substring(q, q + 1), Normalizer.Form.NFKC).trim());
				}
				p = q + 1;
			}
		}
		if (builder == null) {
			return handleDirection(word);
		} else {
			builder.append(word.substring(p, q));
			return handleDirection(builder.toString());
		}
	}

	/**
	 * Handles the LTR and RTL direction of the given words. The whole
	 * implementation stands and falls with the given word. If the word is a
	 * full line, the results will be the best. If the word contains of single
	 * words or characters, the order of the characters in a word or words in a
	 * line may wrong, due to RTL and LTR marks and characters!
	 * 
	 * Based on
	 * http://www.nesterovsky-bros.com/weblog/2013/07/28/VisualToLogicalConversionInJava.aspx
	 * 
	 * @param word
	 *            The word that shall be processed
	 * @return new word with the correct direction of the containing characters
	 */
	private String handleDirection(String word) {
		Bidi bidi = new Bidi(word, Bidi.DIRECTION_DEFAULT_LEFT_TO_RIGHT);

		// if there is pure LTR text no need to process further
		if (!bidi.isMixed() && bidi.getBaseLevel() == Bidi.DIRECTION_LEFT_TO_RIGHT) {
			return word;
		}

		// collect individual bidi information
		int runCount = bidi.getRunCount();
		byte[] levels = new byte[runCount];
		Integer[] runs = new Integer[runCount];

		for (int i = 0; i < runCount; i++) {
			levels[i] = (byte) bidi.getRunLevel(i);
			runs[i] = i;
		}

		// reorder individual parts based on their levels
		Bidi.reorderVisually(levels, 0, runs, 0, runCount);

		// collect the parts based on the direction within the run
		StringBuilder result = new StringBuilder();

		for (int i = 0; i < runCount; i++) {
			int index = runs[i];
			int start = bidi.getRunStart(index);
			int end = bidi.getRunLimit(index);

			int level = levels[index];

			if ((level & 1) != 0) {
				for (; --end >= start;) {
					char character = word.charAt(end);
					if (Character.isMirrored(word.codePointAt(end))) {
						if (MIRRORING_CHAR_MAP.containsKey(character)) {
							result.append(MIRRORING_CHAR_MAP.get(character));
						} else {
							result.append(character);
						}
					} else {
						result.append(character);
					}
				}
			} else {
				result.append(word, start, end);
			}
		}

		return result.toString();
	}

	private static Map<Character, Character> MIRRORING_CHAR_MAP = new HashMap<Character, Character>();

	static {
		String path = "org/apache/pdfbox/resources/text/BidiMirroring.txt";
		InputStream input = VividingStripper.class.getClassLoader().getResourceAsStream(path);
		try {
			parseBidiFile(input);
		} catch (IOException e) {
			LOG.warn("Could not parse BidiMirroring.txt, mirroring char map will be empty: " + e.getMessage());
		} finally {
			try {
				input.close();
			} catch (IOException e) {
				LOG.error("Could not close BidiMirroring.txt ", e);
			}
		}
	}

	/**
	 * This method parses the bidi file provided as inputstream.
	 * 
	 * @param inputStream
	 *            - The bidi file as inputstream
	 * @throws IOException
	 *             if any line could not be read by the LineNumberReader
	 */
	private static void parseBidiFile(InputStream inputStream) throws IOException {
		LineNumberReader rd = new LineNumberReader(new InputStreamReader(inputStream));

		do {
			String s = rd.readLine();
			if (s == null) {
				break;
			}

			int comment = s.indexOf('#'); // ignore comments
			if (comment != -1) {
				s = s.substring(0, comment);
			}

			if (s.length() < 2) {
				continue;
			}

			StringTokenizer st = new StringTokenizer(s, ";");
			int nFields = st.countTokens();
			Character[] fields = new Character[nFields];
			for (int i = 0; i < nFields; i++) {
				fields[i] = (char) Integer.parseInt(st.nextToken().trim(), 16);
			}

			if (fields.length == 2) {
				// initialize the MIRRORING_CHAR_MAP
				MIRRORING_CHAR_MAP.put(fields[0], fields[1]);
			}

		} while (true);
	}


	/**
	 * a list of regular expressions that match commonly used list item formats,
	 * i.e. bullets, numbers, letters, Roman numerals, etc. Not meant to be
	 * comprehensive.
	 */
	private static final String[] LIST_ITEM_EXPRESSIONS = { "\\.", "\\d+\\.", "\\[\\d+\\]", "\\d+\\)", "[A-Z]\\.",
			"[a-z]\\.", "[A-Z]\\)", "[a-z]\\)", "[IVXL]+\\.", "[ivxl]+\\.", };

	private List<Pattern> listOfPatterns = null;

	/**
	 * use to supply a different set of regular expression patterns for matching
	 * list item starts.
	 *
	 * @param patterns
	 *            list of patterns
	 */
	protected void setListItemPatterns(List<Pattern> patterns) {
		listOfPatterns = patterns;
	}

	/**
	 * returns a list of regular expression Patterns representing different
	 * common list item formats. For example numbered items of form:
	 * <ol>
	 * <li>some text</li>
	 * <li>more text</li>
	 * </ol>
	 * or
	 * <ul>
	 * <li>some text</li>
	 * <li>more text</li>
	 * </ul>
	 * etc., all begin with some character pattern. The pattern "\\d+\."
	 * (matches "1.", "2.", ...) or "\[\\d+\]" (matches "[1]", "[2]", ...).
	 * <p>
	 * This method returns a list of such regular expression Patterns.
	 * 
	 * @return a list of Pattern objects.
	 */
	protected List<Pattern> getListItemPatterns() {
		if (listOfPatterns == null) {
			listOfPatterns = new ArrayList<Pattern>();
			for (String expression : LIST_ITEM_EXPRESSIONS) {
				Pattern p = Pattern.compile(expression);
				listOfPatterns.add(p);
			}
		}
		return listOfPatterns;
	}

	/**
	 * iterates over the specified list of Patterns until it finds one that
	 * matches the specified string. Then returns the Pattern.
	 * <p>
	 * Order of the supplied list of patterns is important as most common
	 * patterns should come first. Patterns should be strict in general, and all
	 * will be used with case sensitivity on.
	 * </p>
	 * 
	 * @param string
	 *            the string to be searched
	 * @param patterns
	 *            list of patterns
	 * @return matching pattern
	 */
	protected static Pattern matchPattern(String string, List<Pattern> patterns) {
		for (Pattern p : patterns) {
			if (p.matcher(string).matches()) {
				return p;
			}
		}
		return null;
	}

	/**
	 * returns the list item Pattern object that matches the text at the
	 * specified PositionWrapper or null if the text does not match such a
	 * pattern. The list of Patterns tested against is given by the
	 * {@link #getListItemPatterns()} method. To add to the list, simply
	 * override that method (if sub-classing) or explicitly supply your own list
	 * using {@link #setListItemPatterns(List)}.
	 * 
	 * @param pw
	 *            position
	 * @return the matching pattern
	 */
	private Pattern matchListItemPattern(PositionWrapper pw) {
		TextPosition tp = pw.getTextPosition();
		String txt = tp.getUnicode();
		return matchPattern(txt, getListItemPatterns());
	}

	/**
	 * Set the desired line separator for output text. The line.separator system
	 * property is used if the line separator preference is not set explicitly
	 * using this method.
	 *
	 * @param separator
	 *            The desired line separator string.
	 */
	public void setLineSeparator(String separator) {
		lineSeparator = separator;
	}

	/**
	 * This will get the line separator.
	 *
	 * @return The desired line separator string.
	 */
	public String getLineSeparator() {
		return lineSeparator;
	}

	/**
	 * This will get the word separator.
	 *
	 * @return The desired word separator string.
	 */
	public String getWordSeparator() {
		return wordSeparator;
	}

	/**
	 * Set the desired word separator for output text. The PDFBox text
	 * extraction algorithm will output a space character if there is enough
	 * space between two words. By default a space character is used. If you
	 * need and accurate count of characters that are found in a PDF document
	 * then you might want to set the word separator to the empty string.
	 *
	 * @param separator
	 *            The desired page separator string.
	 */
	public void setWordSeparator(String separator) {
		wordSeparator = separator;
	}

	/**
	 * @return Returns the suppressDuplicateOverlappingText.
	 */
	public boolean getSuppressDuplicateOverlappingText() {
		return suppressDuplicateOverlappingText;
	}

	/**
	 * Get the current page number that is being processed.
	 *
	 * @return A 1 based number representing the current page.
	 */
	public int getCurrentPageNo() {
		return currentPageNo;
	}

	/**
	 * The output stream that is being written to.
	 *
	 * @return The stream that output is being written to.
	 */
	// protected Writer getOutput()
	// {
	// return output;
	// }

	/**
	 * Character strings are grouped by articles. It is quite common that there
	 * will only be a single article. This returns a List that contains List
	 * objects, the inner lists will contain TextPosition objects.
	 *
	 * @return A double List of TextPositions for all text strings on the page.
	 */
	public List<List<TextPosition>> getCharactersByArticle() {
		return charactersByArticle;
	}

	/**
	 * By default the text stripper will attempt to remove text that overlapps
	 * each other. Word paints the same character several times in order to make
	 * it look bold. By setting this to false all text will be extracted, which
	 * means that certain sections will be duplicated, but better performance
	 * will be noticed.
	 *
	 * @param suppressDuplicateOverlappingTextValue
	 *            The suppressDuplicateOverlappingText to set.
	 */
	public void setSuppressDuplicateOverlappingText(boolean suppressDuplicateOverlappingTextValue) {
		suppressDuplicateOverlappingText = suppressDuplicateOverlappingTextValue;
	}

	/**
	 * This will tell if the text stripper should separate by beads.
	 *
	 * @return If the text will be grouped by beads.
	 */
	public boolean getSeparateByBeads() {
		return shouldSeparateByBeads;
	}

	// /**
	// * Set if the text stripper should group the text output by a list of
	// beads. The default value is true!
	// *
	// * @param aShouldSeparateByBeads The new grouping of beads.
	// */
	// public void setShouldSeparateByBeads(boolean aShouldSeparateByBeads)
	// {
	// shouldSeparateByBeads = aShouldSeparateByBeads;
	// }

	/**
	 * Get the bookmark where text extraction should end, inclusive. Default is
	 * null.
	 *
	 * @return The ending bookmark.
	 */
	public PDOutlineItem getEndBookmark() {
		return endBookmark;
	}

	/**
	 * Set the bookmark where the text extraction should stop.
	 *
	 * @param aEndBookmark
	 *            The ending bookmark.
	 */
	public void setEndBookmark(PDOutlineItem aEndBookmark) {
		endBookmark = aEndBookmark;
	}

	/**
	 * Get the bookmark where text extraction should start, inclusive. Default
	 * is null.
	 *
	 * @return The starting bookmark.
	 */
	public PDOutlineItem getStartBookmark() {
		return startBookmark;
	}

	/**
	 * Set the bookmark where text extraction should start, inclusive.
	 *
	 * @param aStartBookmark
	 *            The starting bookmark.
	 */
	public void setStartBookmark(PDOutlineItem aStartBookmark) {
		startBookmark = aStartBookmark;
	}

	/**
	 * This will tell if the text stripper should add some more text formatting.
	 * 
	 * @return true if some more text formatting will be added
	 */
	public boolean getAddMoreFormatting() {
		return addMoreFormatting;
	}

	/**
	 * There will some additional text formatting be added if addMoreFormatting
	 * is set to true. Default is false.
	 * 
	 * @param newAddMoreFormatting
	 *            Tell PDFBox to add some more text formatting
	 */
	public void setAddMoreFormatting(boolean newAddMoreFormatting) {
		addMoreFormatting = newAddMoreFormatting;
	}

	/**
	 * Get the current space width-based tolerance value that is being used to
	 * estimate where spaces in text should be added. Note that the default
	 * value for this has been determined from trial and error.
	 * 
	 * @return The current tolerance / scaling factor
	 */
	public float getSpacingTolerance() {
		return spacingTolerance;
	}

	/**
	 * Set the space width-based tolerance value that is used to estimate where
	 * spaces in text should be added. Note that the default value for this has
	 * been determined from trial and error. Setting this value larger will
	 * reduce the number of spaces added.
	 * 
	 * @param spacingToleranceValue
	 *            tolerance / scaling factor to use
	 */
	public void setSpacingTolerance(float spacingToleranceValue) {
		spacingTolerance = spacingToleranceValue;
	}

	/**
	 * Get the current character width-based tolerance value that is being used
	 * to estimate where spaces in text should be added. Note that the default
	 * value for this has been determined from trial and error.
	 * 
	 * @return The current tolerance / scaling factor
	 */
	public float getAverageCharTolerance() {
		return averageCharTolerance;
	}

	/**
	 * Set the character width-based tolerance value that is used to estimate
	 * where spaces in text should be added. Note that the default value for
	 * this has been determined from trial and error. Setting this value larger
	 * will reduce the number of spaces added.
	 * 
	 * @param averageCharToleranceValue
	 *            average tolerance / scaling factor to use
	 */
	public void setAverageCharTolerance(float averageCharToleranceValue) {
		averageCharTolerance = averageCharToleranceValue;
	}

	/**
	 * returns the multiple of whitespace character widths for the current text
	 * which the current line start can be indented from the previous line start
	 * beyond which the current line start is considered to be a paragraph
	 * start.
	 * 
	 * @return the number of whitespace character widths to use when detecting
	 *         paragraph indents.
	 */
	public float getIndentThreshold() {
		return indentThreshold;
	}

	/**
	 * sets the multiple of whitespace character widths for the current text
	 * which the current line start can be indented from the previous line start
	 * beyond which the current line start is considered to be a paragraph
	 * start. The default value is 2.0.
	 *
	 * @param indentThresholdValue
	 *            the number of whitespace character widths to use when
	 *            detecting paragraph indents.
	 */
	public void setIndentThreshold(float indentThresholdValue) {
		indentThreshold = indentThresholdValue;
	}

	/**
	 * the minimum whitespace, as a multiple of the max height of the current
	 * characters beyond which the current line start is considered to be a
	 * paragraph start.
	 * 
	 * @return the character height multiple for max allowed whitespace between
	 *         lines in the same paragraph.
	 */
	static public float getDropThreshold() {
		return dropThreshold;
	}

	/**
	 * Write a Java string to the output stream.
	 *
	 * @param text
	 *            The text to write to the stream.
	 * @throws IOException
	 *             If there is an error when writing the text.
	 */
	protected void writeString(String text, StringBuilder sb) {
		sb.append(text);
	}

	protected void fillBeadRectangles(PDPage page) {
		beadRectangles = new ArrayList<PDRectangle>();
		for (PDThreadBead bead : page.getThreadBeads()) {
			if (bead == null) {
				// can't skip, because of null entry handling in
				// processTextPosition()
				beadRectangles.add(null);
				continue;
			}

			PDRectangle rect = bead.getRectangle();

			// bead rectangle is in PDF coordinates (y=0 is bottom),
			// glyphs are in image coordinates (y=0 is top),
			// so we must flip
			PDRectangle mediaBox = page.getMediaBox();
			float upperRightY = mediaBox.getUpperRightY() - rect.getLowerLeftY();
			float lowerLeftY = mediaBox.getUpperRightY() - rect.getUpperRightY();
			rect.setLowerLeftY(lowerLeftY);
			rect.setUpperRightY(upperRightY);

			// adjust for cropbox
			PDRectangle cropBox = page.getCropBox();
			if (cropBox.getLowerLeftX() != 0 || cropBox.getLowerLeftY() != 0) {
				rect.setLowerLeftX(rect.getLowerLeftX() - cropBox.getLowerLeftX());
				rect.setLowerLeftY(rect.getLowerLeftY() - cropBox.getLowerLeftY());
				rect.setUpperRightX(rect.getUpperRightX() - cropBox.getLowerLeftX());
				rect.setUpperRightY(rect.getUpperRightY() - cropBox.getLowerLeftY());
			}

			beadRectangles.add(rect);
		}
	}

	/**
	 * This will process a TextPosition object and add the text to the list of
	 * characters on a page. It takes care of overlapping text.
	 *
	 * @param text
	 *            The text to process.
	 */
//	@Override
//	protected void processTextPosition(TextPosition text) {
//		boolean showCharacter = true;
//		if (suppressDuplicateOverlappingText) {
//			showCharacter = false;
//			String textCharacter = text.getUnicode();
//			float textX = text.getX();
//			float textY = text.getY();
//			TreeMap<Float, TreeSet<Float>> sameTextCharacters = characterListMapping.get(textCharacter);
//			if (sameTextCharacters == null) {
//				sameTextCharacters = new TreeMap<Float, TreeSet<Float>>();
//				characterListMapping.put(textCharacter, sameTextCharacters);
//			}
//			// RDD - Here we compute the value that represents the end of the
//			// rendered
//			// text. This value is used to determine whether subsequent text
//			// rendered
//			// on the same line overwrites the current text.
//			//
//			// We subtract any positive padding to handle cases where extreme
//			// amounts
//			// of padding are applied, then backed off (not sure why this is
//			// done, but there
//			// are cases where the padding is on the order of 10x the character
//			// width, and
//			// the TJ just backs up to compensate after each character). Also,
//			// we subtract
//			// an amount to allow for kerning (a percentage of the width of the
//			// last
//			// character).
//			boolean suppressCharacter = false;
//			float tolerance = text.getWidth() / textCharacter.length() / 3.0f;
//
//			SortedMap<Float, TreeSet<Float>> xMatches = sameTextCharacters.subMap(textX - tolerance, textX + tolerance);
//			for (TreeSet<Float> xMatch : xMatches.values()) {
//				SortedSet<Float> yMatches = xMatch.subSet(textY - tolerance, textY + tolerance);
//				if (!yMatches.isEmpty()) {
//					suppressCharacter = true;
//					break;
//				}
//			}
//			if (!suppressCharacter) {
//				TreeSet<Float> ySet = sameTextCharacters.get(textX);
//				if (ySet == null) {
//					ySet = new TreeSet<Float>();
//					sameTextCharacters.put(textX, ySet);
//				}
//				ySet.add(textY);
//				showCharacter = true;
//			}
//		}
//		if (showCharacter) {
//			// if we are showing the character then we need to determine which
//			// article it belongs to
//			int foundArticleDivisionIndex = -1;
//			int notFoundButFirstLeftAndAboveArticleDivisionIndex = -1;
//			int notFoundButFirstLeftArticleDivisionIndex = -1;
//			int notFoundButFirstAboveArticleDivisionIndex = -1;
//			float x = text.getX();
//			float y = text.getY();
//			if (shouldSeparateByBeads) {
//				for (int i = 0; i < beadRectangles.size() && foundArticleDivisionIndex == -1; i++) {
//					PDRectangle rect = beadRectangles.get(i);
//					if (rect != null) {
//						if (rect.contains(x, y)) {
//							foundArticleDivisionIndex = i * 2 + 1;
//						} else if ((x < rect.getLowerLeftX() || y < rect.getUpperRightY())
//								&& notFoundButFirstLeftAndAboveArticleDivisionIndex == -1) {
//							notFoundButFirstLeftAndAboveArticleDivisionIndex = i * 2;
//						} else if (x < rect.getLowerLeftX() && notFoundButFirstLeftArticleDivisionIndex == -1) {
//							notFoundButFirstLeftArticleDivisionIndex = i * 2;
//						} else if (y < rect.getUpperRightY() && notFoundButFirstAboveArticleDivisionIndex == -1) {
//							notFoundButFirstAboveArticleDivisionIndex = i * 2;
//						}
//					} else {
//						foundArticleDivisionIndex = 0;
//					}
//				}
//			} else {
//				foundArticleDivisionIndex = 0;
//			}
//			int articleDivisionIndex;
//			if (foundArticleDivisionIndex != -1) {
//				articleDivisionIndex = foundArticleDivisionIndex;
//			} else if (notFoundButFirstLeftAndAboveArticleDivisionIndex != -1) {
//				articleDivisionIndex = notFoundButFirstLeftAndAboveArticleDivisionIndex;
//			} else if (notFoundButFirstLeftArticleDivisionIndex != -1) {
//				articleDivisionIndex = notFoundButFirstLeftArticleDivisionIndex;
//			} else if (notFoundButFirstAboveArticleDivisionIndex != -1) {
//				articleDivisionIndex = notFoundButFirstAboveArticleDivisionIndex;
//			} else {
//				articleDivisionIndex = charactersByArticle.size() - 1;
//			}
//
//			List<TextPosition> textList = charactersByArticle.get(articleDivisionIndex);
//
//			// In the wild, some PDF encoded documents put diacritics (accents
//			// on
//			// top of characters) into a separate Tj element. When displaying
//			// them
//			// graphically, the two chunks get overlayed. With text output
//			// though,
//			// we need to do the overlay. This code recombines the diacritic
//			// with
//			// its associated character if the two are consecutive.
//			if (textList.isEmpty()) {
//				textList.add(text);
//			} else {
//				// test if we overlap the previous entry.
//				// Note that we are making an assumption that we need to only
//				// look back
//				// one TextPosition to find what we are overlapping.
//				// This may not always be true. */
//				TextPosition previousTextPosition = textList.get(textList.size() - 1);
//				if (text.isDiacritic() && previousTextPosition.contains(text)) {
//					previousTextPosition.mergeDiacritic(text);
//				}
//				// If the previous TextPosition was the diacritic, merge it into
//				// this
//				// one and remove it from the list.
//				else if (previousTextPosition.isDiacritic() && text.contains(previousTextPosition)) {
//					text.mergeDiacritic(previousTextPosition);
//					textList.remove(textList.size() - 1);
//					textList.add(text);
//				} else {
//					textList.add(text);
//				}
//			}
//		}
//	}
	String title = null;
	public String getTitle() {
		if (title != null) {
			return title;
		}
		String titleGuess = document.getDocumentInformation().getTitle();
		if (titleGuess != null && titleGuess.length() > 0) {
			title = titleGuess;
			return title;
		} else {
			Iterator<List<TextPosition>> textIter = getCharactersByArticle().iterator();
			float lastFontSize = -1.0f;

			StringBuilder titleText = new StringBuilder();
			while (textIter.hasNext()) {
				for (TextPosition position : textIter.next()) {
					float currentFontSize = position.getFontSize();
					// If we're past 64 chars we will assume that we're past the
					// title
					// 64 is arbitrary
					if (currentFontSize != lastFontSize || titleText.length() > 64) {
						if (titleText.length() > 0) {
							title = titleText.toString();
							return title;
						}
						lastFontSize = currentFontSize;
					}
					if (currentFontSize > 13.0f) { // most body text is 12pt
						titleText.append(position.getUnicode());
					}
				}
			}
		}
		return title;
	}

	static public String TOC = "Table of Contents";
	static public String PREFACE = "PREFACE";
	static public String PART = "Part";
	static public String CHAPTER = "CHAPTER";
	static public String OVERVIEW = "OVERVIEW";
	static public String SECTION = "SECTION";
	
	static public String BOOK = "BOOK";
	static public String SUBSECTION = "SUBSECTION";
	static public String REVIEW = "Chapter Review";
	static public String KEYTERMS = "KEY TERMS";

	static public String REVIEWSHORTANSWER =null;
	static public String REVIEWFURTHERRESEARCH =null;
	static public String REVIEWREFERENCES =null;
	static public String CHAPTERREVIEWPARTEN=null;
	static public String CHAPTERREVIEW = "CHAPTER REVIEW";
	static public String INTERACTIVELINK = "INTERACTIVE LINK QUESTIONS";
	static public String CONNECTIONQUESTIONS = "ART CONNECTION QUESTIONS";
	static public String REVIEWQUESTIONS = "REVIEW QUESTIONS";
	static public String CRITICALTHINKING = "CRITICAL THINKING QUESTIONS";
	static public String FURTHERRESEARCH = null;
	static public String FURTHERSTUDY = null;
	static public String ANSWERKEY = "ANSWER KEY";
	static public String QUESTIONSEPARATOR = " ";
	static public String REFERENCES = "REFERENCES";
	static public String APPENDIX = "APPENDIX";
	static public String APPENDICES = "Appendices";
	
	static public String INDEX = "INDEX";
	static public String KEYTERMSFONT;
	static public String FIRSTLETTERFONT;
	static public String REVIEWSECTIONFONT;
	static public String REVIEWSHORTANSWERFONT =null;
	static public String REVIEWFURTHERRESEARCHFONT =null;
	static public String REVIEWREFERENCESFONT =null;
	static public String REVIEWTITLEFONT = null;
	static public String KEYFONT;
	static public String CHAPTERREVIEWFONT;
	static public String REVIEWFONT;
	static public String INTERACTIVELINKFONT;
	static public String CONNECTIONQUESTIONSFONT;
	static public String REVIEWQUESTIONSFONT;
	static public String CRITICALTHINKINGFONT;
	static public String ANSWERKEYFONT;
	static public String REFERENCESFONT;
	static public String INDEXFONT;
	static public String BOXTITLEFONT;
	static public String BOXSUBTITLEFONT;
	static public String BOXBODYFONT;
	static public String CHAPTERTITLEFONT;
	static public String DEFAULTFONT;
	static public String CHAPTERFONT;
	static public String PARTFONT;
	static public String PARTTITLEFONT;
	
	static public String SECTIONTITLEFONT;
	static public String CHAPTERNUMFONT;
	static public String SECTIONNUMFONT;
	
	static public String CHAPTERSEPARATORONT;
	static public String SECTIONSEPARATORFONT;
	
	static public String BODYTITLEFONT;
	static public String CHAPTERPARTEN;
	static public String SECTIONPATTERN;
	static public String SUBSECTIONPATTERN;
	static public String APPENDIXPATTERN;
	static public String INDEXPATTERN;
	static public String CHAPTEROBJECTIVE = "Chapter Objectives";
	static public String BODYBACKGROUND = null;
	static public String linkBoxBACKGROUND = null;
	static public String linkBoxHeaderFont = null;
	static public String boxBACKGROUND = null;
	static public String CHAPTEROBJECTIVES = "Chapter Objectives";
	static public String INTRODUCTION = "Introduction";
	
	static public String TWOCOLUMNS = "";
	static public int LONGESTLENGTH = 30;
	static public float BOX_MIN_HEIGHT = 60f;
	static public float BOX_MIN_WIDTH = 200f;
}