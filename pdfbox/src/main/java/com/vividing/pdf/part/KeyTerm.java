package com.vividing.pdf.part;

import com.vividing.pdf.Util;
import com.vividing.pdf.svg.element.SVGElement;
import com.vividing.pdf.svg.element.SVGTextElement;
import com.vividing.pdf.tag.Article;

public class KeyTerm extends ReviewPart {

//	public KeyTerm() {
//		super();
//	}

	public KeyTerm(String key, String type, String name) {
		super(key, type, name, -1);
	}
	
	@Override
	public void processHTML(Article article) {

		//elements2.sort(new SVGElementComparator());
		Article table = this.safeReviewBox(article, elements2, true);
		
		SVGTextElement e;
		SVGTextElement last = null;
		while (m_index < elements2.size()) {
			SVGElement a = elements2.get(m_index); 
			if (a instanceof SVGTextElement) {
				e = (SVGTextElement) a;
//				Util.p(e);
				if (Util.isBold(e.getTextPosition())) {
					table.reset();
					Article tr = table.safeTR();
					tr.addLine(e, "td");
					m_index++;
					last = e;
					continue;
				} else {
					if (last != null) {
						Article tr = table.safeTR();
//						if (Util.isBold(last.getTextPosition())) {
//							tr.addLine(e, "td");							
//						} else 
						{
							
							tr.appendLine(e, "td");
						}
						
					}
					
					last = e;
					m_index++;
					continue;
				}
			} else {
				m_index++;
			}
		}
	}

}
