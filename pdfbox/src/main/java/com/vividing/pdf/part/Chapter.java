package com.vividing.pdf.part;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.vividing.pdf.Util;
import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.svg.element.SVG;
import com.vividing.pdf.svg.element.SVGElement;
import com.vividing.pdf.svg.element.SVGTextElement;
import com.vividing.pdf.tag.Article;

public class Chapter extends PartInfo {

	public Map<String, PartInfo> sections = new LinkedHashMap<String, PartInfo>();
	public Map<String, Section> m_sections = new LinkedHashMap<String, com.vividing.pdf.part.Section>();
	public Map<String, Part> m_parts = new LinkedHashMap<String, com.vividing.pdf.part.Part>();
	Review m_review = null;
	Overview m_overview = null;
	public boolean appendices = false;
	public Chapter(String key, String type, String name, int page) {
		super(key, type, name, page);
	}
	
	protected void processHead(Article article, List<SVGElement> textList) {
		PartInfo overview = this.safeOverview(VividingStripper.OVERVIEW );
		SVGTextElement e;

		//debug= true;
		// Article overview = article.safeArticle("div", "overview s");
		while (m_index < textList.size()) {
			SVGElement a = textList.get(m_index);
			if (a instanceof SVGTextElement) {
				e = (SVGTextElement) a;
				Util.p(e);
				if (Util.isPart(e)) {
					Part part = safePart(textList, e);
					processPart(part, textList);
					continue;
				} else if (Util.isChapterTitle(e.getTextStyle())) {
					//m_index++;
					//This might Book Title
					if (e.pageNum > 1) {
						Part part2 = new Part("dumy", VividingStripper.PART, "dumy",-1);
						m_parts.put("dumy", part2);
						Section section = safeChapter(part2, textList, e);
						
						processChapter(part2, section, textList);
						continue;
					}
				}
			}
			// this.handleEelement(overview,a);
			overview.getElements2().add(a);
			m_index++;
		}
	}
	
	protected void processPart(Part part, List<SVGElement> textList) {
		SVGTextElement e;
		// Article section = null;
		while (m_index < textList.size()) {
			SVGElement a = textList.get(m_index);
			if (a instanceof SVGTextElement) {
				e = (SVGTextElement) a;
				
				if (Util.isPart(e)) {
					Part part2 = safePart(textList, e);
					processPart(part2, textList);
					continue;
				} else if (Util.isChapterTitle(e)) {
					PartInfo s = safeChapter(part, textList, e);
					if (s != null) {
						processChapter(part, s, textList);
						return;
					}
				} else { //Create an empty Chapoter ....
					PartInfo s = safeChapter(part, textList, e);
					if (s != null) {
						processChapter(part, s, textList);
						return;
					}
				}
			}

			if (a != null) {
				part.getElements2().add(a);
			}
			m_index++;
		}
	}
	
	protected void processChapter(Part part, PartInfo section, List<SVGElement> textList) {
		SVGTextElement e;
		// Article section = null;
		while (m_index < textList.size()) {
			SVGElement a = textList.get(m_index);
			if (a instanceof SVGTextElement) {
				e = (SVGTextElement) a;
				
				if (Util.isPart(e)) {
					Part part2 = safePart(textList, e);
					processPart(part2, textList);
					continue;
				} else if (Util.isChapterTitle(e)) {
					//Chapter as section
					PartInfo s = safeChapter(part, textList, e);
					if (s != null) {
						processChapter(part, s, textList);
						return;
					}

				}
			}

			if (a != null) {
				section.getElements2().add(a);
			}
			m_index++;
		}
	}
	
	static Pattern partPattern = Pattern.compile("^Part ([\\d]+)$");
	
	public Part safePart(List<SVGElement> textList, SVGTextElement e) {
		
		String name = e.getText().toString();
		int partNum = -1;
		//Util.p(e);
		Matcher m = partPattern.matcher(e.getText());
		if (m.find()) {
			// article.reset();
			partNum = Integer.parseInt(m.group(1));
		}
		
		Part part = new Part(name, VividingStripper.PART, name, -1);
		m_parts.put(name, part);
		part.part = partNum;
		//part.getElements2().add(e);
		String title = null;
		m_index ++;
		for (;;) { 
			SVGElement a = textList.get(m_index);
			if (a instanceof SVGTextElement) {
				SVGTextElement e1 = (SVGTextElement) a;
				if (Util.isPartTitle(e1)) {
					
					if (title == null) {
						title = e1.getText().toString();
					} else {
						title += " " + e1.getText().toString();
					}
					m_index ++;
					
				} else {
					if (title != null) {
						part.setTitle(title);
					}
					return part;
				}
			}
		}
		//PARTTITLEFONT
		
		//return part;
	}
	
	public Section safeChapter(Part part, List<SVGElement> textList, SVGTextElement e) {
		Section partInfo = null;
		int chapter = 0;
		String chapterHead = null;
		String title = null;
		for (;;) { 
			SVGElement a = textList.get(m_index);
			if (a instanceof SVGTextElement) {
				SVGTextElement e1 = (SVGTextElement) a;
				//Util.p(e1);
				
				if (Util.isChapterTitle(e1)) {
					if (Util.isFont(VividingStripper.CHAPTERFONT, e1)) {
						chapterHead = e1.getText().toString();
						m_index ++;
						continue;
					} else if (Util.isFont(VividingStripper.CHAPTERNUMFONT, e1)) {
						try {
							chapter = Integer.parseInt(e1.getText().toString());
						} catch (NumberFormatException e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
							Util.p(e1);
						}
						m_index ++;
						continue;
					} else if (Util.isFont(VividingStripper.CHAPTERTITLEFONT, e1)) {
						title = ((SVGTextElement)a).getText().toString();
						m_index ++;
						continue;
					} else {
						title = ((SVGTextElement)a).getText().toString();
						m_index ++;
						continue;
					}
					
				} else {
					if (chapter == 0) { //In Case not found chapter number, we generate one,,,,
						chapter = part.m_sections.size() + 1;
					}
					String id = "part-" + part.part + "-chapter-" + chapter;
					partInfo = new Section(id, VividingStripper.SECTION, chapterHead, -1);

					partInfo.getAttrs().put("id", id);
					partInfo.chapter =chapter;
					partInfo.parent = part;
					
					if (Util.notEmpty(title)) {
						partInfo.name = title;
						partInfo.setTitle(title);
					} else {
						partInfo.setTitle(null);
					}
					part.m_sections.put(id, partInfo);
					return partInfo;
				}
			
			}
		}
	}
	
	public void toHtml(Article article, StringBuilder book, VividingStripper stripper, StringBuilder style, boolean withTable) throws IOException {
		article.reset();
		//
		
		if (VividingStripper.isOneChapterOnly() || VividingStripper.NOSECTION) {
			super.scan();
			if (VividingStripper.withChapter) {
				//super.processHTML(article);
				
				processHead(article, elements2);
				if (m_overview != null) {
					m_overview.processHTML(article);
					//article.write(stripper, book);
				}

				for (Part p : m_parts.values()) {
					p.processHTML(article);
				}
				//article.write(stripper, book);
			} else {
				super.processHTML(article);
			}
		} else {
			
			scan(article);
			processHTML(article);
		}
		//article.write(stripper, book);
		// Util.toFile(stripper, book, style, title, useTable);
	}
	
	@Override
	public void processHTML(Article article) {
		if (VividingStripper.isOneChapterOnly() || VividingStripper.NOSECTION) {
			super.processHTML(article);
		} else {
			// We need cut the chapter into picesse first, then handle it

			this.processOverView(elements2);
			this.processSection(null, elements2);

			Map<String, String> map = new HashMap<String, String>();
			map.put("class", "content " + Util.toStyleClass(VividingStripper.CHAPTER));
			map.put("id", this.key.replace(' ', '-'));
			map.put("data-chapter", String.valueOf(this.chapter));

			map.put("data-spy", "scroll");
			map.put("data-target", "#scrollspy-"+chapter);
			
			Article bodyArticle = article.safeArticle("div", map);
			StringBuilder head = bodyArticle.head;
			
			if (m_overview != null) {
					head.append(String.format("<nav class=\"navbar navbar-inverse\" style=\"height: 50px; overflow: hidden;\" id=\"scrollspy-%s\">", chapter));
					head.append("<ul class=\"nav navbar-nav nav-pills\">");
					head.append(String.format("<li class=\"active\"><a href=\"#%s\">Overview</a></li>", m_overview.getAttrs().get("id")));
			}
			for (Section s : m_sections.values()) {
				head.append(String.format("<li><a href=\"#%s\">%s</a></li>", s.getAttrs().get("id"), s.getTitle()));
			}
			if (m_review != null) {				
				head.append(String.format("<li><a href=\"#%s\">Review</a></li>", m_review.getAttrs().get("id")));
			}		
			head.append("</ul></nav>\n");
			if (m_overview != null) {
				m_overview.processHTML(bodyArticle);
			}

			for (Section s : m_sections.values()) {
				s.processHTML(bodyArticle);
			}
			
			if (m_review != null) {
				m_review.processHTML(bodyArticle);
			}
		}
	}
	Pattern sectionTitlePattern = Pattern.compile("^([\\d]+)\\.([\\d]+) (.*)$");
	Pattern sectionKeyPattern = Pattern.compile("^([\\d]+)\\.([\\d]+)$");

	protected void scan(Article article) {
		 for(SVG svg : this.svgs.values()) {
			 //svg.paragraphing();
			 svg.checkTable();
			 if (pageSize == null) {
				 pageSize = svg.getPageSize();
			 }
			 
			 // svg.checkTable();
			 List<SVGElement> list = svg.getElements();
			 
			 for(int i = 0; i<list.size(); i++) {
				 SVGElement e = list.get(i);
				 if (Util.getBox(e) != null) {
					 continue;
				 }
				 elements2.add(e);
			 }
		 }
	}
	
	protected void processOverView(List<SVGElement> textList) {
		PartInfo overview = this.safeOverview(VividingStripper.OVERVIEW + "-" + chapter);
		SVGTextElement e;

		while (m_index < textList.size()) {
			SVGElement a = textList.get(m_index);
			if (a instanceof SVGTextElement) {
				e = (SVGTextElement) a;
				
//				if (e.find(" The Americas")) {
//					debug = true;
//				}
				if (debug) {
					Util.p(e);
				}
				
				
				if (Util.isSectionTitle(e)) {
					
					if (!e.find("Introduction") && !e.find("Chapter")) {
						Matcher m = sectionTitlePattern.matcher(e.getText());
						if (m.find()) {
							// article.reset();
							return;
						} else {
							m = sectionKeyPattern.matcher(e.getText());
							if (m.find()) {
								// article.reset();
								return;
							}
							
						}
					}

				} 
				
				if (Util.isChapterTitle(e.getTextStyle())) {
					if (!e.find("Introduction")) {
						m_index++;
						continue;
					}
				}
				
			}
			// this.handleEelement(overview,a);
			overview.getElements2().add(a);
			m_index++;
		}
	}

	public Overview safeOverview(String key) {
		if (m_overview == null) {
			m_overview = new Overview(this, key, VividingStripper.OVERVIEW, key, -1);
			m_overview.getAttrs().put("id", "overview-"+this.chapter);
			m_overview.chapter =chapter;
			
			
		}
		return m_overview;
	}

	public Review safeReview() {
		
		String key = "Review";
		
		if (m_review == null) {
			m_review = new Review(key, key, key, -1);
			m_review.getAttrs().put("id", "review-"+this.chapter);
			m_review.chapter =chapter;
			m_review.parent = this;
		}
		return m_review;
	}
	
	public Section safeSection(List<SVGElement> textList, SVGTextElement e) {
		//In Case of Section Title, it might be
//		if (debug)  
//		{
//			Util.p(e);
//		}
		String key = null;
		if (Util.isSectionTitle(e)) {
			Matcher m = sectionTitlePattern.matcher(e.getText());
			if (m.find()) {
				// System.out.println(m.group(1));
				// System.out.println(m.group(2));
				// System.out.println(m.group(3));

				//String key = m.group(1) + "-" + m.group(2) + '-' + m.group(3);
				key ="chapter-"+m.group(1) + "-" + m.group(2);
				String name = m.group(3);
				Section partInfo = new Section(key, VividingStripper.SECTION, name, -1);
				partInfo.setTitle(m.group(3));
				partInfo.getAttrs().put("id", key);
				partInfo.chapter =chapter;
				partInfo.parent = this;
				
				m_sections.put(key, partInfo);
				m_index ++;
				partInfo.getElements2().add(e);
				return partInfo;
			}
		}
		
		Matcher m = sectionKeyPattern.matcher(e.getText());
		if (m.find()) {
			key ="chapter-"+m.group(1) + "-" + m.group(2);			
		}
		
		if (m_index+2 < textList.size()) {
			SVGElement a = textList.get(2+m_index);
			SVGTextElement separator = null;
			SVGTextElement t = null;
			if (Util.isSectionTitle(a)) {
				
				t = (SVGTextElement) a;
				separator = (SVGTextElement) textList.get(1+m_index);
				m_index += 3;
			} else {
				a = textList.get(1+m_index);
				if (Util.isSectionTitle(a)) {
					t = (SVGTextElement) a;
					m_index += 2;
					
				} else {
					Util.err("NOT a section TITLE????,  What???? >>> " + a);
				}
			}
			
			if (t != null) {
				String name = t.getText().toString();
				Section partInfo = new Section(key, VividingStripper.SECTION, name, -1);
				partInfo.setTitle(name);
				partInfo.getAttrs().put("id", key);
				partInfo.chapter =chapter;
				partInfo.parent = this;
				
				m_sections.put(key, partInfo);
				
				partInfo.getElements2().add(e);
				
				if (separator != null) {
					partInfo.getElements2().add(separator);
				}
				partInfo.getElements2().add(t);
				
//				Util.p(e);
//				if (separator != null) {
//					Util.p(separator);
//				}
//				Util.p(t);
				return partInfo;
			}
			
		}
		return null;		
	}

	public Review safeReview(SVGTextElement e) {
		boolean founded = false;
		// Some book has review title, some start at keyterms
		if (e.getTextStyle().toString().equals(VividingStripper.REVIEWFONT)) {
			// Util.p(e);
			if (e.find(VividingStripper.REVIEW)) {
				m_index++;
				founded = true;
			}
		} else if (e.getTextStyle().toString().equals(VividingStripper.REVIEWTITLEFONT) || e.getTextStyle().toString().equals(VividingStripper.KEYTERMSFONT)) {
			// Util.p(e);
			if (e.find(VividingStripper.KEYTERMS)) {
				founded = true;
			}
		}

		if (founded) {
			
			return safeReview();
		}
		return null;
	}

	protected void processSection(PartInfo section, List<SVGElement> textList) {
		SVGTextElement e;

		// Article section = null;
		while (m_index < textList.size()) {
			SVGElement a = textList.get(m_index);
			if (a instanceof SVGTextElement) {
				e = (SVGTextElement) a;
				
				if (Util.isSectionTitle(e)) {
					PartInfo s = safeSection(textList, e);
					if (s != null) {
						processSection(s, textList);
						return;
					}

				}
				if (e.getTextStyle().toString().equals(VividingStripper.REVIEWFONT)) {
					// Util.p(e);
					if (e.find("Chapter Review")) {
						Review r = safeReview(e);
						r.titleElement = e;
						if (r != null) {
							r.getElements2().add(e);
							this.processReview(r, textList);
							return;
						}

					}
				}
				
//				if (e.pageNum == 31) {
//					Util.p(e);
//					
//					Util.p(e.getTextStyle().toString());
//					Util.p(VividingStripper.KEYTERMSFONT);
//					Util.p(VividingStripper.REVIEWTITLEFONT);
//				}
				if (e.getTextStyle().toString().equals(VividingStripper.REVIEWTITLEFONT)
						|| e.getTextStyle().toString().equals(VividingStripper.KEYTERMSFONT)) {
					
					if (e.find(VividingStripper.KEYTERMS)) {
						Review r = safeReview(e);
						if (r != null) {
							r.getElements2().add(e);
							this.processReview(r, textList);
							return;
						}
					}
				}
			}

			if (a != null) {
				section.getElements2().add(a);
			}
			m_index++;
		}
	}

	protected void processReview(Review review, List<SVGElement> textList) {
		SVGTextElement e;
		while (m_index < textList.size()) {
			SVGElement a = textList.get(m_index);
			if (a instanceof SVGTextElement) {
				e = (SVGTextElement) a;

				if (e.getTextStyle().toString().equals(VividingStripper.REVIEWTITLEFONT)) {
					m_index++;
					if (e.find(VividingStripper.KEYTERMS)) {
						this.processReviewPart(review, review.safeKeyTerm(e), textList);
					} else if (e.find(VividingStripper.CHAPTERREVIEW)) {

						this.processReviewPart(review, review.safeChapterReview(e), textList);
					} else if (e.find(VividingStripper.INTERACTIVELINK)) {
						this.processReviewPart(review, review.safeInteractiveLink(e), textList);
					} else if (e.find(VividingStripper.REVIEWQUESTIONS)) {
						this.processReviewPart(review, review.safeQuestion(e), textList);
					} else if (e.find(VividingStripper.CRITICALTHINKING)) {
						this.processReviewPart(review, review.safeCriticalThinking(e), textList);
					} else if (e.find(VividingStripper.FURTHERSTUDY)) {
						this.processReviewPart(review, review.safeFurtherStudy(e), textList);
					} else if (e.find(VividingStripper.FURTHERRESEARCH)) {
						this.processReviewPart(review, review.safeFurtherResearch(e), textList);
					} else {
						this.processReviewPart(review, review.safeReviewPart(e), textList);
					}
				}
			}
			// this.handleEelement(article,a);
			m_index++;
			// m_index++;
		}
	}

	protected void processReviewPart(Review review, ReviewPart reviewPart, List<SVGElement> textList) {
		SVGTextElement e;
		while (m_index < textList.size()) {
			SVGElement a = textList.get(m_index);
			if (a instanceof SVGTextElement) {
				e = (SVGTextElement) a;

				if (e.getTextStyle().toString().equals(VividingStripper.REVIEWTITLEFONT)
						|| e.getTextStyle().toString().equals(VividingStripper.CHAPTERREVIEWFONT)
						|| e.getTextStyle().toString().equals(VividingStripper.INTERACTIVELINKFONT)
						|| e.getTextStyle().toString().equals(VividingStripper.REVIEWQUESTIONSFONT)
						|| e.getTextStyle().toString().equals(VividingStripper.REVIEWQUESTIONSFONT)) {

					this.processReview(review, textList);
					return;
				}
			}
			m_index++;
			reviewPart.getElements2().add(a);
		}
	}

	

	public void toTSV(StringBuilder toc) {

		super.toTSV(toc);
		if (this.m_overview != null) {
			this.m_overview.toTSV(toc);
		}
		for (PartInfo info : m_sections.values()) {
			info.toTSV(toc);
		}

		if (this.m_review != null) {
			this.m_review.toTSV(toc);
		}
		// for(PartInfo info: reviews.values()) {
		// info.toTSV(toc);
		// }
	}
}
