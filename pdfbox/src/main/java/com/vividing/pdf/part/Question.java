package com.vividing.pdf.part;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.vividing.pdf.Util;
import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.svg.element.SVGElement;
import com.vividing.pdf.svg.element.SVGTextElement;
import com.vividing.pdf.tag.Article;

public class Question extends ReviewPart {

	
//	public Question() {
//		super();
//	}

	public Question(String key, String type, String name) {
		super(key, type, name, -1);
	}
	
	protected Article safeQTIBox(Article article, SVGTextElement e, String num, String title) {
		article.reset();
		Article qti = article.safeDiv();

		qti.putAttr("class", "panel panel-sm panel-info qti-panel panel-flex");

		qti.head.append("<div class=\"panel-heading panel-flex-heading\">")
		//.append("<div class=\"panel-title alert alert-sm alert-info\">TITLE</div></div>");
		.append("<div class=\"alert alert-sm alert-info\">TITLE</div></div>");
		
		
		Article body = qti.safeDiv();

		body.putAttr("class", "panel-body panel-flex-body overflow-y");
		body.addLine(e, "h4", "prompt");
		return body;
	}
	@Override
	public void processHTML(Article article) {
		//debug = true;
		//to2Column();
		
		if (VividingStripper.TWOCOLUMNSMAP.containsKey(name)) {
			to2Column();
		} else {
			paragraphing();
		}
		
		Article body = this.safeReviewBox(article, elements2);
		SVGTextElement e = null;
		Article qtiBody = null;
		while (m_index < elements2.size()) {
			SVGElement a = elements2.get(m_index);
			if (debug) {
				Util.p(a);
			}
			
			if (a instanceof SVGTextElement) {
				e = (SVGTextElement) a;
				if(e.getTextStyle().toString().equals(VividingStripper.REVIEWSECTIONFONT)) {
					//We r in section seperator ....	
					body.reset();
					this.handleEelement(body,a);
					qtiBody = null;
					m_index ++;
					continue;
						//this.processReview(article, textList);
					} else {
					//15. Kenneth and Mamie Clark used sociological research to show that segregation was:
					StringBuilder sb = e.getText(); 	
					//Util.p(sb);
					String pattern = "^([\\d]{1,2})[\\.](.*)";
					Pattern r = Pattern.compile(pattern);
					Matcher m = r.matcher(sb);
					if (m.find()) {
						qtiBody = safeQTIBox(body, e, m.group(1), m.group(2));
						m_index ++;
						continue;
					} else { String pattern1 = "^([a-zA-Z])[\\.] (.*)";
					 r = Pattern.compile(pattern1);
					 m = r.matcher(sb);
					 if (m.find()) {
						 if (qtiBody == null) {
							 System.err.println("Error No QTI?? " + e);
						 } else {
							//qtiBody = safeQTIBox(body, e, m.group(1), m.group(2));
							Map<String, String> h = new HashMap<String, String>();
							h.put("class", "choice");
							h.put("data-id", "Choice" + Character.toUpperCase(m.group(1).charAt(0)));
							//
							qtiBody.addLine("div", sb.toString(), h);
						 }
							//qtiBody.reset();
							m_index ++;
							continue;
					 }	
				
					}
					
					
				}
			}
			this.handleEelement(body, a);
		}
	}
}
