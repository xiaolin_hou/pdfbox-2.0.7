package com.vividing.pdf.part;

import java.util.LinkedHashMap;
import java.util.Map;

import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.svg.element.SVGTextElement;
import com.vividing.pdf.tag.Article;

public class Review extends PartInfo {

	SVGTextElement titleElement = null;
	public Map<String, ReviewPart> reviews = new LinkedHashMap<String, ReviewPart>();
	
	public Review() {
		super();
	}

	public Review(String key, String type, String name, int page) {
		super(key, type, name, page);
	}
	
	public KeyTerm safeKeyTerm(SVGTextElement t) {
		KeyTerm keyTerm = new KeyTerm("chapter-key-terms-"+chapter, Review, VividingStripper.KEYTERMS);
		reviews.put(VividingStripper.KEYTERMS, keyTerm);
		keyTerm.getElements2().add(t);
		return keyTerm;
	}
	
	public ChapterReview safeChapterReview(SVGTextElement t) {
		ChapterReview chapterReview = new ChapterReview("chapter-review-"+chapter, Review, VividingStripper.CHAPTERREVIEW, -1);
		
		reviews.put(VividingStripper.CHAPTERREVIEW, chapterReview);
		chapterReview.getElements2().add(t);
		return chapterReview;
	}
	public InteractiveLink safeInteractiveLink(SVGTextElement t) {
		InteractiveLink interactiveLink = new InteractiveLink("chapter-interactive-link-" +chapter, Review, VividingStripper.INTERACTIVELINK);
		reviews.put(VividingStripper.INTERACTIVELINK, interactiveLink);
		interactiveLink.getElements2().add(t);
		return interactiveLink;
	}
	public CriticalThinking safeCriticalThinking(SVGTextElement t) {
		CriticalThinking criticalThinking = new CriticalThinking("chapter-critical-thinking="+chapter, Review, VividingStripper.CRITICALTHINKING);
		reviews.put(VividingStripper.CRITICALTHINKING, criticalThinking);
		criticalThinking.getElements2().add(t);
		return criticalThinking;
	}
	
	
	public FurtherStudy safeFurtherStudy(SVGTextElement t) {
		FurtherStudy furtherStudy = new FurtherStudy("chapter-further-study-" +chapter, Review, VividingStripper.FURTHERSTUDY);
		reviews.put(VividingStripper.FURTHERSTUDY, furtherStudy);
		furtherStudy.getElements2().add(t);
		return furtherStudy;
	}
	
	public FurtherResearch safeFurtherResearch(SVGTextElement t) {
		FurtherResearch furtherResearch = new FurtherResearch("chapter-further-research-"+chapter, Review, VividingStripper.FURTHERRESEARCH);
		reviews.put(VividingStripper.FURTHERRESEARCH, furtherResearch);
		furtherResearch.getElements2().add(t);
		return furtherResearch;
	}
	
	
	public Question safeQuestion(SVGTextElement t) {
		Question question = new Question("chapter-queston-"+chapter, Review, VividingStripper.REVIEWQUESTIONS);
		reviews.put(VividingStripper.REVIEWQUESTIONS, question);
		question.getElements2().add(t);
		return question;
	}
	
	public ReviewPart safeReviewPart(SVGTextElement t) {
		ReviewPart reviewPart = new ReviewPart();
		reviews.put(t.getText().toString(), reviewPart);
		reviewPart.getElements2().add(t);
		return reviewPart;
	}
	
	
	
	@Override
	public void processHTML(Article article) {
		article.reset();
		Article review = article.safeArticle("div", "review s");
		review.getAttrs().put("id", this.getAttrs().get("id"));
		//?????
//		review.head.append("<div class=\"panel-heading panel-flex-heading\">")
//		//.append("<div class=\"panel-title alert alert-sm alert-info\">TITLE</div></div>");
//		.append("<div class=\"alert alert-sm alert-info\">Chapter Review</div></div>");
//		
		if (titleElement != null) {
			review.addLine(titleElement, "div");
			review.reset();
		}
		
		//Article body = review.safeDiv();
		
		for(ReviewPart s : reviews.values()) {
			s.processHTML(review);
		}
	}
}