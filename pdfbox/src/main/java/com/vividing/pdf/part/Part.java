package com.vividing.pdf.part;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.vividing.pdf.Util;
import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.tag.Article;

public class Part extends PartInfo {

	protected int part = -1; 
	public Map<String, Section> m_sections = new LinkedHashMap<String, com.vividing.pdf.part.Section>();
	
	public Part(String key, String type, String name, int page) {
		super(key, type, name, page);
	}
	
	public void processHTML(Article article) {
		article.reset();
		
		Article partArticle = article.safeArticle("div", "part");
		
		Map<String, String> map = new HashMap<String, String>();
		
		map.put("class", "content " + Util.toStyleClass(VividingStripper.CHAPTER));
		map.put("id", this.key.replace(' ', '-'));
		map.put("data-chapter", String.valueOf(this.part));

		map.put("data-spy", "scroll");
		map.put("data-target", "#scrollspy-"+ String.valueOf(this.part));
		
		Article bodyArticle = partArticle.safeArticle("div", map);
		article.getAttrs().put("id", this.getAttrs().get("id"));
		StringBuilder head = partArticle.head;
		String partTitle = "";
		if (this.getTitle() != null) {
			partTitle = "| <span class=\"num\">" + this.getTitle() + "</span>"; 
		}
		
		if (this.part != -1) {
			
			
			head.append("<div class=\"c\"><span class=\"title\">Part " + this.part + "</span>" + partTitle + "</div>");
		} else {
			head.append("<div class=\"c\"><span class=\"title\">Part 1</span>" + partTitle + "</div>");
		}
		head.append(String.format("<nav class=\"navbar navbar-default\" id=\"scrollspy-%s\">", this.part));
		head.append("<div class=\"container-fluid\">");
		head.append("<ul class=\"nav navbar-nav nav-left\">");
		head.append("<li><a class=\"first\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"First\">First</a></li>");
		head.append("<li><a class=\"previous\" aria-label=\"Previous\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Previous\"><span aria-hidden=\"true\">&laquo;</span> <span class=\"sr-only\">Previous</span></a>");
		head.append("</ul>\n<div class=\"nav navbar-nav nav-page\" >");
		head.append("<ul class=\"nav nav-pills\">");
		
		int i = 0;
		for (Section s : m_sections.values()) {
			s.processHTML(bodyArticle);
			
			//"Chapter " + chapter
			String sectionTitle= s.getTitle();
			
//			String title = "<span class=\"tp\">Part " + this.part + "</spa> | <class=\"tc\">Chapter " + s.chapter + "</span>";
//			
//			if (sectionTitle != null) {
//				title += " | <span class=\"tt\">" + sectionTitle + "</span>";
//			}
			
			String title;
			
			

			if (sectionTitle != null) {
				title = "Chapter " + s.chapter + " | " + sectionTitle;
			} else {
				if (this.part == -1) {
					title = "Part 1 | Chapter " + s.chapter;
				} else {
					title = "Part " + this.part + " | Chapter " + s.chapter;
				}	
			}
			if (i++ < VividingStripper.DISPLAYITEMS) {
				head.append(String.format("<li data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"%s\"><a href=\"#%s\">%s</a></li>", title, s.getAttrs().get("id"),s.chapter));
			} else {
				head.append(String.format("<li data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"%s\" class=\"hidden\"><a href=\"#%s\">%s</a></li>", title, s.getAttrs().get("id"),s.chapter));
			}
		}
		head.append("</ul></div>\n");
		
		head.append("<ul class=\"nav navbar-nav navbar-right\">");
		//head.append("<li><span class=\"navbar-text tooltip-text\"></span></li>");
		head.append("<li class=\"tooltip-text\" data-toggle=\"tooltip\" data-placement=\"bottom\"></li>");
		head.append("<li><a class=\"next\" aria-label=\"Next\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Next\"> <span aria-hidden=\"true\">&raquo;</span></a></li>");
		head.append("<li><a class=\"last\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Last\">Last</a></li>");
		head.append("</ul></div></nav>\n");
	}

}
