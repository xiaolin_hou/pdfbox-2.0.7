package com.vividing.pdf.part;

import java.util.HashMap;
import java.util.Map;

import com.vividing.pdf.tag.Article;

public class Appendix extends PartInfo {
	protected String strClass = "section s";
	protected Map<String, String> map = new HashMap<String, String>(); 
	String title = null;
	
	public Appendix() {
		super();
	}

	public Appendix(String key, String type, String name, int page) {
		super(key, type, name, page);
	}

	@Override
	public void processHTML(Article article) {
		//paragraphing();
		article.reset();
		
		Article section = null; 
		if (map.size() > 0) {
			section = article.safeArticle("div", map);
		} else {
			section = article.safeArticle("div", strClass);
		}
		
		section.getAttrs().put("id", this.getAttrs().get("id"));
		super.processHTML(section);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
}
