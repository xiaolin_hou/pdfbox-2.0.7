package com.vividing.pdf.part;

import com.vividing.pdf.svg.element.SVGElementComparator;
import com.vividing.pdf.tag.Article;

public class ChapterReview extends ReviewPart {

//	public ChapterReview() {
//		super();
//	}

	public ChapterReview(String key, String type, String name, int page) {
		super(key, type, name, page);
	}

	@Override
	public void processHTML(Article article) {
		elements2.sort(new SVGElementComparator());
		//to2Column();
		//debug = true;
		paragraphing();
		Article body = this.safeReviewBox(article, elements2);
		
		while (m_index < elements2.size()) {
			this.handleEelement(body,elements2.get(m_index), false);
		}
	}
}
