package com.vividing.pdf.part;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDPageTree;

import com.vividing.pdf.Util;
import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.svg.SVGPDFRenderer;
import com.vividing.pdf.tag.Article;

public class Appendices extends PartInfo {

	public Map<String, Appendix> appendices = new LinkedHashMap<>();
	
	public void add(Appendix appendix) {
		appendices.put(appendix.key, appendix);
	}
	public Appendices(String key, String type, String name, int page) {
		super(key, type, name, page);
	}

	public void process(VividingStripper stripper, PDPageTree pages, SVGPDFRenderer renderer){
		for (Appendix s : appendices.values()) {
			s.process(stripper, pages, renderer);
		}
	}
	
	@Override
	public void processHTML(Article article) {

		for (Appendix s : appendices.values()) {
			s.scan();
			s.processHTML(article);
		}
		
	}
	
	public void toHtml(Article article, StringBuilder book, VividingStripper stripper, StringBuilder style, boolean withTable) throws IOException {
		//Article article = new Article("div", null, "");
		article.reset();
		Map<String, String> map = new HashMap<String, String>();
		map.put("class", "content " + Util.toStyleClass(VividingStripper.APPENDICES) + " " + Util.toStyleClass(VividingStripper.CHAPTER));
		map.put("id", this.key.replace(' ', '-'));
		map.put("data-chapter", String.valueOf(this.chapter));


		map.put("data-spy", "scroll");
		map.put("data-target", "#scrollspy-"+chapter);
		
		Article bodyArticle = article.safeArticle("div", map);
		
		StringBuilder head = bodyArticle.head;

		head.append(String.format("<nav class=\"navbar navbar-inverse\" style=\"height: 50px; overflow: hidden;\" id=\"scrollspy-%s\">", chapter));
		head.append("<ul class=\"nav nav-pills\">");
		int i = 0;
		for (Appendix p : appendices.values()) {
			//s.scan();
			String id = "appendix-" + p.key;
			p.putAttr("id", id);
			String key = (i==0) ? VividingStripper.APPENDIX + " " + p.key : p.key;
			
			head.append(String.format("<li><a href=\"#%s\">%s</a></li>", id, key));
			i++;
		}		
				
		head.append("</ul></nav>\n");
		
		processHTML(bodyArticle);
		
		//article.write(stripper, book);		
	}
	
	public void toTSV(StringBuilder toc) {
//		super.toTSV(toc);
//		toc.append('\n');
		for (Appendix p : appendices.values()) {
			p.toTSV(toc);
		}
	}
}

