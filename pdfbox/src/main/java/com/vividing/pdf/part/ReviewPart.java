package com.vividing.pdf.part;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.vividing.pdf.Util;
import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.svg.element.SVGElement;
import com.vividing.pdf.svg.element.SVGElementComparator;
import com.vividing.pdf.svg.element.SVGTextElement;
import com.vividing.pdf.tag.Article;
import com.vividing.pdf.tag.Paragraph;

public class ReviewPart extends PartInfo {

	public ReviewPart() {
		super();
	}

	public ReviewPart(String key, String type, String name, int page) {
		super(key, type, name, page);
	}
	
	protected Article safeReviewBox(Article article, List<SVGElement> textList) {
		return this.safeReviewBox(article, textList, false);
	}
	protected Article safeReviewBox(Article article, List<SVGElement> textList, boolean table) {
		article.reset();
		
		SVGTextElement e = null;
		
		Article view = (table) ? article.safeTable("table table-bordered r"):article.safeArticle("div", "r");
		while(true) {
			if (m_index < textList.size()) {
				SVGElement a = textList.get(m_index);
//				Util.p(a);
				if (a instanceof SVGTextElement) {
					e = (SVGTextElement) a;
					break;
				}
				m_index++;
			}
		}
		 
		
		if (e != null) {
			
			if (table) {
				Article tr = view.safeTR();
				//tr.addLine(e, "td");
				
				Paragraph td = tr.safeParagraph("td");
				td.addLine(e);
				//td.addAttr("colspan", "2");
				view.reset();
			} else {
				view.addLine(e);
			}
			
		} 
		
		setId(view);
		m_index++;
		if (table) {
			view.reset();
			return view;
		}
		return view.safeArticle("div", "body");
		
	}
	
	
	
	
	public void processHTML(Article article, boolean twoFolumns) {
		
	}
	
	@Override
	public void processHTML(Article article) {
		
		elements2.sort(new SVGElementComparator());
		if (VividingStripper.TWOCOLUMNSMAP.containsKey(name)) {
			to2Column();
		} else {
			paragraphing();
		}
		Article body = this.safeReviewBox(article, elements2);
		
		//m_index = 0;
		while (m_index < elements2.size()) {
			SVGElement a = elements2.get(m_index); 
			this.handleEelement(body,a);
			//m_index++;
		}
		
		
	}
	
	
	
	/*
	 * For review section, it is two columns
	 */
	protected void to2Column() {
		if (VividingStripper.MULTICOLUMNS) {
		
			float secondCol = VividingStripper.pageSize.getWidth() /2;
		//List<SVGElement> col1 = new ArrayList<>();
		//List<SVGElement> col2 = new ArrayList<>();
		
		Map<Integer, List<SVGElement>> col1 = new LinkedHashMap<>();
		Map<Integer, List<SVGElement>> col2 = new LinkedHashMap<>();
		Map<Integer, List<SVGElement>> col = null;
		for(SVGElement e : elements2) {
			
//			if (e.pageNum >= 41 && e.pageNum <= 42) {
//				debug = true;
//			} else {
//				debug = false;
//			}
//			
//			if (e instanceof SVGTextElement) {
//				if(((SVGTextElement)e).find(VividingStripper.CHAPTERREVIEW)) {
//					debug = true;
//				}
//			}
			
			if (debug) {
				Util.p(e.pageNum);
				Util.p(e);
			}
			if (e.x < secondCol) {
				col = col1;
				//col1.add(e);
			} else {
				col = col2;
				//col2.add(e);
			}
			
			List<SVGElement> list = col.get(e.pageNum);
			
			if (list==null) {
				list = new ArrayList<>();
				col.put(e.pageNum, list);
			}
			list.add(e);
		}
		
		elements2.clear();
		for(Integer i : col1.keySet()) {
			if (debug) {
				Util.p("XXXXXXXXXXXXXXXXXXXX = " + i);
				Util.p(col1.get(i));
			}
			elements2.addAll(col1.get(i));
			if (col2.containsKey(i)) {
				if (debug) {
					Util.p("==========");
					Util.p(col1.get(i));
				}
				
				elements2.addAll(col2.get(i));
			}
		}
		}
		paragraphing();
		if (debug) {
			for(SVGElement e : elements2) {
				if (e instanceof SVGTextElement)
				Util.p(((SVGTextElement)e).getText());
			}
		}
	}
}


