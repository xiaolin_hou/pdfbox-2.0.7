package com.vividing.pdf.part;

import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.svg.element.SVGElement;
import com.vividing.pdf.tag.Article;

public class Overview extends PartInfo {

	public Overview() {
		super();
	}

	public Overview(PartInfo parent, String key, String type, String name, int page) {
		super(parent, key, type, name, page);		
	}

	@Override
	public void processHTML(Article article) {
		//paragraphing();
		article.reset();
		Article overview = article.safeArticle("div", "overview s");
		overview.getAttrs().put("id", this.getAttrs().get("id"));
		
		
		StringBuilder head = overview.head;

//		if (Util.notEmpty(parent.subType)) {
//			// System.out.println(this);
//			Util.p(parent.subType);
//		}
		if (parent.subType != null && parent.subType.equals(VividingStripper.APPENDICES)) {
			head.append(String.format("<div class=\"c\"><span class=\"num\">%s ", VividingStripper.APPENDICES));
			head.append(parent.key);
		} else 
		{
			head.append("<div class=\"c\"><span class=\"num\">CHAPTER ");
			head.append(parent.chapter);
		}
		head.append("</span><span class=\"separator\"> | </span><span class=\"title\">");
		head.append(this.parent.name);
		head.append("</span></div>");
		//super.processHTML(overview);
		super.processHTML(overview, false);
	}
}
