package com.vividing.pdf.part;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.text.TextPosition;

import com.vividing.pdf.LineItem;
import com.vividing.pdf.Util;
import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.svg.SVGGraphics2D;
import com.vividing.pdf.svg.SVGPDFRenderer;
import com.vividing.pdf.svg.element.SVG;
import com.vividing.pdf.svg.element.SVGElement;
import com.vividing.pdf.svg.element.SVGElementComparator;
import com.vividing.pdf.svg.element.SVGGElement;
import com.vividing.pdf.svg.element.SVGTextElement;
import com.vividing.pdf.tag.Article;
import com.vividing.pdf.tag.Paragraph;

public class PartInfo extends SVGGElement {
	
	public static final String Book = "Book";
	public static final String Preface = "Preface";
	public static final String Chapter = "Chapter";
	public static final String Overview = "Overview";
	public static final String Section = "Section";
	public static final String Review = "Review";
	public static final String Answerkey = "Answerkey";
	public static final String areferences = "areferences";
	public static final String References = "References";
	public static final String Appendix = "Appendix";
	public static final String Index = "Index";
	protected PDRectangle pageSize = null;
	String title = null;
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	/*
	 * Key ID type is kind Name display
	 */
	public PartInfo(String key, String type, String name, int page) {
		super(page);
		this.key = key;
		this.type = type;
		this.name = name;
		this.page = page;
	}

	public PartInfo(PartInfo parent, String key, String type, String name, int page) {
		super(page);
		this.key = key;
		this.type = type;
		this.name = name;
		this.page = page;
		this.parent = parent;
		
		if (parent != null) {
			this.pageSize = parent.pageSize;
		}
	}
	
	public PartInfo parent = null;
	public int flag = 0;

	int imageCounter = 0;
	int image_sm_Counter = 0;
	int fontCounter = 0;

	public int nextImageId() {
		return ++imageCounter;
	}

	public int nextSmallImageId() {
		return ++image_sm_Counter;
	}

	public int nextFontId() {
		return ++fontCounter;
	}

	protected List<List<TextPosition>> pagePositionList = new ArrayList<List<TextPosition>>();

	public void addList(VividingStripper stripper, List<List<TextPosition>> listPos) {
		pagePositionList.addAll(listPos);
	}

	public void add(VividingStripper stripper, List<TextPosition> listPos) {
		pagePositionList.add(listPos);
	}

	/**
	 * @return the chapter
	 */
	public int getChapter() {
		return chapter;
	}

	/**
	 * @param chapter
	 *            the chapter to set
	 */
	public void setChapter(int chapter) {
		this.chapter = chapter;
	}

	public String key;
	public String type;
	public String subType;
	public String name;
	public int chapter = -1;
	public int section = -1;
	public int subSection = -1;
	public int page;

	public int endPage = -1;
	int length = -1;

	public PartInfo() {
		super(0);
	}

	public String toString() {
		if (endPage == -1) {
			return String.format("[key=%s, type=%s, name=%s, page=%s]", key, type, name, page);
		} else {
			return String.format("[key=%s, type=%s, name=%s, Page start=%s, end=%s]", key, type, name, page, endPage);
		}
	}

	public void writeBookBody(VividingStripper stripper, StringBuilder style, String title) throws IOException {
		this.writeBookBody(stripper, style, title, title, true);
	}

	public void writeBookBody(VividingStripper stripper, StringBuilder style, String title, String type, boolean useTable) throws IOException {
		StringBuilder book = new StringBuilder();
		Article article = new Article("div", null, Util.toStyleClass(type));
		elements2 = new ArrayList<SVGElement>();
		scan();
		processHTML(article);
		article.write(stripper, book);
		
		Util.toFile(stripper, book, book, title, stripper.getOutputDir() + title + ".html");
	}

	
	protected void scan() {
		//last = null;
		SVGTextElement paragraph = null;
		SVGTextElement lastLine = null;
		for (SVG svg : this.svgs.values()) {
			
			//debug = svg.getPageNum() == 31; 
			//svg.paragraphing();
			
			if (pageSize == null) {
				pageSize = svg.getPageSize();
			}
			List<SVGElement> textList = svg.getElements();
			
			if (VividingStripper.isOneChapterOnly()) {
				//elements2.add(svg.getElement());
				if (VividingStripper.withChapter) {
					textList.sort(new SVGElementComparator());
				}
				SVGElement e = svg.getElement();
				
				if (e instanceof SVGGElement) {
					if (VividingStripper.withChapter) {
						List<SVGElement>  ems = ((SVGGElement) e).getElements();
						ems.sort(new SVGElementComparator());
						elements2.addAll( ems);
					} else {
						elements2.addAll(((SVGGElement) e).getElements());
					}
				} else {
					elements2.add(e);
				}
				
			} else {

				boolean firstLine = true;
				for (int i = 0; i < textList.size(); i++) {
					SVGElement e = textList.get(i);
					
					if (Util.getBox(e) != null) {
						continue;
					}

					if (i == textList.size() - 1) {
						elements2.add(e);
						if (e instanceof SVGTextElement) {
							paragraph = (SVGTextElement) e;
						} else {
							paragraph = null;
						}
					} else if (firstLine) {
						firstLine = false;
						if (paragraph != null) {
							if (lastLine == null) {
								lastLine = paragraph;
							}
							if (e instanceof SVGTextElement) {
								SVGTextElement current = (SVGTextElement) e;
								int r = Util.inLine(paragraph, current, lastLine, pageSize.getWidth());
								switch (r) {
								case Util.NEWLINE: {
									elements2.add(current);
									continue;
								}
								case Util.BRLINE: {
									paragraph.addBR();
									paragraph.add(current);
									continue;
								}
								case Util.NOSPACE: {
									// if
									// (last.find("http://www.sociologyencyclopedia.com/public/")){
									// Util.p(last);
									// Util.p(current);
									// }
									paragraph.add(current);
									lastLine = current;
									// Util.p(last);
								}
								
								default: {
									paragraph.addSpace();
									paragraph.add(current);
								}
								}
							}
						} else {
							elements2.add(e);
							paragraph = null;
						}
					} else {
						elements2.add(e);
					}

					//
					// System.out.println(current);
				}

			}
		}

	}

	public Map<String, PartInfo> children = new LinkedHashMap<String, PartInfo>();
	protected VividingStripper stripper;

	protected void processPage(VividingStripper stripper, PDPageTree pages, SVGPDFRenderer renderer, int bookPageNum,
			int pdfPageNum) {

		try {
			renderer.renderPageToGraphics(this, pdfPageNum, new SVGGraphics2D());
			// if (!stripper.isTextOnly()) { // For now to same some time
			// ImageGraphicsEngine extractor = new ImageGraphicsEngine(page,
			// this, stripper);
			// extractor.run();
			// }
		} catch (IOException e) {
			e.printStackTrace();
		}
		// return true;
	}

	public Map<String, SVG> svgs = new LinkedHashMap<String, SVG>();

	public void process(VividingStripper stripper, PDPageTree pages, SVGPDFRenderer renderer) {
		this.stripper = stripper;
		int bookStartPage = stripper.getBookPageStart() == -1 ? 1 : stripper.getBookPageStart();

		for (int bookPageNum = page; bookPageNum <= endPage; bookPageNum++) {

			int pdfPageNum = bookPageNum + bookStartPage;
			if (pdfPageNum > pages.getCount() || pdfPageNum > VividingStripper.endPage) {
				return;
			}
			
			if (VividingStripper.SKIPLASTPAGE && pdfPageNum == pages.getCount()) {
				return;
			}

			if (pdfPageNum < VividingStripper.startPage) {
				continue;
			}
			
//			if (stripper.getStartPage() != -1) {
//				if (bookPageNum <= stripper.getStartPage()) {
//					continue;
//				}
//			}
//
//			if (stripper.getEndPage() != -1) {
//				if (bookPageNum >= stripper.getEndPage()) {
//					return;
//				}
//			}

			this.processPage(stripper, pages, renderer, bookPageNum, pdfPageNum);

		}
	}

	public void toTSV(StringBuilder toc) {
		toc.append(type).append('\t');
		toc.append(key).append('\t');
		toc.append(name).append('\t');
		toc.append(page).append('\t');
		toc.append(endPage).append('\t');
		
		if (parent != null && parent.key != null) {
			toc.append(parent.key);
		}
		toc.append('\t');
		toc.append(chapter);
		toc.append('\n');
	}

	public void paragraphing() {
		elements.clear();
		SVGTextElement paragraph = null;
		SVGTextElement lastLine = null;
		for (SVGElement e : elements2) {

			if (Util.getBox(e.x, e.y, e.right - e.x, e.bottom - e.y) != null) {
				continue;
			}
			
//			if (debug) {
//				Util.p(e);
//			}
			if (e instanceof SVGTextElement) {

				SVGTextElement current = (SVGTextElement) e;
				
//				if (current.find("When in the Course of human events, it becomes necessary")) {
//					debug = true;
//				}
				
				int r = Util.inLine(paragraph, current, lastLine, (pageSize == null) ? VividingStripper.pageSize.getWidth() : pageSize.getWidth());
				switch (r) {
					case Util.NEWLINE: {
						elements.add(current);
						lastLine = paragraph = current;
						continue;
					}
					case Util.BRLINE: {
						paragraph.addBR();
						paragraph.add(current);
						lastLine = current;
						continue;
					}
					case Util.NOSPACE: {
	
						paragraph.add(current);
						lastLine = current;
						break;
					}
	
					case Util.FIRSTLETTER: {
						
						
						List<LineItem> items = paragraph.getLine().getLine();
						
//						List<LineItem> lastItems = lastLine.getLine().getLine();
//						int at = items.size() -  lastItems.size() - 1;
//						Util.p(items.size() + ", " + lastItems.size() + ", at = " + at + ", current = " + current.getLine().getLine().size());
//						Util.p(lastLine);
//						Util.p(current);
						items.addAll(0, current.getLine().getLine());
						//lastLine.add(current);
						break;
					}
					
					default: {
	
						if (paragraph != null) {
							paragraph.addSpace();
							paragraph.add(current);
						} else {
							elements.add(current);
							paragraph = current;
						}
						lastLine = current;
					}
					}
				} else {
	
					if (e instanceof SVGGElement) {
						((SVGGElement) e).paragraphing();
					}
					elements.add(e);
					// last = null;
				}
			// System.out.println(current);
		}
		elements2.clear();
		elements2.addAll(elements);
		
//		if(debug) {
//			Util.p(elements2);
//		}
		//elements2 = elements;
	}

	protected Article safeReviewBox(Article article, List<SVGElement> textList) {
		return this.safeReviewBox(article, textList, false, -1);
	}
	
	protected Article safeReviewBox(Article article, List<SVGElement> textList, boolean table, int columns) {
		if (textList.size() == 0) {
			return article;
		}
		article.reset();
		
		SVGTextElement e = null;
		String className = "";
		if (this instanceof ReviewPart) {
			if (table) {
				className = "table table-bordered r";
			} else {
				className = "r";
			}
		} else {
			if (table) {
				className = "table table-bordered";
			} else {
				className = "";
			}
		}
		
		Article view = (table) ? article.safeTable(className):article.safeArticle("div", className);
		while(true) {
			if (m_index < textList.size()) {
				SVGElement a = textList.get(m_index);
				if (a instanceof SVGTextElement) {
					e = (SVGTextElement) a;
					break;
				}
				m_index++;
			} else {
				break;
			}
		}
		 
		
		if (e != null) {
			
			if (table) {
				Article tr = view.safeTR();
				//tr.addLine(e, "td");
				
				Paragraph td = tr.safeParagraph("td");
				td.addLine(e);
				if (columns != -1) {
					td.addAttr("colspan", String.valueOf(columns));
				}
				view.reset();
			} else {
				view.addLine(e);
			}
			
		} 
		
		setId(view);
		m_index++;
		if (table) {
			view.reset();
			return view;
		}
		return view.safeArticle("div", "body");
		
	}
	@Override
	public void processHTML(Article article) {
		this.processHTML(article, true);
	}

	//@Override
	public void processHTML(Article article, boolean newArtical) {
		
		//elements2.sort(new SVGElementComparator());
		
		paragraphing();
		
		Article body = null;
			if (newArtical) {
			if (VividingStripper.isOneChapterOnly() || VividingStripper.NOSECTION) {
				body = article;
			} else {
				body = this.safeReviewBox(article, elements2);
			}
		} else {
			body = article;
		}
		handleElement(body);
		
	}
	
	protected void handleElement(Article article) {
		while (m_index < elements2.size()) {
			SVGElement a = elements2.get(m_index); 
			this.handleEelement(article,a);
			//m_index++;
		}
	}
	protected void setId(Article body) {
		String id = this.getAttrs().get("id");
		
		if (id == null) {
			id = this.key;
		}
		
		if (Util.notEmpty(id)) {
			body.putAttr("id", id);
		}
	}
}

