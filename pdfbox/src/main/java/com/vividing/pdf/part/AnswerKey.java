package com.vividing.pdf.part;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.vividing.pdf.svg.element.SVGElement;
import com.vividing.pdf.svg.element.SVGElementComparator;
import com.vividing.pdf.svg.element.SVGTextElement;
import com.vividing.pdf.tag.Article;
import com.vividing.pdf.tag.Paragraph;

public class AnswerKey extends PartInfo {

	public AnswerKey() {
		super();
	}

	public AnswerKey(String key, String type, String name, int page) {
		super(key, type, name, page);
	}
	
	public static Pattern CHAPTERPATTERN = Pattern.compile("^Chapter[\\s]?([\\d]+)");
	
	
	void checkItem(Article tbody, String id, StringBuilder text) {
		tbody.reset();
		Article tr = tbody.safeTR();
		if (id != null) {
			Paragraph td = tr.safeParagraph("td", id);
			tr.reset();
			td = tr.safeParagraph("td", text.toString());
			//td.addLine(id);
		//	td.addLine(text.toString());
		}
	}
	
	void handleQuestion(Article tbody,  StringBuilder sb, String chaptNum) {
		String id = null;
		StringBuilder text = new StringBuilder();
		for(int i = 0; i < sb.length(); i++) {
			char c = sb.charAt(i);
			
			if (Character.isDigit(c)) {
				if (i+1 < sb.length() && sb.charAt(i+1) == '.') {
					checkItem(tbody, id, text);
					id = "question-" + chaptNum + "-" + c;
					text = new StringBuilder();
					i++;
					continue;
				} else if (i+2 < sb.length() && Character.isDigit(sb.charAt(i+1)) && sb.charAt(i+2) == '.') {
					checkItem(tbody, id, text);
					id = chaptNum + "-" + c + sb.charAt(i+1);
					text = new StringBuilder();
					i +=2;
					continue;
				} 
			}
			
			text.append(c);
		}
		checkItem(tbody, id, text);
	}
	
	void handleChapter(Article tbody, SVGTextElement c, String chaptNum) {
		//tbody.addLine(c);
		StringBuilder sb = new StringBuilder();
		while (m_index < elements2.size()) {
			SVGElement a = elements2.get(m_index);
			
			if (a instanceof SVGTextElement) {
				SVGTextElement t = (SVGTextElement) a;
				Matcher m = CHAPTERPATTERN.matcher(t.getText());
				if (m.find()) {
					handleQuestion(tbody, sb, chaptNum);
					m_index++;
					handleChapter(tbody, t, m.group(1));
					continue;
				} else {
					//body.addLine(t);
					sb.append(t.getText());
				}
			} 
			//Util.p(a);
			//this.handleEelement(body,a);
			m_index++;
		}
	}
	
	@Override
	public void processHTML(Article article) {
		
		elements2.sort(new SVGElementComparator());
		
		//paragraphing();
		
		Article tbody = this.safeReviewBox(article, elements2, true, 2);
		
		//m_index = 0;
		
		while (m_index < elements2.size()) {
			SVGElement a = elements2.get(m_index);
			
			if (a instanceof SVGTextElement) {
				SVGTextElement t = (SVGTextElement) a;
				Matcher m = CHAPTERPATTERN.matcher(t.getText());
				if (m.find()) {
					m_index++;
					handleChapter(tbody, t, m.group(1));
					continue;
				} else {
//Err
					tbody.addLine(t);
					
				}
			} 
			m_index++;
			//Util.p(a);
			//this.handleEelement(body,a);
			//m_index++;
		}
	}	
	
}


