package com.vividing.pdf.part;

import java.util.HashMap;
import java.util.Map;

import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.tag.Article;

public class Section extends PartInfo {
	protected String strClass = "section s";
	protected Map<String, String> map = new HashMap<String, String>();
	
	
	public Section() {
		super();
	}

	public Section(String key, String type, String name, int page) {
		super(key, type, name, page);
	}

	@Override
	public void processHTML(Article article) {
		article.reset();
		
		Article section = null; 
		if (map.size() > 0) {
			section = article.safeArticle("div", map);
		} else {
			section = article.safeArticle("div", strClass);
		}
	
		if (VividingStripper.withChapter) {
			StringBuilder head = section.head;
			head.append("<div class=\"c\"><span class=\"num\">CHAPTER ");
			head.append(this.chapter);
			if (this.name != null && !this.name.equalsIgnoreCase(VividingStripper.CHAPTER)) {
				head.append("</span><span class=\"separator\"> | </span><span class=\"title\">");
				head.append(this.name);
			} 
			head.append("</span></div>");
		}
		
		section.getAttrs().put("id", this.getAttrs().get("id"));
		super.processHTML(section, false);
	}

}
