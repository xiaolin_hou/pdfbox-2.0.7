package com.vividing.pdf.part;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDPageTree;

import com.vividing.pdf.Util;
import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.svg.SVGPDFRenderer;
import com.vividing.pdf.tag.Article;

public class Book extends PartInfo {
	
	Toc 	toc = new Toc();	
	public Map<String, Chapter> chaptersMap = new LinkedHashMap<String, Chapter>();
	private Appendices appendices = null; 
	public Appendices getAppendices() {
		if (appendices == null) {
			appendices = new Appendices(Appendix, Appendix, Appendix, -1);
		}
		return appendices;
	}
	private PartInfo preface = null;
	public PartInfo getPreface() {
		return preface;
	}

	public void setAppendices(Appendices appendices) {
		this.appendices = appendices;
	}

	public void setPreface(PartInfo preface) {
		this.preface = preface;
	}
	private AnswerKey answerKey = null; 
			//new PartInfo(VividingStripper.ANSWERKEY, VividingStripper.ANSWERKEY,VividingStripper.ANSWERKEY, -1);
	private PartInfo reference = null; 
			//new PartInfo(VividingStripper.REFERENCES, VividingStripper.REFERENCES, VividingStripper.REFERENCES, -1);
	
	private PartInfo index = null;

	
	public Book(String name, int page) {
		super(PartInfo.Book, PartInfo.Book, name, page);
	}
	
	public void setOneChapterOnly(VividingStripper stripper, int pageNum) {
		int bookStartPage = stripper.getBookPageStart() == -1 ? 1 : stripper.getBookPageStart();
		Chapter chapter = new Chapter(VividingStripper.CHAPTER, VividingStripper.CHAPTER, VividingStripper.CHAPTER, 0);
		chapter.endPage = bookStartPage + pageNum;
		chaptersMap.put(VividingStripper.CHAPTER, chapter);
	}
	
	public Map<String, Chapter> getChaptersMap() {
		return chaptersMap;
	}

	public void setChaptersMap(Map<String, Chapter> chaptersMap) {
		this.chaptersMap = chaptersMap;
	}

	public AnswerKey getAnswerKey() {
		return answerKey;
	}

	public void setAnswerKey(AnswerKey answerKey) {
		this.answerKey = answerKey;
	}

	public PartInfo getReference() {
		return reference;
	}

	public void setReference(PartInfo reference) {
		this.reference = reference;
	}

	public PartInfo getIndex() {
		return index;
	}

	public void setIndex(PartInfo index) {
		this.index = index;
	}
	
	public Chapter getChapter(int pageNum) {

		for (Chapter c : chaptersMap.values()) {
			if (pageNum >= c.page && pageNum <= c.endPage) {
				return c;
			}
		}

		Chapter c = Util.getLastChapter(chaptersMap);

		if (c != null && c.page <= pageNum) {
			return c;
		}
		return null;
	}

	public void loadToc(VividingStripper stripper, PDPageTree pages, SVGPDFRenderer renderer, int tocStart, int tocEnd){
		toc.page = tocStart;
		toc.endPage = tocEnd;
		toc.process(stripper, pages, renderer);
	}
	
	public void process(VividingStripper stripper, PDPageTree pages, SVGPDFRenderer renderer){ 
		boolean b = false;
		if (!VividingStripper.isOneChapterOnly()) {
			
			if (preface != null) {
				preface.process(stripper, pages, renderer);
			}
			
			if (appendices != null) {
				appendices.process(stripper, pages, renderer);
			}
			
			if (answerKey != null) {
				answerKey.process(stripper, pages, renderer);
			}

			if (reference != null) {
				reference.process(stripper, pages, renderer);
			}
			if (index != null) {
				index.process(stripper, pages, renderer);
			}
			
		}
		
		int i = 0;
		for (Chapter c : getChaptersMap().values()) {
			c.process(stripper, pages, renderer);
			if (b && i == 1) {
				break;
			}
			i++;
		}
	}
	
	public StringBuilder toTSV() {
		StringBuilder toc = new StringBuilder();
		toTSV(toc);
		return toc;
	}
	@Override
	public void toTSV(StringBuilder toc) {
		toc.append("type").append('\t');
		toc.append("key").append('\t');
		toc.append("name").append('\t');
		toc.append("page").append('\t');
		toc.append("endPage").append('\t');
		toc.append("parent").append('\t');
		toc.append("chapter").append('\n');

		//super.toTSV(toc);
		if (preface != null) {
			preface.toTSV(toc);
		}
		
		for (Chapter c : getChaptersMap().values()) {
			c.toTSV(toc);
		}
		
		if (appendices != null){
			appendices.toTSV(toc);
		}

		if (answerKey != null){
			answerKey.toTSV(toc);
		}
		if (reference != null) {
			reference.toTSV(toc);
		}
		if (index != null) {
			index.toTSV(toc);
		}
		
	}
	public void toHtml(VividingStripper stripper, StringBuilder style) throws IOException {
		if (!VividingStripper.isOneChapterOnly()) {
			if (preface != null) {
				preface.writeBookBody(stripper, style, "Preface", VividingStripper.PREFACE, true);
			}
//			if (appendices != null){
//				appendices.writeBookBody(stripper, style, VividingStripper.APPENDICES);
//			}
//			
			if (answerKey != null){
				answerKey.writeBookBody(stripper, style, VividingStripper.ANSWERKEY);
			}
			if (reference != null) {
				reference.writeBookBody(stripper, style, VividingStripper.REFERENCES);
			}
			if (index != null) {
				index.writeBookBody(stripper, style, VividingStripper.INDEX);
			}
		}
		
		Article article = new Article("div", null, "container");
		//<div class="container">${body}</div>
		
		StringBuilder combine = new StringBuilder();

		for (Chapter c : getChaptersMap().values()) {

			c.toHtml(article, combine, stripper, style, true);
		}
		
		if (appendices != null) {
			appendices.toHtml(article, combine, stripper, style, true);
		}
		
		article.write(stripper, combine);
		Util.toFile(stripper, combine, style, stripper.getTitle(), stripper.getOutputName()+ ".html");
	}
}

