package com.vividing.pdf.part;

import java.util.ArrayList;

import org.apache.pdfbox.pdmodel.PDPageTree;

import com.vividing.pdf.Util;
import com.vividing.pdf.VividingStripper;
import com.vividing.pdf.svg.SVGPDFRenderer;
import com.vividing.pdf.svg.element.SVGElement;
import com.vividing.pdf.svg.element.SVGTextElement;

public class Toc extends PartInfo {

	public Toc() {
		super();
	}

	public Toc(PartInfo parent, String key, String type, String name, int page) {
		super(parent, key, type, name, page);		
	}

	public void process(VividingStripper stripper, PDPageTree pages, SVGPDFRenderer renderer) {
		this.stripper = stripper;
		int bookStartPage = 0;	//in this case

		for (int bookPageNum = page; bookPageNum <= endPage; bookPageNum++) {

			int pdfPageNum = bookPageNum + bookStartPage;
			if (pdfPageNum > pages.getCount()) {
				return;
			}
			this.processPage(stripper, pages, renderer, bookPageNum, pdfPageNum);
		}
		
		processHTML();
	}
	
	
	public void processHTML() {
		elements2 = new ArrayList<SVGElement>();
		scan();
		SVGTextElement lastLine = null;
		while (m_index < elements2.size()) {
			SVGElement a = elements2.get(m_index);
			if (a instanceof SVGTextElement) {
				SVGTextElement current = (SVGTextElement) a;
				//Util.p(((SVGTextElement) a).getText());
				if (lastLine == null) {
					lastLine = current;
				}
				else {
					//int r = Util.inLine(lastLine, t, lastLine, pageSize.getWidth());
					float diffY  = current.y - lastLine.y;
					float h0 = lastLine.getTextPosition().getHeight();
					if (Util.equal(diffY,0, h0/2)) {
						lastLine.addSpace();
						lastLine.add(current);
					} else {
						stripper.getTocInfo(lastLine.getText());
						lastLine = current;
					}
				
				}
				
			}
			m_index++;
		}
		
		if (lastLine != null) {
			stripper.getTocInfo(lastLine.getText());
		}
	}
}
